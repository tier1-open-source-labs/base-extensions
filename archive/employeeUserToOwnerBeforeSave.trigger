/** 
 * Name: employeeUserToOwnerBeforeSave 
 * Description: Upon Employee creation/update:
 *              If the user is an active user, then make him/her 
 *              the owner of this employee record
 *
 * Confidential & Proprietary, 2012 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1CRM.
 *
 * PS Base 3.7.4
 */
 
trigger employeeUserToOwnerBeforeSave on T1C_Base__Employee__c (before insert, before update) 
{
    //check if the trigger is already ran once in this transaction
    //if it did, abort
    if (!DMLUtil.hasRun.add('employeeUserToOwnerBeforeSave'))
    {
        return;
    }
	
	/* commenting out as Class is currently in Sharing framework, which is installed after base extensions
    //batch job is in process so do not run the trigger
    if(SecurityUtil.securityBatchInProgress)
    {
        return;
    }
	*/
	
    //check if the trigger is disabled in AFR
    //if it is, abort
    if(T1C_Base.ACETriggerCheck.isDisabled('employeeUserToOwnerBeforeSave'))
    {
        return;
    }
    
//  Make OwnerId = UserId, if the user is active.
//  -----------------------------------------------
    List<Id> userIds = new List<Id>{};
    for ( T1C_Base__Employee__c e : Trigger.new ) 
    {
        if ( e.T1C_Base__User__c != Null ) 
        {
            userIds.add(e.T1C_Base__User__c);
        }
    }
    Map<Id, User> users = new Map<Id, User> ([Select Id from User where Id IN :userIds and IsActive =: true]);
    for ( T1C_Base__Employee__c e : Trigger.new ) 
    {
        System.debug(e);
        if(users.containsKey(e.T1C_Base__User__c))
        {
            e.OwnerId = e.T1C_Base__User__c;
        }
    }                         
}