/*
 * Name:    EmployeeLookupUserSync
 *
 * Description: 
 *       - Get Employee lookup field values and sync with user fields. Mostly name from the lookup records.
 *       - AFR ACE.Extensions.EmployeeLookupUserSync
 *
 * Confidential & Proprietary, 2017 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole or in part
 * without written permission from Tier1CRM.
 *
 * PS Base 3.7.6
 */
trigger EmployeeLookupUserSync on T1C_Base__Employee__c (after insert, after update) 
{
	//check if the trigger is already ran once in this transaction
    //if it did, abort
    if (!DMLUtil.hasRun.add('EmployeeLookupUserSync'))
    {
        return;
    }
    
	/* commenting out as Class is currently in Sharing framework, which is installed after base extensions
	//batch job is in process so do not run the trigger
    if(SecurityUtil.securityBatchInProgress)
    {
        return;
    }
	*/
	
    //check if the trigger is disabled in AFR
    //if it is, abort
    if (T1C_Base.ACETriggerCheck.isDisabled('EmployeeLookupUserSync'))
    {
        return;
    }
	
    public static final String AFR_ACE_EXTENSIONS_EUS = 'ACE.Extensions.EmployeeLookupUserSync';
    
    //Grab the feature tree out of AFR.
    list<T1C_FR.Feature> subFeatures = T1C_FR.featureCacheWebSvc.getAttributeTree ( UserInfo.getUserId() , AFR_ACE_EXTENSIONS_EUS );
    
    if ( subFeatures == null ) 
	{
		return;
	}

    EmployeeUserFields[] fields = new EmployeeUserFields[]{};
    String empQuery = 'Select ';
    for (T1C_FR.Feature subFeature : subFeatures)
    {
    	String empField = subFeature.getAttributeValue('EmployeeField');
    	String userField = subFeature.getAttributeValue('UserField');
    
    	if (!String.isEmpty(empField) && !String.isEmpty(userField))
    	{
	        EmployeeUserFields val = new EmployeeUserFields();
	        val.employeeField = empField;
	        val.userField = userField;
	        fields.add(val);
	
	        if (val.employeeField != null && val.employeeField != '')
	        {
	            empQuery += val.employeeField + ', ';
	        }
    	}
    }
    set<Id> empIds = trigger.newMap.keySet();
    empQuery += 'Id, T1C_Base__User__c from T1C_Base__Employee__c where Id IN: empIds and T1C_Base__User__c != null';
    system.debug('fields>>>>>>>>>>>>>' + fields);
    system.debug('empQuery>>>>>>>>>>>>>' + empQuery);

    //if AFR contain any field information, continue
    if (fields.size() <= 0)
    {
        return;
    }

    User[] usersToUpdate = new User[]{};
    for (T1C_Base__Employee__c emp : Database.query(empQuery))
    {
        User u = new User(Id = emp.T1C_Base__User__c);

        for (EmployeeUserFields field : fields)
        {
            //if it is a lookup field, we need to make sure, it is not a null value before get the name from it
            String lookupField = field.employeeField;
            
            String lookupVal = (String)T1C_Base.AceUtil.getSObjFld ( emp , lookupField );
            u.put(field.userField, lookupVal);
        }

        usersToUpdate.add(u);
    }

    system.debug('usersToUpdate>>>>>>>>>>>>>' + usersToUpdate);
    if (usersToUpdate.size() > 0)
    {
        update usersToUpdate;
    }



    //wrapper class to contain employee and user fields
    public class EmployeeUserFields
    {
        public String employeeField;
        public String userField;
        
        public EmployeeUserFields()
        {
            
        }
    }
}