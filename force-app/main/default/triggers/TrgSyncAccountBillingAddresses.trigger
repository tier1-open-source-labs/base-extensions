/**
 * Name: TrgSyncAccountBillingAddresses 
 * Description: A trigger that updates the associated Billing and Shipping Addresses 
 * of an account when the Account Address is updated. Also when a new account is inserted,
 * automatically creates a new Address Object for the Billing and Shipping Addresses
 * where applicable 
 *
 * Confidential & Proprietary, 2019 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1CRM.
 *
 * PS Base 3.7.6
 */
trigger TrgSyncAccountBillingAddresses on Account ( after insert, before update ) 
{
   //This is the product decoupling trigger; It will interfere with what we're doing in this trigger so we shouldn't execute if it's running
   if( !T1C_Base.ACETriggerCheck.isDisabled('AccountAddressChange'))
   {
       return;
   }
   if( T1C_Base.ACETriggerCheck.isDisabled('TrgSyncAccountBillingAddresses') )
   {
       return;
   }
   
   if(Trigger.isInsert)
   {
      //If we already executed in this context, dont run again
      if(!AddressManagerController.finishedTriggers.add('TrgSyncAccountBillingAddressesInsert')  || Test.isRunningTest())
      {
         return;
      }
      AddressSyncTriggerHelper.SyncInsertedAccountAddress(trigger.newMap);
      
   }
   else if (Trigger.isUpdate)
   {
      //This trigger is self referencing, this is to prevent recursion
      if(!AddressManagerController.finishedTriggers.add('TrgSyncAccountBillingAddressesUpdate')  || Test.isRunningTest())
      {
         return;
      }
      AddressSyncTriggerHelper.SyncUpdatedAccountAddress(trigger.newMap);
      
   }//else if
}//trigger TrgSyncAccountBillingAddresses on Account ( after insert, after update )