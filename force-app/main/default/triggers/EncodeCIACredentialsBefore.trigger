/** 
 * Name: EncodeCIACredentialsBefore 
 * Description: TEMP trigger for encoding CIA credentials during a dataload. 
 *
 * IMPORTANT: Should be removed/disabled otherwise, as this will cause issues for ALM's normal functions
 *
 * Confidential & Proprietary, 2016 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1CRM.
 *
 * PS Base 3.7.6
 */
trigger EncodeCIACredentialsBefore on User (before insert, before update)
{

    if (T1C_Base.ACETriggerCheck.isDisabled('EncodeCIACredentialsBefore'))
    {
        return;
    }

    for (User u : Trigger.new)
    {
        if (Trigger.isInsert || (Trigger.isUpdate && u.T1C_Base__Telephony_Username__c != Trigger.oldMap.get(u.Id).T1C_Base__Telephony_Username__c))
        {
            String newValue = u.T1C_Base__Telephony_Username__c;

            if (newValue != null &&
                !isEncoded(newValue))
            {
                u.T1C_Base__Telephony_Username__c = EncodingUtil.base64encode(Blob.valueOf(newValue));
            }
        }
        if (Trigger.isInsert || (Trigger.isUpdate && u.T1C_Base__Telephony_Password__c != Trigger.oldMap.get(u.Id).T1C_Base__Telephony_Password__c))
        {
            String newValue = u.T1C_Base__Telephony_Password__c;

            if (newValue != null &&
                !isEncoded(newValue))
            {
                u.T1C_Base__Telephony_Password__c = EncodingUtil.base64encode(Blob.valueOf(newValue));
            }
        }
    }

    private static Boolean isEncoded(String stringValue)
    {
        Boolean isEncoded = false;
        try
        {
            String decodedUsername = EncodingUtil.base64decode(stringValue).toString();
            isEncoded = true;
        }
        catch(Exception e)
        {
            // if no exception that means it is properly decoded
            // otherwise if it hits here its not a valid base64 string
        }
        return isEncoded;
    }

}