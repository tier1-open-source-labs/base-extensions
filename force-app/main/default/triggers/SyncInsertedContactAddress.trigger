/** 
 * Name: SyncInsertedContactAddress 
 * Description: When a Contact is inserted, but neither the account
 *              nor the addresses for it were updated. We will 
 *              need a special trigger to find the addresses and 
 *              sync them automatically
 *
 * Confidential & Proprietary, 2012 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1CRM.
 *
 * PS Base 3.7.6
 */
 
trigger SyncInsertedContactAddress on Contact (before insert) 
{
   if( T1C_Base.ACETriggerCheck.isDisabled('SyncInsertedContactAddress'))
	{
	    return;
	}
   
   if(!AddressManagerController.finishedTriggers.add('SyncInsertedContactAddress') || !AddressManagerController.enableAutoSync)
	{
        return;
	}

   //Sift through the Addresses that need syncing. We need to weed out the
   //contacts that belong to those addresses' accounts since the batch job will handle them
   
   Set<Id> accountIdsToExclude = new Set<Id>();
   
   Set<Id> accountIdsToSearch = new Set<Id>();
   //Exclude the contacts that have address objects marked for update by the batch
   for(Contact cnt : trigger.new)
   {
      accountIdsToSearch.add(cnt.AccountId);
   }
   
   Map<Id, List<T1C_Base__Address__c>> accountAddressesMap = new Map<Id, List<T1C_Base__Address__c>>();
   //Finally map the Addresses together
   for(T1C_Base__Address__c addr : AddressSyncTriggerHelper.getAddressesForAccountIds(accountIdsToSearch))
   {
      List<T1C_Base__Address__c> acntAddrs = accountAddressesMap.get(addr.T1C_Base__Account__c);
         
      if(acntAddrs == null)
      {
         acntAddrs = new List<T1C_Base__Address__c>();
      }
      
      acntAddrs.add(addr);
      
      accountAddressesMap.put(addr.T1C_Base__Account__c, acntAddrs);
   }
   
   //Data prep complete, now sync the contact with Address Ids via fuzzy match using Levenshtein distance to compare
   //N^2 very ugly
   for(Contact cnt : trigger.new)
   {
      //If we're inserting a contact with a mailing address Id, skip it
      if(cnt.T1C_Base__Mailing_Address_Id__c != null || cnt.AccountId == null || accountAddressesMap.get(cnt.AccountId) == null)
      {
         continue;
      }
      
      Integer currentScore = AddressManagerController.autoSyncEditThreshold;
      T1C_Base__Address__c addrToApply = null;
      for( T1C_Base__Address__c accntAddr : accountAddressesMap.get(cnt.AccountId) )
      {
         Integer compScore = AddressContactSyncBatch.compareAddress( cnt, accntAddr );
         if(compScore < currentScore )
         {
            currentScore = compScore;
            addrToApply = accntAddr;
            cnt.T1C_Base__Mailing_Address_Id__c = accntAddr.Id;
            /*cnt.MailingStreet     = accntAddr.T1C_Base__Street__c;
            cnt.MailingCity       = accntAddr.T1C_Base__City__c;
            cnt.MailingCountry    = accntAddr.T1C_Base__Country__c;
            cnt.MailingState      = accntAddr.T1C_Base__State_Province__c;
            cnt.MailingPostalCode = accntAddr.T1C_Base__Zip_Postal_Code__c;*/
         }
      }

      if(addrToApply != null)
      {
         AddressSyncTriggerHelper.applyAddressChange(addrToApply, cnt, AddressSyncTriggerHelper.contactFieldsMap);
      }

   }
}