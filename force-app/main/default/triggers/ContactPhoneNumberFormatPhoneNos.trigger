/*
 * Name:        ContactPhoneNumberFormatPhoneNos
 *
 * Description: Trigger BEFORE Contact Phone Number Insert/Update, to format the Phone Number
 *				fields of the participating Contact Phone Number records.
 * Notes: 		
 *
 * Confidential & Proprietary, 2014 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole or in part
 * without written permission from Tier1CRM.
 *
 * PS Base 3.7.6
 */
trigger ContactPhoneNumberFormatPhoneNos on T1C_Base__Contact_Phone_Number__c (before insert, before update) 
{
	//check if the trigger is active
    //if it is not, abort
    if(T1C_Base.ACETriggerCheck.isDisabled('ContactPhoneNumberFormatPhoneNos'))
    {
    	return;
    }
    
	T1C_Base.AceUtil.formatSObjPhoneNos ( Trigger.new , Null );
}