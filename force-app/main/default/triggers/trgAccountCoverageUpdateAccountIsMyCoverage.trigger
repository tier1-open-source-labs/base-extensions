/** 
 * Name: trgAccountCoverageUpdateAccountIsMyCoverage 
 * Description: Updates the account with a string of User IDs that cover the account
 *
 * Confidential & Proprietary, 2014 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1CRM.
 *
 * PS Base 3.7.6
 */

trigger trgAccountCoverageUpdateAccountIsMyCoverage on T1C_Base__Account_Coverage__c (after insert, after update, after delete) 
{
	if(T1C_Base.ACETriggerCheck.isDisabled('trgAccountCoverageUpdateAccountIsMyCoverage'))
    {
      return;
    }
    
    Integer userIdLength = 15;
    
    String configLength = T1C_FR.featureCacheWebSvc.getAttribute(UserInfo.getUserId(), 'ACE.Extensions.IsMyCoverage','UserIdLength');
    
    if(!String.isEmpty(configLength) && configLength.isNumeric())
    	{
    		userIdLength = Integer.valueOf(configLength);
    }
    
    Integer offset = (255 / (1 + userIdLength));
    
	list<Account> acctToUpdate = new list<Account>{};
	set<Id> acctIds = new set<Id>{};
	
	if(trigger.isInsert || trigger.isUpdate)
	{
		for(T1C_Base__Account_Coverage__c ac : (Trigger.new))
		{
			if(ac.T1C_Base__Account__c != null)
			{
				acctIds.add(ac.T1C_Base__Account__c);	
			}
		}
	}
	
	else if(trigger.isDelete)
	{
		for(T1C_Base__Account_Coverage__c ac : Trigger.old)
		{
			if(ac.T1C_Base__Account__c != null)
			{
				acctIds.add(ac.T1C_Base__Account__c);	
			}
		}
	}
	
	for(Account acct : [select Id, Name, Coverage_Ids__c, Coverage_Ids2__c, Coverage_Ids3__c, Coverage_Ids4__c, Coverage_Ids5__c, Coverage_Ids6__c, 
			(select T1C_Base__Account__c, T1C_Base__Employee__r.T1C_Base__User__c, T1C_Base__Inactive__c from T1C_Base__Account_Coverage__r 
				where T1C_Base__Employee__r.T1C_Base__User__c != null and T1C_Base__Inactive__c = 'false' order by CreatedDate) 
			from Account where Id in :acctIds])
	{
		//Clear the Id Fields
		acct.Coverage_Ids__c = null;
		acct.Coverage_Ids2__c = null;
		acct.Coverage_Ids3__c = null;
		acct.Coverage_Ids4__c = null;
		acct.Coverage_Ids5__c = null;
		acct.Coverage_Ids6__c = null;
		
		integer i = 0;
		list<String> coverageIds1 = new list<String>();
		list<String> coverageIds2 = new list<String>();
		list<String> coverageIds3 = new list<String>();
		list<String> coverageIds4 = new list<String>();
		list<String> coverageIds5 = new list<String>();
		list<String> coverageIds6 = new list<String>();
				
		for(T1C_Base__Account_Coverage__c ac : acct.T1C_Base__Account_Coverage__r)
		{
			i++;
			String userId = ac.T1C_Base__Employee__r.T1C_Base__User__c;
			userId = userId.left(15); // Salesforce returns the 15 charcter Id in the formula field
			userId = userId.right(userIdLength);
			if(i<=offset)
			{
				coverageIds1.add(userId);
			}
			else if(i<=(offset*2))
			{
				coverageIds2.add(userId);
			}
			else if(i<=(offset*3))
			{
				coverageIds3.add(userId);
			}
			else if(i<=(offset*4))
			{
				coverageIds4.add(userId);
			}
			else if(i<=(offset*5))
			{
				coverageIds5.add(userId);
			}
			else if(i<=(offset*6))
			{
				coverageIds6.add(userId);
			}
			else
			{
				continue;
			}		
		}

		if(!coverageIds1.isEmpty())
		{
			acct.Coverage_Ids__c = T1C_Base.AceUtil.concatList(new List<String>(coverageIds1), ',', false);
		}
		
		if(!coverageIds2.isEmpty())
		{
			acct.Coverage_Ids2__c = T1C_Base.AceUtil.concatList(new List<String>(coverageIds2), ',', false);
		}

		if(!coverageIds3.isEmpty())
		{
			acct.Coverage_Ids3__c = T1C_Base.AceUtil.concatList(new List<String>(coverageIds3), ',', false);
		}
		
		if(!coverageIds4.isEmpty())
		{
			acct.Coverage_Ids4__c = T1C_Base.AceUtil.concatList(new List<String>(coverageIds4), ',', false);
		}
		
		if(!coverageIds5.isEmpty())
		{
			acct.Coverage_Ids5__c = T1C_Base.AceUtil.concatList(new List<String>(coverageIds5), ',', false);
		}

		if(!coverageIds6.isEmpty())
		{
			acct.Coverage_Ids6__c = T1C_Base.AceUtil.concatList(new List<String>(coverageIds6), ',', false);
		}
		
		acctToUpdate.add(acct);		
	}
	
	if (!acctToUpdate.isEmpty())
	{
		update(acctToUpdate);
	}

}