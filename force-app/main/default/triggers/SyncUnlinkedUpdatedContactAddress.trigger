/** 
 * Name: SyncUnlinkedUpdatedContactAddress 
 * Description: When a Contact is updated from Prospector or the ADH service,
 *              it needs to be linked to a Strong address match
 *
 * Confidential & Proprietary, 2012 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1CRM.
 *
 * PS Base 3.7.6
 */
 
trigger SyncUnlinkedUpdatedContactAddress on Contact (before update) 
{
    if (T1C_Base.ACETriggerCheck.isDisabled('SyncUpdatedContactAddress') || !T1C_Base.ACETriggerCheck.isDisabled('ContactAddressChange'))
    {
        return;
    }
   
    //Since we run on updates now, this trigger CAN be recursive when execution comes from Address Manager
    if(!AddressManagerController.finishedTriggers.add('SyncUnlinkedUpdatedContactAddress') || !AddressManagerController.enableAutoSync)
	{
        return;
	}

    Set<Id> accountIds = new Set<Id>();
    //Get the Accounts that these Contacts belong to, and their addresses
    for(Contact cnt : Trigger.new)
    {  
        Contact oldCnt = Trigger.oldMap.get(cnt.Id);

        if(cnt.AccountId != null && cnt.AccountId != oldCnt.AccountId)
        {
            continue;
        }

        Boolean addressChanged = cnt.MailingCity != oldCnt.MailingCity ||
            cnt.MailingCountry != oldCnt.MailingCountry ||
            cnt.MailingPostalCode != oldCnt.MailingPostalCode ||
            cnt.MailingState != oldCnt.MailingState ||
            cnt.MailingStreet != oldCnt.MailingStreet;
        
        //Ignore Employment changes
        
        
        if(cnt.T1C_Base__Mailing_Address_Id__c == null || addressChanged)
        {
            accountIds.add(cnt.AccountId);
        }
    }

    if(accountIds.isempty())
    {
        return;
    }

    //Map Containing Lists of Addresses per account
    //Key -> Account Ids
    //Value -> A list containing all of the addresses that belong to each account
    Map<Id, List<T1C_Base__Address__c>> accountAddressesMap = new Map<Id, List<T1C_Base__Address__c>>();
    for(T1C_Base__Address__c addr : AddressSyncTriggerHelper.getAddressesForAccountIds(accountIds))
    {
        //populate the address list map for this address' account. This is needed for the contact linking step

        List<T1C_Base__Address__c> acntAddrs = accountAddressesMap.get(addr.T1C_Base__Account__c);

        if(acntAddrs == null)
        {
            acntAddrs = new List<T1C_Base__Address__c>();
        }

        acntAddrs.add(addr);

        accountAddressesMap.put(addr.T1C_Base__Account__c, acntAddrs);
    }

    for(Contact ct : Trigger.new)
    {
        //if at this point we find that the Contact DOES have a mailing address ID, that means its address changed.ApexPages
        //Null it out and look for another one to match, else stay delinked
        ct.T1C_Base__Mailing_Address_Id__c = null;

        Integer currentScore = AddressManagerController.autoSyncEditThreshold;

        Contact oldCnt = Trigger.oldMap.get(ct.Id);

        if(accountAddressesMap.get(ct.AccountId) == null || (ct.AccountId != null && ct.AccountId != oldCnt.AccountId) )
        {
            continue;
        }
        
        //Add a temporary compareTo variable to preserve to original as we compare
        Contact tempCon = ct.clone(false, true);
        
        T1C_Base__Address__c addrToApply = null;
        //Try to match this contact using its Address fields to an existing Address record for this account
        for( T1C_Base__Address__c accntAddr : accountAddressesMap.get(ct.AccountId) )
        {
            //Match postal code first. This is the most definitive match when trying to figure out the address
            if(accntAddr.T1C_Base__Zip_Postal_Code__c != null && accntAddr.T1C_Base__Zip_Postal_Code__c == tempCon.MailingPostalCode)
            {
                ct.T1C_Base__Mailing_Address_Id__c = accntAddr.Id;
                addrToApply = accntAddr;
                /*ct.MailingStreet     = accntAddr.T1C_Base__Street__c;
                ct.MailingCity       = accntAddr.T1C_Base__City__c;
                ct.MailingCountry    = accntAddr.T1C_Base__Country__c;
                ct.MailingState      = accntAddr.T1C_Base__State_Province__c;
                ct.MailingPostalCode = accntAddr.T1C_Base__Zip_Postal_Code__c;*/
                break;
            }else
            {
                //We're going to try to minimize here, so if we cross the intiial threshold of 5 then we pick it as the current winner

                Integer compScore = AddressContactSyncBatch.compareAddress(tempCon, accntAddr);
                if(compScore < currentScore )
                {
                    currentScore = compScore;
                    addrToApply = accntAddr;
                    ct.T1C_Base__Mailing_Address_Id__c = accntAddr.Id;
                    /*ct.MailingStreet     = accntAddr.T1C_Base__Street__c;
                    ct.MailingCity       = accntAddr.T1C_Base__City__c;
                    ct.MailingCountry    = accntAddr.T1C_Base__Country__c;
                    ct.MailingState      = accntAddr.T1C_Base__State_Province__c;
                    ct.MailingPostalCode = accntAddr.T1C_Base__Zip_Postal_Code__c;*/
                }
            }
        }

        if(addrToApply != null)
        {
            AddressSyncTriggerHelper.applyAddressChange(addrToApply, ct, AddressSyncTriggerHelper.contactFieldsMap);
        }
    }
}