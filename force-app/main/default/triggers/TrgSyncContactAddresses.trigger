/**
 * Name: TrgSyncContactAddresses 
 * Description: A trigger that updates all associated Contact Addresses when an Address
 * Object is updated. Uses the T1C_Base__Mailing_Address_Id__c lookup to determine related 
 * Contacts.
 *
 * Confidential & Proprietary, 2019 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1CRM.
 *
 * PS Base 3.7.6
 */
trigger TrgSyncContactAddresses on T1C_Base__Address__c ( before update ) 
{
   if( T1C_Base.ACETriggerCheck.isDisabled('TrgSyncContactAddresses'))
	{
	    return;
	}
   
   if(!AddressManagerController.finishedTriggers.add('TrgSyncContactAddresses'))
	{
        return;
	}
   
    if( !Test.isRunningTest() && !System.isBatch() )
    {
	  Database.executeBatch( new SyncContactAddressesBatch(trigger.newMap) );
	} else 
    {
        //Mark these Addresses for post-batch processing
        for( T1C_Base__Address__c addr : trigger.newMap.values() )
        {
            addr.Needs_Sync__c = true;
        }
        
    }
	
	//Billing/shipping logic
   //If it's a Billing address, sync it with the Account.
	//Dont need to batch this one since Billing Addresses to Accounts are 1:1
	 /*SELECT Name, BillingStreet, BillingCity, BillingState, BillingPostalCode,
       BillingCountry, ShippingStreet, ShippingCity, ShippingState, ShippingPostalCode,
       ShippingCountry
     FROM Account*/
   
   //This trigger is self referencing, this is to prevent recursion
	if(!AddressManagerController.finishedTriggers.add('TrgSyncAccountBillingAddressesUpdate')  || Test.isRunningTest())
	{
	   return;
	}
   AddressSyncTriggerHelper.SyncAddressToAccountAndContacts(trigger.newMap);	
}