({
	doInit : function(component, event, helper) {
        var value = helper.getParameterByName(component , event, 'inContextOfRef');
        var recordId = '';
        if(value)
        {
                var context = JSON.parse(window.atob(value));
                recordId = context.attributes.recordId;
        }else
        {
                recordId = component.get('v.recordId');
        }
        
        component.set("v.parentRecordId", recordId);
	}
})