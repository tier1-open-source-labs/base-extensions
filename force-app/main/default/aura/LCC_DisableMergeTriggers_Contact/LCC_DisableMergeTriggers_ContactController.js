({
	apexExecute : function(component, event, helper) {
		//Call Your Apex Controller Method.
		var action = component.get("c.enableDisableTriggers");

		action.setParams({
			'val': 'true'
		});

		action.setCallback(this, function(response) {
			var state = response.getState();
			console.log(state);
			
			if (state === "SUCCESS") {
				//after code
				var result = response.getReturnValue();
helper.showTextAlert(component, 'Triggers are Disabled');

			} else {
				
			}

			
		});

		$A.enqueueAction(action);
	},

	accept : function(component, event, helper) {
		$A.get("e.force:closeQuickAction").fire();
	}
})