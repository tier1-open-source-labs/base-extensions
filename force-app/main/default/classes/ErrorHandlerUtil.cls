/**
 * Name: ErrorHandlerUtil
 * Description: Contains handler to return user friendly error messages
 *
 * Confidential & Proprietary, 2016 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1CRM.
 * 
 *	PS Base 3.7.6
 *
 */

public with sharing class ErrorHandlerUtil 
{
	public ErrorHandlerUtil() 
	{
	}

//  ============================================================================
//  Method: getErrorMessage
//
//  Desc: Loops through all the error messages available and appends to a variable 
//
//  Args: DMLException   
//   
//  Return: Error Message
//  ============================================================================
	public static string getErrorMessage(DmlException d)
	{
		string errorMessage = '';
	    for(Integer i=0;i<d.getNumDml();i++) 
	    {
	        errorMessage = errorMessage + ' Error ' + (i+1) + ': ' + d.getDmlMessage(i) +';\n';
	    }	    
		return errorMessage;
	}	
}