/** 
 * Name: IsMyCoverageTests 
 * Description: Tests the Is My Coverage Triggers
 *
 * Confidential & Proprietary, 2014 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1CRM.
 * 
 *	PS Base 3.7.6
 *
 */
@isTest
private class IsMyCoverageTests 
{
    private static final Tier1CRMTestDataFactory dataFactory = Tier1CRMTestDataFactorySetup.getDataFactory();

    static testMethod void isMyCoverageTest() 
    {
    	dataFactory.createCBSAFR();
		T1C_FR__Feature__c ext = new T1C_FR__Feature__c(Name = 'Extensions', T1C_FR__Name__c = 'ACE.Extensions');
        T1C_FR__Feature__c isMyCov = new T1C_FR__Feature__c(Name = 'IsMyCoverage', T1C_FR__Name__c = 'ACE.Extensions.IsMyCoverage');
    	
		upsert new list<T1C_FR__Feature__c>{ext,isMyCov};

		insert new T1C_FR__Attribute__c(T1C_FR__Feature__c = isMyCov.Id, Name = 'UserIdLength', T1C_FR__Value__c = '18');
		
		map<String, Id> baseDataMap = dataFactory.setupBaseDataAndCoverage();
		
		Id contId = baseDataMap.get('Contact');	
		list<Id> acIds = new list<Id>{baseDataMap.get('AccountCoverage')};
		list<Id> ccIds = new list<Id>{baseDataMap.get('ContactCoverage')};
		
		Test.startTest();
			
		Database.DeleteResult[] delRes = database.delete(acIds, false);		
		delRes = database.delete(ccIds, false);
		
		Test.stopTest();
    }
}