/** 
 * Name: Tier1CRMTestDataFactorySetup 
 * Description: Class to centralize the creation of data for test classes
 *
 * Returning objects to avoid many DML Executions and allows further customization in the Test Class
 *
 * Confidential & Proprietary, 2019 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1CRM.
 * 
 *	PS Base 3.7.6
 *
 */
 
 public with sharing class Tier1CRMTestDataFactorySetup 
{
    public static Tier1CRMTestDataFactory getDataFactory()
    {
        String dataFactoryClassName = 'Tier1CRMTestDataFactoryCore';

        for(StaticResource sr : [SELECT Id, Body FROM StaticResource WHERE Name = 'DataFactoryName'])
        {
            dataFactoryClassName = sr.Body.toString();
        }
        
        Tier1CRMTestDataFactory dataFactory = (Tier1CRMTestDataFactory)Type.forName(dataFactoryClassName).newInstance();
        
        return dataFactory;
    }
}