/**
 * Name: AddressSyncTriggerHelper 
 * Description: Helper class designed to facilitate 2-way synchronization
 * between the Account Billing/Shipping Fields and the Tier1 Address Object
 *
 * Confidential & Proprietary, 2019 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1CRM.
 *
 *  PS Base 3.7.6
 * 
 */
global without sharing class AddressSyncTriggerHelper 
{
    public static String ADDRESS_MANAGER_FEATURE_PATH = 'ACE.Extensions.AddressManager';
    
    //Map of Account Billing address fields that map to Address object Fields
    public static Map<String, String> billingFieldsMap
    {
        set
        {

        }
        get
        {
            if(billingFieldsMap == null)
            {
                T1C_FR.Feature fieldsMapFeat = T1C_FR.FeatureCacheWebSvc.getAttributeTreeRecursive(UserInfo.getUserId(), ADDRESS_MANAGER_FEATURE_PATH + '.SyncMaps.Billing');
                billingFieldsMap = new Map<String, String>();

                //Default settings
                billingFieldsMap.put('BillingCity'      , 'T1C_Base__City__c');
                billingFieldsMap.put('BillingCountry'   , 'T1C_Base__Country__c');
                billingFieldsMap.put('BillingPostalCode', 'T1C_Base__Zip_Postal_Code__c');
                billingFieldsMap.put('BillingState'     , 'T1C_Base__State_Province__c');
                billingFieldsMap.put('BillingStreet'    , 'T1C_Base__Street__c');

                if(fieldsMapFeat != null)
                {
                    for(T1C_FR.Feature feat : fieldsMapFeat.subFeatures)
                    {
                        String acntFld = T1C_FR.FeatureCacheWebSvc.getAttribute(UserInfo.getUserId(), feat.Name, 'AccountField');
                        String addrFld = T1C_FR.FeatureCacheWebSvc.getAttribute(UserInfo.getUserId(), feat.Name, 'AddressField');

                        billingFieldsMap.put(acntFld, addrFld);
                    }
                }
            }
            return billingFieldsMap;
        }
    }

    //Map of Contact fields that map to Address Fields
    public static Map<String, String> contactFieldsMap
    {
        set
        {

        }
        get
        {
            if(contactFieldsMap == null)
            {
                T1C_FR.Feature fieldsMapFeat = T1C_FR.FeatureCacheWebSvc.getAttributeTreeRecursive(UserInfo.getUserId(), ADDRESS_MANAGER_FEATURE_PATH + '.SyncMaps.Contacts');
                contactFieldsMap = new Map<String, String>();

                //Default settings
                contactFieldsMap.put('MailingCity'      , 'T1C_Base__City__c');
                contactFieldsMap.put('MailingCountry'   , 'T1C_Base__Country__c');
                contactFieldsMap.put('MailingPostalCode', 'T1C_Base__Zip_Postal_Code__c');
                contactFieldsMap.put('MailingState'     , 'T1C_Base__State_Province__c');
                contactFieldsMap.put('MailingStreet'    , 'T1C_Base__Street__c');

                if(fieldsMapFeat != null)
                {
                    for(T1C_FR.Feature feat : fieldsMapFeat.subFeatures)
                    {
                        String acntFld = T1C_FR.FeatureCacheWebSvc.getAttribute(UserInfo.getUserId(), feat.Name, 'ContactField');
                        String addrFld = T1C_FR.FeatureCacheWebSvc.getAttribute(UserInfo.getUserId(), feat.Name, 'AddressField');

                        contactFieldsMap.put(acntFld, addrFld);
                    }
                }
            }
            return contactFieldsMap;
        }
    }

    //Map of Account Billing address fields that map to Address object Fields
    public static Map<String, String> shippingFieldsMap
    {
        set
        {

        }
        get
        {
            if(shippingFieldsMap == null)
            {
                T1C_FR.Feature fieldsMapFeat = T1C_FR.FeatureCacheWebSvc.getAttributeTreeRecursive(UserInfo.getUserId(), ADDRESS_MANAGER_FEATURE_PATH + '.SyncMaps.Shipping');
                shippingFieldsMap = new Map<String, String>();

                //Default settings
                shippingFieldsMap.put('ShippingCity'      , 'T1C_Base__City__c');
                shippingFieldsMap.put('ShippingCountry'   , 'T1C_Base__Country__c');
                shippingFieldsMap.put('ShippingPostalCode', 'T1C_Base__Zip_Postal_Code__c');
                shippingFieldsMap.put('ShippingState'     , 'T1C_Base__State_Province__c');
                shippingFieldsMap.put('ShippingStreet'    , 'T1C_Base__Street__c');

                if(fieldsMapFeat != null)
                {
                    for(T1C_FR.Feature feat : fieldsMapFeat.subFeatures)
                    {
                        String acntFld = T1C_FR.FeatureCacheWebSvc.getAttribute(UserInfo.getUserId(), feat.Name, 'AccountField');
                        String addrFld = T1C_FR.FeatureCacheWebSvc.getAttribute(UserInfo.getUserId(), feat.Name, 'AddressField');

                        shippingFieldsMap.put(acntFld, addrFld);
                    }
                }
            }
            return shippingFieldsMap;
        }
    }

    public static String allowedAccountTypes
    {
        set
        {
        
        }
        get
        {
            if (allowedAccountTypes == null)
            {
                allowedAccountTypes = T1C_FR.FeatureCacheWebSvc.getAttribute(UserInfo.getUserId(), ADDRESS_MANAGER_FEATURE_PATH, 'SyncAllowedAccountTypes');
            }
            return allowedAccountTypes;
        }
    }
    
    public static String allowedAccountTypeField
    {
        set
        {
        
        }
        get
        {
            if (allowedAccountTypeField == null)
            {
                allowedAccountTypeField = T1C_FR.FeatureCacheWebSvc.getAttribute(UserInfo.getUserId(), ADDRESS_MANAGER_FEATURE_PATH, 'SyncAllowedAccountTypesField');
                allowedAccountTypeField = allowedAccountTypeField != null && allowedAccountTypeField.length() > 0 ? allowedAccountTypeField : '';
            }
            return allowedAccountTypeField;
        }
    }

    //Have a specific AFR to determine behaviour on Account insert
    public static String allowedAccountTypesOnInsert
    {
        set
        {
        
        }
        get
        {
            if (allowedAccountTypesOnInsert == null)
            {
                allowedAccountTypesOnInsert = T1C_FR.FeatureCacheWebSvc.getAttribute(UserInfo.getUserId(), ADDRESS_MANAGER_FEATURE_PATH, 'OnInsertRestrictedAccountTypes');
            }
            return allowedAccountTypesOnInsert;
        }
    }
    
    public static String allowedAccountTypeOnInsertField
    {
        set
        {
        
        }
        get
        {
            if (allowedAccountTypeOnInsertField == null)
            {
                allowedAccountTypeOnInsertField = T1C_FR.FeatureCacheWebSvc.getAttribute(UserInfo.getUserId(), ADDRESS_MANAGER_FEATURE_PATH, 'OnInsertRestrictedAccountTypesField');
                allowedAccountTypeOnInsertField = allowedAccountTypeOnInsertField != null && allowedAccountTypeOnInsertField.length() > 0 ? allowedAccountTypeOnInsertField : '';
            }
            return allowedAccountTypeOnInsertField;
        }
    }
    
    public static String addrName
    {
        set
        {
        
        }
        get
        {
            if (addrName == null)
            {
                addrName = T1C_FR.FeatureCacheWebSvc.getAttribute(UserInfo.getUserId(), ADDRESS_MANAGER_FEATURE_PATH, 'BillingAddressName');
                addrName = addrName != null && addrName.length() > 0 ? addrName : 'Billing Address';
            }
            return addrName;
        }
    }
    
    public static String shipAddrName
    {
        set
        {
        
        }
        get
        {
            if (shipAddrName == null)
            {
                shipAddrName = T1C_FR.FeatureCacheWebSvc.getAttribute(UserInfo.getUserId(), ADDRESS_MANAGER_FEATURE_PATH, 'ShippingAddressName');
                shipAddrName = shipAddrName.length() > 0 ? shipAddrName : 'Shipping Address';
            }
            return shipAddrName;
        }
    }
   
    public static void SyncInsertedAccountAddress(map<Id, Account> inputAddresses)
    {
        List<T1C_Base__Address__c> addrBillLst = new List<T1C_Base__Address__c>(); 
        List<T1C_Base__Address__c> addrShipLst = new List<T1C_Base__Address__c>(); 
        //Insert logic; if a new Account is created, take the Billing and Shipping addresses and create new Address objects from them
        for(Account curAcnt : inputAddresses.values())
        {
            
            String accountTypeString = allowedAccountTypeField != null && allowedAccountTypeField.length() > 0 ? 
                                       getAllowedFieldValue( (SObject)curAcnt, allowedAccountTypeField ) : '';

            if( allowedAccountTypes.length() > 0 && !allowedAccountTypes.contains(String.valueOf(accountTypeString)) )
            {
                continue;
            }

            //Look at Insert Restriction AFR for unallowed Source Types for Account 
            String insertTypeString = allowedAccountTypeOnInsertField != null && allowedAccountTypeOnInsertField.length() > 0 ? 
                                       getAllowedFieldValue( (SObject)curAcnt, allowedAccountTypeOnInsertField ) : '';

            if( allowedAccountTypesOnInsert.length() > 0 && allowedAccountTypesOnInsert.contains(String.valueOf(insertTypeString)) )
            {
                continue;
            }

            System.debug(curAcnt.BillingAddress + '/' + curAcnt.ShippingAddress);
            if( curAcnt.BillingCity != null || curAcnt.BillingCountry != null || curAcnt.BillingPostalCode != null || curAcnt.BillingStreet != null || curAcnt.BillingState != null)
            {
                T1C_Base__Address__c newAddress = new T1C_Base__Address__c();
                newAddress.T1C_Base__City__c = curAcnt.BillingCity;
                newAddress.T1C_Base__Country__c = curAcnt.BillingCountry;
                newAddress.T1C_Base__Zip_Postal_Code__c = curAcnt.BillingPostalCode;
                newAddress.T1C_Base__State_Province__c = curAcnt.BillingState;
                newAddress.T1C_Base__Street__c = curAcnt.BillingStreet;
                newAddress.T1C_Base__Account__c = curAcnt.Id;

                newAddress.T1C_Base__Address_Name__c = addrName;

                addrBillLst.add(newAddress);
            }
         
            if( curAcnt.ShippingCity != null || curAcnt.ShippingCountry != null || curAcnt.ShippingPostalCode != null || curAcnt.ShippingStreet != null || curAcnt.ShippingState != null)
            {
                T1C_Base__Address__c newAddress = new T1C_Base__Address__c();
                newAddress.T1C_Base__City__c = curAcnt.ShippingCity;
                newAddress.T1C_Base__Country__c = curAcnt.ShippingCountry;
                newAddress.T1C_Base__Zip_Postal_Code__c = curAcnt.ShippingPostalCode;
                newAddress.T1C_Base__State_Province__c = curAcnt.ShippingState;
                newAddress.T1C_Base__Street__c = curAcnt.ShippingStreet;
                newAddress.T1C_Base__Account__c = curAcnt.Id;

                newAddress.T1C_Base__Address_Name__c = shipAddrName;

                addrShipLst.add(newAddress);
            }
        }

        Schema.DescribeSObjectResult dsorAddress = T1C_Base__Address__c.sObjectType.getDescribe();

        if(!addrBillLst.isEmpty() && dsorAddress.isCreateable())
        {
            //system.debug('Found addresses to Insert');
            insert addrBillLst;
        }
        if(!addrShipLst.isEmpty() && dsorAddress.isCreateable())
        {
            //system.debug('Found addresses to Insert');
            insert addrShipLst;
        }

        Set<Id> updateIds = new Set<Id>();
        List<Account> updateList = new List<Account>();

        Set<Id> accountIds = inputAddresses.keySet();

        Map<Id, Account> accntsMap = new Map<Id, Account>([SELECT Id, 
                                                                Name, 
                                                                T1C_Base__Billing_Address_Id__c,
                                                                T1C_Base__Shipping_Address_Id__c  
                                                            FROM Account 
                                                            WHERE Id IN :accountIds]);

        //use the reference of the list to assign the T1C_Base__Billing_Address_Id__c and T1C_Base__Shipping_Address_Id__c fields in the trigger context
        for (T1C_Base__Address__c addr : addrBillLst)
        {
            Account curAccnt = accntsMap.get(addr.T1C_Base__Account__c);
            curAccnt.T1C_Base__Billing_Address_Id__c = addr.Id;
            accntsMap.put(curAccnt.Id, curAccnt);
            updateIds.add(curAccnt.Id);
        }
        for (T1C_Base__Address__c addr : addrShipLst)
        {
            Account curAccnt = accntsMap.get(addr.T1C_Base__Account__c);
            curAccnt.T1C_Base__Shipping_Address_Id__c = addr.Id;
            accntsMap.put(curAccnt.Id, curAccnt);
            updateIds.add(curAccnt.Id);
        }

        for (Id curId : updateIds)
        {
            updateList.add(accntsMap.get(curId));
        }

        Schema.DescribeSObjectResult dsorAccount = Account.sObjectType.getDescribe();
        if(dsorAccount.isUpdateable())
        {
            update updateList;
        }
        
    }//public static void SyncInsertedAccountAddress
   
    
    public static void SyncUpdatedAccountAddress(map<Id, Account> inputAddresses)
    {   
        List<T1C_Base__Address__c> addrBillLst = new List<T1C_Base__Address__c>(); 
        List<T1C_Base__Address__c> addrShipLst = new List<T1C_Base__Address__c>(); 
        
        //Update Logic - if an account address was updated (delta in the Billing/Shipping fields) we update the associated Address Object
        
        //Get the configured fields from the Billing and Shipping field maps as well
        Set<String> billingFields = billingFieldsMap.keySet();
        Set<String> shippingFields = shippingFieldsMap.keySet();

        Set<Id> accounts = inputAddresses.keySet();
        String fld = allowedAccountTypeField.length() > 0 ? allowedAccountTypeField : null;

        Set<String> queryFields = new Set<String>();
        queryFields.addAll(billingFields);
        queryFields.addAll(shippingFields);
        queryFields.add('T1C_Base__Billing_Address_Id__c');
        queryFields.add('T1C_Base__Shipping_Address_Id__c');
        queryFields.add('T1C_Base__Billing_Address_Id__r.T1C_Base__Address_Name__c');
        queryFields.add('T1C_Base__Shipping_Address_Id__r.T1C_Base__Address_Name__c');
        queryFields.add('Id');
        if (fld != null){
            queryFields.add(fld);
        }
        String query = 'SELECT ' + String.join(new List<String>(queryFields), ',') +
                         ' FROM Account '+
                        'WHERE Id IN :accounts';
        System.debug(query);
        
        list<Account> queriedAccounts = Database.query(query);
        map<Id, T1C_Base__Address__c> addressMap = new map<Id, T1C_Base__Address__c>();
        
        for(Account qAcct : queriedAccounts)
        {
            if(qAcct.T1C_Base__Billing_Address_Id__c != null)
            {
                addressMap.put(qAcct.T1C_Base__Billing_Address_Id__c, null);
            }

            if(qAcct.T1C_Base__Shipping_Address_Id__c != null)
            {
                addressMap.put(qAcct.T1C_Base__Shipping_Address_Id__c , null);
            }
        }
        
        //Map out all of the Addresses so that we can extract their names
        for(T1C_Base__Address__c addr : [Select Id, 
                                                T1C_Base__Address_Name__c, 
                                                T1C_Base__Account__c  
                                           from T1C_Base__Address__c  
                                          where T1C_Base__Account__c in :accounts])
        {
            addressMap.put(addr.Id, addr);
        }
        
        //The old map does not necessarily have the Billing and Shipping IDs 
        for(Account oldAcnt : queriedAccounts)
        {
            Account newAcnt = inputAddresses.get(oldAcnt.Id);

            String accountTypeString = allowedAccountTypeField != null && allowedAccountTypeField.length() > 0 ? 
                                       getAllowedFieldValue( (SObject)newAcnt, allowedAccountTypeField ) : '';

            if( allowedAccountTypes.length() > 0 && !allowedAccountTypes.contains(String.valueOf(accountTypeString)) )
            {
                continue;
            }
            
            //if the account is now permitted to be synced. This means going from a disallowed state to an allowed state based
            //on the allowedAccountTypes
            
            system.debug('Found account: ' + oldAcnt);

            T1C_Base__Address__c newAddress = new T1C_Base__Address__c();
            
            Boolean billingChanged = applyAccountAddressChange(newAddress, newAcnt, oldAcnt, billingFieldsMap);

            //If we changed the Billing address of the Account, we update the address. If we switched to another address, leave it alone
            if(billingChanged)
            {
                //Special case for address name
                String addressName = newAcnt.T1C_Base__Billing_Address_Id__r.T1C_Base__Address_Name__c != null ? 
                    newAcnt.T1C_Base__Billing_Address_Id__r.T1C_Base__Address_Name__c : addrName;
                    
                T1C_Base__Address__c existingAddress;
                
                if(newAcnt.T1C_Base__Billing_Address_Id__c != null && addressMap.containsKey(newAcnt.T1C_Base__Billing_Address_Id__c))
                {
                    existingAddress = addressMap.get(newAcnt.T1C_Base__Billing_Address_Id__c);
                    addressName = existingAddress.T1C_Base__Address_Name__c;
                }    
                
                newAddress.T1C_Base__Address_Name__c = addressName;
                newAddress.T1C_Base__Account__c = newAcnt.Id;
                
                newAddress.Id = newAcnt.T1C_Base__Billing_Address_Id__c != null ? newAcnt.T1C_Base__Billing_Address_Id__c : null;     

            
                addrBillLst.add(newAddress);
            }
            ////////////////////Shipping address
            T1C_Base__Address__c newshipAddress = new T1C_Base__Address__c();

            Boolean shippingChange = applyAccountAddressChange(newshipAddress, newAcnt, oldAcnt, shippingFieldsMap);
            
            if(shippingChange)
            {                
                String shipAddressName = newAcnt.T1C_Base__Shipping_Address_Id__r.T1C_Base__Address_Name__c != null ? 
                    newAcnt.T1C_Base__Shipping_Address_Id__r.T1C_Base__Address_Name__c : shipAddrName;

                T1C_Base__Address__c existingAddress;
                
                if(newAcnt.T1C_Base__Shipping_Address_Id__c != null && addressMap.containsKey(newAcnt.T1C_Base__Shipping_Address_Id__c))
                {
                    existingAddress = addressMap.get(newAcnt.T1C_Base__Shipping_Address_Id__c);
                    shipAddressName = existingAddress.T1C_Base__Address_Name__c;
                }
                
                newshipAddress.T1C_Base__Address_Name__c = shipAddressName;
                newshipAddress.T1C_Base__Account__c = newAcnt.Id;

                newshipAddress.Id = newAcnt.T1C_Base__Shipping_Address_Id__c != null ? newAcnt.T1C_Base__Shipping_Address_Id__c : null;
            
                addrShipLst.add(newshipAddress);
            }
            
         
        }//for(Account curAcnt : trigger.new)
      
        Schema.DescribeSObjectResult dsorAddress = T1C_Base__Address__c.sObjectType.getDescribe();
        if(!addrBillLst.isEmpty() && dsorAddress.isUpdateable())
        {
            upsert addrBillLst;
        }
        if(!addrShipLst.isEmpty() && dsorAddress.isUpdateable())
        {
            upsert addrShipLst;
        }
        //Make sure that the billing and shipping addresses are linked to the account. This is only relevant for before Updates
        //use the reference of the list to assign the T1C_Base__Billing_Address_Id__c and T1C_Base__Shipping_Address_Id__c fields in the trigger context
        for (T1C_Base__Address__c addr : addrBillLst)
        {
            Account curAccnt = inputAddresses.get(addr.T1C_Base__Account__c);
            curAccnt.T1C_Base__Billing_Address_Id__c = addr.Id;
        }
        for (T1C_Base__Address__c addr : addrShipLst)
        {
            Account curAccnt = inputAddresses.get(addr.T1C_Base__Account__c);
            curAccnt.T1C_Base__Shipping_Address_Id__c = addr.Id;
        }
    }//public static void SyncInsertedAccountAddress

    //Kind of misleading; The contact sync part is now handled via a batch job
    public static void SyncAddressToAccountAndContacts(map<Id, T1C_Base__Address__c> inputAddresses)
    {
        //get all of the required fields. This is assumed to be alright since we're loading in batches or individually
        Set<Id> addressIDs = inputAddresses.keySet();

        //Get the configured fields from the Billing and Shipping field maps as well
        Set<String> billingFields = billingFieldsMap.keySet();
        Set<String> shippingFields = shippingFieldsMap.keySet();

        Set<String> queryFields = new Set<String>();
        queryFields.addAll(billingFields);
        queryFields.addAll(shippingFields);
        queryFields.add('T1C_Base__Billing_Address_Id__c');
        queryFields.add('T1C_Base__Shipping_Address_Id__c');
        queryFields.add('Id');

        String query = 'SELECT ' + String.join(new List<String>(queryFields), ',') +
                        ' FROM Account ' +
                        'WHERE T1C_Base__Billing_Address_Id__c IN :addressIDs OR ' + 
                             ' T1C_Base__Shipping_Address_Id__c IN :addressIDs ';

        List<Account> acntList = Database.query(query);  
       
        List<Account> acntUpdateList = new List<Account>();
          
        system.debug('Fetched Accounts: ' + acntList);
        
        for(Account curAcnt : acntList)
        { 
            Boolean billingChange = false;
            Boolean shippingChange = false;                     
          
            T1C_Base__Address__c newBillAddress = inputAddresses.get(curAcnt.T1C_Base__Billing_Address_Id__c);

            if(curAcnt.T1C_Base__Billing_Address_Id__c != null && newBillAddress != null){
             
                billingChange = applyAddressChange(newBillAddress, curAcnt, billingFieldsMap);

                /*curAcnt.BillingCity = newBillAddress.T1C_Base__City__c;
                curAcnt.BillingCountry = newBillAddress.T1C_Base__Country__c;
                curAcnt.BillingPostalCode = newBillAddress.T1C_Base__Zip_Postal_Code__c;
                curAcnt.BillingState = newBillAddress.T1C_Base__State_Province__c;
                curAcnt.BillingStreet = newBillAddress.T1C_Base__Street__c;*/
             
            }

            T1C_Base__Address__c newAddress = inputAddresses.get(curAcnt.T1C_Base__Shipping_Address_Id__c);

            if(curAcnt.T1C_Base__Shipping_Address_Id__c != null && newAddress != null)
            {
                shippingChange = applyAddressChange(newAddress, curAcnt, shippingFieldsMap);
             
                /*curAcnt.ShippingCity = newAddress.T1C_Base__City__c;
                curAcnt.ShippingCountry = newAddress.T1C_Base__Country__c;
                curAcnt.ShippingPostalCode = newAddress.T1C_Base__Zip_Postal_Code__c;
                curAcnt.ShippingState = newAddress.T1C_Base__State_Province__c;
                curAcnt.ShippingStreet = newAddress.T1C_Base__Street__c;*/
            }
          
            if( billingChange || shippingChange)
            {
                acntUpdateList.add(curAcnt);
            }
        }//for(Account curAcnt : acntList)
       
        Schema.DescribeSObjectResult dsorAccount = Account.sObjectType.getDescribe();
        if(!acntUpdateList.isEmpty() && dsorAccount.isUpdateable() )
        {
            update acntUpdateList;
        }
    }//public static void SyncAddressToAccountAndContacts
   
    //Even if logically the address should always be applied, we should still be checking for changes
    //Since account triggers are expensive and we dont want to trip them to update nothing

    //Apply the address change to the passed in account
    public static Boolean applyAddressChange(SObject inAddr, SObject inAcnt, Map<String, String> fieldsMap)
    {
        //Check if our fields are in the SObject
        Map<String, Object> populatedFields = inAcnt.getPopulatedFieldsAsMap();
        Map<String, String> sanitizedFieldsMap = new Map<String, String>();

        for(String field : fieldsMap.keySet())
        {
            if(populatedFields.get(field) != null)
            {
                sanitizedFieldsMap.put(field, fieldsMap.get(field));
            }
        }

        Boolean isChanged = false;
        //The map has the account field as its key
        for(String acntFld : sanitizedFieldsMap.keySet())
        {
            String addrFld = sanitizedFieldsMap.get(acntFld);

            isChanged = isChanged || (inAddr.get(addrFld) != inAcnt.get(acntFld));
            
            inAcnt.put(acntFld, inAddr.get(addrFld));
        }
        
        return isChanged;
    }

    //Same thing but instead we compare the old account to the new account info
    private static Boolean applyAccountAddressChange(SObject inAddr, SObject inNewAcnt, SObject inOldAcnt, Map<String, String> fieldsMap)
    {
        
        //Check if our fields are in the SObject
        Map<String, Object> populatedFields = inNewAcnt.getPopulatedFieldsAsMap();
        Map<String, String> sanitizedFieldsMap = new Map<String, String>();

        for(String field : fieldsMap.keySet())
        {
            if(populatedFields.get(field) != null)
            {
                sanitizedFieldsMap.put(field, fieldsMap.get(field));
            }
        }     

        Boolean isChanged = false;
        //The map has the account field as its key
        for(String acntFld : sanitizedFieldsMap.keySet())
        {
            String addrFld = sanitizedFieldsMap.get(acntFld);

            isChanged = isChanged || (inOldAcnt.get(acntFld) != inNewAcnt.get(acntFld));
            
            inAddr.put(addrFld, inNewAcnt.get(acntFld));
        }
        
        return isChanged;
    }

    //Recursive function
    private static String getAllowedFieldValue(SObject inObj, String propertyName)
    {
        List<String> propsList = propertyName.split('\\.');
        
        //Remove the last item and use it as the get after the loop
        String finalProperty = propsList.size() > 0 ? propsList.remove( propsList.size() - 1 ) : propertyName;
        
        //For record type, in order to save on a query we'll use schema instead
        //This is done because the record type is not actually saved, but rather the recordtypeid
        if(propertyName.contains('RecordType'))
        {
            Map<Id, Schema.RecordTypeInfo> infos = Schema.SObjectType.Account.getRecordTypeInfosById();
            Id recordTypeId = (Id)inObj.get('RecordTypeId');
            String recordTypeName = infos.get( recordTypeId ).getName();
            return recordTypeName;
        }

        if(propsList.size() > 0)
        {
            for(String curProp : propsList)
            {
                curProp = curProp.replace('__r', '__c' );
                inObj = inObj.getSObject(curProp);
            }
        }
        
        try{
            return inObj.get(finalProperty) != null ? (String)inObj.get(finalProperty) : 'null';
        }catch(Exception e)
        {
            return '';
        }
        
    }

    public static List<T1C_Base__Address__c> getAddressesForAccountIds(Set<Id> accountIdsToSearch)
    {
        Set<String> queryFields =  new Set<String>{
            'Id', 
            'T1C_Base__Street__c',
            'T1C_Base__City__c',
            'T1C_Base__Country__c',
            'T1C_Base__State_Province__c',
            'T1C_Base__Zip_Postal_Code__c',
            'T1C_Base__Account__c'
        };

        queryFields.addAll( shippingFieldsMap.values() );
        queryFields.addAll( billingFieldsMap.values() );
        queryFields.addAll( contactFieldsMap.values() );

        String query = 'SELECT ' + String.join(new List<String>(queryFields), ',') +
                    ' FROM T1C_Base__Address__c ' +
                    ' WHERE T1C_Base__Account__c in :accountIdsToSearch ' +
                    ' AND Needs_Sync__c=false Limit 10000';

        return Database.query(query);
    }

}//public without sharing class AddressSyncTriggerHelper