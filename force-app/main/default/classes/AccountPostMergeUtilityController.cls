/** 
 * Name:  AccountPostMergeUtilityController
 * Description: Controller for the Account Post Merge
 *
 * Confidential & Proprietary, 2017 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1CRM.
 * 
 *	PS Base 3.7.6
 *
 */
global with sharing class AccountPostMergeUtilityController 
{
	public static final String AFR_POSTMERGE_LOOKUP = 'ACE.Extensions.PostMergeUtility';
	
	WebService static String getStaticResource(String resourceName)
    {
        String url;
        for (StaticResource item : [ SELECT Name, NamespacePrefix, SystemModStamp from StaticResource WHERE Name = :resourceName ])
        {
            if (item != null)
            {
                String namespace = item.NamespacePrefix;
                url = 'resource/' + item.SystemModStamp.getTime() + '/';
                
                if (namespace != null && namespace != '')
                {
                    url += namespace + '__';
                }
                url += item.Name;
            }
        }
        return url;
	}
	
	@AuraEnabled
	WebService static String enableDisableTriggers(String val)
	{
		T1C_FR.featureCacheWebSvc.createFeatures 
        (
		            new String[] {
		                'ACE',
		                'ACE.Core'
		            } ,
		            false
		);
        String featName = 'ACE.Core.DisabledTriggers';
		T1C_FR.featureCacheWebSvc.createFeature(featName, false);
		list<T1C_FR.Attribute> attribs = new list<T1C_FR.Attribute>
		{
			new T1C_FR.Attribute('acctBlockCoveredDelete', val),
			new T1C_FR.Attribute('acctBlockBookedDelete', val)
		};
		
		T1C_FR.featureCacheWebSvc.setUserAttributes(UserInfo.getUserId(), featName, attribs);
		
		if ( val == 'false')
		{
			return 'Triggers are Enabled';
		}
		return 'Triggers are Disabled';
	}
	
	WebService static Boolean ReparentKeyContact(String keyAccountIds, String dupkeyAccountIds)
	{
        Boolean isAEMExist = true;

        //check if T1C_AEM is exist in the org
        map<String, Schema.SObjectType> mapSObjects = Schema.getGlobalDescribe();
        Schema.SObjectType tSObj = mapSObjects.get('T1C_AEM__Invited_Contact__c');
        if(tSObj == null)
        {
            isAEMExist = false;
        }

        if (isAEMExist)
        {
    		list<String> keyAccounts = keyAccountIds.split(':');
    		list<String> dupkeyAccounts = dupkeyAccountIds.split(':');
    		String temp = string.join(dupkeyAccounts,',');
    		list<String> alldupkeyAccounts = temp.split(',');

            String name_of_object = 'T1C_AEM__Event_Contact__c';
    		
    		map<Id, set<Id>> keyAcctToContacts = new map<Id, set<Id>>{};
            String queryStr = 'select Id, T1C_AEM__Event_Account__c from T1C_AEM__Event_Contact__c where T1C_AEM__Event_Account__c IN: alldupkeyAccounts';
    		for (SObject keyCont : Database.Query(queryStr))
    		{
    			set<Id> kcIds = keyAcctToContacts.get((Id)keyCont.get('T1C_AEM__Event_Account__c'));
    			if (kcIds == null)
    			{
    				kcIds = new set<Id>{};
    				keyAcctToContacts.put((Id)keyCont.get('T1C_AEM__Event_Account__c'), kcIds);
    			}
    			kcIds.add(keyCont.Id);
    		}
    		
    		list<SObject> keyContactsToUpsert = new list<SObject>{};
    		for (Integer i = 0; i < keyAccounts.size(); i++)
    		{
    			String keyAcctId = keyAccounts[i];
    			if (keyAcctId == null)
    			{
    				continue;
    			}
    			
    			list<String> dupKeyIds = dupkeyAccounts[i].split(',');
    			
    			for (String dupka : dupKeyIds)
    			{
    				if (dupka == null || dupka == '')
    				{
    					continue;
    				}
    				set<Id> kcIds = keyAcctToContacts.get(dupka);
    				if (kcIds != null)
    				{
    					for(Id kc : kcIds)
    					{
                            sObject obj = Schema.getGlobalDescribe().get(name_of_object).newSObject();
                            obj.put('Id', 'kc');
                            obj.put('T1C_AEM__Event_Account__c', 'keyAcctId');
                            keyContactsToUpsert.add(obj);
    					}
    				}
    			}
    		}
    		
    		if (keyContactsToUpsert.size() > 0)
    		{
    			upsert keyContactsToUpsert;
    		}
        }
        return true;
	}
	
	//function called from the js page to get the account related lists detail
    WebService static String getAccountRelatedList(Id accountId)
    {    	
    	AccountRelatedList acctRelatedListInfo;
    	
    	if (accountId.getSObjectType().getDescribe().getName() == 'Account')
    	{ 
    		acctRelatedListInfo = new AccountRelatedList(accountId);
    	}
    	
    	return JSON.serialize(acctRelatedListInfo);
    } 
    
    //wrapper class to send all the related list info to front end
    global class AccountRelatedList
    {
    	Account acct;
    	Object rolepicklistConfig;
    	Object lookUPConfig;
    	Boolean tiggersEnabled;
    	list<AccountRelatedListDetail> keyAccountInfo = new list<AccountRelatedListDetail>();
    	list<AccountRelatedListDetail> aceListInfo = new list<AccountRelatedListDetail>();
    	list<AccountRelatedListDetail> interestInfo = new list<AccountRelatedListDetail>();
    	list<AccountRelatedListDetail> acctCoverageInfo = new list<AccountRelatedListDetail>();
    	list<AccountRelatedListDetail> interestSubjectInfo = new list<AccountRelatedListDetail>();
    	
        Boolean isAEMExist = true;

    	global AccountRelatedList(Id accountId)
    	{
            //check if T1C_AEM is exist in the org
            map<String, Schema.SObjectType> mapSObjects = Schema.getGlobalDescribe();
            Schema.SObjectType tSObj = mapSObjects.get('T1C_AEM__Invited_Contact__c');
            if(tSObj == null)
            {
                isAEMExist = false;
            }

    		acct = [Select Id, Name from Account where Id = :accountId limit 1];
            
            if(isAEMExist)
            {
                /****Query all Key Account records where Account Id is ‘Winning Account’ and Event Id is not unique***/
                map<String, AccountRelatedListDetail> eventIdToKeyAcct = new map<String, AccountRelatedListDetail>{};
                String queryStr = 'select Name, Id, T1C_AEM__Event__c, T1C_AEM__Event__r.Name, T1C_AEM__Event__r.T1C_AEM__Event_Date__c, T1C_AEM__Role__c from T1C_AEM__Event_Account__c where T1C_AEM__Account__c = :accountId';
    			for (SObject key : Database.Query(queryStr))
                {
                    AccountRelatedListDetail keyAcct = eventIdToKeyAcct.get((String)key.get('T1C_AEM__Event__c'));
                    if (keyAcct == null)
                    {
                        String name = 'Event - ' + (String)T1C_Base.AceUtil.getSObjFld (key, 'T1C_AEM__Event__r.Name') + ' on ' +  String.valueOf((Date)T1C_Base.AceUtil.getSObjFld (key, 'T1C_AEM__Event__r.T1C_AEM__Event_Date__c'));
                        keyAcct = new AccountRelatedListDetail((String)key.get('T1C_AEM__Event__c'), name);
                        eventIdToKeyAcct.put((String)key.get('T1C_AEM__Event__c'), keyAcct);
                    }
                    keyAcct.addToKeyAcctList(key);
                }
                //get Only Clones
                for (String kid : eventIdToKeyAcct.keyset())
                {
                    AccountRelatedListDetail kinfo = eventIdToKeyAcct.get(kid);
                    if (kinfo.keyAccounts.size() > 1)
                    {
                        keyAccountInfo.add(kinfo);
                    }
                }
            }
    		
    		
    		/****Query all List Entry records where Account Id is ‘Winning Account’ and List Id is not unique***/
    		map<String, AccountRelatedListDetail> aceListIdToEntries = new map<String, AccountRelatedListDetail>{};
    		list<T1C_Base__Ace_List_Entry__c> aceListEntryToUpd = new list<T1C_Base__Ace_List_Entry__c>{};
    		for (T1C_Base__Ace_List_Entry__c entry : [select Name, Id, T1C_Base__Entry_List_Id__c, T1C_Base__Ace_List__c, T1C_Base__Ace_List__r.Owner.Name, T1C_Base__Ace_List__r.Name, T1C_Base__Account__r.Name from T1C_Base__Ace_List_Entry__c where T1C_Base__Account__c = :accountId])
    		{
    			AccountRelatedListDetail aceListEntry = aceListIdToEntries.get(entry.T1C_Base__Ace_List__c);
    			if (aceListEntry == null)
    			{
    				aceListEntry = new AccountRelatedListDetail(entry.T1C_Base__Ace_List__c, entry.T1C_Base__Ace_List__r.Name);
    				aceListIdToEntries.put(entry.T1C_Base__Ace_List__c, aceListEntry);
    			}
    			aceListEntry.addToEntryList(entry);
    			
    			//to update T1C_Base__Entry_Id__c field with Winning Account’s Id
    			if (entry.T1C_Base__Entry_List_Id__c != accountId)
    			{
	    			T1C_Base__Ace_List_Entry__c entryL = new T1C_Base__Ace_List_Entry__c(Id = entry.Id, T1C_Base__Entry_List_Id__c = accountId);
	    			aceListEntryToUpd.add(entryL);
    			}
    		}
    		//get Only Clones
    		for (String lid : aceListIdToEntries.keyset())
    		{
    			AccountRelatedListDetail linfo = aceListIdToEntries.get(lid);
    			
    			if (linfo.aceListEntries.size() > 1)
    			{
    				aceListInfo.add(linfo);
    			}
    		}
			//update the records
			try
			{
				database.update(aceListEntryToUpd, true);
			}
			catch(Exception e) 
			{
			    String txt = e.getMessage();
			    system.debug(txt);
			}
			
			
    		/****Query all Interest records where Account Id is ‘Winning Account’ and Interest Subject Id is not unique***/
    		map<String, AccountRelatedListDetail> interestSubIdToInterest = new map<String, AccountRelatedListDetail>{};
    		for (T1C_Base__Interest__c inte : [select Name, Id, T1C_Base__Contact__c,T1C_Base__Account__c, T1C_Base__Account__r.name, T1C_Base__Reason__c, T1C_Base__Note__c, LastModifiedDate, T1C_Base__Interest_Subject__c, T1C_Base__Interest_Subject__r.Name, T1C_Base__Interest_Subject_Type__r.Name, T1C_Base__As_Of_Date__c, T1C_Base__Source__c, T1C_Base__Last_Discussed__c from T1C_Base__Interest__c where T1C_Base__Account__c = :accountId and T1C_Base__Expired__c = false and T1C_Base__Reason__c = 'Interest' order by T1C_Base__Last_Discussed__c desc])
    		{
                //Use Contact, Account and Interest Subject as a key in the map
                String key = (String)inte.T1C_Base__Contact__c + (String)inte.T1C_Base__Account__c + (String)inte.T1C_Base__Interest_Subject__c;
    			AccountRelatedListDetail interest = interestSubIdToInterest.get(key);
    			if (interest == null)
    			{	
    				String name = inte.T1C_Base__Interest_Subject__r.Name + ' / ' + inte.T1C_Base__Interest_Subject_Type__r.Name;
    				interest = new AccountRelatedListDetail(inte.T1C_Base__Interest_Subject__c, name);
    				interestSubIdToInterest.put(key, interest);
    			}
    			interest.addToInterestList(inte);
    		} 
    		//get Only Clones
    		for (String iid : interestSubIdToInterest.keyset())
    		{
    			AccountRelatedListDetail iinfo = interestSubIdToInterest.get(iid);
    			if (iinfo.interestWithReasonInterest.size() > 1)
    			{
    				interestInfo.add(iinfo);
    			}
    		}
    		
    		
    		/****Query all Account Coverage records where Account Id is ‘Winning Account’ and Unique Id is not unique***/
    		map<String, AccountRelatedListDetail> uniqueIdToAcctCoverage = new map<String, AccountRelatedListDetail>{};
    		String uniqueId = '';
    		for (T1C_Base__Account_Coverage__c ac : [select Name, Id, T1C_Base__Account__c, T1C_Base__Account__r.Name, T1C_Base__Employee__c, T1C_Base__Employee__r.name, T1C_Base__Role__c, T1C_Base__Start_Date__c, T1C_Base__End_Date__c, T1C_Base__Unique_Id__c, T1C_Base__Is_Backup__c, T1C_Base__Product_Lookup__c, T1C_Base__Product_Lookup__r.Name from T1C_Base__Account_Coverage__c where T1C_Base__Account__c = :accountId and T1C_Base__Inactive__c = 'false'])
    		{
    			uniqueId = ac.T1C_Base__Account__c + ':' + ac.T1C_Base__Employee__c;// + ':' + ac.T1C_Base__Role__c;
    			AccountRelatedListDetail acctCoverage = uniqueIdToAcctCoverage.get(uniqueId);
    			if (acctCoverage == null)
    			{
    				acctCoverage = new AccountRelatedListDetail(uniqueId, ac.T1C_Base__Employee__r.Name);
    				uniqueIdToAcctCoverage.put(uniqueId, acctCoverage);
    			}
    			acctCoverage.addToAcctCoverageList(ac);
    		} 
    		//get Only Clones
    		for (String iid : uniqueIdToAcctCoverage.keyset())
    		{
    			AccountRelatedListDetail iinfo = uniqueIdToAcctCoverage.get(iid);
    			if (iinfo.accountCoverages.size() > 1)
    			{
    				acctCoverageInfo.add(iinfo);
    			}
    		}
    		
    		
    		/****Query all Interest subject records where Account Id is ‘Winning Account’ and Interest Subject Id is not unique***/
    		map<String, AccountRelatedListDetail> interestSubTypeIdToInterest = new map<String, AccountRelatedListDetail>{};
    		for (T1C_Base__Interest_Subject__c inteSub : [select Name, Id, T1C_Base__Account__c, T1C_Base__Account__r.name, T1C_Base__Unique_Key__c, T1C_Base__Interest_Subject_Type__c, T1C_Base__Interest_Subject_Type__r.Name, T1C_Base__Parent_Interest_Subject__c, T1C_Base__Description__c, T1C_Base__Inactive__c from T1C_Base__Interest_Subject__c where T1C_Base__Account__c = :accountId order by name asc])
    		{
    			AccountRelatedListDetail interestSubject = interestSubTypeIdToInterest.get(inteSub.T1C_Base__Unique_Key__c);
    			if (interestSubject == null)
    			{	
    				String name = inteSub.Name + '/' + inteSub.T1C_Base__Interest_Subject_Type__r.Name;
    				interestSubject = new AccountRelatedListDetail(inteSub.T1C_Base__Unique_Key__c, name);
    				interestSubTypeIdToInterest.put(inteSub.T1C_Base__Unique_Key__c, interestSubject);
    			}
    			interestSubject.addToInterestSubjectList(inteSub);
    		} 
    		//get Only Clones
    		for (String iid : interestSubTypeIdToInterest.keyset())
    		{
    			AccountRelatedListDetail iinfo = interestSubTypeIdToInterest.get(iid);
    			if (iinfo.interestSubject.size() > 1)
    			{
    				interestSubjectInfo.add(iinfo);
    			}
    		}
    		
    		
    		//get cc role picklist values
			set<String> rolePicklistValues = new set<String>{};
			String roleDefault = '';
	        Schema.DescribeFieldResult ccFieldDescription = T1C_Base__Account_Coverage__c.T1C_Base__Role__c.getDescribe();
	        for (Schema.Picklistentry picklistEntry: ccFieldDescription.getPicklistValues())
	        {
	        	rolePicklistValues.add(pickListEntry.getLabel());
	            if (picklistEntry.defaultValue  )
	            {
	                roleDefault = pickListEntry.getValue();
	            } 
	        }
	        
	        SObjectUtil.DynamicUIPicklistConfig orc = new SObjectUtil.DynamicUIPicklistConfig();
            orc.dataProvider = rolePicklistValues;
            orc.defaultValue = roleDefault;
            rolepicklistConfig = new SObjectUtil.DynamicUIPicklistConfig();
            rolepicklistConfig = orc;
            
            SObjectUtil.DynamicUILookupConfig o = new SObjectUtil.DynamicUILookupConfig(AFR_POSTMERGE_LOOKUP + '.FieldConfig');
	        lookUPConfig = new SObjectUtil.DynamicUILookupConfig();
	        lookUPConfig = o;
	        
	        tiggersEnabled = !T1C_Base.ACETriggerCheck.isDisabled('acctBlockCoveredDelete');
    	}
    }
    
    //wrapper class to have account related list infomation
    global class AccountRelatedListDetail
    {
    	String id;
    	String name;
    	list<SObject> keyAccounts;
    	list<T1C_Base__Ace_List_Entry__c> aceListEntries;
    	list<T1C_Base__Interest__c> interestWithReasonInterest;
    	list<T1C_Base__Account_Coverage__c> accountCoverages;
    	list<T1C_Base__Interest_Subject__c> interestSubject;
    	
    	global AccountRelatedListDetail(String d, String n)
    	{
    		id = d;
    		name = n;
    		keyAccounts = new list<SObject>{};
    		aceListEntries = new list<T1C_Base__Ace_List_Entry__c>{};
    		interestWithReasonInterest = new list<T1C_Base__Interest__c>{};
    		accountCoverages = new list<T1C_Base__Account_Coverage__c>{};
    		interestSubject = new list<T1C_Base__Interest_Subject__c>{};
    	}
    	
    	global void addToKeyAcctList(SObject key)
    	{
    		keyAccounts.add(key);
    	}
    	
    	global void addToEntryList(T1C_Base__Ace_List_Entry__c entry)
    	{
    		aceListEntries.add(entry);
    	}
    	
    	global void addToInterestList(T1C_Base__Interest__c interest)
    	{
    		interestWithReasonInterest.add(interest);
    	}
    	
    	global void addToAcctCoverageList(T1C_Base__Account_Coverage__c acctCov)
    	{
    		accountCoverages.add(acctCov);
    	}
    	
    	global void addToInterestSubjectList(T1C_Base__Interest_Subject__c intSub)
    	{
    		interestSubject.add(intSub);
    	}
    }

}