/**
 * Name: AbstractComparableConfig
 * Description: Base config for ACE Products that will be orderable by the Order attribute in the AFR
 *
 * Confidential & Proprietary, 2015 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1CRM.
 * 
 *	PS Base 3.7.6
 *
 */
public abstract with sharing class AbstractComparableConfig implements Comparable
{
	public Integer order;

	public AbstractComparableConfig()
	{

	}

	public AbstractComparableConfig(T1C_FR.Feature feature)
	{
		parse(feature);
	}
 
//	==================================================================
//	Method: parse
//	Description: Parses the AFR to build the properties for the config
//	Params: feature - Feature to parse and build the config from
//	==================================================================
	public virtual void parse(T1C_FR.Feature feature)
	{
		this.order = AFRUtil.getIntegerAttributeValue(feature, 'Order', 999);
	}

	public virtual Integer compareTo(Object compareTo)
	{
		AbstractComparableConfig b = (AbstractComparableConfig)compareTo;

		if (order > b.order)
		{
			return 1;
		}
		else if (order < b.order)
		{
			return -1;
		}
		return 0;
	}
}