/* 
 * Name: PageUtil 
 * Description: PageUtil
 *
 * Confidential & Proprietary, 2017 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1CRM.
 * 
 *	PS Base 3.7.6
 *
 */
 
 public with sharing class PageUtil {
   //Custom page exception
   public class PageException extends Exception {}

	/**
	* add Message to apex page.
	*/
	public static void addMessage(Apexpages.Severity severity, String message){
		Apexpages.addMessage(new Apexpages.Message(severity, message));
	}
	
	/**
	* add Message to apex page, with optional prefix.
	* Prefix must be six characters long and begin with 'msg'. Ignored if null.
	*/
	public static void addMessage(String prefix, Apexpages.Severity severity, String message){
		if (prefix != null) {
			System.assert(prefix.length()==6, 'Invalid prefix. Must be six characters.');
			System.assert(prefix.startsWith('msg'), 'Invalid prefix. Must start with "msg".');
			addMessage(severity, prefix + '-' + message);
		} else {
			addMessage(severity, message);
		}			
	}
	
	/**
	* add Message to apex page, with optional prefix.
	* Prefix must be six characters long and begin with 'msg'. Ignored if null.
	*/
	public static void addExceptionMessage(String prefix, Apexpages.Severity severity, Exception e){
		if (e.getTypeName() == 'System.DMLException') {
			System.Dmlexception dmle = (System.Dmlexception) e;
			String message;
			for (Integer i = 0; i <dmle.getNumDml(); i++) {
				if (dmle.getDmlType(i) == System.Statuscode.FIELD_CUSTOM_VALIDATION_EXCEPTION || dmle.getDmlType(i) == System.Statuscode.INSUFFICIENT_ACCESS_OR_READONLY || dmle.getDmlType(i) == System.Statuscode.INSUFFICIENT_ACCESS_ON_CROSS_REFERENCE_ENTITY) {
					addMessage(prefix, severity, dmle.getDmlMessage(i));
				} else {
					addMessage(prefix, severity, 'DML ERROR: ' + e.getMessage());
				}
			}
		} else {
			addProgrammingError(prefix, e);
		}
		
	}
	// Default severity: error
	public static void addExceptionMessage(String prefix, Exception e){
		addExceptionMessage(prefix, Apexpages.Severity.ERROR, e);
	}
	// Default severity: error, default prefix: null
	public static void addExceptionMessage(Exception e){
		addExceptionMessage(null, Apexpages.Severity.ERROR, e);
	}
	
	/**
	* Handles unknown exceptions with the default message.
	*/
	public static void addProgrammingError(String prefix, Exception e) {
	 	addProgrammingError(prefix, e, 'An unexpected error has ocurred.  Please contact administrator for assistance.'); // For production/testing
		
	}

	/**
	* Handles unknown exceptions (programming errors). In development, rethrows the exception. In prod, it shows the message.
	* Always logs the exception.
	*/
	public static void addProgrammingError(String prefix, Exception e, String message) {
		System.debug(System.Logginglevel.ERROR, e);
	 	//throw e; // For development. DON'T SWALLOW UNKNOWN ERRORS. EVER! 
	 	addMessage(prefix, Apexpages.Severity.ERROR, message); // For production/testing
	}
// Request messages for debug and the sort
    private static String requestMessages = 'Request Messages:';
    public static void addRequestMessage(String message) {
        requestMessages += '(' + (System.now()) + ')|' + message;
    }
    
    public static String getRequestMessages() {
        return requestMessages;
    }
}