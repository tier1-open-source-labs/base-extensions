/** 
 * Name:  PostMergeUtilityController
 * Description: Controller for the Post Merge
 *
 * Confidential & Proprietary, 2016 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1CRM.
 * 
 *	PS Base 3.7.6
 *
 */
global with sharing class PostMergeUtilityController 
{
	public static final String AFR_POSTMERGE_LOOKUP = 'ACE.Extensions.PostMergeUtility';
	
	WebService static String getStaticResource(String resourceName)
    {
        String url;
        for (StaticResource item : [ SELECT Name, NamespacePrefix, SystemModStamp from StaticResource WHERE Name = :resourceName ])
        {
            if (item != null)
            {
                String namespace = item.NamespacePrefix;
                url = 'resource/' + item.SystemModStamp.getTime() + '/';
                
                if (namespace != null && namespace != '')
                {
                    url += namespace + '__';
                }
                url += item.Name;
            }
        }
        return url;
	}
	
	@AuraEnabled
	WebService static String enableDisableTriggers(String val)
	{
		T1C_FR.featureCacheWebSvc.createFeatures 
        (
		            new String[] {
		                'ACE',
		                'ACE.Core'
		            } ,
		            false
		);
        String featName = 'ACE.Core.DisabledTriggers';
		T1C_FR.featureCacheWebSvc.createFeature(featName, false);
		list<T1C_FR.Attribute> attribs = new list<T1C_FR.Attribute>
		{
			new T1C_FR.Attribute('contBlockCoveredDelete', val),
			new T1C_FR.Attribute('contBlockBookedDelete', val)
		};
		
		T1C_FR.featureCacheWebSvc.setUserAttributes(UserInfo.getUserId(), featName, attribs);
				
		if( val == 'false')
		{
			return 'Triggers are Enabled';
		}
		return 'Triggers are Disabled';
	}
	
	//function called from the js page to get the contact related lists detail
    WebService static String getContactRelatedList(Id contactId)
    {    	
    	ContactRelatedList contRelatedListInfo;
    	
    	if (contactId.getSObjectType().getDescribe().getName() == 'Contact')
    	{ 
    		contRelatedListInfo = new ContactRelatedList(contactId);
    	}
    	
    	return JSON.serialize(contRelatedListInfo);
    } 
    
    //wrapper class to send all the related list info to front end
    global class ContactRelatedList
    {
    	Contact cont;
    	Object rolepicklistConfig;
    	Object lookUPConfig;
    	Boolean tiggersEnabled;
    	list<ContactRelatedListDetail> meetingInfo = new list<ContactRelatedListDetail>();
    	list<ContactRelatedListDetail> keyContactInfo = new list<ContactRelatedListDetail>();
    	list<ContactRelatedListDetail> collectionKeyContInfo = new list<ContactRelatedListDetail>();
    	list<ContactRelatedListDetail> aceListInfo = new list<ContactRelatedListDetail>();
    	list<ContactRelatedListDetail> interestInfo = new list<ContactRelatedListDetail>();
    	list<ContactRelatedListDetail> interestAPMInfo = new list<ContactRelatedListDetail>();
    	list<ContactRelatedListDetail> contCoverageInfo = new list<ContactRelatedListDetail>();
    	list<ContactRelatedListDetail> callReportInfo = new list<ContactRelatedListDetail>();
    	list<ContactRelatedListDetail> dealInfo = new list<ContactRelatedListDetail>();

        Boolean isAEMExist = true;
    	
    	global ContactRelatedList(Id contactId)
    	{
            //check if T1C_AEM is exist in the org
            map<String, Schema.SObjectType> mapSObjects = Schema.getGlobalDescribe();
            Schema.SObjectType tSObj = mapSObjects.get('T1C_AEM__Invited_Contact__c');
            if(tSObj == null)
            {
                isAEMExist = false;
            }

    		cont = [Select Id, Name from Contact where Id = :contactId limit 1];
			
            if(isAEMExist)
            {
        		/****Query all Meeting Attendee records where Contact Id is ‘Winning Contact’ and Meeting Id is not unique***/
        		map<String, ContactRelatedListDetail> meetingIdToAttendees = new map<String, ContactRelatedListDetail>{};
                String queryStr = 'select Name, Id, T1C_AEM__Invitation__r.T1C_AEM__Meeting__r.T1C_AEM__Aem_Event__r.Name, T1C_AEM__Invitation__r.T1C_AEM__Meeting__r.T1C_AEM__Aem_Event__r.T1C_AEM__Event_Date__c, T1C_AEM__Role__c, T1C_AEM__Status__c, T1C_AEM__Invitation__r.T1C_AEM__Meeting__c, T1C_AEM__Invitation__r.T1C_AEM__Meeting__r.Name, T1C_AEM__Booking_Owner__r.Name from T1C_AEM__Invited_Contact__c where T1C_AEM__Contact__c = :contactId';
                for (SObject attendee : Database.Query(queryStr))
                {
                    String invitaionMeeting = (String)T1C_Base.AceUtil.getSObjFld ( attendee , 'T1C_AEM__Invitation__r.T1C_AEM__Meeting__c');
                    ContactRelatedListDetail meetingAttendee = meetingIdToAttendees.get(invitaionMeeting);
                    if (meetingAttendee == null)
                    {
                    	String eventName = (String)T1C_Base.AceUtil.getSObjFld ( attendee , 'T1C_AEM__Invitation__r.T1C_AEM__Meeting__r.T1C_AEM__Aem_Event__r.Name');
                    	String eventDate = String.valueof((Date)T1C_Base.AceUtil.getSObjFld ( attendee , 'T1C_AEM__Invitation__r.T1C_AEM__Meeting__r.T1C_AEM__Aem_Event__r.T1C_AEM__Event_Date__c'));
                        String name = 'Event - ' + eventName + ' on ' + eventDate + '<br><br>Meeting - ' + (String)T1C_Base.AceUtil.getSObjFld ( attendee , 'T1C_AEM__Invitation__r.T1C_AEM__Meeting__r.Name' );
                        meetingAttendee = new ContactRelatedListDetail(invitaionMeeting, name);
                        meetingIdToAttendees.put(invitaionMeeting, meetingAttendee);
                    }
                    meetingAttendee.addToAttendeeList(attendee);
                } 
                //get Only Clones
                for (String mid : meetingIdToAttendees.keyset())
                {
                    ContactRelatedListDetail minfo = meetingIdToAttendees.get(mid);
                    if (minfo.meetingAttendees.size() > 1)
                    {
                        meetingInfo.add(minfo);
                    }
                }

			
    			/****Query all Key Contact records where Contact Id is ‘Winning Contact’ and Event Id is not unique***/
        		map<String, ContactRelatedListDetail> eventIdToKeyCont = new map<String, ContactRelatedListDetail>{};
                queryStr = 'select Name, Id, T1C_AEM__Event__c, T1C_AEM__Event__r.Name, T1C_AEM__Event__r.T1C_AEM__Event_Date__c, T1C_AEM__Role__c, T1C_AEM__Title__c from T1C_AEM__Event_Contact__c where T1C_AEM__Contact__c = :contactId';
                for (SObject key : Database.Query(queryStr))
                {
                    ContactRelatedListDetail keyCont = eventIdToKeyCont.get((String)key.get('T1C_AEM__Event__c'));
                    if (keyCont == null)
                    {
                        String name = 'Event - ' + (String)T1C_Base.AceUtil.getSObjFld (key, 'T1C_AEM__Event__r.Name') + ' on ' +  String.valueOf((Date)T1C_Base.AceUtil.getSObjFld (key, 'T1C_AEM__Event__r.T1C_AEM__Event_Date__c'));
                        keyCont = new ContactRelatedListDetail((String)key.get('T1C_AEM__Event__c'), name);
                        eventIdToKeyCont.put((String)key.get('T1C_AEM__Event__c'), keyCont);
                    }
                    keyCont.addToKeyContList(key);
                }
                //get Only Clones
                for (String kid : eventIdToKeyCont.keyset())
                {
                    ContactRelatedListDetail kinfo = eventIdToKeyCont.get(kid);
                    if (kinfo.keyContacts.size() > 1)
                    {
                        keyContactInfo.add(kinfo);
                    }
                }
        		
        		
        		/****Query all Collection Key Contact records where Contact Id is ‘Winning Contact’ and Collection Id is not unique***/
        		map<String, ContactRelatedListDetail> collectionIdToKeyCont = new map<String, ContactRelatedListDetail>{};
                queryStr = 'select Name, Id, T1C_AEM__Event_Collection__c, T1C_AEM__Event_Collection__r.Name, T1C_AEM__Event_Collection__r.T1C_AEM__Start_Time__c, T1C_AEM__Title__c, T1C_AEM__Role__c from T1C_AEM__Event_Collection_Contact__c where T1C_AEM__Contact__c = :contactId';
                for (SObject ckey : Database.Query(queryStr))
                {
                    ContactRelatedListDetail colKeyCont = collectionIdToKeyCont.get((String)ckey.get('T1C_AEM__Event_Collection__c'));
                    if (colKeyCont == null)
                    {
                        String name = 'Event - ' + (String)T1C_Base.AceUtil.getSObjFld (ckey, 'T1C_AEM__Event_Collection__r.Name') + ' on ' +  String.valueOf((Datetime)T1C_Base.AceUtil.getSObjFld (ckey, 'T1C_AEM__Event_Collection__r.T1C_AEM__Start_Time__c'));
                        colKeyCont = new ContactRelatedListDetail((String)ckey.get('T1C_AEM__Event_Collection__c'), name);
                        collectionIdToKeyCont.put((String)ckey.get('T1C_AEM__Event_Collection__c'), colKeyCont);
                    }
                    colKeyCont.addToCollectionKeyContList(ckey);
                } 
                //get Only Clones
                for (String ckid : collectionIdToKeyCont.keyset())
                {
                    ContactRelatedListDetail ckinfo = collectionIdToKeyCont.get(ckid);
                    if (ckinfo.collectionKeyContacts.size() > 1)
                    {
                        collectionKeyContInfo.add(ckinfo);
                    }
                }
    		}

    		
    		/****Query all List Entry records where Contact Id is ‘Winning Contact’ and List Id is not unique***/
    		map<String, ContactRelatedListDetail> aceListIdToEntries = new map<String, ContactRelatedListDetail>{};
    		list<T1C_Base__Ace_List_Entry__c> aceListEntryToUpd = new list<T1C_Base__Ace_List_Entry__c>{};
    		for (T1C_Base__Ace_List_Entry__c entry : [select Name, Id, T1C_Base__Entry_List_Id__c, T1C_Base__Ace_List__c, T1C_Base__Ace_List__r.Owner.Name, T1C_Base__Ace_List__r.Name, T1C_Base__Contact__r.Name from T1C_Base__Ace_List_Entry__c where T1C_Base__Contact__c = :contactId])
    		{
    			ContactRelatedListDetail aceListEntry = aceListIdToEntries.get(entry.T1C_Base__Ace_List__c);
    			if (aceListEntry == null)
    			{
    				aceListEntry = new ContactRelatedListDetail(entry.T1C_Base__Ace_List__c, entry.T1C_Base__Ace_List__r.Name);
    				aceListIdToEntries.put(entry.T1C_Base__Ace_List__c, aceListEntry);
    			}
    			aceListEntry.addToEntryList(entry);
    			
    			//to update T1C_Base__Entry_Id__c field with Winning Contact’s Id
    			if (entry.T1C_Base__Entry_List_Id__c != contactId)
    			{
	    			T1C_Base__Ace_List_Entry__c entryL = new T1C_Base__Ace_List_Entry__c(Id = entry.Id, T1C_Base__Entry_List_Id__c = contactId);
	    			aceListEntryToUpd.add(entryL);
    			}
    		}
    		//get Only Clones
    		for (String lid : aceListIdToEntries.keyset())
    		{
    			ContactRelatedListDetail linfo = aceListIdToEntries.get(lid);
    			
    			if (linfo.aceListEntries.size() > 1)
    			{
    				aceListInfo.add(linfo);
    			}
    		}
			//update the records
			try
			{
				database.update(aceListEntryToUpd, true);
			}
			catch(Exception e) 
			{
			    String txt = e.getMessage();
			    system.debug(txt);
			}

    		
    		
    		/****Query all User Contact Interactions records where Contact Id is ‘Winning Contact’ and User Id is not unique***/
    		map<String, T1C_Base__User_Contact_Interaction__c> userIdToUserContInt = new map<String, T1C_Base__User_Contact_Interaction__c>{};
    		list<T1C_Base__User_Contact_Interaction__c> userContIntToDel = new list<T1C_Base__User_Contact_Interaction__c>{};
    		list<T1C_Base__User_Contact_Interaction__c> userContIntToUpdate = new list<T1C_Base__User_Contact_Interaction__c>{};
    		for (T1C_Base__User_Contact_Interaction__c user : [select Name, Id, T1C_Base__Last_Interaction_Date__c, T1C_Base__User__c, T1C_Base__User__r.Name from T1C_Base__User_Contact_Interaction__c where T1C_Base__Contact__c = :contactId order by T1C_Base__Last_Interaction_Date__c desc nulls last])
    		{
    			T1C_Base__User_Contact_Interaction__c userContInt = userIdToUserContInt.get(user.T1C_Base__User__c);
    			if (userContInt == null)
    			{
    				//if no duplicate is found for the user...
    				userIdToUserContInt.put(user.T1C_Base__User__c, user);
    			}
    			else
    			{
    				//if a duplicate is found, leave the latest and delete the rest
    				if (userContInt.T1C_Base__Last_Interaction_Date__c >= user.T1C_Base__Last_Interaction_Date__c)
    				{
    					userContIntToDel.add(user);
    				}
    				else
    				{
    					userContIntToDel.add(userContInt);
    					userIdToUserContInt.put(user.T1C_Base__User__c, user);
    				}
    			}
    		} 

			if((T1C_Base__User_Contact_Interaction__c.sObjectType.getDescribe().isDeletable()) && (userContIntToDel.size() > 0) )
			{
    			delete userContIntToDel;
    		}
			
    		//get the unique user... after the duplications are deleted
			for (String uId : userIdToUserContInt.keySet())
			{
				userContIntToUpdate.add(userIdToUserContInt.get(uId));
			}
			//do a blind update to correct the unique id
			if (userContIntToUpdate.size() > 0)
    		{
    			update userContIntToUpdate;
    		}
    		
			
    		/****Query all Interest records where Contact Id is ‘Winning Contact’ and Interest Subject Id is not unique***/
    		map<String, ContactRelatedListDetail> interestSubIdToInterest = new map<String, ContactRelatedListDetail>{};
    		Boolean isResearch;
    		for (T1C_Base__Interest__c inte : [select Name, Id, T1C_Base__Contact__c, T1C_Base__Contact__r.name, T1C_Base__Reason__c, T1C_Base__Note__c, LastModifiedDate, T1C_Base__Interest_Subject__c, T1C_Base__Interest_Subject__r.Name, T1C_Base__Interest_Subject_Type__r.Name, T1C_Base__As_Of_Date__c, T1C_Base__Source__c, T1C_Base__Last_Discussed__c from T1C_Base__Interest__c where T1C_Base__Contact__c = :contactId and T1C_Base__Expired__c = false order by T1C_Base__Reason__c, T1C_Base__Last_Discussed__c desc])
    		{
    			ContactRelatedListDetail interest = interestSubIdToInterest.get(inte.T1C_Base__Interest_Subject__c);
    			if (interest == null)
    			{	String name = inte.T1C_Base__Interest_Subject__r.Name + ' / ' + inte.T1C_Base__Interest_Subject_Type__r.Name;
    				interest = new ContactRelatedListDetail(inte.T1C_Base__Interest_Subject__c, name);
    				interestSubIdToInterest.put(inte.T1C_Base__Interest_Subject__c, interest);
    			}
    			isResearch = inte.T1C_Base__Reason__c == 'Research' ? true : false;
    			interest.addToInterestList(inte, isResearch);
    		} 
    		//get Only Clones
    		for (String iid : interestSubIdToInterest.keyset())
    		{
    			ContactRelatedListDetail iinfo = interestSubIdToInterest.get(iid);
    			if (iinfo.interestWithReasonInterest.size() > 1)
    			{
    				interestInfo.add(iinfo);
    			}
    			if (iinfo.interestWithReasonResearch.size() > 1)
    			{
    				interestAPMInfo.add(iinfo);
    			}
    		}
    		
    		
    		/****Query all Contact Coverage records where Contact Id is ‘Winning Contact’ and Unique Id is not unique***/
    		map<String, ContactRelatedListDetail> uniqueIdToContCoverage = new map<String, ContactRelatedListDetail>{};
    		String uniqueId = '';
    		for (T1C_Base__Contact_Coverage__c cc : [select Name, Id, T1C_Base__Contact__c, T1C_Base__Contact__r.Name, T1C_Base__Employee__c, T1C_Base__Employee__r.name, T1C_Base__Role__c, T1C_Base__Start_Date__c, T1C_Base__End_Date__c, T1C_Base__Unique_Id__c, T1C_Base__Is_Backup__c from T1C_Base__Contact_Coverage__c where T1C_Base__Contact__c = :contactId and T1C_Base__Inactive__c = 'false'])
    		{
    			uniqueId = cc.T1C_Base__Contact__c + ':' + cc.T1C_Base__Employee__c;// + ':' + cc.T1C_Base__Role__c;
    			ContactRelatedListDetail contCoverage = uniqueIdToContCoverage.get(uniqueId);
    			if (contCoverage == null)
    			{
    				contCoverage = new ContactRelatedListDetail(uniqueId, cc.T1C_Base__Employee__r.Name);
    				uniqueIdToContCoverage.put(uniqueId, contCoverage);
    			}
    			contCoverage.addToContCoverageList(cc);
    		} 
    		//get Only Clones
    		for (String iid : uniqueIdToContCoverage.keyset())
    		{
    			ContactRelatedListDetail iinfo = uniqueIdToContCoverage.get(iid);
    			if (iinfo.contactCoverages.size() > 1)
    			{
    				contCoverageInfo.add(iinfo);
    			}
    		}
    		
    		
    		/****Query all Call Report Attendee records where Contact Id is ‘Winning Contact’ and call report Id is not unique***/
    		map<String, ContactRelatedListDetail> callReportIdToAttendees = new map<String, ContactRelatedListDetail>{};
    		for (T1C_Base__Call_Report_Attendee_Client__c attendee : [select Name, Id, T1C_Base__Contact__c, T1C_Base__Contact__r.Name, T1C_Base__Call_Report__c, T1C_Base__Call_Report__r.Name, T1C_Base__Call_Report__r.T1C_Base__Subject_Meeting_Objectives__c from T1C_Base__Call_Report_Attendee_Client__c where T1C_Base__Contact__c = :contactId])
    		{
    			ContactRelatedListDetail callReportAttendee = callReportIdToAttendees.get(attendee.T1C_Base__Call_Report__c);
    			if (callReportAttendee == null)
    			{
    				callReportAttendee = new ContactRelatedListDetail(attendee.T1C_Base__Call_Report__c, attendee.T1C_Base__Call_Report__r.Name);
    				callReportIdToAttendees.put(attendee.T1C_Base__Call_Report__c, callReportAttendee);
    			}
    			callReportAttendee.addToCRAttendeeList(attendee);
    		} 
    		//get Only Clones
    		for (String crid : callReportIdToAttendees.keyset())
    		{
    			ContactRelatedListDetail crinfo = callReportIdToAttendees.get(crid);
    			if (crinfo.crAttendees.size() > 1)
    			{
    				callReportInfo.add(crinfo);
    			}
    		}
    		
    		/****Query all Deal Contact records where Contact Id is ‘Winning Contact’ and deal Id is not unique***/
    		map<String, ContactRelatedListDetail> dealIdToContacts = new map<String, ContactRelatedListDetail>{};
    		uniqueId = '';
    		for (T1C_Base__Deal_Contact__c cont : [select Name, Id, T1C_Base__Contact__c, T1C_Base__Contact__r.Name, T1C_Base__Role__c, T1C_Base__Deal__c, T1C_Base__Deal__r.Name from T1C_Base__Deal_Contact__c where T1C_Base__Contact__c = :contactId])
    		{
    			uniqueId = cont.T1C_Base__Deal__c;// + ':' + cont.T1C_Base__Role__c;
    			ContactRelatedListDetail dealContact = dealIdToContacts.get(uniqueId);
    			if (dealContact == null)
    			{
    				dealContact = new ContactRelatedListDetail(uniqueId, cont.T1C_Base__Deal__r.Name);
    				dealIdToContacts.put(uniqueId, dealContact);
    			}
    			dealContact.addToDealContList(cont);
    		} 
    		//get Only Clones
    		for (String dealid : dealIdToContacts.keyset())
    		{
    			ContactRelatedListDetail dinfo = dealIdToContacts.get(dealid);
    			if (dinfo.dealContacts.size() > 1)
    			{
    				dealInfo.add(dinfo);
    			}
    		}
    		
    		//get cc role picklist values
			set<String> rolePicklistValues = new set<String>{};
			String roleDefault = '';
	        Schema.DescribeFieldResult ccFieldDescription = T1C_Base__Contact_Coverage__c.T1C_Base__Role__c.getDescribe();
	        for (Schema.Picklistentry picklistEntry: ccFieldDescription.getPicklistValues())
	        {
	        	rolePicklistValues.add(pickListEntry.getLabel());
	            if (picklistEntry.defaultValue  )
	            {
	                roleDefault = pickListEntry.getValue();
	            } 
	        }
	        
	        SObjectUtil.DynamicUIPicklistConfig orc = new SObjectUtil.DynamicUIPicklistConfig();
            orc.dataProvider = rolePicklistValues;
            orc.defaultValue = roleDefault;
            rolepicklistConfig = new SObjectUtil.DynamicUIPicklistConfig();
            rolepicklistConfig = orc;
            
            SObjectUtil.DynamicUILookupConfig o = new SObjectUtil.DynamicUILookupConfig(AFR_POSTMERGE_LOOKUP + '.FieldConfig');
	        lookUPConfig = new SObjectUtil.DynamicUILookupConfig();
	        lookUPConfig = o;
	        
	        tiggersEnabled = !T1C_Base.ACETriggerCheck.isDisabled('contBlockCoveredDelete');
    	}
    }
    
    //wrapper class to have contact related list infomation
    global class ContactRelatedListDetail
    {
    	String id;
    	String name;
    	list<SObject> meetingAttendees;
    	list<SObject> keyContacts;
    	list<SObject> collectionKeyContacts;
    	list<T1C_Base__Ace_List_Entry__c> aceListEntries;
    	list<T1C_Base__Interest__c> interestWithReasonInterest;
    	list<T1C_Base__Interest__c> interestWithReasonResearch;
    	list<T1C_Base__Contact_Coverage__c> contactCoverages;
    	list<T1C_Base__Call_Report_Attendee_Client__c> crAttendees;
    	list<T1C_Base__Deal_Contact__c> dealContacts;
    	
    	global ContactRelatedListDetail(String d, String n)
    	{
    		id = d;
    		name = n;
    		meetingAttendees = new list<SObject>{};
    		keyContacts = new list<SObject>{};
    		collectionKeyContacts = new list<SObject>{};
    		aceListEntries = new list<T1C_Base__Ace_List_Entry__c>{};
    		interestWithReasonInterest = new list<T1C_Base__Interest__c>{};
    		interestWithReasonResearch = new list<T1C_Base__Interest__c>{};
    		contactCoverages = new list<T1C_Base__Contact_Coverage__c>{};
    		crAttendees = new list<T1C_Base__Call_Report_Attendee_Client__c>{};
    		dealContacts = new list<T1C_Base__Deal_Contact__c>{};
    	}
    	
    	global void addToAttendeeList(SObject attendee)
    	{
    		meetingAttendees.add(attendee);
    	}
    	
    	global void addToKeyContList(SObject key)
    	{
    		keyContacts.add(key);
    	}
    	
    	global void addToCollectionKeyContList(SObject ckey)
    	{
    		collectionKeyContacts.add(ckey);
    	}
    	
    	global void addToEntryList(T1C_Base__Ace_List_Entry__c entry)
    	{
    		aceListEntries.add(entry);
    	}
    	
    	global void addToInterestList(T1C_Base__Interest__c interest, Boolean isResearch)
    	{
    		if (isResearch)
    		{
    			interestWithReasonResearch.add(interest);
    		}
    		else
    		{
    			interestWithReasonInterest.add(interest);
    		}
    	}
    	global void addToContCoverageList(T1C_Base__Contact_Coverage__c contCov)
    	{
    		contactCoverages.add(contCov);
    	}
    	global void addToCRAttendeeList(T1C_Base__Call_Report_Attendee_Client__c attendee)
    	{
    		crAttendees.add(attendee);
    	}
    	global void addToDealContList(T1C_Base__Deal_Contact__c cont)
    	{
    		dealContacts.add(cont);
    	}
    }

}