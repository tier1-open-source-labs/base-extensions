/**
* Name: TestAddressSyncTriggerHelper 
* Description: Test Class for the Address Sync Trigger Helper Class
*
* Confidential & Proprietary, 2019 Tier1CRM Inc.
* Property of Tier1CRM Inc.
* This document shall not be duplicated, transmitted or used in whole
* or in part without written permission from Tier1CRM.
* 
*	PS Base 3.7.6
*
*/
@isTest
private class TestAddressSyncTriggerHelper
{
	private static final Tier1CRMTestDataFactory dataFactory = Tier1CRMTestDataFactorySetup.getDataFactory();

	//Method to setup Data needed for each test scenario
	static void setupData(String allowedField, String allowedFieldTypes)
	{
		//Create AFRs
		T1C_FR__Feature__c aceFeature     = new T1C_FR__Feature__c(Name = 'ACE',            T1C_FR__Name__c = 'ACE');
		T1C_FR__Feature__c extensions     = new T1C_FR__Feature__c(Name = 'Extensions',     T1C_FR__Name__c = 'ACE.Extensions');
		T1C_FR__Feature__c addressManager = new T1C_FR__Feature__c(Name = 'AddressManager', T1C_FR__Name__c = 'ACE.Extensions.AddressManager');
        
        T1C_FR__Feature__c coreFeature = new T1C_FR__Feature__c(Name = 'Core', T1C_FR__Name__c = 'ACE.Core');
        T1C_FR__Feature__c disabledTriggers = new T1C_FR__Feature__c(Name = 'DisabledTriggers', T1C_FR__Name__c = 'ACE.Core.DisabledTriggers');

        //ACE.Core.DisabledTriggers
        //AccountAddressChange
        //ContactAddressChange
        
        
		upsert new list<T1C_FR__Feature__c>{aceFeature, extensions, addressManager, coreFeature, disabledTriggers};

		// =========================================================
		// SyncAllowedAccountTypesField and SyncAllowedAccountTypes AFRs specify the Accountfiled and the values that syncying are allowed for
		// client scenarios: Only sync the address if SyncAllowedAccountTypesField= 'Type' and SyncAllowedAccountTypes='Client' or 'Research'
		// In this test, We'll use the Type field to restrict the syncing since this field will always exist.This is just to test if the AFR works.
		// =========================================================
        T1C_FR__Attribute__c allowedFieldAttribute      = new T1C_FR__Attribute__c(T1C_FR__Feature__c=addressManager.Id, Name='SyncAllowedAccountTypesField', T1C_FR__Value__c = allowedField);
		T1C_FR__Attribute__c allowedFieldTypesAttribute = new T1C_FR__Attribute__c(T1C_FR__Feature__c=addressManager.Id, Name='SyncAllowedAccountTypes'     , T1C_FR__Value__c = allowedFieldTypes);
        
        T1C_FR__Attribute__c accountAddressChange       = new T1C_FR__Attribute__c(T1C_FR__Feature__c=disabledTriggers.Id, Name='AccountAddressChange'        , T1C_FR__Value__c = 'TRUE');
		T1C_FR__Attribute__c contactAddressChange       = new T1C_FR__Attribute__c(T1C_FR__Feature__c=disabledTriggers.Id, Name='ContactAddressChange'        , T1C_FR__Value__c = 'TRUE');

		insert new List<T1C_FR__Attribute__c>{allowedFieldAttribute, allowedFieldTypesAttribute};
  	}

	//Test SyncInsertedAccountAddress method with a billing address to see if address object is created
	static testMethod void testSyncInsertedAccountAddressBilling()
   	{
		setupData('Type','Other');

		Test.startTest();

		Account testAccnt = dataFactory.makeAccount('testAccount');
        testAccnt.Type = 'Other';
		testAccnt.BillingStreet = 'testStreet';
		testAccnt.BillingCity = 'testCity';
		testAccnt.BillingCountry = 'testCountry';
		insert testAccnt;

		Map<Id,Account> accntMap = new Map<Id,Account>();			
		accntMap.put(testAccnt.Id, testAccnt);		
		AddressSyncTriggerHelper.SyncInsertedAccountAddress(accntMap);        
		
		Id acntId = testAccnt.Id;
		//Get the account from database again
		Account curAcnt = [SELECT Id,T1C_Base__Billing_Address_Id__r.T1C_Base__Street__c FROM Account WHERE Id = :acntId][0];
		
		Boolean confirm = testAccnt.BillingStreet == curAcnt.T1C_Base__Billing_Address_Id__r.T1C_Base__Street__c;

		Test.stopTest();

		//System.assert(confirm);
	}

	//Test SyncInsertedAccountAddress method with a shipping address to see if address object is created
	static testMethod void testSyncInsertedAccountAddressShipping()
   	{
		setupData('Type','Other');
		
		Test.startTest();
		
		Account testAccnt = dataFactory.makeAccount('testAccount');
        testAccnt.Type = 'Other';
		testAccnt.ShippingStreet = 'ShipStreet';
		testAccnt.ShippingCity = 'ShipCity';
		testAccnt.ShippingCountry = 'ShipCountry';
		insert testAccnt;
					
		Map<Id,Account> accntMap = new Map<Id,Account>();		
		accntMap.put(testAccnt.Id, testAccnt);		
		AddressSyncTriggerHelper.SyncInsertedAccountAddress(accntMap);              
		
		Id acntId = testAccnt.Id;
		//Get the account from database again
		Account curAcnt = [SELECT Id,T1C_Base__Shipping_Address_Id__r.T1C_Base__Street__c FROM Account WHERE Id = :acntId][0];
		
		Boolean confirm = testAccnt.ShippingStreet == curAcnt.T1C_Base__Shipping_Address_Id__r.T1C_Base__Street__c;

		Test.stopTest();

		//System.assert(confirm);
	}

	//Test SyncInsertedAccountAddress method with a shipping and billing address to see if both address objects are created
	static testMethod void testSyncInsertedAccountAddressBoth()
   	{
		setupData('Type','Other');

		Test.startTest();

		Account testAccnt = dataFactory.makeAccount('testAccount');
        testAccnt.Type = 'Other';
		testAccnt.ShippingStreet = 'ShipStreet';
		testAccnt.ShippingCity = 'ShipCity';
		testAccnt.ShippingCountry = 'ShipCountry';
		testAccnt.BillingStreet = 'testStreet';
		testAccnt.BillingCity = 'testCity';
		testAccnt.BillingCountry = 'testCountry';
		insert testAccnt;
					
		Map<Id,Account> accntMap = new Map<Id,Account>();		
		accntMap.put(testAccnt.Id, testAccnt);		
		AddressSyncTriggerHelper.SyncInsertedAccountAddress(accntMap);              
		
		Id acntId = testAccnt.Id;
		//Get the account from database again
		Account curAcnt = [SELECT 	Id,
									T1C_Base__Shipping_Address_Id__r.T1C_Base__Street__c,
									T1C_Base__Billing_Address_Id__r.T1C_Base__Street__c 
							FROM 	Account 
							WHERE 	Id = :acntId][0];
		
		Boolean confirmBilling = testAccnt.BillingStreet == curAcnt.T1C_Base__Billing_Address_Id__r.T1C_Base__Street__c;
		Boolean confirmShipping = testAccnt.ShippingStreet == curAcnt.T1C_Base__Shipping_Address_Id__r.T1C_Base__Street__c;

		Test.stopTest();

		//System.assert(confirmBilling);
		//System.assert(confirmShipping);
	}

	//Test SyncInsertedAccountAddress method with account types which are not allowed to see if syncing fails
	static testMethod void testSyncInsertedAccountAddressNotAllowedAccountType()
	{      
		setupData('Type','Other');

		Test.startTest();

		Account testAccnt = dataFactory.makeAccount('testAccount');
        testAccnt.Type = 'Not Other';
		testAccnt.ShippingStreet = 'ShipStreet';
		testAccnt.ShippingCity = 'ShipCity';
		testAccnt.ShippingCountry = 'ShipCountry';
		testAccnt.BillingStreet = 'testStreet';
		testAccnt.BillingCity = 'testCity';
		testAccnt.BillingCountry = 'testNotAllowedCountry';
		insert testAccnt; 
			
		Map<Id,Account> accntMap = new Map<Id,Account>();
		accntMap.put(testAccnt.Id, testAccnt);
		AddressSyncTriggerHelper.SyncInsertedAccountAddress(accntMap);

		Account confirmAccount = [SELECT Id, Name, BillingCountry FROM Account LIMIT 1][0];

		Boolean confirm = testAccnt.BillingCountry == confirmAccount.BillingCountry;

		Test.stopTest();

		//System.Assert(confirm);      
	}

	//Test SyncUpdatedAccountAddress method with a billing address to see if address object is created
	static testMethod void testSyncUpdatedAccountAddressBilling()
	{      
		setupData('Type','Other');

		Test.startTest();

		Account testAccnt = dataFactory.makeAccount('testAccount');
		testAccnt.BillingStreet = 'testStreet';
		testAccnt.BillingCity = 'testCity';
		testAccnt.BillingCountry = 'testCountry';
        testAccnt.Type = 'Other';
		insert testAccnt; 		
	
        Map<Id,Account> accntMap = new Map<Id,Account>();			
		accntMap.put(testAccnt.Id, testAccnt);		
		AddressSyncTriggerHelper.SyncInsertedAccountAddress(accntMap);
        
		Id acntId = testAccnt.Id;
		Account curAcnt = [SELECT Id,
                                Type,
								BillingCity,
								BillingCountry,
								BillingPostalCode,
								BillingState,
								BillingStreet,
								T1C_Base__Billing_Address_Id__c,
								T1C_Base__Billing_Address_Id__r.T1C_Base__Street__c,
                                T1C_Base__Billing_Address_Id__r.T1C_Base__Address_Name__c
							FROM Account
							WHERE Id = :acntId][0];

		curAcnt.BillingStreet = 'West Virginia';
		accntMap.put(curAcnt.Id, curAcnt);      
		AddressSyncTriggerHelper.SyncUpdatedAccountAddress(accntMap);        
				
		T1C_Base__Address__c confirmAddress = [SELECT Id, T1C_Base__Street__c FROM T1C_Base__Address__c WHERE T1C_Base__Street__c = 'West Virginia'][0];
		
		Boolean confirm = curAcnt.BillingStreet == confirmAddress.T1C_Base__Street__c;
		
		Test.stopTest();

		//System.Assert(confirm);      
	}

	//Test SyncUpdatedAccountAddress method with a billing address that is same as the one which was inserted to see if address object doesnt change
	static testMethod void testSyncSameUpdatedAccountAddressBilling()
	{      
		setupData('Type','Other');

		Test.startTest();
		
		Account testAccnt = dataFactory.makeAccount('testAccount');
		testAccnt.BillingStreet = 'testStreet';
		testAccnt.BillingCity = 'testCity';
		testAccnt.BillingCountry = 'testCountry';
		insert testAccnt; 	

		Map<Id,Account> accntMap = new Map<Id,Account>();

		Id acntId = testAccnt.Id;
		Account curAcnt = [SELECT Id,
								BillingCity,
								BillingCountry,
								BillingPostalCode,
								BillingState,
								BillingStreet,
								T1C_Base__Billing_Address_Id__c,
								T1C_Base__Billing_Address_Id__r.T1C_Base__Street__c,
                                T1C_Base__Billing_Address_Id__r.T1C_Base__Address_Name__c
							FROM Account
							WHERE Id = :acntId][0];

		curAcnt.BillingStreet = 'testStreet';
		accntMap.put(curAcnt.Id, curAcnt);      
		AddressSyncTriggerHelper.SyncUpdatedAccountAddress(accntMap);        
				
		List<T1C_Base__Address__c> confirmAddress = [SELECT Id, T1C_Base__Street__c FROM T1C_Base__Address__c WHERE T1C_Base__Street__c = 'testStreet'];
		
		Boolean confirm = confirmAddress.size() == 0;
		
		Test.stopTest();

		//System.Assert(confirm);           
	}

	//Test SyncUpdatedAccountAddress method with a shipping address to see if address object is created
	static testMethod void testSyncUpdatedAccountAddressShipping()
	{      
		setupData('Type','Other');

		Test.startTest();
		
		Account testAccnt = dataFactory.makeAccount('testAccount');
        testAccnt.Type = 'Other';
		testAccnt.ShippingStreet = 'ShipStreet';
		testAccnt.ShippingCity = 'ShipCity';
		testAccnt.ShippingCountry = 'ShipCountry';
		insert testAccnt; 	

		Map<Id,Account> accntMap = new Map<Id,Account>();			
		accntMap.put(testAccnt.Id, testAccnt);		
		AddressSyncTriggerHelper.SyncInsertedAccountAddress(accntMap);

		Id acntId = testAccnt.Id;
		//Get the account from database again
		Account curAcnt = [SELECT Id,
                                Type,
								ShippingCity,
								ShippingCountry,
								ShippingPostalCode,
								ShippingState,
								ShippingStreet,
								T1C_Base__Shipping_Address_Id__c,
								T1C_Base__Shipping_Address_Id__r.T1C_Base__Street__c,
                                T1C_Base__Shipping_Address_Id__r.T1C_Base__Address_Name__c
							FROM Account
							WHERE Id = :acntId][0];

		curAcnt.ShippingStreet='Country Roads';
		accntMap.put(curAcnt.Id, curAcnt);      
		AddressSyncTriggerHelper.SyncUpdatedAccountAddress(accntMap);        

		T1C_Base__Address__c confirmAddress = [SELECT Id, T1C_Base__Street__c FROM T1C_Base__Address__c WHERE T1C_Base__Street__c = 'Country Roads'][0];
		
		Boolean confirm = curAcnt.ShippingStreet == confirmAddress.T1C_Base__Street__c;
		
		Test.stopTest();

		//System.Assert(confirm);      
	}

	//Test SyncUpdatedAccountAddress method with a billing address that is same as the one which was inserted to see if address object doesnt change
	static testMethod void testSyncSameUpdatedAccountAddressShipping()
	{      
		setupData('Type','Other');

		Test.startTest();
		
		Account testAccnt = dataFactory.makeAccount('testAccount');
        testAccnt.Type = 'Other';
		testAccnt.ShippingStreet = 'ShipStreet';
		testAccnt.ShippingCity = 'ShipCity';
		testAccnt.ShippingCountry = 'ShipCountry';
		insert testAccnt; 	

		Map<Id,Account> accntMap = new Map<Id,Account>();

		Id acntId = testAccnt.Id;
		//Get the account from database again
		Account curAcnt = [SELECT Id,
								Type,
								ShippingCity,
								ShippingCountry,
								ShippingPostalCode,
								ShippingState,
								ShippingStreet,
								T1C_Base__Shipping_Address_Id__c,
								T1C_Base__Shipping_Address_Id__r.T1C_Base__Street__c
							FROM Account
							WHERE Id = :acntId][0];

		curAcnt.ShippingStreet='ShipStreet';
		accntMap.put(curAcnt.Id, curAcnt);      
		AddressSyncTriggerHelper.SyncUpdatedAccountAddress(accntMap);        

		List<T1C_Base__Address__c> confirmAddress = [SELECT Id, T1C_Base__Street__c FROM T1C_Base__Address__c WHERE T1C_Base__Street__c = 'ShipStreet'];
		
		Boolean confirm = confirmAddress.size() == 0;
		
		Test.stopTest();

		//System.Assert(confirm);      
	}

	//Test SyncUpdatedAccountAddress method with a shipping and billing address to see if address objects are created
	static testMethod void testSyncUpdatedAccountAddressBoth()
	{      
		setupData('Type','Other');

		Test.startTest();
		
		Account testAccnt = dataFactory.makeAccount('testAccount');
        testAccnt.Type = 'Other';
		testAccnt.ShippingStreet = 'ShipStreet';
		testAccnt.ShippingCity = 'ShipCity';
		testAccnt.ShippingCountry = 'ShipCountry';
		testAccnt.BillingStreet = 'testStreet';
		testAccnt.BillingCity = 'testCity';
		testAccnt.BillingCountry = 'testCountry';
		insert testAccnt; 	

		Map<Id,Account> accntMap = new Map<Id,Account>();			
		accntMap.put(testAccnt.Id, testAccnt);		
		AddressSyncTriggerHelper.SyncInsertedAccountAddress(accntMap);

		Id acntId = testAccnt.Id;
		//Get the account from database again
		Account curAcnt = [SELECT Id,
                                Type,
								ShippingCity,
								ShippingCountry,
								ShippingPostalCode,
								ShippingState,
								ShippingStreet,
								T1C_Base__Shipping_Address_Id__c,
								T1C_Base__Shipping_Address_Id__r.T1C_Base__Street__c,
                                T1C_Base__Shipping_Address_Id__r.T1C_Base__Address_Name__c,
								BillingCity,
								BillingCountry,
								BillingPostalCode,
								BillingState,
								BillingStreet,
								T1C_Base__Billing_Address_Id__c,
								T1C_Base__Billing_Address_Id__r.T1C_Base__Street__c,
                                T1C_Base__Billing_Address_Id__r.T1C_Base__Address_Name__c
							FROM Account
							WHERE Id = :acntId][0];

		curAcnt.ShippingStreet='Country Roads';
		curAcnt.BillingStreet='West Roads';
		accntMap.put(curAcnt.Id, curAcnt);      
		AddressSyncTriggerHelper.SyncUpdatedAccountAddress(accntMap);        


		T1C_Base__Address__c shippingAddress = [SELECT Id, T1C_Base__Street__c FROM T1C_Base__Address__c WHERE T1C_Base__Street__c = 'Country Roads'][0];
		T1C_Base__Address__c billingAddress = [SELECT Id, T1C_Base__Street__c FROM T1C_Base__Address__c WHERE T1C_Base__Street__c = 'West Roads'][0];
		
		Boolean confirmShipping = curAcnt.ShippingStreet == shippingAddress.T1C_Base__Street__c;
		Boolean confirmBilling = curAcnt.BillingStreet == billingAddress.T1C_Base__Street__c;
		
		Test.stopTest();

		//System.Assert(confirmShipping);
		//System.Assert(confirmBilling);
	}

	//Test SyncUpdatedAccountAddress method with account types which are not allowed to see if syncing fails
	static testMethod void testSyncUpdatedAccountAddressNotAllowedAccountType()
   	{      
		setupData('Type','Other');

		Test.startTest();
		
		Account testAccnt = dataFactory.makeAccount('testAccount');
        testAccnt.Type = 'Not Allowed';
		testAccnt.BillingStreet = 'testStreet';
		testAccnt.BillingCity = 'testCity';
		testAccnt.BillingCountry = 'testCountry';
		insert testAccnt; 	
		
		Map<Id,Account> accntMap = new Map<Id,Account>();

		Id acntId = testAccnt.Id;
		//Get the account from database again
		Account curAcnt = [SELECT Id, 
									Type,
									BillingCity,
									BillingCountry,
									BillingPostalCode,
									BillingState,
									BillingStreet,
									T1C_Base__Billing_Address_Id__c,
									T1C_Base__Billing_Address_Id__r.T1C_Base__Country__c
								FROM Account
							WHERE Id = :acntId][0];
		
		curAcnt.BillingCountry= 'testCountry2';

		accntMap.put(curAcnt.Id, curAcnt);      
		AddressSyncTriggerHelper.SyncUpdatedAccountAddress(accntMap);        
		
		List<T1C_Base__Address__c> confirmAddress = [SELECT Id, T1C_Base__Country__c FROM T1C_Base__Address__c WHERE T1C_Base__Country__c = 'testCountry2'];

		Boolean confirm = confirmAddress.size() == 0;
		
		Test.stopTest();
		
		//System.Assert(confirm);      
   	}

	//Test SyncAddressToAccountAndContacts method with shipping address to see if address object is modified
	static testMethod void testSyncAddressToAccountAndContactsShipping()
	{      
		setupData('Type','Other');

		Test.startTest();
		
		Account testAccnt = dataFactory.makeAccount('testAccount');
        testAccnt.Type = 'Other';
		testAccnt.ShippingStreet = 'ShipStreet';
		testAccnt.ShippingCity = 'ShipCity';
		testAccnt.ShippingCountry = 'ShipCountry';
		insert testAccnt; 	
		
		//Create test Addresses
		T1C_Base__Address__c testAddress = dataFactory.makeAddress('Test Address1', testAccnt.Id);
		testAddress.T1C_Base__Street__c = 'Test Street Test';
		testAddress.T1C_Base__City__c = 'Test City';
		testAddress.T1C_Base__Country__c = 'Test Country';
		testAddress.T1C_Base__Zip_Postal_Code__c = 'test12345';
		insert testAddress;

		testAccnt.T1C_Base__Shipping_Address_Id__c = testAddress.Id;
		update testAccnt;		

		testAddress.T1C_Base__Street__c  = 'Third Test1';

		Map<Id, T1C_Base__Address__c> addrMap = new Map<Id, T1C_Base__Address__c>();
		addrMap.put(testAddress.Id, testAddress);

		AddressSyncTriggerHelper.SyncAddressToAccountAndContacts(addrMap);

		Account confirmAccount = [SELECT Id, Name, ShippingStreet FROM Account LIMIT 1][0];

		Boolean confirmA = confirmAccount.ShippingStreet == testAddress.T1C_Base__Street__c;

		Test.stopTest();

		//System.Assert(confirmA); 
   	}

	//Test SyncAddressToAccountAndContacts method with billing address to see if address object is modified
   	static testMethod void testSyncAddressToAccountAndContactsBilling()
	{      
		setupData('Type','Other');

		Test.startTest();
		
		Account testAccnt = dataFactory.makeAccount('testAccount');
        testAccnt.Type = 'Other';
		testAccnt.BillingStreet = 'testStreet';
		testAccnt.BillingCity = 'testCity';
		testAccnt.BillingCountry = 'testCountry';
		insert testAccnt; 	

		//Create test Addresses
		T1C_Base__Address__c testAddress = dataFactory.makeAddress('Test Address1', testAccnt.Id);
		testAddress.T1C_Base__Street__c = 'Test Street Test';
		testAddress.T1C_Base__City__c = 'Test City';
		testAddress.T1C_Base__Country__c = 'Test Country';
		testAddress.T1C_Base__Zip_Postal_Code__c = 'test12345';
		insert testAddress;

		testAccnt.T1C_Base__Billing_Address_Id__c = testAddress.Id;
		update testAccnt;
		
		testAddress.T1C_Base__Street__c  = 'Third Test2';

		Map<Id, T1C_Base__Address__c> addrMap = new Map<Id, T1C_Base__Address__c>();
		addrMap.put(testAddress.Id, testAddress);

		AddressSyncTriggerHelper.SyncAddressToAccountAndContacts(addrMap);

		Account confirmAccount = [SELECT Id, Name, BillingStreet FROM Account LIMIT 1][0];

		Boolean confirmA = testAddress.T1C_Base__Street__c == confirmAccount.BillingStreet;

		Test.stopTest();

		//System.Assert(confirmA);   
   	}

	//Test SyncAddressToAccountAndContacts method with shipping and billing address to see if both address objects are modified
    static testMethod void testSyncAddressToAccountAndContactsBoth()
	{      
		setupData('Type','Other');

		Test.startTest();
		
		List<Account> accntList = new List<Account>();

		Account testAccnt = dataFactory.makeAccount('testAccount');
        testAccnt.Type = 'Other';
		testAccnt.ShippingStreet = 'ShipStreet';
		testAccnt.ShippingCity = 'ShipCity';
		testAccnt.ShippingCountry = 'ShipCountry';
		testAccnt.BillingStreet = 'testStreet';
		testAccnt.BillingCity = 'testCity';
		testAccnt.BillingCountry = 'testCountry';

		Account testAccnt2 = dataFactory.makeAccount('testAccount2');
        testAccnt2.Type = 'Other';
		testAccnt2.ShippingStreet = 'ShipStreet';
		testAccnt2.ShippingCity = 'ShipCity';
		testAccnt2.ShippingCountry = 'ShipCountry';
		testAccnt2.BillingStreet = 'testStreet';
		testAccnt2.BillingCity = 'testCity';
		testAccnt2.BillingCountry = 'testCountry';
		
		accntList.add(testAccnt);
		accntList.add(testAccnt2);

		insert accntList;

		//Create test Addresses
		T1C_Base__Address__c testAddress = dataFactory.makeAddress('Test Address1', testAccnt.Id);
		testAddress.T1C_Base__Street__c = 'Test Street Test';
		testAddress.T1C_Base__City__c = 'Test City';
		testAddress.T1C_Base__Country__c = 'Test Country';
		testAddress.T1C_Base__Zip_Postal_Code__c = 'test12345';
		insert testAddress;
		
		T1C_Base__Address__c testAddress2 = dataFactory.makeAddress('Test Address2', testAccnt2.Id);
		testAddress2.T1C_Base__Street__c = 'Test Street Shipping';
		testAddress2.T1C_Base__City__c = 'Test City2';
		testAddress2.T1C_Base__Country__c = 'Test Country2';
		testAddress2.T1C_Base__Zip_Postal_Code__c = 'test12';
		insert testAddress2;

		testAccnt.T1C_Base__Billing_Address_Id__c = testAddress.Id;
		update testAccnt;

		testAccnt2.T1C_Base__Shipping_Address_Id__c = testAddress2.Id;
		update testAccnt2;
			
		testAddress.T1C_Base__Street__c  = 'Third Test1';		
		testAddress2.T1C_Base__Street__c  = 'Third Test2';

		Map<Id, T1C_Base__Address__c> addrMap = new Map<Id, T1C_Base__Address__c>();
		addrMap.put(testAddress.Id, testAddress);
		addrMap.put(testAddress2.Id, testAddress2);

		AddressSyncTriggerHelper.SyncAddressToAccountAndContacts(addrMap);

		Account confirmAccount = [SELECT Id, Name, BillingStreet FROM Account WHERE Id = :testAccnt.Id][0];
		Account confirmAccount2 = [SELECT Id, Name, ShippingStreet FROM Account WHERE Id = :testAccnt2.Id][0];

		Boolean confirmA = testAddress.T1C_Base__Street__c == confirmAccount.BillingStreet;
		Boolean confirmA2 = testAddress2.T1C_Base__Street__c == confirmAccount2.ShippingStreet;

		System.debug(confirmAccount);
		System.debug(confirmAccount2);
		Test.stopTest();

		//System.Assert(confirmA);
		//System.Assert(confirmA2);     
	}

	//Test getAllowedFieldValue method with billing address
	//Since getAllowedFieldValue is a private method, hit all the cases for getAllowedFieldValue with this test method
	static testMethod void testGetAllowedFieldValue()
   	{
		setupData('Owner.Name','testCountry');

		Test.startTest();		
		
		Account testAccnt = dataFactory.makeAccount('testAccount');
		testAccnt.BillingStreet = 'testStreet';
		testAccnt.BillingCity = 'testCity';
		testAccnt.BillingCountry = 'testCountry';
		insert testAccnt;
      
		Map<Id,Account> accntMap = new Map<Id,Account>();
		
		accntMap.put(testAccnt.Id, testAccnt);
		
		AddressSyncTriggerHelper.SyncInsertedAccountAddress(accntMap);

	 	Id acntId = testAccnt.Id;
		//Get the account from database again
		Account curAcnt = [SELECT Id, 
                                  BillingCity,
                                  BillingCountry,
                                  BillingPostalCode,
                                  BillingState,
                                  BillingStreet,
                                  T1C_Base__Billing_Address_Id__c,
                                  T1C_Base__Billing_Address_Id__r.T1C_Base__Country__c
                             FROM Account
                            WHERE Id = :acntId][0];
                
      	Boolean confirm = curAcnt.T1C_Base__Billing_Address_Id__r.T1C_Base__Country__c == testAccnt.BillingCountry;
      
		Test.stopTest();
		
		//System.Assert(confirm);
	}
  
	//Final redundant method to test all cases at once 
	static testMethod void testAllCases()
	{      
		setupData('Type','Other');

		Account testAccnt = dataFactory.makeAccount('testAccount');
        testAccnt.Type = 'Other';
		testAccnt.ShippingStreet = 'ShipStreet';
		testAccnt.ShippingCity = 'ShipCity';
		testAccnt.ShippingCountry = 'ShipCountry';
		testAccnt.BillingStreet = 'testStreet';
		testAccnt.BillingCity = 'testCity';
		testAccnt.BillingCountry = 'testCountry';
		insert testAccnt;

		// =========================================================
		// This is to test if no change in address, we don't sync
		// =========================================================
		testAccnt.Phone = '111-111-1111';
		update testAccnt;

		// =========================================================
		// This is to test if specified SyncAllowedAccountTypesField(in this test BillingCountry) not equal to specified SyncAllowedAccountTypes (in this test testCountry), the account won't be synced
		// This account won't be synced since its BillingCountry =/= testCountry (see AFRs above)
		// =========================================================
		Account testAccnt2 = dataFactory.makeAccount('testAccount2');
		insert testAccnt2;

		testAccnt2.BillingCity = 'Test New Billing';
		update testAccnt2;

		Map<Id,Account> accntMap = new Map<Id,Account>();

		accntMap.put(testAccnt.Id, testAccnt);

		AddressSyncTriggerHelper.SyncInsertedAccountAddress(accntMap);

		Contact testCont = dataFactory.makeContact(testAccnt.Id,'test1','test1');
		insert testCont;


		//Create test Addresses
		T1C_Base__Address__c testAddress2 = dataFactory.makeAddress('Test Address2',testAccnt.Id);
		testAddress2.T1C_Base__Street__c = 'testStreet';
		testAddress2.T1C_Base__City__c = 'testCity';
		testAddress2.T1C_Base__Country__c = 'testCountry';
		testAddress2.T1C_Base__Zip_Postal_Code__c = 'testPostal12345';

		//Create test Addresses
		T1C_Base__Address__c testAddress = dataFactory.makeAddress('Test Address1',testAccnt.Id);
		testAddress.T1C_Base__Street__c = 'Test Street Test';
		testAddress.T1C_Base__City__c = 'Test City';
		testAddress.T1C_Base__Country__c = 'Test Country';
		testAddress.T1C_Base__Zip_Postal_Code__c = 'test12345';
		insert testAddress;

		Object addr2 = JSON.deserialize(
            AddressManagerController.saveAddress( JSON.serialize(testAddress2),'false','false' )
        , T1C_Base__Address__c.class);

        String addrId2 = String.valueOf( ((T1C_Base__Address__c)addr2).Id );

		testAccnt.T1C_Base__Billing_Address_Id__c = addrId2;
		testAccnt.T1C_Base__Shipping_Address_Id__c = testAddress.Id;
		update testAccnt;

		testCont.T1C_Base__Mailing_Address_Id__c = Id.valueOf(addrId2);
		update testCont;

		testAddress2.Id = addrId2;

		Id acntId = testAccnt.Id;
		//Get the account from database again
		Account curAcnt = [SELECT Id, 
										Type,
										BillingCity,
										BillingCountry,
										BillingPostalCode,
										BillingState,
										BillingStreet,
										ShippingCity,
										ShippingCountry,
										ShippingPostalCode,
										ShippingState,
										ShippingStreet,
										T1C_Base__Billing_Address_Id__c,
										T1C_Base__Billing_Address_Id__r.T1C_Base__Address_Name__c,
										T1C_Base__Shipping_Address_Id__c,
										T1C_Base__Shipping_Address_Id__r.T1C_Base__Address_Name__c
								FROM Account
								WHERE Id = :acntId][0];

		curAcnt.BillingStreet='Country Roads';
		accntMap.put(curAcnt.Id, curAcnt);      
		AddressSyncTriggerHelper.SyncUpdatedAccountAddress(accntMap);

		//Changing just Shipping Address
		curAcnt.ShippingStreet='Testers Street';

		accntMap.put(curAcnt.Id, curAcnt);      
		AddressSyncTriggerHelper.SyncUpdatedAccountAddress(accntMap);

		//Update Address object
		testAddress2.T1C_Base__Street__c = 'Third Test2';
		testAddress.T1C_Base__Street__c  = 'Third Test1';

		Map<Id, T1C_Base__Address__c> addrMap = new Map<Id, T1C_Base__Address__c>();
		addrMap.put(addrId2, testAddress2);
		addrMap.put(testAddress.Id, testAddress);

		AddressSyncTriggerHelper.SyncAddressToAccountAndContacts(addrMap);

		Account confirmAccount = [SELECT Id, Name, BillingStreet FROM Account LIMIT 1][0];

		Boolean confirm = testAddress2.T1C_Base__Street__c == confirmAccount.BillingStreet;

		//System.Assert(confirm);      
	} 
}