/** 
 * Name: AFRUtilTests 
 * Description: Tests for AFRUtil
 *
 * Confidential & Proprietary, 2019 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1CRM.
 * 
 *	PS Base 3.7.6
 *
 */
@isTest 
public with sharing class AFRUtilTests 
{
    public static final Tier1CRMTestDataFactory dataFactory = Tier1CRMTestDataFactorySetup.getDataFactory();

    static testMethod void getAttributesTest() 
    {
    	dataFactory.createCBSAFR();

        T1C_FR.Attribute[] attribs = AFRUtil.getAttributes(UserInfo.getUserId(), 'ACE.Core.AceShareUtil.Objects.Account.Account');

        String attribValue = AFRUtil.getAttributeValue(UserInfo.getUserId(), 'ACE.Core.AceShareUtil.Objects.Account.Account', 'AccessLevel', 'Read');
        AFRUtil.getAttributeValue(UserInfo.getUserId(), 'ACE.Core.AceShareUtil.Objects.Account.Account', 'AccessLevel', 'Read');

        for(T1C_FR.Attribute attr : attribs)
        {
            attr.bUserOverridable = true;
        }
        
        map<String, T1C_FR.Attribute[]> attribMap = new map<String, T1C_FR.Attribute[]>();
        attribMap.put('ACE.Core.AceShareUtil.Objects.Account.Account', attribs);

        AFRUtil.setAttributes(attribMap);
    }

    static testMethod void getAttributesTest2() 
    {
    	dataFactory.createCBSAFR();

        String attribValue = AFRUtil.getAttributeValue(UserInfo.getUserId(), 'ACE.Core.AceShareUtil.Objects.Account.Account', 'AccessLevel');
    }

    static testMethod void getAttributesTest3() 
    {
    	dataFactory.createCBSAFR();

        String attribValue = AFRUtil.getAttribute(UserInfo.getUserId(), 'ACE.Core.AceShareUtil.Objects.Account.Account', 'AccessLevel');
        attribValue = AFRUtil.getAttribute(UserInfo.getUserId(), 'ACE.Core.AceShareUtil.Objects.Account.Account', 'AccessLevel');
    }

    static testMethod void getFeaturesTest() 
    {
    	dataFactory.createCBSAFR();
        
        T1C_FR.Feature feat = T1C_FR.FeatureCacheWebSvc.getAttributeTreeRecursive(UserInfo.getUserId(), 'ACE.Core.AceShareUtil.Objects.Account');

        T1C_FR.Feature subFeat = AFRUtil.getSubFeature(feat, 'Account');

        String attribString = AFRUtil.getAttributeValue(subFeat,'AccessLevel','Read');
        attribString = AFRUtil.getAttributeValue(subFeat,'NullAttribute','Read');
        attribString = AFRUtil.getAttribute(UserInfo.getUserId(), 'ACE.Core.AceShareUtil.Objects.Account.Account', 'AccessLevel');
        Integer intAttribValue = AFRUtil.getIntegerAttributeValue(subFeat, 'Iteration', 1);
        intAttribValue = AFRUtil.getIntegerAttributeValue(subFeat, 'AccessLevel', 1);

        Boolean isBatch = AFRUtil.getBooleanAttributeValue(subFeat, 'BatchOnly', true);
        isBatch = AFRUtil.getBooleanAttributeValue(subFeat, 'IsBatch', true);
    }
}