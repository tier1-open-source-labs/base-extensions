/** 
 * Name: DMLUtil 
 * Description: Utility to check if a trigger has run already (clone of SecurityUtil)
 *
 * Confidential & Proprietary, 2019 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1CRM.
 * 
 *  	PS Base 3.7.6

 */
public without sharing class DMLUtil 
{
    public static set<String> hasRun = new set<String>{};
}