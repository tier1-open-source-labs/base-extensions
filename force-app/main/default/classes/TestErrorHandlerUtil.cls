/**
 * Name: TestErrorHandlerUtil
 * Description: Contains the test methods to test ErrorHandlerUtil
 *
 * Confidential & Proprietary, 2016 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1CRM.
 * 
 *	PS Base 3.7.6
 *
 */

@isTest
public with sharing class TestErrorHandlerUtil
{
    static testMethod void TestErrorHandlerUtil()
    {
    	Account a = new Account();
    	try
    	{
    		insert a;
    	}
    	catch (DmlException e)
    	{
    		string sMessage = ErrorHandlerUtil.getErrorMessage(e);
    	}
    }
}