/* 
 * Name: TestPostMergeUtility 
 * Description: Test
 *                  - AccountPostMergeUtilityController
 *                  - PostMergeUtilityController
 *
 * Confidential & Proprietary, 2017 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1CRM.
 * 
 *	PS Base 3.7.6
 *
 */
@isTest (seeAllData = false)
private class TestPostMergeUtility 
{
    private static final Tier1CRMTestDataFactory dataFactory = Tier1CRMTestDataFactorySetup.getDataFactory();
    static T1C_Base__Employee__c emp1;
    static T1C_Base__Employee__c emp2;
    static Account acct;
    static Contact cont;
    static User thisUser;
    static sObject ea1;
    static sObject ea2;
    
    static void setupTestData () 
    {
        Boolean isAEMExist = true;

        thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        
        dataFactory.createCBSAFR();
        String core = ':CORE';
        T1C_FR__Feature__c ACEExtensions = new T1C_FR__Feature__c(Name = 'Extensions',T1C_FR__Name__c = 'ACE.Extensions', T1C_FR__External_Id__c = 'ACE.Extensions' + core);
        T1C_FR__Feature__c postMerge = new T1C_FR__Feature__c(Name = 'PostMergeUtility',T1C_FR__Name__c = 'ACE.Extensions.PostMergeUtility', T1C_FR__External_Id__c = 'ACE.Extensions.PostMergeUtility' + core);
		T1C_FR__Feature__c ACECoreAceShareUtilObjectsActivitySecurityc = new T1C_FR__Feature__c(Name = 'Activity_Security__c', T1C_FR__Name__c = 'ACE.Core.AceShareUtil.Objects.Activity_Security__c');
		T1C_FR__Feature__c ACECoreAceShareUtilObjectsActivitySecuritycActivitySecurityPublic = new T1C_FR__Feature__c(Name = 'ActivitySecurityPublic', T1C_FR__Name__c = 'ACE.Core.AceShareUtil.Objects.Activity_Security__c.ActivitySecurityPublic');

		insert new list<T1C_FR__Feature__c>{ACEExtensions,postMerge,ACECoreAceShareUtilObjectsActivitySecurityc,ACECoreAceShareUtilObjectsActivitySecuritycActivitySecurityPublic};
		        
         insert new list<T1C_FR__Attribute__c>
        {
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECoreAceShareUtilObjectsActivitySecuritycActivitySecurityPublic.Id, Name = 'AccessLevel', T1C_FR__Value__c = 'Read'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECoreAceShareUtilObjectsActivitySecuritycActivitySecurityPublic.Id, Name = 'BatchOnly', T1C_FR__Value__c = 'FALSE'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECoreAceShareUtilObjectsActivitySecuritycActivitySecurityPublic.Id, Name = 'ObjectName', T1C_FR__Value__c = 'Activity_Security__c'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECoreAceShareUtilObjectsActivitySecuritycActivitySecurityPublic.Id, Name = 'ParentCoverageFilterClause', T1C_FR__Value__c = ''),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECoreAceShareUtilObjectsActivitySecuritycActivitySecurityPublic.Id, Name = 'ParentLookupField', T1C_FR__Value__c = 'Account__c'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECoreAceShareUtilObjectsActivitySecuritycActivitySecurityPublic.Id, Name = 'ParentObjectType', T1C_FR__Value__c = 'Account'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECoreAceShareUtilObjectsActivitySecuritycActivitySecurityPublic.Id, Name = 'RowCause', T1C_FR__Value__c = 'Account_Coverage__c')
        };
          
        emp1 = dataFactory.makeEmployee('Tyler', 'Durden', thisUser.Id);
        emp1.T1C_Base__Department__c = 'Test1';
        emp2 =  dataFactory.makeEmployee('Robert', 'Paulson', thisUser.Id);
        emp2.T1C_Base__Department__c = 'Test2';
        
        insert new list<T1C_Base__Employee__c>{emp1, emp2};
        
        acct = dataFactory.makeAccount('Account0');
        insert acct;
        
        cont = dataFactory.makeContact(acct.id,'Miles', 'Dyson');
        insert cont;
        
        //check if T1C_AEM is exist in the org
        map<String, Schema.SObjectType> mapSObjects = Schema.getGlobalDescribe();
        Schema.SObjectType tSObj = mapSObjects.get('T1C_AEM__Invited_Contact__c');
        
        if(tSObj == null)
        {
            isAEMExist = false;
        }
        
        if (isAEMExist)
        {
            sObject gtz = Schema.getGlobalDescribe().get('T1C_AEM__Time_Zone__c').newSObject();
            gtz.put('Name', 'Test Time Zone');
            gtz.put('T1C_AEM__Tzid__c', 'Canada/Eastern');
            gtz.put('T1C_AEM__Order__c', 0);
            insert gtz;
            
            T1C_Base__Region__c gr = new T1C_Base__Region__c(Name = 'Test Region');
            insert gr;
            
            sObject gc = Schema.getGlobalDescribe().get('T1C_AEM__City__c').newSObject();
            gc.put('Name', 'Test City');
            gc.put('T1C_AEM__Region__c', gr.Id);
            gc.put('T1C_AEM__Time_Zone__c', gtz.Id);
            insert gc;
            
            Date day = Date.today();
            
            sObject col = Schema.getGlobalDescribe().get('T1C_AEM__Event_Collection__c').newSObject();
            col.put('Name', 'Test');
            col.put('T1C_AEM__Event_Collection_Name__c', 'Test');
            col.put('T1C_AEM__Status__c', 'test status');
            col.put('T1C_AEM__Type__c', 'test type');
            insert col;
            
            sObject ge = Schema.getGlobalDescribe().get('T1C_AEM__Aem_Event__c').newSObject();
            ge.put('Name', 'Test Event test status Confirmed');
            ge.put('T1C_AEM__Event_Collection__c', col.Id);
            ge.put('T1C_AEM__Event_Date__c', day);
            ge.put('T1C_AEM__Type__c', 'TestMarketing');
            ge.put('T1C_AEM__Type_Label__c', 'Test Marketing');
            ge.put('T1C_AEM__City__c', gc.Id);
            insert ge;
            
            sObject gm = Schema.getGlobalDescribe().get('T1C_AEM__Meeting__c').newSObject();
            gm.put('T1C_AEM__Local_Start_Time__c', '1:00 PM');
            gm.put('T1C_AEM__Local_End_Time__c', '2:00 PM');
            gm.put('T1C_AEM__Aem_Event__c', ge.Id);
            gm.put('T1C_AEM__Type__c', 'One-on-One');
            gm.put('T1C_AEM__Allow_Multiple_Accounts__c', false);
            gm.put('T1C_AEM__Address__c', '23 sdfjsafj street, Toronto, ON k2k 3j4');           
            insert gm;
            
            sObject mi = Schema.getGlobalDescribe().get('T1C_AEM__Meeting_Invitation__c').newSObject();
            mi.put('T1C_AEM__Meeting__c', gm.Id);
            mi.put('T1C_AEM__Status__c', 'Confirmed');
            insert mi;
            
            sObject extIC = Schema.getGlobalDescribe().get('T1C_AEM__Invited_Contact__c').newSObject();
            extIC.put('T1C_AEM__Contact__c', cont.Id);
            extIC.put('T1C_AEM__Invitation__c', mi.Id);
            extIC.put('T1C_AEM__Role__c', 'Corporate');
            extIC.put('T1C_AEM__Is_Internal__c', false);
            extIC.put('T1C_AEM__Is_Key_Participant__c', false);
            extIC.put('T1C_AEM__Status__c', 'Status');
            sObject extIC2 = Schema.getGlobalDescribe().get('T1C_AEM__Invited_Contact__c').newSObject();
            extIC2.put('T1C_AEM__Contact__c', cont.Id);
            extIC2.put('T1C_AEM__Invitation__c', mi.Id);
            extIC2.put('T1C_AEM__Role__c', 'Corporate');
            extIC2.put('T1C_AEM__Is_Internal__c', false);
            extIC2.put('T1C_AEM__Is_Key_Participant__c', false);
            extIC2.put('T1C_AEM__Status__c', 'Status');
            list<SObject> extICList = new list<SObject>{extIC, extIC2};
            insert extICList;
            
            ea1 = Schema.getGlobalDescribe().get('T1C_AEM__Event_Account__c').newSObject();
            ea1.put('T1C_AEM__Account__c', acct.Id);
            ea1.put('T1C_AEM__Event__c', ge.Id);
            ea2 = Schema.getGlobalDescribe().get('T1C_AEM__Event_Account__c').newSObject();
            ea2.put('T1C_AEM__Account__c', acct.Id);
            ea2.put('T1C_AEM__Event__c', ge.Id);
            list<SObject> eaList = new list<SObject>{ea1, ea2};
            insert eaList;
            
            sObject ec1 = Schema.getGlobalDescribe().get('T1C_AEM__Event_Contact__c').newSObject();
            ec1.put('T1C_AEM__Contact__c', cont.Id);
            ec1.put('T1C_AEM__Event__c', ge.Id);
            ec1.put('T1C_AEM__Event_Account__c', ea1.Id);
            insert ec1;
            
            sObject eca1 = Schema.getGlobalDescribe().get('T1C_AEM__Event_Collection_Account__c').newSObject();
            eca1.put('T1C_AEM__Account__c', acct.Id);
            eca1.put('T1C_AEM__Event_Collection__c', col.Id);
            insert eca1;
            
            sObject ecc1 = Schema.getGlobalDescribe().get('T1C_AEM__Event_Collection_Contact__c').newSObject();
            ecc1.put('T1C_AEM__Contact__c', cont.Id);
            ecc1.put('T1C_AEM__Event_Collection_Account__c', eca1.Id);
            ecc1.put('T1C_AEM__Event_Collection__c', col.Id);
            insert ecc1;
        }
        
        T1C_Base__Ace_List__c aceList = new T1C_Base__Ace_List__c(Name = 'list', T1C_Base__Employee__c = emp1.Id);
        insert aceList;
        
        T1C_Base__Ace_List_Entry__c listEntry = new T1C_Base__Ace_List_Entry__c(T1C_Base__Contact__c = cont.Id, T1C_Base__Ace_List__c = aceList.Id, T1C_Base__Account__c = acct.Id);
        insert listEntry;
        
        System.runAs(thisUser) 
        {
            Date date1 = date.today();
            T1C_Base__User_Contact_Interaction__c[] uciList = new T1C_Base__User_Contact_Interaction__c[]{};
            uciList.add(new T1C_Base__User_Contact_Interaction__c(T1C_Base__Contact__c = cont.Id, T1C_Base__User__c = thisUser.Id, T1C_Base__Last_Interaction_Date__c = date1));
            uciList.add(new T1C_Base__User_Contact_Interaction__c(T1C_Base__Contact__c = cont.Id, T1C_Base__Last_Interaction_Date__c = date1.addDays(-2)));
            insert uciList;
        }
        
        T1C_Base__Interest_Subject_Type__c ist = dataFactory.makeInterestSubjectType('Ticker', null);
        insert ist;
        
        T1C_Base__Interest_Subject__c intSub = dataFactory.makeInterestSubject(ist.Id, 'Tier1CRM', null);
        insert intSub;  
            
        T1C_Base__Interest__c interest1 = dataFactory.makeInterest(acct.Id,cont.Id,ist.Id,intSub.Id);
        interest1.T1C_Base__Reason__c = 'Research';
        
        T1C_Base__Interest__c interest2 = dataFactory.makeInterest(acct.Id,cont.Id,ist.Id,intSub.Id);
        interest1.T1C_Base__Reason__c = 'Interest';
        
        insert new list<T1C_Base__Interest__c>{interest1,interest2};
    }
    
    static testMethod void PostMergeUtilityControllerTest()
    {
        setupTestData ();
        
        Test.startTest();
        
        T1C_Base__Contact_Coverage__c cc1 = new T1C_Base__Contact_Coverage__c(T1C_Base__Contact__c = cont.Id, T1C_Base__Employee__c = emp2.Id);
        insert new T1C_Base__Contact_Coverage__c []{cc1};
        
        PostMergeUtilityController controller = new PostMergeUtilityController();
        PostMergeUtilityController.getStaticResource('MomentJS');
        PostMergeUtilityController.enableDisableTriggers('false');
        PostMergeUtilityController.enableDisableTriggers('true');
        PostMergeUtilityController.getContactRelatedList(cont.Id);
        
        Test.stopTest();
    }
    
    static testMethod void PostMergeUtilityControllerTest2()
    {
        setupTestData ();
        
        Test.startTest();
        
        map<String, Id> crDataMap = dataFactory.setupCallReport(acct.Id,cont.Id,emp1.Id);
               
        T1C_Base__Deal__c deal1 = dataFactory.makeDeal('Big Deal', 'Big Deal', 'Open', acct.Id);
        insert deal1;
        
        T1C_Base__Deal_Contact__c dc = dataFactory.makeDealContact(deal1.Id, cont.Id, 1, 'Primary');
        insert dc;
        
        PostMergeUtilityController.getContactRelatedList(cont.Id);
        
        Test.stopTest();
    }
    
    static testMethod void AccountPostMergeUtilityControllerTest()
    {
        setupTestData ();
        
        Test.startTest();
        T1C_Base__Account_Coverage__c ac1 = dataFactory.makeAccountCoverage(acct.Id, emp1.Id);
        T1C_Base__Account_Coverage__c ac2 = dataFactory.makeAccountCoverage(acct.Id, emp2.Id);
		insert new list<T1C_Base__Account_Coverage__c>{ac1,ac2};
        
        AccountPostMergeUtilityController controller = new AccountPostMergeUtilityController();
        AccountPostMergeUtilityController.getStaticResource('MomentJS');
        AccountPostMergeUtilityController.enableDisableTriggers('false');
        AccountPostMergeUtilityController.enableDisableTriggers('true');
        if (ea1 != null && ea2 != null)
        {
            AccountPostMergeUtilityController.ReparentKeyContact(String.ValueOf((Id)ea1.get('Id')), String.ValueOf((Id)ea2.get('Id')));
        }
        AccountPostMergeUtilityController.getAccountRelatedList(acct.Id);
        
        Test.stopTest();
    }

    static testMethod void AccountPostMergeUtilityControllerTest2()
    {
        setupTestData ();

        Account acct2 = dataFactory.makeAccount('Paper Street Soap Company');
        insert acct2;
        
        Test.startTest();
        T1C_Base__Account_Coverage__c ac1 = dataFactory.makeAccountCoverage(acct.Id, emp1.Id);
        T1C_Base__Account_Coverage__c ac2 = dataFactory.makeAccountCoverage(acct.Id, emp2.Id);
		insert new list<T1C_Base__Account_Coverage__c>{ac1,ac2};

        T1C_Base__Interest_Subject_Type__c ist = dataFactory.makeInterestSubjectType('Ticker', null);
        insert ist;
        
        list<T1C_Base__Interest_Subject__c> intSubs = new list<T1C_Base__Interest_Subject__c>();
        T1C_Base__Interest_Subject__c intSub = dataFactory.makeInterestSubject(ist.Id, 'Tier1CRM', null);
        intSub.T1C_Base__Account__c = acct.Id;
        intSubs.add(intSub);
        T1C_Base__Interest_Subject__c intSub2 = dataFactory.makeInterestSubject(ist.Id, 'Tier1CRM', null);
        intSub2.T1C_Base__Account__c = acct2.Id;
        intSubs.add(intSub2);

        merge acct acct2;
        
        AccountPostMergeUtilityController controller = new AccountPostMergeUtilityController();
        AccountPostMergeUtilityController.getStaticResource('MomentJS');
        AccountPostMergeUtilityController.enableDisableTriggers('false');
        AccountPostMergeUtilityController.enableDisableTriggers('true');
        if (ea1 != null && ea2 != null)
        {
            AccountPostMergeUtilityController.ReparentKeyContact(String.ValueOf((Id)ea1.get('Id')), String.ValueOf((Id)ea2.get('Id')));
        }
        AccountPostMergeUtilityController.getAccountRelatedList(acct.Id);
        
        Test.stopTest();
    }
}