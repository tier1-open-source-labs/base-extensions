/**
 * Name: AFRUtil 
 * Description: Class to wrap utility functions around the AFR
 *
 * Confidential & Proprietary, 2015 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1CRM.
 * 
 *	PS Base 3.7.6
 *
 */
public with sharing class AFRUtil 
{  
  public static Map<String, String> attributeMap = new Map<String, String>{};
  public static Map<String, List<T1C_FR.Attribute>> attributesMap = new Map<String, List<T1C_FR.Attribute>>{};

//  ======================================================================================================
//  Method: getAttributes
//  Description: Retrieve the set of attributes for the indicated feature.
//  Params: userId - Id of the user to get the AFR relative to
//          featureName - Fully qualified name of feature
//  Return: List of attributes associated to feature (empty if not found).
//  ======================================================================================================
    public static T1C_FR.Attribute[] getAttributes(Id userId, String featureName)
    {
        String key = userId + '.' + featureName;
        T1C_FR.Attribute[] attributes;

        if (attributesMap.containsKey(key))
        {
            attributes = attributesMap.get(key);
        }
        else
        {
          // Cache attributes to save T1C_FR SOQL limit
            attributes = T1C_FR.featureCacheWebSvc.getAttributes(userId, featureName);
            attributesMap.put(key, attributes);
        }
        return attributes;
    }


//  ======================================================================================================
//  Method: getAttribute
//  Description: Method that will retrieve the specified attribute from the AFR
//  Params: userId - Id of the user to get the AFR relative to
//          featureName - Feature name that is the parent of the attribute
//          attributeName - Name of the attribute to retrieve
//  Return: Attribute value
//  ======================================================================================================
    public static String getAttribute(Id userId, String featureName, String attributeName)
    {
        String key = userId + '.' + featureName + '.' + attributeName;
        String cacheFeatureKey = featureName + '.' + userId;
        String attributeValue;

        // Check if feature has been cached already
        if (attributeMap.containsKey(cacheFeatureKey))
        {
            attributeValue = attributeMap.get(key);
        }
        else
        {
          // Cache all attributes to save T1C_FR SOQL limit
          for(T1C_FR.Attribute attrib : T1C_FR.featureCacheWebSvc.getAttributes(userId, featureName))
          {
            String attribKey = userId + '.' + featureName + '.' + attrib.Name;
              attributeMap.put(attribKey, attrib.Value);
          }
          attributeValue = attributeMap.get(key);
          attributeMap.put(cacheFeatureKey, 'cached');
        }

        // Check for null attribute value
        if (attributeValue == null)
        {
            attributeValue = '';
        }

        return attributeValue;
    }

//  ===========================================================================================
//  Method: getSubFeature
//  Description: Utility method that will get the specific sub feature from the specified parent. Supports . notation in the name of the feature to climb the tree and does not need the parent feature name
//  Params: parent - Parent feature to get the child for
//          name - Name of the child feature to get (can include . notation to climb the sub feature tree), do not include the fully qualified name 
//  Return: The feature if found, null otherwise
//  =========================================================================================== 
    public static T1C_FR.Feature getSubFeature(T1C_FR.Feature parent, String name)
    {
        T1C_FR.Feature o;
        
        String[] featureNames = name.split('\\.');
        String currentName = parent.Name;       
        
        for (Integer i = 0 ; i < featureNames.size() ; i++)
        {
            if (parent != null)
            {
                currentName += '.' + featureNames[i];
                parent = parent.subFeatMap.get(currentName);
                //Last item in the list
                if (i == featureNames.size() - 1)
                {                   
                    o = parent;
                }
            }                       
        }       
        
        return o;
    }       
    	
//  ===========================================================================================
//  Method: getAttributeValue
//  Description: Gets the attribute value from the specified feature doing proper null checking
//  Params: feature - Feature to get the value from
//      attributeName - Name of the attribute to get 
//          defaultValue - The default value to return
//  Return: The value if found, The default value otherwise
//  ===========================================================================================
    public static String getAttributeValue(String feature, String attributeName, String defaultValue)
    {
        return getAttributeValue(UserInfo.getUserId(), feature, attributeName, defaultValue);
    }

//  ===========================================================================================
//  Method: getAttributeValue
//  Description: Gets the attribute value from the specified feature doing proper null checking
//  Params: userId - Id of the user to get the value for
//          feature - Feature name to get the value from
//          attributeName - Name of the attribute to get
//          defaultValue - The default value to return 
//  Return: The value if found, The default value otherwise
//  ===========================================================================================
    public static String getAttributeValue(Id userId, String feature, String attributeName, String defaultValue)
    {
        String val = T1C_FR.FeatureCacheWebSvc.getAttribute(userId, feature, attributeName);
    
    if (val == null || val == '')
    {
      val = defaultValue;
    }
    
    return val;
  }
  
  public static String getAttributeValue(T1C_FR.Feature feature, String attributeName)
  {
    return getAttributeValue(feature, attributeName, '');
  }
  
  public static String getAttributeValue(T1C_FR.Feature feature, String attributeName, String defaultValue)
  {
    String attributeValue = defaultValue;
    if (feature != null)
    {
      attributeValue = feature.getAttributeValue(attributeName);
      
      if (attributeValue == null || attributeValue == '')
      {
        attributeValue = defaultValue;
      }
    }
    
    return attributeValue;
  }

//  ===========================================================================================
//  Method: getBooleanAttributeValue
//  Description: Gets the attribute from the feature as a boolean value
//  Params: feature - Feature to get the value from
//      attributeName - Name of the attribute to get
//      defaultValue - Value to return as default if attribute doesn't exist 
//  Return: The value if found, default value otherwise
//  ===========================================================================================  
  public static Boolean getBooleanAttributeValue(T1C_FR.Feature feature, String attributeName, Boolean defaultValue)
  {
    String attributeValue = getAttributeValue(feature, attributeName);
    
    if (attributeValue == '')
    {
      return defaultValue;
    }
    
    return attributeValue == 'true';
  }  

//  ===========================================================================================
//  Method: getIntegerAttributeValue
//  Description: Gets the attribute from the feature as an integer value
//  Params: feature - Feature to get the value from
//      attributeName - Name of the attribute to get
//      defaultValue - Value to return as default if attribute doesn't exist 
//  Return: The value if found, default value otherwise
//  ===========================================================================================    
  public static Integer getIntegerAttributeValue(T1C_FR.Feature feature, String attributeName, Integer defaultValue)
  {
    String attributeValue = getAttributeValue(feature, attributeName);
    
    if (attributeValue == '')
    {
      return defaultValue;
    }
    
	Integer value = defaultvalue;
    
    try
    {
        value = Integer.valueOf(attributeValue);
    }
    catch(Exception e)
    {
        
    }
    return value;
  }  

//  ===========================================================================================
//  Method: setAttributes
//  Description: Utility method to insert a map of attributes across different features (used to bypass the FR webservice and reduce SOQL query usage)
//  Params: attributeMap - Map containing the list of attributes based off the feature name 
//  Return : List of success / failures
//  ===========================================================================================      
  public static void setAttributes(Map<String, T1C_FR.Attribute[]> attributeMap)
  {
    T1C_FR__Attribute__c[] attribList = new T1C_FR__Attribute__c[]{};
    Map<String, Id> featureMap = new Map<String, Id>{};
    Set<String> externalIds = new Set<String>{};
        //Contains the T1C_FR__Attribute__c Ids relative to there External Id
        //Key -> T1C_FR__Attribute__c external Id
        //Value -> T1C_FR__Attribute__C Id
        Map<String, Id> externalIdToAttributeId = new Map<String, Id>{};
    
    for (String key : attributeMap.keySet())
    {
        String extFeatureId = key + ':CORE';
        externalIds.add(extFeatureId);

        for (T1C_FR.Attribute item : attributeMap.get(key))
        {                
           externalIdToAttributeId.put(extFeatureId + ':' + item.Name, null);
		}
    }
        
    for (T1C_FR__Feature__c item : [ SELECT Id, T1C_FR__Name__c FROM T1C_FR__Feature__c WHERE T1C_FR__External_Id__c IN :externalIds])
    {
    	featureMap.put(item.T1C_FR__Name__c, item.Id);
    }

    for (T1C_FR__Attribute__c item : [ SELECT Id,T1C_FR__External_Id__c FROM T1C_FR__Attribute__c WHERE T1C_FR__External_Id__c IN :externalIdToAttributeId.keySet()])
    {            
        externalIdToAttributeId.put(item.T1C_FR__External_Id__c, item.Id);
    }  
        
    for (String featureName : attributeMap.keySet())
    {
      Id o = featureMap.get(featureName);
      
      if (o != null)
      {
        for (T1C_FR.Attribute item : attributeMap.get(featureName))
        {
	        //get the attribute Id based off the fully qualified external Id
	        //Fully Qualified Feature Name + :CORE: + Attribute name e.g.                
	        //ACE.Test.MyFeature:CORE:MyAttribute
	        Id attributeId = externalIdToAttributeId.get(featureName + ':CORE:' + item.Name);
	        Id featureId = (attributeId == null ? o : null);

          attribList.add(new T1C_FR__Attribute__c
          (
            Id = attributeId,
            Name = item.Name,
            T1C_FR__Value__c = item.Value,
            T1C_FR__User_Overridable__c = item.bUserOverridable,
            T1C_FR__Feature__c = featureId
          ));
        }
      }
    }
    
    if (!attribList.isEmpty())
    {
    	upsert attribList;
    }
  }
}