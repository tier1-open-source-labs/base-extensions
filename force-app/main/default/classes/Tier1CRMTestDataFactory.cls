/** 
 * Name: Tier1CRMTestDataFactory 
 * Description: Class to centralize the creation of data for test classes (will be customized for each environment)
 *
 * Returning objects to avoid many DML Executions and allows further customization in the Test Class
 *
 * Confidential & Proprietary, 2018 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1CRM.
 * 
 *	PS Base 3.7.6
 *
 */
 
public interface Tier1CRMTestDataFactory 
{
    // Convience methods (will do the DML for you, but return a map with Ids)
	map<String,Id> setupBaseDataAndCoverage();
	
	map<String,Id> setupCallReport(Id acctId, Id contId, Id empId);
	
	// Main utility methods
    Account makeAccount(String acctName);
        
    Contact makeContact(Id acctId, String fName, String lName);
        
    T1C_Base__Employee__c makeLoggedInUserEmployee();
    
    T1C_Base__Employee__c makeEmployee(String fName, String lName, Id userId);
    
    T1C_Base__Account_Coverage__c makeAccountCoverage(Id acctId, Id empId);
    
    T1C_Base__Contact_Coverage__c makeContactCoverage(Id contId, Id empId);
    
    Task makeTask(Id whoId, Id whatId, Date activityDate);
    
    T1C_Base__Employee_Away_Date__c makeEmpAwayDate(Id empId, Date startDate, Integer daysAfterStart);
    
    Event makeEvent(Id whoId, Id whatId, DateTime startTime);
    
    T1C_Base__Call_Report__c makeCallReport(String intType, Id acctId, Date meetingDate);
    
    T1C_Base__Call_Report_Attendee_Client__c makeCallReportClientAttendee(Id crId, Id contId);
    
    T1C_Base__Call_Report_Email_Recipient__c makeCallReportEmailRecipient(Id crId, Id empId);
    
    T1C_Base__Call_Report_Attendee_Internal__c makeCallReportInternalAttendee(Id crId, Id empId);
    
    T1C_Base__Call_Report_Interest_Subject__c makeCallReportInterestSubject(Id crId, Id intSubId);
    
    T1C_Base__Interest_Subject_Type__c makeInterestSubjectType(String istName, Id parentId);
    
    T1C_Base__Interest_Subject__c makeInterestSubject(Id ist, String intSubName, Id parentId);
    
    T1C_Base__Interest__c makeInterest(Id acctId, Id contId, Id istId, Id isId);
    
    T1C_Base__Organization_Structure__c makeOrgStructureSpecRTId(String name, Id rtId);
    
    T1C_Base__Organization_Structure__c makeOrgStructureWithLOBRT(String name);

    T1C_Base__Product__c makeProduct(String name, Id orgStructureId);
    
    T1C_Base__Deal__c makeDeal(String name, String alias, String stage, Id acctId);
    
    T1C_Base__Deal_Contact__c makeDealContact(Id dealId, Id contactId, Integer orderNumber, String role);
    
    T1C_Base__Deal_Team__c makeDealTeamMember(Id dealId, Id empId, Integer orderBy, String role);
    
    T1C_Base__Deal_Product__c makeDealProduct(Id dealId, String status, Id productId);
    
    T1C_Base__Deal_Product_Team__c makeDealProductTeam(Id dealProductId, Boolean isPrimary, Id dealTeamMemberId);
    
    T1C_Base__Address__c makeAddress(String name, Id acctId);
    
    T1C_Base__Contact_Phone_Number__c makeContactPhoneNumber(Id contId, String numberType, String value);
    
    // Sharing Framework AFR Loading 
    void createCBSAFR();
}