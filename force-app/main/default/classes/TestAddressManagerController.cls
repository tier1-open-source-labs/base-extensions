/**
 * Name: TestAddressManagerController 
 * Description: Test Class for the Address Manager Controller
 *
 * Confidential & Proprietary, 2016 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1CRM.
 *
 *	PS Base 3.7.6
 * 
 */
@isTest
private class TestAddressManagerController 
{
    private static final Tier1CRMTestDataFactory dataFactory = Tier1CRMTestDataFactorySetup.getDataFactory();

    private static void setAFRs()
    {
      T1C_FR__Feature__c aceFeature     = new T1C_FR__Feature__c(Name = 'ACE',            T1C_FR__Name__c = 'ACE');
      T1C_FR__Feature__c extensions     = new T1C_FR__Feature__c(Name = 'Extensions',     T1C_FR__Name__c = 'ACE.Extensions');
      T1C_FR__Feature__c addressManager = new T1C_FR__Feature__c(Name = 'AddressManager', T1C_FR__Name__c = 'ACE.Extensions.AddressManager');
      T1C_FR__Feature__c tabs           = new T1C_FR__Feature__c(Name = 'Tabs',           T1C_FR__Name__c = 'ACE.Extensions.AddressManager.Tabs');
      T1C_FR__Feature__c contactsTab    = new T1C_FR__Feature__c(Name = 'Contact',        T1C_FR__Name__c = 'ACE.Extensions.AddressManager.Tabs.Contacts');
      T1C_FR__Feature__c layout         = new T1C_FR__Feature__c(Name = 'Layout',         T1C_FR__Name__c = 'ACE.Extensions.AddressManager.Tabs.Contacts.Layout');
      T1C_FR__Feature__c grid           = new T1C_FR__Feature__c(Name = 'Grid',           T1C_FR__Name__c = 'ACE.Extensions.AddressManager.Tabs.Contacts.Layout.Grid');
      T1C_FR__Feature__c columns        = new T1C_FR__Feature__c(Name = 'Columns',        T1C_FR__Name__c = 'ACE.Extensions.AddressManager.Tabs.Contacts.Layout.Grid.Columns');
      T1C_FR__Feature__c nameCol        = new T1C_FR__Feature__c(Name = 'Name',           T1C_FR__Name__c = 'ACE.Extensions.AddressManager.Tabs.Contacts.Layout.Grid.Columns.Name');
      T1C_FR__Feature__c fieldConfig    = new T1C_FR__Feature__c(Name = 'FieldConfig',    T1C_FR__Name__c = 'ACE.Extensions.AddressManager.Tabs.Contacts.Layout.Grid.Columns.Name.FieldConfig');
      T1C_FR__Feature__c pickCol        = new T1C_FR__Feature__c(Name = 'PickList',       T1C_FR__Name__c = 'ACE.Extensions.AddressManager.Tabs.Contacts.Layout.Grid.Columns.PickList');
      
      T1C_FR__Feature__c rightPanel     = new T1C_FR__Feature__c(Name = 'RightPanel',     T1C_FR__Name__c = 'ACE.Extensions.AddressManager.Tabs.Contacts.Layout.RightPanel');
      T1C_FR__Feature__c cardFields     = new T1C_FR__Feature__c(Name = 'Fields',         T1C_FR__Name__c = 'ACE.Extensions.AddressManager.Tabs.Contacts.Layout.RightPanel.Fields');
      T1C_FR__Feature__c nameCardField  = new T1C_FR__Feature__c(Name = 'Name',           T1C_FR__Name__c = 'ACE.Extensions.AddressManager.Tabs.Contacts.Layout.RightPanel.Fields.Name');
      
      T1C_FR__Feature__c editcontrols   = new T1C_FR__Feature__c(Name = 'EditControls',   T1C_FR__Name__c = 'ACE.Extensions.AddressManager.Tabs.Contacts.Layout.RightPanel.EditControls');
      T1C_FR__Feature__c editFields     = new T1C_FR__Feature__c(Name = 'Fields',         T1C_FR__Name__c = 'ACE.Extensions.AddressManager.Tabs.Contacts.Layout.RightPanel.EditControls.Fields');
      T1C_FR__Feature__c editNameField  = new T1C_FR__Feature__c(Name = 'Name',           T1C_FR__Name__c = 'ACE.Extensions.AddressManager.Tabs.Contacts.Layout.RightPanel.EditControls.Fields.Name');
      
      upsert new list<T1C_FR__Feature__c>{aceFeature, extensions, addressManager, tabs, contactsTab, layout, grid, columns, 
         nameCol, fieldConfig, pickCol, rightPanel, cardFields, nameCardField, editcontrols, editFields, editNameField};        
      insert new list<T1C_FR__Attribute__c>
      {    
         new T1C_FR__Attribute__c(T1C_FR__Feature__c = contactsTab.Id, Name = 'SObjectName', T1C_FR__Value__c = 'Contact'),
                             
         new T1C_FR__Attribute__c(T1C_FR__Feature__c = nameCol.Id, Name = 'DataField', T1C_FR__Value__c = 'T1C_Base__Preferred_Assistant_Phone__c'),
         new T1C_FR__Attribute__c(T1C_FR__Feature__c = nameCol.Id, Name = 'FieldType', T1C_FR__Value__c = 'LOOKUP'),
         new T1C_FR__Attribute__c(T1C_FR__Feature__c = nameCol.Id, Name = 'Visible', T1C_FR__Value__c = 'true'),
         new T1C_FR__Attribute__c(T1C_FR__Feature__c = nameCol.Id, Name = 'Order', T1C_FR__Value__c = '1'),
         
         new T1C_FR__Attribute__c(T1C_FR__Feature__c = pickCol.Id, Name = 'DataField', T1C_FR__Value__c = 'T1C_Base__Job_Function__c'),
         new T1C_FR__Attribute__c(T1C_FR__Feature__c = pickCol.Id, Name = 'FieldType', T1C_FR__Value__c = 'PICKLIST'),
         new T1C_FR__Attribute__c(T1C_FR__Feature__c = pickCol.Id, Name = 'Visible', T1C_FR__Value__c = 'true'),
         new T1C_FR__Attribute__c(T1C_FR__Feature__c = pickCol.Id, Name = 'Order', T1C_FR__Value__c = '1'),
         new T1C_FR__Attribute__c(T1C_FR__Feature__c = pickCol.Id, Name = 'SObjectName', T1C_FR__Value__c = 'Contact'),
         
         new T1C_FR__Attribute__c(T1C_FR__Feature__c = nameCardField.Id, Name = 'DataField', T1C_FR__Value__c = 'T1C_Base__Address_Name__c'),
         new T1C_FR__Attribute__c(T1C_FR__Feature__c = nameCardField.Id, Name = 'Order', T1C_FR__Value__c = '1'),
         
         new T1C_FR__Attribute__c(T1C_FR__Feature__c = editNameField.Id, Name = 'DataField', T1C_FR__Value__c = 'Name'),
         new T1C_FR__Attribute__c(T1C_FR__Feature__c = editNameField.Id, Name = 'FieldType', T1C_FR__Value__c = 'TEXT'),
         new T1C_FR__Attribute__c(T1C_FR__Feature__c = editNameField.Id, Name = 'Order', T1C_FR__Value__c = '1')
         
      };
      
   }

   static testMethod void myUnitTest() 
   {
        setAFRs();
        AddressManagerController addrControl = new AddressManagerController();
        
        Account testAccnt = dataFactory.makeAccount('Paper Street Soap Company');
        testAccnt.BillingStreet='1280 Main street';
        testAccnt.ShippingStreet='5775 Yonge street';
        insert testAccnt;
        
        Contact testCont = dataFactory.makeContact(testAccnt.Id, 'Marla', 'Singer');
        insert testCont;
        
        String testString = AddressManagerController.getConfig(String.valueOf(testAccnt.Id));
        system.debug('Found Config: \r\n' + testString);
        
        T1C_Base__Address__c testAddress = new T1C_Base__Address__c(T1C_Base__Address_Name__c='Test Address',T1C_Base__Account__c=testAccnt.Id, T1C_Base__Street__c='5255 Yonge street');
        
        Object addr = JSON.deserialize(
            AddressManagerController.saveAddress( JSON.serialize(testAddress),'false','false' )
        , T1C_Base__Address__c.class);

        String addrId = String.valueOf( ((T1C_Base__Address__c)addr).Id );
        testAddress.Id = addrId;
        
        testCont.T1C_Base__Mailing_Address_Id__c = Id.valueOf(addrId);
        
        List<SObject> sObjList = new List<SObject>();
        
        sObjList.add(testCont);
        
        AddressManagerController.updateSObjects( JSON.serialize(sObjList) );
        
        T1C_Base__Address__c testAddress2 = new T1C_Base__Address__c(T1C_Base__Address_Name__c='Test Address2',T1C_Base__Account__c=testAccnt.Id,T1C_Base__Street__c='2300 Yonge street');
        
        Object addr2 = JSON.deserialize(
            AddressManagerController.saveAddress( JSON.serialize(testAddress2),'false','false' )
        , T1C_Base__Address__c.class);

        String addrId2 = String.valueOf( ((T1C_Base__Address__c)addr2).Id );

        testAddress2.Id = addrId2;
        
        testAccnt.T1C_Base__Billing_Address_Id__c = addrId2;
        testAccnt.T1C_Base__Shipping_Address_Id__c = addrId;
        
        update testAccnt;
        
        AddressManagerController.finishedTriggers = new Set<String>();
        
        Map<Id, T1C_Base__Address__c> addrMap = new Map<Id, T1C_Base__Address__c>();
        
        testAddress2.T1C_Base__Street__c = '5700 Yonge Street';
        testAddress.T1C_Base__Street__c = '2100 Yonge Street';
        testAddress.T1C_Base__City__c = 'North York';
        
        addrMap.put(testAddress.Id, testAddress);
        addrMap.put(testAddress2.Id, testAddress2);
        
        AddressManagerController.finishedTriggers = new Set<String>();
        
        testAddress2.T1C_Base__Street__c = '13 elm street';
        testAddress2.T1C_Base__City__c = 'Boston';
        testAddress.T1C_Base__Street__c = '99th Avenue west';
        testAddress.T1C_Base__City__c = 'Salem';
        
        update testAddress;
        update testAddress2; 
        
        AddressManagerController.finishedTriggers = new Set<String>();
        
        testAddress2.T1C_Base__Street__c = '2145 The West Mall';
        testAddress.T1C_Base__Street__c = '1800 Sheppard Ave E';
 
        update testAddress;
        update testAddress2;
        
        testAccnt.BillingState = 'Testing Account update';
        testAccnt.ShippingState = 'Testing Account update';
        
        update testAccnt;
        
        AddressManagerController.finishedTriggers = new Set<String>();
        Test.startTest();
        Database.executeBatch( new SyncContactAddressesBatch(addrMap) );
        Test.stopTest();
        
   }

   static testMethod void redirectTest() 
   {
        setAFRs();

        Account testAccnt = dataFactory.makeAccount('Paper Street Soap Company');
        testAccnt.BillingStreet='testStreet';
        testAccnt.ShippingStreet='ShipStreet';
        insert testAccnt;

        T1C_Base__Address__c testAddress = new T1C_Base__Address__c(T1C_Base__Address_Name__c='Test Address',T1C_Base__Account__c=testAccnt.Id);
        testAddress.T1C_Base__Street__c = 'Test Street Test Street';
        testAddress.T1C_Base__City__c = 'Test City City'; 
        insert testAddress;

        Contact testCont = dataFactory.makeContact(testAccnt.Id, 'Marla', 'Singer');
        Contact testCont2 = dataFactory.makeContact(testAccnt.Id, 'Tyler', 'Durden');
        testCont2.MailingPostalCode = '54321';
        Contact testCont3 = dataFactory.makeContact(testAccnt.Id, 'Miles', 'Dyson');
        testCont3.MailingPostalCode = '12345';

        insert new list<Contact>{testCont,testCont2,testCont3};
        
        PageReference pageRef = Page.AddressManagerRedirect;
        ApexPages.currentPage().getParameters().put('Id',testAddress.Id);
        AddressManagerController addrControl = new AddressManagerController(new ApexPages.StandardController(testAddress));        
        pageRef = addrControl.redirect();

        //T1C_Base__Address__c addr = AddressManagerController.getNewAddressFromDatabase(null);
        //addr = AddressManagerController.getNewAddressFromDatabase(testAddress.Id);
        
        String testString = AddressManagerController.getConfig(testCont.Id);
        testString = AddressManagerController.getConfig(testAddress.Id);

        AddressManagerController.deleteAddress(JSON.serialize(testAddress));
   }

   static testMethod void addressContactSyncBatchTest() 
   {
        setAFRs();
        AddressManagerController addrControl = new AddressManagerController();
        
        Account testAccnt = dataFactory.makeAccount('Paper Street Soap Company');
        testAccnt.BillingStreet='5775 Yonge street';
        testAccnt.ShippingStreet='2771 Eglinton';
        insert testAccnt;
        
        Contact testCont = dataFactory.makeContact(testAccnt.Id, 'Marla', 'Singer');
        Contact testCont2 = dataFactory.makeContact(testAccnt.Id, 'Tyler', 'Durden');
        testCont2.MailingPostalCode = '54321';
        Contact testCont3 = dataFactory.makeContact(testAccnt.Id, 'Miles', 'Dyson');
        testCont3.MailingPostalCode = '12345';

        insert new list<Contact>{testCont,testCont2,testCont3};
        
        String testString = AddressManagerController.getConfig(String.valueOf(testAccnt.Id));
        system.debug('Found Config: \r\n' + testString);
        
        T1C_Base__Address__c testAddress = new T1C_Base__Address__c(T1C_Base__Address_Name__c='Test Address',T1C_Base__Account__c=testAccnt.Id);
        
        Object addr = JSON.deserialize(
            AddressManagerController.saveAddress( JSON.serialize(testAddress),'false','false' )
        , T1C_Base__Address__c.class);

        String addrId = String.valueOf( ((T1C_Base__Address__c)addr).Id );
        testAddress.Id = addrId;
        
        testCont.T1C_Base__Mailing_Address_Id__c = Id.valueOf(addrId);
        
        List<SObject> sObjList = new List<SObject>();
        
        sObjList.add(testCont);
        
        AddressManagerController.updateSObjects( JSON.serialize(sObjList) );
        
        T1C_Base__Address__c testAddress2 = new T1C_Base__Address__c(T1C_Base__Address_Name__c='Test Address2',T1C_Base__Account__c=testAccnt.Id);
        
        Object addr2 = JSON.deserialize(
            AddressManagerController.saveAddress( JSON.serialize(testAddress2),'false','false' )
        , T1C_Base__Address__c.class);

        String addrId2 = String.valueOf( ((T1C_Base__Address__c)addr2).Id );
        
        testAddress2.Id = addrId2;
        
        testAccnt.T1C_Base__Billing_Address_Id__c = addrId2;
        testAccnt.T1C_Base__Shipping_Address_Id__c = addrId;
        
        update testAccnt;
        
        AddressManagerController.finishedTriggers = new Set<String>();
        
        Map<Id, T1C_Base__Address__c> addrMap = new Map<Id, T1C_Base__Address__c>();
        
        testAddress2.T1C_Base__Street__c = '182 The West Mall';
        testAddress.T1C_Base__Street__c = '2267 Islington Ave';
        testAddress.T1C_Base__City__c = 'Etobicoke'; 
        
        
        addrMap.put(testAddress.Id, testAddress);
        addrMap.put(testAddress2.Id, testAddress2);
        
        AddressManagerController.finishedTriggers = new Set<String>();
        
        testAddress2.T1C_Base__Street__c = '5255 Yonge Street';
        testAddress2.T1C_Base__City__c = 'North York';
        testAddress.T1C_Base__Zip_Postal_Code__c = '12345';
        testAddress2.T1C_Base__Zip_Postal_Code__c = 'M2J 2E4';
        testAddress.T1C_Base__Street__c = '401 College Street';
        testAddress.T1C_Base__City__c = 'Toronto';

        update new list<T1C_Base__Address__c>{testAddress,testAddress2}; 
        
        AddressManagerController.finishedTriggers = new Set<String>();
        
        testAddress.Needs_Sync__c = true;
        testAddress2.Needs_Sync__c = true;

        testAddress2.T1C_Base__Street__c = '2145 Dunwin Dr';
        testAddress.T1C_Base__Street__c = '31 Erindale Ave';
 
        update new list<T1C_Base__Address__c>{testAddress,testAddress2}; 
        
        
        list<AddressManagerController.AddressWrapper> addrWraps = new list<AddressManagerController.AddressWrapper>();
        addrWraps.add(new AddressManagerController.AddressWrapper(testAddress));
        addrWraps.add(new AddressManagerController.AddressWrapper(testAddress2));
        addrWraps.sort();

        addrWraps = new list<AddressManagerController.AddressWrapper>();
        addrWraps.add(new AddressManagerController.AddressWrapper(testAddress2));
        addrWraps.add(new AddressManagerController.AddressWrapper(testAddress));
        addrWraps.sort();

        testAccnt.BillingState = 'Testing Account update';
        testAccnt.ShippingState = 'Testing Account update';
        
        update testAccnt;
        
        AddressManagerController.finishedTriggers = new Set<String>();
        Test.startTest();
        Database.executeBatch( new AddressContactSyncBatch() );
        Test.stopTest();        
   }
}