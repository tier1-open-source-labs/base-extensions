/**
 * Name: SyncContactAddressesBatch 
 * Description: Batch Job that updates all associated Contact Addresses when an Address
 * Object is updated. Uses the T1C_Base__Mailing_Address_Id__c lookup to determine related 
 * Contacts.
 *
 * Confidential & Proprietary, 2016 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1CRM.
 * 
 *  PS Base 3.7.6
 */
global without sharing class SyncContactAddressesBatch implements Database.batchable<Contact>
{
   private Map<Id, T1C_Base__Address__c> addressMap;
   
   global SyncContactAddressesBatch( Map<Id, T1C_Base__Address__c> addressInputMap )
	{ 
	   this.addressMap = addressInputMap;
	}
   
   global Iterable<Contact> start(Database.BatchableContext bc) 
   {
      Set<Id> addressIDs = this.addressMap.keySet();
      
      
      List<Contact> contactList = [SELECT Id, 
		                                    Name, 
		                                    MailingStreet,  
		                                    MailingCity,
		                                    MailingCountry,
		                                    MailingState,
		                                    MailingPostalCode,
		                                    T1C_Base__Mailing_Address_Id__c
                                     FROM Contact 
                                    WHERE T1C_Base__Mailing_Address_Id__c IN :addressIDs];
      
      
      return contactList;
   }
   
   global void execute(Database.BatchableContext BC, List<Contact> scope)
   {
      
      /*
      'T1C_Base__Street__c'         , 'MailingStreet'
      'T1C_Base__City__c'           , 'MailingCity'
      'T1C_Base__Country__c'        , 'MailingCountry'
      'T1C_Base__State_Province__c' , 'MailingState'
      'T1C_Base__Zip_Postal_Code__c', 'MailingPostalCode'
      */
      
      AddressManagerController.finishedTriggers.add('SyncUnlinkedUpdatedContactAddress');
      
	   //Get all related contacts
	   for(Contact ct : scope)
	   {
         T1C_Base__Address__c contactAddress = this.addressMap.get( (Id)ct.get('T1C_Base__Mailing_Address_Id__c') );
         
         AddressSyncTriggerHelper.applyAddressChange(contactAddress, ct, AddressSyncTriggerHelper.contactFieldsMap);
		   /*ct.MailingStreet     = contactAddress.T1C_Base__Street__c;
	      ct.MailingCity       = contactAddress.T1C_Base__City__c;
	      ct.MailingCountry    = contactAddress.T1C_Base__Country__c;
	      ct.MailingState      = contactAddress.T1C_Base__State_Province__c;
	      ct.MailingPostalCode = contactAddress.T1C_Base__Zip_Postal_Code__c;*/
	   }
      
      update scope;
    
   }
   
   global void finish(Database.BatchableContext BC)
   {
      
   }
}