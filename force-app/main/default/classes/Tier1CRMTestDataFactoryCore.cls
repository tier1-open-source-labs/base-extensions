/** 
 * Name: Tier1CRMTestDataFactoryCore 
 * Description: Class to centralize the creation of data for test classes (will be customized for each environment)
 *
 * Returning objects to avoid many DML Executions and allows further customization in the Test Class
 *
 * Confidential & Proprietary, 2018 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1CRM.
 * 
 *	PS Base 3.7.6
 *
 */
 
public virtual class Tier1CRMTestDataFactoryCore implements Tier1CRMTestDataFactory
{
    // Convience methods (will do the DML for you, but return a map with Ids)
	public virtual map<String,Id> setupBaseDataAndCoverage()
	{
		map<String, Id> returnMap = new map<String, Id>();
		
		Account acct = makeAccount('Paper Street Soap Company');
		insert acct;
		returnMap.put('Account', acct.Id);
		
		Contact cont = makeContact(acct.Id, 'Marla', 'Singer');
		insert cont;
		returnMap.put('Contact', cont.Id);
		
		T1C_Base__Employee__c emp = makeLoggedInUserEmployee();
		insert emp;
		returnMap.put('Employee', emp.Id);
		
		T1C_Base__Account_Coverage__c ac = makeAccountCoverage(acct.Id, emp.Id);
		insert ac;
		returnMap.put('AccountCoverage', ac.Id);
		
		T1C_Base__Contact_Coverage__c cc = makeContactCoverage(cont.Id, emp.Id);
		insert cc;
		returnMap.put('ContactCoverage', cc.Id);
		
		return returnMap;
	}
	
	public virtual map<String,Id> setupCallReport(Id acctId, Id contId, Id empId)
	{
		map<String, Id> returnMap = new map<String, Id>();
		
		T1C_Base__Call_Report__c cr = makeCallReport('Call', acctId, Date.today());
		insert cr;
		returnMap.put('CallReport', cr.Id);
		
		T1C_Base__Call_Report_Attendee_Client__c crca = makeCallReportClientAttendee(cr.Id, contId);
		insert crca;
		returnMap.put('CallReportAttendeeClient', crca.Id);
		
		T1C_Base__Call_Report_Attendee_Internal__c crai = makeCallReportInternalAttendee(cr.Id, empId);
		insert crai;
		returnMap.put('CallReportAttendeeInterenal', crai.Id);
		
		T1C_Base__Call_Report_Email_Recipient__c crer = makeCallReportEmailRecipient(cr.Id, empId);
		insert crer;
		returnMap.put('CallReportEmailRecipient', crer.Id);
		
		return returnMap;
	}
	
	// Main utility methods
    public virtual Account makeAccount(String acctName)
    {
    	return new Account(Name = acctName);
    } 
    
    public virtual Contact makeContact(Id acctId, String fName, String lName)
    {
    	return new Contact(AccountId = acctId, FirstName = fName, LastName = lName, Email = fName + lName + '@someplace.com');
    }
    
    public virtual T1C_Base__Employee__c makeLoggedInUserEmployee()
    {
    	return new T1C_Base__Employee__c(Name = UserInfo.getName(), T1C_Base__Email__c = UserInfo.getUserEmail(), T1C_Base__First_Name__c = UserInfo.getFirstName(), T1C_Base__Last_Name__c = UserInfo.getLastName(), T1C_Base__User__c = UserInfo.getUserId());
    }
    
    public virtual T1C_Base__Employee__c makeEmployee(String fName, String lName, Id userId)
    {
    	return new T1C_Base__Employee__c(Name = fName + ' ' + lName, T1C_Base__Email__c = fName + lName + '@thiscompany.com', T1C_Base__First_Name__c = fName, T1C_Base__Last_Name__c = lName, T1C_Base__User__c = userId);    	
    }
    
    public virtual T1C_Base__Account_Coverage__c makeAccountCoverage(Id acctId, Id empId)
    {
    	return new T1C_Base__Account_Coverage__c(T1C_Base__Account__c = acctId, T1C_Base__Employee__c = empId);
    }
    
    public virtual T1C_Base__Contact_Coverage__c makeContactCoverage(Id contId, Id empId)
    {
    	return new T1C_Base__Contact_Coverage__c(T1C_Base__Contact__c = contId, T1C_Base__Employee__c = empId);
    }
    
    public virtual Task makeTask(Id whoId, Id whatId, Date activityDate)
    {
    	return new Task(T1C_Base__Interaction_Type__c = 'Call', Subject = 'Morning Call', ActivityDate = activityDate, Status = 'Completed', WhoId = whoId, WhatId = whatId, Description = 'Had a good call');
    }
    
    public virtual T1C_Base__Employee_Away_Date__c makeEmpAwayDate(Id empId, Date startDate, Integer daysAfterStart)
    {
    	return new T1C_Base__Employee_Away_Date__c(T1C_Base__Employee__c = empId, T1C_Base__Start_Date__c = startDate, T1C_Base__End_Date__c = startDate.addDays(daysAfterStart));
    }
    
    public virtual Event makeEvent(Id whoId, Id whatId, DateTime startTime)
    {
    	return new Event(T1C_Base__Interaction_Type__c = 'Call', Subject = 'Morning Call', ActivityDate = startTime.date(), StartDateTime = startTime, DurationInMinutes = 30, WhoId = whoId, WhatId = whatId, Description = 'Had a good call');
    }
    
    public virtual T1C_Base__Call_Report__c makeCallReport(String intType, Id acctId, Date meetingDate)
    {
    	return new T1C_Base__Call_Report__c(T1C_Base__Client__c = acctId, T1C_Base__Interaction_Type__c = intType, T1C_Base__Subject_Meeting_Objectives__c = 'Morning Call', T1C_Base__Meeting_Date__c = meetingDate, T1C_Base__Status__c = 'Completed', T1C_Base__Notes__c = 'Had a good call', T1C_Base__Start_Time__c=Date.today());
    }
    
    public virtual T1C_Base__Call_Report_Attendee_Client__c makeCallReportClientAttendee(Id crId, Id contId)
    {
    	return new T1C_Base__Call_Report_Attendee_Client__c(T1C_Base__Call_Report__c = crId, T1C_Base__Contact__c = contId);
    }
    
    public virtual T1C_Base__Call_Report_Email_Recipient__c makeCallReportEmailRecipient(Id crId, Id empId)
    {
    	return new T1C_Base__Call_Report_Email_Recipient__c(T1C_Base__Call_Report__c = crId, T1C_Base__Employee__c = empId);
    }
    
    public virtual T1C_Base__Call_Report_Attendee_Internal__c makeCallReportInternalAttendee(Id crId, Id empId)
    {
    	return new T1C_Base__Call_Report_Attendee_Internal__c(T1C_Base__Call_Report__c = crId, T1C_Base__Employee__c = empId);
    }
    
    public virtual T1C_Base__Call_Report_Interest_Subject__c makeCallReportInterestSubject(Id crId, Id intSubId)
    {
    	return new T1C_Base__Call_Report_Interest_Subject__c(T1C_Base__Call_Report__c = crId, T1C_Base__Interest_Subject__c = intSubId);
    }
    
    public virtual T1C_Base__Interest_Subject_Type__c makeInterestSubjectType(String istName, Id parentId)
    {
    	return new T1C_Base__Interest_Subject_Type__c(Name = istName, T1C_Base__Parent_Interest_Subject_Type__c = parentId);
    }
    
    public virtual T1C_Base__Interest_Subject__c makeInterestSubject(Id ist, String intSubName, Id parentId)
    {
    	return new T1C_Base__Interest_Subject__c(Name = intSubName, T1C_Base__Parent_Interest_Subject__c = parentId, T1C_Base__Interest_Subject_Type__c = ist);
    }
    
    public virtual T1C_Base__Interest__c makeInterest(Id acctId, Id contId, Id istId, Id isId)
    {
    	return new T1C_Base__Interest__c(T1C_Base__Account__c = acctId, T1C_Base__Contact__c = contId, T1C_Base__Interest_Subject_Type__c = istId, T1C_Base__Interest_Subject__c = isId);
    }
    
    public virtual T1C_Base__Organization_Structure__c makeOrgStructureSpecRTId(String name, Id rtId)
    {
    	return new T1C_Base__Organization_Structure__c(RecordTypeId = rtId, Name = name);
    }
    
    public virtual T1C_Base__Organization_Structure__c makeOrgStructureWithLOBRT(String name)
    {
    	Id lobRt =  Schema.SObjectType.T1C_Base__Organization_Structure__c.getRecordTypeInfosByName().get('LOB').getRecordTypeId();
				
    	return new T1C_Base__Organization_Structure__c(RecordTypeId = lobRt, Name = name);
    }
    
    public virtual T1C_Base__Product__c makeProduct(String name, Id orgStructureId)
    {
    	return new T1C_Base__Product__c(Name = name, T1C_Base__LOB__c = orgStructureId);
    }
    
    public virtual T1C_Base__Deal__c makeDeal(String name, String alias, String stage, Id acctId)
    {
    	return new T1C_Base__Deal__c(T1C_Base__Account__c = acctId, T1C_Base__Total_Actual_Revenue__c = 100000,T1C_Base__Project_Alias__c = alias, T1C_Base__Deal_Name__c = name, Name = name, T1C_Base__Stage__c = stage);
    }
    
    public virtual T1C_Base__Deal_Contact__c makeDealContact(Id dealId, Id contactId, Integer orderNumber, String role)
    {
    	return new T1C_Base__Deal_Contact__c(T1C_Base__Deal__c = dealId, T1C_Base__Contact__c = contactId, T1C_Base__Order__c = orderNumber, T1C_Base__Role__c = role);
    }
    
    public virtual T1C_Base__Deal_Team__c makeDealTeamMember(Id dealId, Id empId, Integer orderBy, String role)
    {
    	return new T1C_Base__Deal_Team__c(T1C_Base__Deal__c = dealId, T1C_Base__Employee__c = empId, T1C_Base__Order__c = orderBy, T1C_Base__Deal_Team_Role__c = role, T1C_Base__Inactive__c = false);
    }
    
    public virtual T1C_Base__Deal_Product__c makeDealProduct(Id dealId, String status, Id productId)
    {
    	return new T1C_Base__Deal_Product__c(T1C_Base__Deal__c = dealId, T1C_Base__Status__c = status, T1C_Base__Product__c = productId);
    } 
    
    public virtual T1C_Base__Deal_Product_Team__c makeDealProductTeam(Id dealProductId, Boolean isPrimary, Id dealTeamMemberId)
    {
    	return new T1C_Base__Deal_Product_Team__c(T1C_Base__Deal_Product_ID__c = dealProductId, T1C_Base__Is_Primary__c = isPrimary, T1C_Base__Deal_Team_Member_ID__c = dealTeamMemberId);
    }
    
    public virtual T1C_Base__Address__c makeAddress(String name, Id acctId)
    {
    	return new T1C_Base__Address__c(T1C_Base__Account__c = acctId, T1C_Base__Address_Name__c = name, T1C_Base__Street__c = '537 Paper Street', T1C_Base__City__c = 'Bradford', T1C_Base__Country__c = 'United States', T1C_Base__State_Province__c = 'Delaware', T1C_Base__Zip_Postal_Code__c = '19808');
    }
    
    public virtual T1C_Base__Contact_Phone_Number__c makeContactPhoneNumber(Id contId, String numberType, String value)
    {
    	return new T1C_Base__Contact_Phone_Number__c(T1C_Base__Contact__c = contId , T1C_Base__Type__c = numberType, T1C_Base__Data__c = value);
    }
    
    // Sharing Framework AFR Loading 
    public virtual void createCBSAFR()
    {
    	T1C_FR__Feature__c ace = new T1C_FR__Feature__c(Name = 'ACE',T1C_FR__Name__c = 'ACE');
    	T1C_FR__Feature__c Core = new T1C_FR__Feature__c(Name = 'Core', T1C_FR__Name__c = 'ACE.Core');
        T1C_FR__Feature__c AceShareUtil = new T1C_FR__Feature__c(Name = 'AceShareUtil', T1C_FR__Name__c = 'ACE.Core.AceShareUtil');
        T1C_FR__Feature__c Objects = new T1C_FR__Feature__c(Name = 'Objects', T1C_FR__Name__c = 'ACE.Core.AceShareUtil.Objects');
        T1C_FR__Feature__c acctObj = new T1C_FR__Feature__c(Name = 'Account', T1C_FR__Name__c = 'ACE.Core.AceShareUtil.Objects.Account');
        T1C_FR__Feature__c acctSetting = new T1C_FR__Feature__c(Name = 'Account', T1C_FR__Name__c = 'ACE.Core.AceShareUtil.Objects.Account.Account');
        T1C_FR__Feature__c contObj = new T1C_FR__Feature__c(Name = 'Contact', T1C_FR__Name__c = 'ACE.Core.AceShareUtil.Objects.Contact');
        T1C_FR__Feature__c contSetting = new T1C_FR__Feature__c(Name = 'Contact', T1C_FR__Name__c = 'ACE.Core.AceShareUtil.Objects.Contact.Contact');        
        T1C_FR__Feature__c crObj = new T1C_FR__Feature__c(Name = 'T1C_Base__Call_Report__c', T1C_FR__Name__c = 'ACE.Core.AceShareUtil.Objects.T1C_Base__Call_Report__c');
        T1C_FR__Feature__c crSetting = new T1C_FR__Feature__c(Name = 'T1C_Base__Call_Report__c', T1C_FR__Name__c = 'ACE.Core.AceShareUtil.Objects.T1C_Base__Call_Report__c.T1C_Base__Call_Report__c');        
        T1C_FR__Feature__c dealObj = new T1C_FR__Feature__c(Name = 'T1C_Base__Deal__c', T1C_FR__Name__c = 'ACE.Core.AceShareUtil.Objects.T1C_Base__Deal__c');
        T1C_FR__Feature__c dealSetting = new T1C_FR__Feature__c(Name = 'T1C_Base__Deal__c', T1C_FR__Name__c = 'ACE.Core.AceShareUtil.Objects.T1C_Base__Deal__c.T1C_Base__Deal__c');        
        T1C_FR__Feature__c teamCCObj = new T1C_FR__Feature__c(Name = 'T1C_Base__Team_Contact_Coverage__c', T1C_FR__Name__c = 'ACE.Core.AceShareUtil.Objects.T1C_Base__Team_Contact_Coverage__c');
        T1C_FR__Feature__c teamCCSetting = new T1C_FR__Feature__c(Name = 'T1C_Base__Team_Contact_Coverage__c', T1C_FR__Name__c = 'ACE.Core.AceShareUtil.Objects.T1C_Base__Team_Contact_Coverage__c.T1C_Base__Team_Contact_Coverage__c');                
        T1C_FR__Feature__c teamACObj = new T1C_FR__Feature__c(Name = 'T1C_Base__Deal__c', T1C_FR__Name__c = 'ACE.Core.AceShareUtil.Objects.T1C_Base__Team_Account_Coverage__c');
        T1C_FR__Feature__c teamACSetting = new T1C_FR__Feature__c(Name = 'T1C_Base__Deal__c', T1C_FR__Name__c = 'ACE.Core.AceShareUtil.Objects.T1C_Base__Team_Account_Coverage__c.T1C_Base__Team_Account_Coverage__c');        
        
        upsert new list<T1C_FR__Feature__c>{ace,Core,AceShareUtil,Objects,acctObj,acctSetting,contObj,contSetting,crObj,crSetting,dealObj,dealSetting,teamCCObj,teamCCSetting,teamACObj,teamACSetting};
                
		insert new list<T1C_FR__Attribute__c>
		{								           
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = acctSetting.Id, Name = 'AccessLevel', T1C_FR__Value__c = 'Edit'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = acctSetting.Id, Name = 'BatchOnly', T1C_FR__Value__c = 'false'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = acctSetting.Id, Name = 'FilterClause', T1C_FR__Value__c = ''),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = acctSetting.Id, Name = 'ParentCoverageFilterClause', T1C_FR__Value__c = ''),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = acctSetting.Id, Name = 'ParentLookupField', T1C_FR__Value__c = 'Id'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = acctSetting.Id, Name = 'ParentObjectType', T1C_FR__Value__c = 'Account'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = acctSetting.Id, Name = 'RowCause', T1C_FR__Value__c = 'Manual'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = acctSetting.Id, Name = 'ObjectName', T1C_FR__Value__c = 'Account'),
           
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = contSetting.Id, Name = 'AccessLevel', T1C_FR__Value__c = 'Edit'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = contSetting.Id, Name = 'BatchOnly', T1C_FR__Value__c = 'false'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = contSetting.Id, Name = 'FilterClause', T1C_FR__Value__c = ''),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = contSetting.Id, Name = 'ParentCoverageFilterClause', T1C_FR__Value__c = ''),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = contSetting.Id, Name = 'ParentLookupField', T1C_FR__Value__c = 'AccountId'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = contSetting.Id, Name = 'ParentObjectType', T1C_FR__Value__c = 'Account'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = contSetting.Id, Name = 'RowCause', T1C_FR__Value__c = 'Manual'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = contSetting.Id, Name = 'ObjectName', T1C_FR__Value__c = 'Contact'),
           
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = crSetting.Id, Name = 'AccessLevel', T1C_FR__Value__c = 'Edit'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = crSetting.Id, Name = 'BatchOnly', T1C_FR__Value__c = 'false'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = crSetting.Id, Name = 'FilterClause', T1C_FR__Value__c = ''),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = crSetting.Id, Name = 'ParentCoverageFilterClause', T1C_FR__Value__c = ''),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = crSetting.Id, Name = 'ParentLookupField', T1C_FR__Value__c = 'T1C_Base__Client__c'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = crSetting.Id, Name = 'ParentObjectType', T1C_FR__Value__c = 'Account'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = crSetting.Id, Name = 'RowCause', T1C_FR__Value__c = 'Account_Coverage__c'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = crSetting.Id, Name = 'ObjectName', T1C_FR__Value__c = 'T1C_Base__Call_Report__c'),
           
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = dealSetting.Id, Name = 'AccessLevel', T1C_FR__Value__c = 'Read'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = dealSetting.Id, Name = 'BatchOnly', T1C_FR__Value__c = 'false'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = dealSetting.Id, Name = 'FilterClause', T1C_FR__Value__c = ''),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = dealSetting.Id, Name = 'ParentCoverageFilterClause', T1C_FR__Value__c = ''),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = dealSetting.Id, Name = 'ParentLookupField', T1C_FR__Value__c = 'T1C_Base__Account__c'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = dealSetting.Id, Name = 'ParentObjectType', T1C_FR__Value__c = 'Account'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = dealSetting.Id, Name = 'RowCause', T1C_FR__Value__c = 'Account_Coverage__c'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = dealSetting.Id, Name = 'ObjectName', T1C_FR__Value__c = 'T1C_Base__Call_Report__c'),
           
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = teamCCSetting.Id, Name = 'AccessLevel', T1C_FR__Value__c = 'Edit'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = teamCCSetting.Id, Name = 'BatchOnly', T1C_FR__Value__c = 'false'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = teamCCSetting.Id, Name = 'FilterClause', T1C_FR__Value__c = ''),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = teamCCSetting.Id, Name = 'ParentCoverageFilterClause', T1C_FR__Value__c = ''),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = teamCCSetting.Id, Name = 'ParentLookupField', T1C_FR__Value__c = 'T1C_Base__Contact__r.AccountId'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = teamCCSetting.Id, Name = 'ParentObjectType', T1C_FR__Value__c = 'Account'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = teamCCSetting.Id, Name = 'RowCause', T1C_FR__Value__c = 'Account_Coverage__c'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = teamCCSetting.Id, Name = 'ObjectName', T1C_FR__Value__c = 'T1C_Base__Team_Contact_Coverage__c'),
           
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = teamACSetting.Id, Name = 'AccessLevel', T1C_FR__Value__c = 'Edit'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = teamACSetting.Id, Name = 'BatchOnly', T1C_FR__Value__c = 'false'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = teamACSetting.Id, Name = 'FilterClause', T1C_FR__Value__c = ''),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = teamACSetting.Id, Name = 'ParentCoverageFilterClause', T1C_FR__Value__c = ''),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = teamACSetting.Id, Name = 'ParentLookupField', T1C_FR__Value__c = 'T1C_Base__Account__c'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = teamACSetting.Id, Name = 'ParentObjectType', T1C_FR__Value__c = 'Account'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = teamACSetting.Id, Name = 'RowCause', T1C_FR__Value__c = 'Account_Coverage__c'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = teamACSetting.Id, Name = 'ObjectName', T1C_FR__Value__c = 'T1C_Base__Team_Account_Coverage__c')
      	};
    }
}