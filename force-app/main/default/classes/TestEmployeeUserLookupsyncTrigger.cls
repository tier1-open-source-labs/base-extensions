/* 
 * Name: TestEmployeeUserLookupsyncTrigger 
 * Description: Contains testMethods for EmployeeUserLookupsync trigger.
 *
 * Confidential & Proprietary, 2017 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1CRM.
 * 
 *	PS Base 3.7.6
 *
 */

@isTest (seeAllData=false)
private class TestEmployeeUserLookupsyncTrigger 
{
    private static final Tier1CRMTestDataFactory dataFactory = Tier1CRMTestDataFactorySetup.getDataFactory();

    static T1C_Base__Employee__c emp = dataFactory.makeLoggedInUserEmployee();
    static T1C_Base__Organization_Structure__c[] osList;
    static T1C_Base__Product__c[] prodList;
	
	static testMethod void employeeUserLookupsyncTest()
    {
        setupTestData ();

   		Test.startTest();
		
		insert emp;
		
    	emp.T1C_Base__LOB__c = osList[0].Id;
    	emp.T1C_Base__Product_Lookup__c = prodList[0].Id;
    	update emp;
    	
    	emp.T1C_Base__LOB__c = osList[1].Id;
    	emp.T1C_Base__Product_Lookup__c = prodList[1].Id;
    	update emp;
    	
    	emp.T1C_Base__LOB__c = null;
    	emp.T1C_Base__Product_Lookup__c = null;
    	update emp;
    	
       	Test.stopTest();
    }
    private static void setupTestData () 
    {
        dataFactory.createCBSAFR();
        	
    	//Setup All the AFR that's needed
	    String core = ':CORE';
	    list<T1C_FR__Feature__c> featList = new list<T1C_FR__Feature__c>{};
	    T1C_FR__Feature__c ACEExtensions = new T1C_FR__Feature__c(Name = 'Extensions',T1C_FR__Name__c = 'ACE.Extensions', T1C_FR__External_Id__c = 'ACE.Extensions' + core);
	    T1C_FR__Feature__c EmployeeLookupUserSync = new T1C_FR__Feature__c(Name = 'EmployeeLookupUserSync',T1C_FR__Name__c = 'ACE.Extensions.EmployeeLookupUserSync', T1C_FR__External_Id__c = 'ACE.Extensions.EmployeeLookupUserSync' + core);
	    T1C_FR__Feature__c LOB = new T1C_FR__Feature__c(Name = 'LOB',T1C_FR__Name__c = 'ACE.Extensions.EmployeeLookupUserSync.LOB', T1C_FR__External_Id__c = 'ACE.Extensions.EmployeeLookupUserSync.LOB' + core);
	    T1C_FR__Feature__c Product = new T1C_FR__Feature__c(Name = 'Product',T1C_FR__Name__c = 'ACE.Extensions.EmployeeLookupUserSync.Product', T1C_FR__External_Id__c = 'ACE.Extensions.EmployeeLookupUserSync.Product' + core);
	     
	    featList.add(ACEExtensions);
	    featList.add(EmployeeLookupUserSync);
	    featList.add(LOB);
	    featList.add(Product);
	    upsert featList;

	    list<T1C_FR__Attribute__c> attributes = new list<T1C_FR__Attribute__c>();
	    attributes.add(new T1C_FR__Attribute__c(T1C_FR__Feature__c = LOB.Id,Name = 'EmployeeField', T1C_FR__Value__c = 'T1C_Base__LOB__r.Name', T1C_FR__User_Overridable__c = true));
	    attributes.add(new T1C_FR__Attribute__c(T1C_FR__Feature__c = LOB.Id,Name = 'UserField', T1C_FR__Value__c = 'T1C_Base__Telephony_Password__c',T1C_FR__User_Overridable__c = true));
	    attributes.add(new T1C_FR__Attribute__c(T1C_FR__Feature__c = Product.Id,Name = 'EmployeeField', T1C_FR__Value__c = 'T1C_Base__Product_Lookup__r.Name',T1C_FR__User_Overridable__c = true));
	    attributes.add(new T1C_FR__Attribute__c(T1C_FR__Feature__c = Product.Id,Name = 'UserField', T1C_FR__Value__c = 'T1C_Base__Telephony_Username__c',T1C_FR__User_Overridable__c = true));
	    insert attributes;

		Map <String,Schema.RecordTypeInfo> recordTypeMapOS = T1C_Base__Organization_Structure__c.sObjectType.getDescribe().getRecordTypeInfosByName();
        Id recTypeId = recordTypeMapOS.get('LOB').getRecordTypeId();
			
		osList = new T1C_Base__Organization_Structure__c[]{};
		osList.add(dataFactory.makeOrgStructureSpecRTId('Derivatives', recTypeId));
		osList.add(dataFactory.makeOrgStructureSpecRTId('Research', recTypeId));
		insert osList;

		prodList = new T1C_Base__Product__c[]{};
		prodList.add(dataFactory.makeProduct('Prod1', null));
		prodList.add(dataFactory.makeProduct('Prod2', null));       
		insert prodList;
    }
}