/** 
 * Name: TestEncodeCIACredentialsBefore 
 * Description: 
 *
 * Confidential & Proprietary, 2019 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1CRM.
 *
 *	PS Base 3.7.6
 *
 */
@isTest
private class TestEncodingCIACredentialsBefore 
{ 
    static testMethod void myUnitTest() 
    {       
        String userId = UserInfo.getUserId();
        User u = [SELECT T1C_Base__Telephony_Username__c, T1C_Base__Telephony_Password__c FROM User WHERE Id = :userId];
        u.T1C_Base__Telephony_Username__c = 'abc123';
        u.T1C_Base__Telephony_Password__c = 'abc123';
        update u;

        u = [SELECT T1C_Base__Telephony_Username__c, T1C_Base__Telephony_Password__c FROM User WHERE Id = :userId];
        System.assertNotEquals(u.T1C_Base__Telephony_Username__c, 'abc123');
        System.assertNotEquals(u.T1C_Base__Telephony_Username__c, 'abc123');
    }
}