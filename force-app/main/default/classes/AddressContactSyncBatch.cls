/**
 * Name: AddressContactSyncBatch 
 * Description: Batch class that is designed to post-sync updated addresses.
 *
 * Confidential & Proprietary, 2019 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1CRM.
 *
 *	PS Base 3.7.6
 * 
 */
global without sharing class AddressContactSyncBatch implements Database.batchable<SObject>, Database.Stateful
{
   public Map<Id, T1C_Base__Address__c> addressMap;
   public Map<Id, List<T1C_Base__Address__c>> accountAddressesMap;
   
   //Do nothing in the constructor. The records are already marked for Syncing, so just pulling them up is enough
   global AddressContactSyncBatch()
   {
      
   }
    
   global Database.QueryLocator start(Database.BatchableContext bc) 
   {
      Set<Id> accountIDs = new Set<Id>();
      
      this.accountAddressesMap = new Map<Id, List<T1C_Base__Address__c>>();
      this.addressMap = new Map<Id, T1C_Base__Address__c>();
      
      //Get the contacts that need syncing based on their accounts which are identified by the addresses
      //This step is necessary since newly created contacts will be unlinked. We need to try and link them as well
      
      for(T1C_Base__Address__c addr : [SELECT Id, 
                                              T1C_Base__Street__c,
                                              T1C_Base__City__c,
                                              T1C_Base__Country__c,
                                              T1C_Base__State_Province__c,
                                              T1C_Base__Zip_Postal_Code__c,
                                              T1C_Base__Account__c
                                         FROM T1C_Base__Address__c
                                        WHERE Needs_Sync__c=true Limit 10000])
      {
         accountIDs.add(addr.T1C_Base__Account__c);

         addressMap.put(addr.Id, addr);
         
         //populate the address list map for this address' account. This is needed for the contact linking step
         
         List<T1C_Base__Address__c> acntAddrs = this.accountAddressesMap.get(addr.T1C_Base__Account__c);
         
         if(acntAddrs == null)
         {
            acntAddrs = new List<T1C_Base__Address__c>();
         }
         
         acntAddrs.add(addr);
         
         this.accountAddressesMap.put(addr.T1C_Base__Account__c, acntAddrs);
      }
      
      String query = 'SELECT Id, ' +
                            'Name, ' +
                            'AccountId,' +
                            'T1C_Base__Mailing_Address_Id__c, ' +
                            'MailingStreet, ' +
                            'MailingCity, ' +
                            'MailingCountry, ' +
                            'MailingState, ' +
                            'MailingPostalCode ' +
                       'FROM CONTACT ' +
                      'WHERE AccountId in :accountIDs';

      return Database.getQueryLocator(query);
   }
   
   global void execute(Database.BatchableContext BC, List<Contact> scope)
   {
      
      /*
      'T1C_Base__Street__c'         , 'MailingStreet'
      'T1C_Base__City__c'           , 'MailingCity'
      'T1C_Base__Country__c'        , 'MailingCountry'
      'T1C_Base__State_Province__c' , 'MailingState'
      'T1C_Base__Zip_Postal_Code__c', 'MailingPostalCode'
      */
      
      //N^2 on the else very ugly
	   for(Contact ct : scope)
	   {
         //If the contact already has a mailing address Id, then grab it from the address map and update the contact
         if(ct.get('T1C_Base__Mailing_Address_Id__c') != null)
         {
            T1C_Base__Address__c contactAddress = this.addressMap.get( (Id)ct.get('T1C_Base__Mailing_Address_Id__c') );
            
            AddressSyncTriggerHelper.applyAddressChange(contactAddress, ct, AddressSyncTriggerHelper.contactFieldsMap);
            
         }else
         {
            Integer currentScore = 4;
            //Try to match this contact using its Address fields to an existing Address record for this account
            for( T1C_Base__Address__c accntAddr : this.accountAddressesMap.get(ct.AccountId) )
            {
               //Match postal code first. This is the most definitive match when trying to figure out the address
               if(accntAddr.T1C_Base__Zip_Postal_Code__c == ct.MailingPostalCode)
               {
                  ct.T1C_Base__Mailing_Address_Id__c = accntAddr.Id;
                  break;
               }else
               {
                  //We're going to try to minimize here, so if we cross the initial threshold of 4 then we pick it as the current winner
                  
                  Integer compScore = compareAddress(ct, accntAddr);
                  if(compScore < currentScore )
                  {
                     currentScore = compScore;
                     ct.T1C_Base__Mailing_Address_Id__c = accntAddr.Id;
                  }
               }
            }
         }
	   }
      
      update scope;
    
   }
   
   global static Integer compareAddress(Contact ct, T1C_Base__Address__c accntAddr)
   {
      
      //combine the address fields into a string and take Levenshtein distance and do a fuzzy match
      //Replace St. with Street and do the same with other road identifiers
      
      //Delimit the components with ;
      String contactAddrStr =  ct.MailingStreet + ';' + ct.MailingCity + ';' + ct.MailingCountry + ';' + ct.MailingState + ';' + ct.MailingPostalCode;
      
      String addrString     = accntAddr.T1C_Base__Street__c  + ';' + accntAddr.T1C_Base__City__c + ';' +
                             accntAddr.T1C_Base__Country__c + ';' +  accntAddr.T1C_Base__State_Province__c + ';' + accntAddr.T1C_Base__Zip_Postal_Code__c;

      contactAddrStr = contactAddrStr.toLowerCase();
      addrString     = addrString.toLowerCase();

      
      return addrString.getLevenshteinDistance(contactAddrStr);
      
   }
   
   
   global void finish(Database.BatchableContext BC)
   {
      //Unmark the addresses
      for( T1C_Base__Address__c adr : addressMap.values() )
      {
         adr.Needs_Sync__c=false;
      }
      
      update addressMap.values();
      
      String jobName = T1C_FR.FeatureCacheWebSvc.getAttribute( UserInfo.getUserId(), AddressManagerController.ADDRESS_MANAGER_FEATURE_PATH, 'AddressBatchAfterFinishJob' );
      //Now Check to see if we have a followup batch based on the AFRs
      if(String.isBlank(jobName) == false)
      {
         Type t = Type.forName(jobName);
         if(t != null)
         {
            try{
               String sbId = Database.executeBatch((Database.Batchable<sObject>)(t.newInstance()));
            }
            catch (Exception ex){
               system.debug(LoggingLevel.ERROR, 'Error: ' + ex.getMessage());
            }
         }
      }
   }
}