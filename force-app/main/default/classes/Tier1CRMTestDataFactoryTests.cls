/** 
 * Name: Tier1CRMTestDataFactoryTests 
 *
 * Confidential & Proprietary, 2018 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1CRM.
 * 
 *	PS Base 3.7.6
 *
 */
 
@isTest
private class Tier1CRMTestDataFactoryTests 
{
	private static final Tier1CRMTestDataFactory dataFactory = Tier1CRMTestDataFactorySetup.getDataFactory();

    static testMethod void dataFactoryTest() 
    {
    	dataFactory.createCBSAFR();
        
		Account acct = dataFactory.makeAccount('Paper Street Soap Company');
		insert acct;
		
		Contact cont = dataFactory.makeContact(acct.Id, 'Marla', 'Singer');
		insert cont;
		
		T1C_Base__Employee__c emp = dataFactory.makeLoggedInUserEmployee();
		insert emp;
				
		dataFactory.setupCallReport(acct.Id, cont.Id, emp.Id);
		T1C_Base__Interest_Subject_Type__c ist = dataFactory.makeInterestSubjectType('Ticker', null);
		insert ist;
		T1C_Base__Interest_Subject__c intSub = dataFactory.makeInterestSubject(ist.Id, 'Paper', null);
		insert intSub;
		T1C_Base__Interest__c intrest = dataFactory.makeInterest(acct.Id, cont.Id, ist.Id, intSub.Id);
		
		T1C_Base__Account_Coverage__c ac = dataFactory.makeAccountCoverage(acct.Id, emp.Id);
		T1C_Base__Contact_Coverage__c cc = dataFactory.makeContactCoverage(cont.Id, emp.Id);
		Task tsk = dataFactory.makeTask(cont.Id, acct.Id, Date.today());
		Event evt = dataFactory.makeEvent(cont.Id, acct.Id, DateTime.now());
		T1C_Base__Employee_Away_Date__c empAway = dataFactory.makeEmpAwayDate(emp.Id, Date.today(), 5);
		T1C_Base__Call_Report__c cr = dataFactory.makeCallReport('Meeting', acct.Id, Date.today());
		insert cr;
		
		T1C_Base__Call_Report_Attendee_Client__c crac = dataFactory.makeCallReportClientAttendee(cr.Id, cont.Id);
		T1C_Base__Call_Report_Email_Recipient__c crer = dataFactory.makeCallReportEmailRecipient(cr.Id, emp.Id);
		T1C_Base__Call_Report_Attendee_Internal__c crai = dataFactory.makeCallReportInternalAttendee(cr.Id, emp.Id);
		T1C_Base__Call_Report_Interest_Subject__c  cris = dataFactory.makeCallReportInterestSubject(cr.Id, intSub.Id);
		
		T1C_Base__Organization_Structure__c orgStructure = dataFactory.makeOrgStructureWithLOBRT('Equities');
		insert orgStructure;
		T1C_Base__Product__c prod = dataFactory.makeProduct('Soap', orgStructure.Id);
		insert prod;
		T1C_Base__Deal__c deal = dataFactory.makeDeal('Project Mayhem', 'Project Mayhem', 'Initail', acct.Id);
		insert deal;
		T1C_Base__Deal_Contact__c dc = dataFactory.makeDealContact(deal.Id, cont.Id, 1, 'Primary');
		T1C_Base__Deal_Team__c dt = dataFactory.makeDealTeamMember(deal.Id, emp.Id, 1, 'Lead Banker');
		T1C_Base__Deal_Product__c dealProd = dataFactory.makeDealProduct(deal.Id, 'Open', prod.Id);
		T1C_Base__Address__c addr = dataFactory.makeAddress('Head Office', acct.Id);
		T1C_Base__Contact_Phone_Number__c cpn = dataFactory.makeContactPhoneNumber(cont.Id, 'Business', '4169324560');
    }
}