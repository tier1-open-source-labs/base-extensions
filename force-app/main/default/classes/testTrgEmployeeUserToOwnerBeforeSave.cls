/* 
 * Name: testTrgEmployeeUserToOwnerBeforeSave 
 * Description: Test
 *					- employeeUserToOwnerBeforeSave
 *
 * Confidential & Proprietary, 2012 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1CRM.
 * 
 *	PS Base 3.7.6
 *
 */
@isTest (seeAllData=false)
private class testTrgEmployeeUserToOwnerBeforeSave 
{
	static User[] userList;
	static T1C_Base__Employee__c[] emplList;
	
	static final String TEST_USER_FNAME = 'Tesstte';
	static final String TEST_USER0_UNAME = 'testuser10@tier1crm.com';
	static final String TEST_USER0_LNAME = 'TestUser10';
	static final String TEST_USER0_ALIAS = 'tusr10';
	static final String TEST_USER_EMAIL = 'testuser10@tier1crm.com';
	static final String TEST_USER_TZ_SID_KEY = 'America/New_York';
	static final String TEST_USER_LOCALE_SID_KEY = 'en_CA';
	static final String TEST_USER_LANG_LOCALE_KEY = 'en_US';
	static final String TEST_USER_EMAIL_ENCODING_KEY = 'ISO-8859-1';
	static final String TEST_USER_DEPARTMENT = 'TestDept';
	static final String TEST_USER_PHONE = '(123) 123-1234';
	static final String TEST_USER_TITLE = 'Mrs.';
	
    static testMethod void myUnitTest() 
    {
    	//create a new users
		Test.startTest();
    	userList = testTrgEmployeeUserToOwnerBeforeSave.createTestUsers(2);
		Test.stopTest();
		
    	System.runAs(userList[0])
        {
	    	//create new employees
			emplList = T1C_Base.AceTestUtil.getTestEmployees ( userList , True );
	        
	        System.debug(emplList);
	        System.debug(emplList[0].T1C_Base__User__r.IsActive);
	        System.debug(emplList[0].T1C_Base__User__r.Name);
	        System.debug(emplList[0].OwnerId);
        }
    }
    
    public static User[] createTestUsers (Integer cnt) 
    {
		Set<Id> uIds = new Set<Id>{};
		User u = new User ( Username = TEST_USER0_UNAME ,
							FirstName = TEST_USER_FNAME ,
							LastName = TEST_USER0_LNAME ,
							Alias = TEST_USER0_ALIAS ,
							Email = TEST_USER_EMAIL ,
							IsActive = True ,
							Department = TEST_USER_DEPARTMENT ,
							MobilePhone = TEST_USER_PHONE ,
							Title = TEST_USER_TITLE ,		
							TimeZoneSidKey = TEST_USER_TZ_SID_KEY ,
							LocaleSidKey = TEST_USER_LOCALE_SID_KEY ,
							LanguageLocaleKey = TEST_USER_LANG_LOCALE_KEY ,
							EmailEncodingKey = TEST_USER_EMAIL_ENCODING_KEY ,
							ProfileId = UserInfo.getProfileId () ,
							UserRoleId = UserInfo.getUserRoleId ()
						); 
		System.runAs ( u ) {}
		
		uIds.add(u.Id);
		
		for (Integer i = 0 ; i < cnt - 1 ; i++ ) 
		{		
			User newUser = u.clone ( False , True );
			newUser.Username = i+ 'testuser0@tier1crm.com' ;
			newUser.LastName = 'TestUser0' + i;
			newUser.Alias = 'tusr0' + i;

			System.runAs ( newUser ) {}
			uIds.add(newUser.Id);
		}
		
		//Select the Users back out of the DB to grab their Name fields.
		Map<Id , User> userById = new Map<Id , User> (
				 [ select
						Department ,
						Email ,
						FirstName ,
						IsActive ,
						LastName ,
						MobilePhone ,
						Name ,
						Phone ,
						Title , 
						UserRoleId
					from
						User
					where
						Id in :uIds 
					]
				);

		return userById.values() ;					
	}
}