/**
    This class contains unit tests for validating the behavior of Apex triggers:
    
    AccountFormatPhoneNos
    AEMVenueFormatPhoneNos
    ContactFormatPhoneNos
    ContactPhoneNumberFormatPhoneNos
    EmployeeFormatPhoneNos
    UserFormatPhoneNos
    
    Confidential & Proprietary, 2015 Tier1CRM Inc.
    Property of Tier1CRM Inc.
    This document shall not be duplicated, transmitted or used in whole
    or in part without written permission from Tier1CRM.
	
 * 
 *	PS Base 3.7.6
 *
 */
@isTest(seeAllData=false)
private class TestFormatPhoneNos 
{ 
    private static final Tier1CRMTestDataFactory dataFactory = Tier1CRMTestDataFactorySetup.getDataFactory();

    static testMethod void myUnitTest() 
    {       
        dataFactory.createCBSAFR();
            
        Account ac1 = dataFactory.makeAccount('test1');
        ac1.Phone = '949243871';
        insert ac1;
        
        Contact c1 = dataFactory.makeContact(ac1.Id, 'testFcontact', 'test1');
        c1.Phone = '949243871';
        insert c1;
        
        T1C_Base__Contact_Phone_Number__c cpn = dataFactory.makeContactPhoneNumber(c1.id, 'Home', '949243871');
        insert cpn;
        
        T1C_Base__Employee__c emp = dataFactory.makeLoggedInUserEmployee();
		emp.T1C_Base__Mobile__c = '949243871';
        insert emp;
    }
}