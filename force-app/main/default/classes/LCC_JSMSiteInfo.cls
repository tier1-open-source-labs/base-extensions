/** 
 *  SFDC suto-generated class required to launch post-merge utility that replaces Javascript buttons.
 *	PS Base 3.7.6
 *
 */

public class LCC_JSMSiteInfo {
    @AuraEnabled
    public String Prefix {get;set;}
    @AuraEnabled
    public String Name {get;set;}
    @AuraEnabled
    public String Domain {get;set;}
}