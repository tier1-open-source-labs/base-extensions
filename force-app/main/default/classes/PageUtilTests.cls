/** 
 * Name: PageUtilTests 
 * Description: Tests for PageUtil
 *
 * Confidential & Proprietary, 2019 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1CRM.
 * 
 *	PS Base 3.7.6
 *
 */
@isTest 

public with sharing class PageUtilTests 
{
    private static final Tier1CRMTestDataFactory dataFactory = Tier1CRMTestDataFactorySetup.getDataFactory();

    static testMethod void pageUtilTests() 
    {
        dataFactory.createCBSAFR();

        Account acct = dataFactory.makeAccount('Paper Street Soap Company');
        insert acct;

        PageUtil pgUtil = new PageUtil();
        
        PageReference pageRef = new ApexPages.StandardController(acct).view();

        try
        {
            insert acct;
        }
        catch(Exception e)
        {
            PageUtil.addExceptionMessage(null, ApexPages.Severity.FATAL, e);
            PageUtil.addExceptionMessage('msg001',e);
            PageUtil.addExceptionMessage(e);
            PageUtil.addProgrammingError('msg001',e,'Fail');
            PageUtil.addProgrammingError('msg001',e);
            PageUtil.addRequestMessage('message');
            STring message = PageUtil.getRequestMessages();
        }
    }
}