/** 
 *  SFDC suto-generated class required to launch post-merge utility that replaces Javascript buttons.
 *	PS Base 3.7.6
 *
 */

public class LCC_JSMUserInfo{
    @AuraEnabled
    public String Id {get;set;}
    @AuraEnabled
    public String FirstName {get;set;}
    @AuraEnabled
    public String LastName {get;set;}
    @AuraEnabled
    public String UserEmail {get;set;}
    @AuraEnabled
    public String Name {get;set;}
    @AuraEnabled
    public String UserName {get;set;}
}