/**
 * Name: SObjectUtilTests
 * Description: SObjectUtilTests
 *
 * Confidential & Proprietary, 2016 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1CRM.
 * 
 *	PS Base 3.7.6
 *
 */
 
 @isTest
public class SObjectUtilTests 
{
	private static final Tier1CRMTestDataFactory dataFactory = Tier1CRMTestDataFactorySetup.getDataFactory();

	static Account acct;
	static Contact cont;
	static T1C_Base__Organization_Structure__c lob;
	static T1C_Base__Product__c product;	
	static T1C_Base__Employee__c emp;

	
	static testMethod void stringSObjectUtilTests() 
	{
		string testString;
		testString = SObjectUtil.getStringValue(Date.today(), 'EEEE');
		System.assert(testString == DateTime.now().format('EEEE'));
		
		testString = SObjectUtil.getStringValue(DateTime.now(), null);
		System.assert(testString == null);
		
		testString = SObjectUtil.getStringValue(DateTime.now(), 'EEEE');
		System.assert(testString == DateTime.now().format('EEEE'));
		
		testString = SObjectUtil.getStringValue(1);
		System.assert(testString == '1');
		
		testString = SObjectUtil.getStringValue(1.11, 2, '$');
		System.assert(testString == '$1.11');
		
		testString = SObjectUtil.getStringValue(1.11, 2, null);
		System.assert(testString == '1.11');
		
		testString = SObjectUtil.getImageUrl('1234567890');
		System.assert(testString != null);
	}
    
    static testMethod void getStaticResourceTest() 
    {
    	String core = ':CORE';
		
		T1C_FR__Feature__c ACE = new T1C_FR__Feature__c(Name = 'ACE',T1C_FR__Name__c = 'ACE', T1C_FR__External_Id__c = 'ACE' + core);
		T1C_FR__Feature__c ACECore = new T1C_FR__Feature__c(Name = 'Core',T1C_FR__Name__c = 'ACE.Core', T1C_FR__External_Id__c = 'ACE.Core' + core);
		T1C_FR__Feature__c TestSearch = new T1C_FR__Feature__c(Name = 'TestSearch',T1C_FR__Name__c = 'ACE.Core.TestSearch', T1C_FR__External_Id__c = 'ACE.Core.TestSearch' + core);
		T1C_FR__Feature__c Columns = new T1C_FR__Feature__c(Name = 'Columns',T1C_FR__Name__c = 'ACE.Core.TestSearch.Columns', T1C_FR__External_Id__c = 'ACE.Core.TestSearch.Columns' + core);
		T1C_FR__Feature__c NameField = new T1C_FR__Feature__c(Name = 'NameField',T1C_FR__Name__c = 'ACE.Core.TestSearch.Columns.NameField', T1C_FR__External_Id__c = 'ACE.Core.TestSearch.Columns.NameField' + core);
		insert new list<T1C_FR__Feature__c>{ACE, ACECORE, TestSearch, Columns,NameField};
		
		list<T1C_FR__Attribute__c> attributes = new list<T1C_FR__Attribute__c>();
		attributes.add(new T1C_FR__Attribute__c(T1C_FR__Feature__c = NameField.Id,Name = 'cssFiles', T1C_FR__Value__c = 'ACEFeatureLoaderBinary', T1C_FR__User_Overridable__c = true));
		attributes.add(new T1C_FR__Attribute__c(T1C_FR__Feature__c = NameField.Id,Name = 'jsFiles', T1C_FR__Value__c = 'ACEFeatureLoaderBinary',T1C_FR__User_Overridable__c = true));
		attributes.add(new T1C_FR__Attribute__c(T1C_FR__Feature__c = NameField.Id,Name = 'zipFile', T1C_FR__Value__c = 'ACEFeatureLoaderBinary',T1C_FR__User_Overridable__c = true));
		attributes.add(new T1C_FR__Attribute__c(T1C_FR__Feature__c = NameField.Id,Name = 'loadStyleSheets', T1C_FR__Value__c = 'true', T1C_FR__User_Overridable__c = true));
		insert attributes;
		
    	String statString = SObjectUtil.getScript('{\"rootFeatureSections\":\"ACE.Core.TestSearch.Columns.NameField\"}');
		System.assert(statString != null);
    }
      
    static testMethod void TestGetCoverageComponents()
    {
    	String core = ':CORE';
		dataFactory.createCBSAFR();
		
		T1C_FR__Feature__c TestSearch = new T1C_FR__Feature__c(Name = 'TestSearch',T1C_FR__Name__c = 'ACE.Core.TestSearch', T1C_FR__External_Id__c = 'ACE.Core.TestSearch' + core);
		T1C_FR__Feature__c NameField = new T1C_FR__Feature__c(Name = 'NameField',T1C_FR__Name__c = 'ACE.Core.TestSearch.NameField', T1C_FR__External_Id__c = 'ACE.Core.TestSearch.NameField' + core);
		T1C_FR__Feature__c Columns = new T1C_FR__Feature__c(Name = 'Columns',T1C_FR__Name__c = 'ACE.Core.TestSearch.NameField.Columns', T1C_FR__External_Id__c = 'ACE.Core.TestSearch.NameField.Columns' + core);
		T1C_FR__Feature__c NameColumn = new T1C_FR__Feature__c(Name = 'NameColumn',T1C_FR__Name__c = 'ACE.Core.TestSearch.NameField.Columns.NameColumn', T1C_FR__External_Id__c = 'ACE.Core.TestSearch.NameField.Columns.NameColumn' + core);
		
		T1C_FR__Feature__c ACECoreAceShareUtilObjectsActivity_Security = new T1C_FR__Feature__c(Name = 'Activity_Security__c', T1C_FR__Name__c = 'ACE.Core.AceShareUtil.Objects.Activity_Security__c');
		T1C_FR__Feature__c ACECoreAceShareUtilObjectsActivity_SecurityActivity_Security = new T1C_FR__Feature__c(Name = 'Activity_Security__c', T1C_FR__Name__c = 'ACE.Core.AceShareUtil.Objects.Activity_Security__c.Activity_Security__c');
	
		insert new list<T1C_FR__Feature__c>{TestSearch,NameField, Columns,NameColumn,ACECoreAceShareUtilObjectsActivity_Security,ACECoreAceShareUtilObjectsActivity_SecurityActivity_Security};
				
		insert new list<T1C_FR__Attribute__c>
		{
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = NameField.Id,Name = 'OrderByColumns', T1C_FR__Value__c = 'Name:Desc', T1C_FR__User_Overridable__c = true),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = NameField.Id,Name = 'AdditionalWhereClause', T1C_FR__Value__c = 'Id != null',T1C_FR__User_Overridable__c = true),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = NameField.Id,Name = 'AdditionalWhereClauseCustom', T1C_FR__Value__c = 'Id != null',T1C_FR__User_Overridable__c = true),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = NameColumn.Id,Name = 'FieldType', T1C_FR__Value__c = 'String', T1C_FR__User_Overridable__c = true),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = NameColumn.Id,Name = 'LookupObject', T1C_FR__Value__c = 'Account', T1C_FR__User_Overridable__c = true),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = NameColumn.Id,Name = 'FieldLabel', T1C_FR__Value__c = 'Name Field', T1C_FR__User_Overridable__c = true),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = NameColumn.Id,Name = 'Displayed', T1C_FR__Value__c = 'TRUE', T1C_FR__User_Overridable__c = true),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = NameColumn.Id,Name = 'Style', T1C_FR__Value__c = 'newIcon', T1C_FR__User_Overridable__c = true),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = NameColumn.Id,Name = 'FieldAPIName', T1C_FR__Value__c = 'Name', T1C_FR__User_Overridable__c = true),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = NameColumn.Id,Name = 'SortBy', T1C_FR__Value__c = 'Name', T1C_FR__User_Overridable__c = true),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = NameColumn.Id,Name = 'ColumnWidth', T1C_FR__Value__c = '100', T1C_FR__User_Overridable__c = true),
		
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECoreAceShareUtilObjectsActivity_SecurityActivity_Security.Id, Name = 'AccessLevel', T1C_FR__Value__c = 'Edit'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECoreAceShareUtilObjectsActivity_SecurityActivity_Security.Id, Name = 'BatchOnly', T1C_FR__Value__c = 'FALSE'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECoreAceShareUtilObjectsActivity_SecurityActivity_Security.Id, Name = 'FilterClause', T1C_FR__Value__c = ''),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECoreAceShareUtilObjectsActivity_SecurityActivity_Security.Id, Name = 'ObjectName', T1C_FR__Value__c = 'Activity_Security__c'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECoreAceShareUtilObjectsActivity_SecurityActivity_Security.Id, Name = 'ParentCoverageFilterClause', T1C_FR__Value__c = ''),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECoreAceShareUtilObjectsActivity_SecurityActivity_Security.Id, Name = 'ParentLookupField', T1C_FR__Value__c = 'Account__c'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECoreAceShareUtilObjectsActivity_SecurityActivity_Security.Id, Name = 'ParentObjectType', T1C_FR__Value__c = 'Account'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECoreAceShareUtilObjectsActivity_SecurityActivity_Security.Id, Name = 'RowCause', T1C_FR__Value__c = 'Account_Coverage__c'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECoreAceShareUtilObjectsActivity_SecurityActivity_Security.Id, Name = 'SharingTeamEditAccess', T1C_FR__Value__c = ''),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECoreAceShareUtilObjectsActivity_SecurityActivity_Security.Id, Name = 'SharingTeamReadAccess', T1C_FR__Value__c = ''),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECoreAceShareUtilObjectsActivity_SecurityActivity_Security.Id, Name = 'SharingTeamRowCause', T1C_FR__Value__c = '')
		};
	
		SObject sobjectType = Schema.getGlobalDescribe().get('T1C_Base__Account_Coverage__c').newSObject() ; 
		
		list<SObjectUtil.CoverageComponents> covComps = SObjectUtil.createCoverageList(new list<SObject>(), sobjectType, T1C_FR.featureCacheWebSvc.getAttributeTreeRecursive(UserInfo.getUserId(), 'ACE.Core.TestSearch.NameField'));
    		
    	acct = dataFactory.makeAccount('Test Account');	
		acct.BillingStreet = '123 Fake Street'; 
		acct.BillingCity = 'Toronto'; 
		acct.BillingState = 'Ontario'; 
		acct.BillingCountry = 'Canada'; 
		acct.BillingPostalCode = 'M4P 1E4'; 
		acct.Description = 'Long String stuff'; 
		acct.T1C_Base__Tier__c = '1'; 
		acct.T1C_Base__Is_In_Voting_Period__c = true; 
		acct.NumberOfEmployees = 6;		
		insert acct;
		
		lob = dataFactory.makeOrgStructureWithLOBRT('Equities');
		insert lob;
	 	
	 	product = dataFactory.makeProduct('Test', lob.Id);
	 	insert product;
		
		emp = dataFactory.makeLoggedInUserEmployee();
		emp.T1C_Base__LOB__c = lob.Id;
		insert emp;
		
		T1C_Base__Account_Coverage__c ac = dataFactory.makeAccountCoverage(acct.Id, emp.Id);
		insert ac;
		
		covComps = SObjectUtil.createCoverageList(database.query('Select Id, Name from T1C_Base__Account_Coverage__c'), sobjectType, T1C_FR.featureCacheWebSvc.getAttributeTreeRecursive(UserInfo.getUserId(), 'ACE.Core.TestSearch.NameField'));
		system.assert(covComps.size() > 0);
    }

	static testMethod void abstractComparibleConfigTests()
	{
		T1C_FR__Feature__c ace = new T1C_FR__Feature__c(Name = 'ACE',T1C_FR__Name__c = 'ACE');
    	T1C_FR__Feature__c abstr = new T1C_FR__Feature__c(Name = 'Abstract', T1C_FR__Name__c = 'ACE.Abstract');
    	T1C_FR__Feature__c C1 = new T1C_FR__Feature__c(Name = 'Check1', T1C_FR__Name__c = 'ACE.Abstract.Check1');
		T1C_FR__Feature__c C2 = new T1C_FR__Feature__c(Name = 'Check2', T1C_FR__Name__c = 'ACE.Abstract.Check2');
		T1C_FR__Feature__c C3 = new T1C_FR__Feature__c(Name = 'Check3', T1C_FR__Name__c = 'ACE.Abstract.Check3');
		T1C_FR__Feature__c C4 = new T1C_FR__Feature__c(Name = 'Check4', T1C_FR__Name__c = 'ACE.Abstract.Check4');
		
		insert new list<T1C_FR__Feature__c>{ace,abstr,C1,C2,C3,C4};

		insert new list<T1C_FR__Attribute__c>
		{
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = C1.Id, Name = 'Order', T1C_FR__Value__c = '5'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = C2.Id, Name = 'Order', T1C_FR__Value__c = '1'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = C3.Id, Name = 'Order', T1C_FR__Value__c = '9'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = C4.Id, Name = 'Order', T1C_FR__Value__c = '9')
		};

		T1C_FR.Feature settingsFeature = T1C_FR.featureCacheWebSvc.getAttributeTreeRecursive(UserInfo.getUserId(), 'ACE.Abstract');

		T1C_FR.Feature feat1 = settingsFeature.SubFeatMap.get('ACE.Abstract.Check1');
		T1C_FR.Feature feat2 = settingsFeature.SubFeatMap.get('ACE.Abstract.Check2');
		T1C_FR.Feature feat3 = settingsFeature.SubFeatMap.get('ACE.Abstract.Check3');

		SObjectUtil.Column conf1 = new SObjectUtil.Column(feat1);
		SObjectUtil.Column conf2 = new SObjectUtil.Column(feat2);
		SObjectUtil.Column conf3 = new SObjectUtil.Column(feat3);

		list<SObjectUtil.Column> configs = new list<SObjectUtil.Column>{conf1,conf2,conf3};

		configs.sort();
	}

	static testMethod void getSoqlQueryTest()
	{
		T1C_FR__Feature__c ace = new T1C_FR__Feature__c(Name = 'ACE',T1C_FR__Name__c = 'ACE');
    	T1C_FR__Feature__c soql = new T1C_FR__Feature__c(Name = 'SOQL', T1C_FR__Name__c = 'ACE.SOQL');
    	T1C_FR__Feature__c C1 = new T1C_FR__Feature__c(Name = 'Check1', T1C_FR__Name__c = 'ACE.SOQL.Check1');
		T1C_FR__Feature__c C2 = new T1C_FR__Feature__c(Name = 'Check2', T1C_FR__Name__c = 'ACE.SOQL.Check2');
		
		insert new list<T1C_FR__Feature__c>{ace,soql,C1,C2};

		insert new list<T1C_FR__Attribute__c>
		{
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = C1.Id, Name = 'FieldType', T1C_FR__Value__c = 'Address'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = C1.Id, Name = 'Displayed', T1C_FR__Value__c = 'true'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = C1.Id, Name = 'AddressFields', T1C_FR__Value__c = 'BillingStreet;BillingCity'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = C2.Id, Name = 'FieldType', T1C_FR__Value__c = 'String'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = C2.Id, Name = 'Displayed', T1C_FR__Value__c = 'true'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = C2.Id, Name = 'FieldAPIName', T1C_FR__Value__c = 'Name')
		};

		T1C_FR.Feature settingsFeature  = T1C_FR.featureCacheWebSvc.getAttributeTreeRecursive(UserInfo.getUserId(), 'ACE.SOQL');

		String queryString = SObjectUtil.getSoqlQuery(settingsFeature, 'Account', 'Id != null');
		queryString = SObjectUtil.getSoqlQuery(settingsFeature, 'Account', null);
	}

	static testMethod void getSoqlQueryTest2()
	{
		T1C_FR__Feature__c ace = new T1C_FR__Feature__c(Name = 'ACE',T1C_FR__Name__c = 'ACE');
    	T1C_FR__Feature__c soql = new T1C_FR__Feature__c(Name = 'SOQL', T1C_FR__Name__c = 'ACE.SOQL');
    	T1C_FR__Feature__c C1 = new T1C_FR__Feature__c(Name = 'Check1', T1C_FR__Name__c = 'ACE.SOQL.Check1');
		T1C_FR__Feature__c C2 = new T1C_FR__Feature__c(Name = 'Check2', T1C_FR__Name__c = 'ACE.SOQL.Check2');
		
		insert new list<T1C_FR__Feature__c>{ace,soql,C1,C2};

		list<T1C_FR.Attribute> attribs1 = new list<T1C_FR.Attribute>
		{
			new T1C_FR.Attribute('Displayed','false')
		};

		String queryString = SObjectUtil.getSoqlQuery(attribs1, 'Account', '(Select Id from Contacts)', null);

		list<T1C_FR.Attribute> attribs2 = new list<T1C_FR.Attribute>
		{
			new T1C_FR.Attribute('Displayed', 'true'),
			new T1C_FR.Attribute('Name', 'true')
		};

		queryString = SObjectUtil.getSoqlQuery(attribs2, 'Account', '(Select Id from Contacts)', null);
	}

	static testMethod void processSObjectTest()
	{
		T1C_FR__Feature__c ace = new T1C_FR__Feature__c(Name = 'ACE',T1C_FR__Name__c = 'ACE');
    	T1C_FR__Feature__c process = new T1C_FR__Feature__c(Name = 'Abstract', T1C_FR__Name__c = 'ACE.Proccess');
    	T1C_FR__Feature__c C1 = new T1C_FR__Feature__c(Name = 'Check1', T1C_FR__Name__c = 'ACE.Proccess.Check1');
		T1C_FR__Feature__c C2 = new T1C_FR__Feature__c(Name = 'Check2', T1C_FR__Name__c = 'ACE.Proccess.Check2');
		T1C_FR__Feature__c C3 = new T1C_FR__Feature__c(Name = 'Check3', T1C_FR__Name__c = 'ACE.Proccess.Check3');
		T1C_FR__Feature__c C4 = new T1C_FR__Feature__c(Name = 'Check4', T1C_FR__Name__c = 'ACE.Proccess.Check4');
		T1C_FR__Feature__c C5 = new T1C_FR__Feature__c(Name = 'Check5', T1C_FR__Name__c = 'ACE.Proccess.Check5');
		T1C_FR__Feature__c C6 = new T1C_FR__Feature__c(Name = 'Check6', T1C_FR__Name__c = 'ACE.Proccess.Check6');
		T1C_FR__Feature__c C7 = new T1C_FR__Feature__c(Name = 'Check7', T1C_FR__Name__c = 'ACE.Proccess.Check7');

		insert new list<T1C_FR__Feature__c>{ace,process,C1,C2,C3,C4,C5,C6,C7};

		insert new list<T1C_FR__Attribute__c>
		{
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = C1.Id, Name = 'FieldAPIName', T1C_FR__Value__c = 'T1C_Base__Last_User_Interaction_Date__c'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = C1.Id, Name = 'FieldType', T1C_FR__Value__c = 'Date'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = C1.Id, Name = 'ValueColor', T1C_FR__Value__c = 'CCC'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = C1.Id, Name = 'LabelColor', T1C_FR__Value__c = 'CCC'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = C1.Id, Name = 'Displayed', T1C_FR__Value__c = 'true'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = C1.Id, Name = 'Format', T1C_FR__Value__c = 'EEEE'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = C1.Id, Name = 'OrderBy', T1C_FR__Value__c = '1'),

			new T1C_FR__Attribute__c(T1C_FR__Feature__c = C2.Id, Name = 'FieldAPIName', T1C_FR__Value__c = 'CreatedDate'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = C2.Id, Name = 'FieldType', T1C_FR__Value__c = 'DateTime'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = C2.Id, Name = 'AlignmentDirection', T1C_FR__Value__c = 'left'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = C2.Id, Name = 'Displayed', T1C_FR__Value__c = 'true'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = C2.Id, Name = 'Format', T1C_FR__Value__c = 'EEEE'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = C2.Id, Name = 'OrderBy', T1C_FR__Value__c = '1'),

			new T1C_FR__Attribute__c(T1C_FR__Feature__c = C3.Id, Name = 'FieldAPIName', T1C_FR__Value__c = 'NumberOfEmployees'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = C3.Id, Name = 'FieldType', T1C_FR__Value__c = 'Number'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = C3.Id, Name = 'FieldLabel', T1C_FR__Value__c = 'Number'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = C3.Id, Name = 'ColumnWidth', T1C_FR__Value__c = '100'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = C3.Id, Name = 'Scale', T1C_FR__Value__c = '0'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = C3.Id, Name = 'CurrencyPrefix', T1C_FR__Value__c = '$'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = C3.Id, Name = 'Displayed', T1C_FR__Value__c = 'true'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = C3.Id, Name = 'OrderBy', T1C_FR__Value__c = '3'),

			new T1C_FR__Attribute__c(T1C_FR__Feature__c = C4.Id, Name = 'FieldAPIName', T1C_FR__Value__c = 'Name'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = C4.Id, Name = 'FieldType', T1C_FR__Value__c = 'String'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = C4.Id, Name = 'Displayed', T1C_FR__Value__c = 'true'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = C4.Id, Name = 'OrderBy', T1C_FR__Value__c = '2'),

			new T1C_FR__Attribute__c(T1C_FR__Feature__c = C5.Id, Name = 'FieldAPIName', T1C_FR__Value__c = 'T1C_Base__Tier__c'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = C5.Id, Name = 'FieldType', T1C_FR__Value__c = 'Picklist'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = C5.Id, Name = 'Displayed', T1C_FR__Value__c = 'true'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = C5.Id, Name = 'OrderBy', T1C_FR__Value__c = '8'),

			new T1C_FR__Attribute__c(T1C_FR__Feature__c = C6.Id, Name = 'FieldAPIName', T1C_FR__Value__c = 'T1C_Base__Logo__c'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = C6.Id, Name = 'FieldType', T1C_FR__Value__c = 'Image'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = C6.Id, Name = 'Displayed', T1C_FR__Value__c = 'true'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = C6.Id, Name = 'OrderBy', T1C_FR__Value__c = '6'),

			new T1C_FR__Attribute__c(T1C_FR__Feature__c = C7.Id, Name = 'FieldAPIName', T1C_FR__Value__c = 'BillingAddress'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = C7.Id, Name = 'FieldType', T1C_FR__Value__c = 'Address'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = C7.Id, Name = 'AddressFields', T1C_FR__Value__c = 'BillingStreet;BillingCity'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = C7.Id, Name = 'Displayed', T1C_FR__Value__c = 'true'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = C7.Id, Name = 'OrderBy', T1C_FR__Value__c = '6')
		};

		T1C_FR.Feature settingsFeature  = T1C_FR.featureCacheWebSvc.getAttributeTreeRecursive(UserInfo.getUserId(), 'ACE.Proccess');

		Account record = dataFactory.makeAccount('Paper Street Soap Company');
		record.T1C_Base__Last_User_Interaction_Date__c = Date.today();
		record.NumberOfEmployees = 9;
		record.T1C_Base__Tier__c = Account.T1C_Base__Tier__c.getDescribe().getPicklistValues()[0].getValue();
		record.T1C_Base__Logo__c = '<img src="http://somelogo.com/image.jpg"></img>';
		record.BillingStreet = '537 Paper Street';
		record.BillingCity = 'Bradford';
		insert record;

		list<SObjectUtil.CustomDetails> custDetails = SObjectUtil.processSObject(settingsFeature, record);

		SObjectUtil.CustomDetails constructor1 = new SObjectUtil.CustomDetails('String', 'Something', 'Something', 1, 'true', 'CCC', 'DDD');
		SObjectUtil.CustomDetails constructor3 = new SObjectUtil.CustomDetails('String', 'Something', 'Something', 1, 'true', 'CCC', 'DDD', '100','Name', 'Name', 'left', new String[]{});				
	}

	static testMethod void getFieldDescribeTest() 
    {
		Schema.DescribeFieldResult descRes = SObjectUtil.getFieldDescribe('T1C_Base__Tier__c', 'Account');
		descRes = SObjectUtil.getFieldDescribe('T1C_Base__Billing_Address_Id__r.Name', Schema.getGlobalDescribe().get('Account').getDescribe());
	}
    
    static testMethod void getPicklistValuesTest() 
    {
    	SObjectUtil.DynamicUIPicklistConfig dupl = new SObjectUtil.DynamicUIPicklistConfig();
    	list<String> pls = dupl.getPicklistValues('Account','T1C_Base__Tier__c');  
    	system.assert(pls.size() > 0);
    	
    	SObjectUtil.KeyValuePair kvp = new SObjectUtil.KeyValuePair('this', 'that');
    }
    
    static testMethod void dynamicLookupTest() 
    {
    	String core = ':CORE';
		
		dataFactory.createCBSAFR();
		
		T1C_FR__Feature__c TestSearch = new T1C_FR__Feature__c(Name = 'TestSearch',T1C_FR__Name__c = 'ACE.Core.TestSearch', T1C_FR__External_Id__c = 'ACE.Core.TestSearch' + core);
		T1C_FR__Feature__c NameField = new T1C_FR__Feature__c(Name = 'NameField',T1C_FR__Name__c = 'ACE.Core.TestSearch.NameField', T1C_FR__External_Id__c = 'ACE.Core.TestSearch.NameField' + core);
		T1C_FR__Feature__c Columns = new T1C_FR__Feature__c(Name = 'Columns',T1C_FR__Name__c = 'ACE.Core.TestSearch.NameField.Columns', T1C_FR__External_Id__c = 'ACE.Core.TestSearch.NameField.Columns' + core);
		T1C_FR__Feature__c NameColumn = new T1C_FR__Feature__c(Name = 'NameColumn',T1C_FR__Name__c = 'ACE.Core.TestSearch.NameField.Columns.NameColumn', T1C_FR__External_Id__c = 'ACE.Core.TestSearch.NameField.Columns.NameColumn' + core);
		
		T1C_FR__Feature__c ACECoreAceShareUtilObjectsActivity_Security = new T1C_FR__Feature__c(Name = 'Activity_Security__c', T1C_FR__Name__c = 'ACE.Core.AceShareUtil.Objects.Activity_Security__c');
		T1C_FR__Feature__c ACECoreAceShareUtilObjectsActivity_SecurityActivity_Security = new T1C_FR__Feature__c(Name = 'Activity_Security__c', T1C_FR__Name__c = 'ACE.Core.AceShareUtil.Objects.Activity_Security__c.Activity_Security__c');
	
		insert new list<T1C_FR__Feature__c>{TestSearch,NameField, Columns,NameColumn,ACECoreAceShareUtilObjectsActivity_Security,ACECoreAceShareUtilObjectsActivity_SecurityActivity_Security};
		
		insert new list<T1C_FR__Attribute__c>
		{
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = NameField.Id,Name = 'SearchFields', T1C_FR__Value__c = 'Id, Name', T1C_FR__User_Overridable__c = true),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = NameField.Id,Name = 'AdditionalWhereClause', T1C_FR__Value__c = 'Id != null',T1C_FR__User_Overridable__c = true),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = NameField.Id,Name = 'AdditionalWhereClauseCustom', T1C_FR__Value__c = 'Id != null',T1C_FR__User_Overridable__c = true),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = NameColumn.Id,Name = 'Width', T1C_FR__Value__c = '100', T1C_FR__User_Overridable__c = true),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = NameColumn.Id,Name = 'ItemRenderer', T1C_FR__Value__c = 'Dummy', T1C_FR__User_Overridable__c = true),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = NameColumn.Id,Name = 'Label', T1C_FR__Value__c = 'Name Field', T1C_FR__User_Overridable__c = true),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = NameColumn.Id,Name = 'Description', T1C_FR__Value__c = 'Description', T1C_FR__User_Overridable__c = true),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = NameColumn.Id,Name = 'Style', T1C_FR__Value__c = 'newIcon', T1C_FR__User_Overridable__c = true),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = NameColumn.Id,Name = 'FieldType', T1C_FR__Value__c = 'STRING', T1C_FR__User_Overridable__c = true),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = NameColumn.Id,Name = 'DataField', T1C_FR__Value__c = 'Name', T1C_FR__User_Overridable__c = true),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = NameColumn.Id,Name = 'OrderByColumns', T1C_FR__Value__c = 'Name', T1C_FR__User_Overridable__c = true),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = NameColumn.Id,Name = 'AltDataField', T1C_FR__Value__c = 'Name', T1C_FR__User_Overridable__c = true),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = NameColumn.Id,Name = 'CssClass', T1C_FR__Value__c = 'newIcon', T1C_FR__User_Overridable__c = true),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = NameColumn.Id,Name = 'ActionType', T1C_FR__Value__c = 'New', T1C_FR__User_Overridable__c = true),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = NameColumn.Id,Name = 'EventType', T1C_FR__Value__c = 'URL', T1C_FR__User_Overridable__c = true),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = NameColumn.Id,Name = 'TextAlign', T1C_FR__Value__c = 'Center', T1C_FR__User_Overridable__c = true),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = NameColumn.Id,Name = 'SortDescending', T1C_FR__Value__c = 'TRUE', T1C_FR__User_Overridable__c = true),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = NameColumn.Id,Name = 'IsHidden', T1C_FR__Value__c = 'TRUE',T1C_FR__User_Overridable__c = true),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = NameColumn.Id,Name = 'IsDefaultSort', T1C_FR__Value__c = 'TRUE',T1C_FR__User_Overridable__c = true),
		
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECoreAceShareUtilObjectsActivity_SecurityActivity_Security.Id, Name = 'AccessLevel', T1C_FR__Value__c = 'Edit'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECoreAceShareUtilObjectsActivity_SecurityActivity_Security.Id, Name = 'BatchOnly', T1C_FR__Value__c = 'FALSE'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECoreAceShareUtilObjectsActivity_SecurityActivity_Security.Id, Name = 'FilterClause', T1C_FR__Value__c = ''),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECoreAceShareUtilObjectsActivity_SecurityActivity_Security.Id, Name = 'ObjectName', T1C_FR__Value__c = 'Activity_Security__c'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECoreAceShareUtilObjectsActivity_SecurityActivity_Security.Id, Name = 'ParentCoverageFilterClause', T1C_FR__Value__c = ''),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECoreAceShareUtilObjectsActivity_SecurityActivity_Security.Id, Name = 'ParentLookupField', T1C_FR__Value__c = 'Account__c'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECoreAceShareUtilObjectsActivity_SecurityActivity_Security.Id, Name = 'ParentObjectType', T1C_FR__Value__c = 'Account'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECoreAceShareUtilObjectsActivity_SecurityActivity_Security.Id, Name = 'RowCause', T1C_FR__Value__c = 'Account_Coverage__c'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECoreAceShareUtilObjectsActivity_SecurityActivity_Security.Id, Name = 'SharingTeamEditAccess', T1C_FR__Value__c = ''),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECoreAceShareUtilObjectsActivity_SecurityActivity_Security.Id, Name = 'SharingTeamReadAccess', T1C_FR__Value__c = ''),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECoreAceShareUtilObjectsActivity_SecurityActivity_Security.Id, Name = 'SharingTeamRowCause', T1C_FR__Value__c = '')
		};
    	
    	SObjectUtil.DynamicUILookupConfig dynUIlookup = new SObjectUtil.DynamicUILookupConfig();
    	dynUIlookup = new SObjectUtil.DynamicUILookupConfig('ACE.Core.TestSearch.NameField');
    	dynUIlookup = new SObjectUtil.DynamicUILookupConfig( T1C_FR.featureCacheWebSvc.getAttributeTreeRecursive(UserInfo.getUserId(), 'ACE.Core.TestSearch.NameField'));
    	SObjectUtil.overrideAttributeValue('ACE.Core.TestSearch.NameField','SearchFields','OtherClass');
    	system.assert('OtherClass' == T1C_FR.featureCacheWebSvc.getAttribute(UserInfo.getUserId(),'ACE.Core.TestSearch.NameField','SearchFields'));
    }
               
    private Class AccountInfo
	{
		private Boolean displayed;
		private Integer numOfColumns;
		private Integer orderBy;
		private list<SObjectUtil.CustomDetails> details;
		
		private AccountInfo(T1C_FR.Feature sectionFeature, list<SObjectUtil.CustomDetails> custDets)
		{
			displayed = (sectionFeature.AttribMap.get('Displayed').Value == 'true');
			orderBy = Integer.valueOf(sectionFeature.AttribMap.get('OrderBy').Value);
			numOfColumns = Integer.valueOf(sectionFeature.AttribMap.get('NumberOfColumns').Value);
			details = custDets;
		}
	}
}