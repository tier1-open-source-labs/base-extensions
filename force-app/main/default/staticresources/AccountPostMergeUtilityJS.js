//=============================================================================
/**
 * Name: AccountPostMergeUtilityJS.js
 * Description: Post Merge Utility UI for the merge button on Account detail page
 *
 * Confidential & Proprietary, 2017 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1CRM.
 */
//=============================================================================
(function($)
{
	var sessionId = ACE.Salesforce.sessionId;
	var serverURL = ACE.Salesforce.serverUrl;
	serverURL = serverURL.substr(0,serverURL.indexOf("/services")) + "/";
	
	sforce.connection.sessionId = ACE.Salesforce.sessionId;
	
	//link the css file
	var cssResources;
	var result = sforce.apex.execute("AccountPostMergeUtilityController", "getStaticResource", {resourceName : 'PostMergeUtilityCSS'}, 
	{ 
		onSuccess: function(result)
		{		
			cssResources  = serverURL + result[0];
			var cssId = 'PostMergeUtilityCSS.css';
			if (!document.getElementById(cssId) && cssResources != null)
			{
			    var head  = document.getElementsByTagName('head')[0];
			    var link  = document.createElement('link');
			    link.id   = cssId;
			    link.rel  = 'stylesheet';
			    link.type = 'text/css';
			    link.href = cssResources;
			    link.media = 'all';
			    head.appendChild(link);
			}
		}, 
		onFailure: function(error)
		{		
			error = 'No CSS file in the static resource : PostMergeUtilityCSS';
			showError(error);
		} 
	});
	
	var mergeUtilityBody = $p('<div class="mergeUtilityWrapper"/>').appendTo("body");
	
	var modal = $p('<div id="errorModal" class="modal" style="display: none;position: fixed;z-index: 2147483647;padding-top: 100px;left: 0;top: 0;vertical-align:middle;text-align: center;width: 100%;height: 100%;overflow: auto;background-color: rgb(255,255,255);background-color: rgba(255,255,255,0.4)">');
	var errorMessageContent = $p('<div id="errorMessage" class="modal-content" style="position: relative;margin: auto;height: auto;width: 80%;padding: 20px;-opacity: 0.5;color: black;font-weight: bold;z-index: 2147483647;border: 1px solid grey;-moz-border-radius: 10px;-webkit-border-radius: 10px;-moz-box-shadow: 0 0 5px grey;-webkit-box-shadow: 0px 0px 5px grey;-text-shadow: 1px 1px 1px white;box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19);-webkit-animation-name: animatetop;-webkit-animation-duration: 0.4s;animation-name: animatetop;animation-duration: 0.4s;vertical-align:middle;text-align: center; background-color: rgb(255,255,255);">');
	$(modal).append(errorMessageContent);
	$(mergeUtilityBody).append(modal);
	
	var headerPanel = $p('<div class="headerPanelContainer"/>').appendTo(mergeUtilityBody);
	$p('<div class="appTitle">Post Merge Utility</div>').appendTo(headerPanel);
	$p('<div class="brandingMessage">Powered by Tier1CRM</div>').appendTo(headerPanel);
	
	var topcloseButtonDiv = $p('<button class="closeFAB" id="hideShowBtn" title="Click to Close"/>').appendTo(headerPanel);
	$p('<div class="closeFABX"></div>').appendTo(topcloseButtonDiv);
	$(topcloseButtonDiv).on('mousedown', function() 
	{
		closeWindow();
	});
	
	var scrollContainer = $p('<div class="scrollContainer"/>').appendTo(mergeUtilityBody);
	var applicationContainer = $p('<div class="applicationContainer"/>').appendTo(scrollContainer);
	
	var accountName;
	var accountId;
	var keyAcctInfoList;
	var acctAceListInfoList;
	var acctInterestInfoList;
	var acctCoverageInfoList;
	var acctInterestSubjectInfoList;
	
	var rolepicklistConfig;
	var lookUPConfig;
	
	var valueExist = false;
	var interestPage = false;
	var coveragePage = false;
	
	var tiggersEnabled = false;
	
	var result = sforce.apex.execute("AccountPostMergeUtilityController", "getAccountRelatedList", {accountId : ACEUtil.getURLParameter('AccountId')}, 
	{ 
		onSuccess: function(result)
		{		
			acctRelatedListInfo = JSON.parse(result);
			
			//account info
			account = acctRelatedListInfo.acct;
			accountName = account.Name;
			accountId = account.Id;
			
			tiggersEnabled = acctRelatedListInfo.tiggersEnabled;

			//enable the triggers
			enableTriggers();
			
			// account coverage role picklist
			rolepicklistConfig = acctRelatedListInfo.rolepicklistConfig;
			
			// account coverage product lookup
			lookUPConfig = acctRelatedListInfo.lookUPConfig;
			
			//key account info
			keyAcctInfoList = acctRelatedListInfo.keyAccountInfo;
			if (keyAcctInfoList.length > 0)
			{
				valueExist = true;
			}
			
			//aceList info
			acctAceListInfoList = acctRelatedListInfo.aceListInfo;
			if (acctAceListInfoList.length > 0)
			{
				valueExist = true;
			}
			
			//interst info
			acctInterestInfoList = acctRelatedListInfo.interestInfo;
			if (acctInterestInfoList.length > 0)
			{
				interestPage = true;
			}
			
			//account coverage
			acctCoverageInfoList = acctRelatedListInfo.acctCoverageInfo;
			if (acctCoverageInfoList.length > 0)
			{
				coveragePage = true;
			}
			
			//interst subject info
			acctInterestSubjectInfoList = acctRelatedListInfo.interestSubjectInfo;
			if (acctInterestSubjectInfoList.length > 0)
			{
				valueExist = true;
			}
			
			generateHTML();
		}, 
		onFailure: function(error)
		{
			showError(error);
		} 
	});

	
	
	//  ==================================================================
	//  Method: generateHTML
	//  Description: generate the whole UI
	//  ==================================================================
	function generateHTML()
	{	
		//print the account name
		var headerSection = $p('<div class="section headerSection"/>').appendTo(applicationContainer);
		if (!valueExist && !coveragePage && !interestPage)
		{
			$p('<div class="nameContainer"><div class="accountName">'+'<a title="click to launch account page" href="' + serverURL + accountId + '" target="_blank">' +  accountName + '</a>'+'</div><div class="appMessage">No duplicates</div></div>').appendTo(headerSection);
		}
		else
		{
			$p('<div class="nameContainer"><div class="accountName">'+'<a title="click to launch account page" href="' + serverURL + accountId + '" target="_blank">' +  accountName + '</a>'+'</div><div class="appMessage">The following duplicates were created during the merge process</div></div>').appendTo(headerSection);
		}
		
		if (valueExist)
		{
			var preserveOnlySection = $p('<div class="section preserveOnlySection"/>').appendTo(applicationContainer);
			var subSection = $p('<div class="subSection"/>').appendTo(preserveOnlySection);
			
			
			//******print the key account infomation**********
			if (keyAcctInfoList != null && keyAcctInfoList != undefined && keyAcctInfoList.length > 0)
			{
				$p('<div class="sectionLabel">Key Accounts</div>').appendTo(subSection);
				var secgroup = $p('<div class="group"/>').appendTo(subSection);
				
				if (Array.isArray(keyAcctInfoList) == false)
				{
					$p('<div class="subLabel">' + keyAcctInfoList.name + '</div>').appendTo(secgroup);
					
					var contentTable = $p('<div class="contentTable"/>').appendTo(secgroup);
					var headerRow = $p('<div class="headerRow"/>').appendTo(contentTable);
					
					$p('<div class="radioColumn"/>').appendTo(headerRow);
					data = ['KP Name', 'KP Role'];
					displayData('keyaccountH', headerRow, data, 'columnHeader');
					
					keyAccountsInfo = keyAcctInfoList.keyAccounts;
					if (keyAccountsInfo != null && keyAccountsInfo != undefined)
					{
						if (Array.isArray(keyAccountsInfo))
						{
							for (var j = 0; j < keyAccountsInfo.length; j++)
							{
								var tableRow = $p('<div class="tableRow"/>').appendTo(contentTable);
								sel = '';
								if (j == 0)
								{
									sel = ' checked="checked"';
								}
								data = ['<a title="click to launch Key Account page" href="' + serverURL + keyAccountsInfo[j].Id + '" target="_blank">' +  keyAccountsInfo[j].Name + '</a>', keyAccountsInfo[j].T1C_AEM__Role__c];
								$p('<div class="radioColumn"><label class="control radio"><input name="keyAccounts0" type="radio" value=' + keyAccountsInfo[j].Id + sel + ' data-type="dateType"><span class="control-indicator"></span></label></div>').appendTo(tableRow);
								displayData('keyaccount0' + j, tableRow, data, 'column');
							}
						}
					}
				}
				else
				{
					for (var i = 0; i < keyAcctInfoList.length; i++)
					{
						$p('<div class="subLabel">' + keyAcctInfoList[i].name  + '</div>').appendTo(secgroup);
						
						var contentTable = $p('<div class="contentTable"/>').appendTo(secgroup);
						var headerRow = $p('<div class="headerRow"/>').appendTo(contentTable);
						
						$p('<div class="radioColumn"/>').appendTo(headerRow);
						data = ['KP Name', 'KP Role'];
						displayData('keyaccountH', headerRow, data, 'columnHeader');
						
						keyAccountsInfo = keyAcctInfoList[i].keyAccounts;
						if (keyAccountsInfo != null && keyAccountsInfo != undefined)
						{
							if (Array.isArray(keyAccountsInfo))
							{
								for (var j = 0; j < keyAccountsInfo.length; j++)
								{
									var tableRow = $p('<div class="tableRow"/>').appendTo(contentTable);
									sel = '';
									if (j == 0)
									{
										sel = ' checked="checked"';
									}
									data = ['<a title="click to launch Key Account page" href="' + serverURL + keyAccountsInfo[j].Id + '" target="_blank">' +  keyAccountsInfo[j].Name + '</a>', keyAccountsInfo[j].T1C_AEM__Role__c];
									$p('<div class="radioColumn"><label class="control radio"><input name="keyAccounts' + i + '" type="radio" value=' + keyAccountsInfo[j].Id + sel + ' data-type="dateType"><span class="control-indicator"></span></label></div>').appendTo(tableRow);
									displayData('keyaccount' + i + j, tableRow, data, 'column');
								}
							}
						}
					}
				}
			}
			
			
			//******print the ace list entry infomation**********
			if (acctAceListInfoList != null && acctAceListInfoList != undefined && acctAceListInfoList.length > 0)
			{
				$p('<div class="sectionLabel">Ace List Entries</div>').appendTo(subSection);
				var secgroup = $p('<div class="group"/>').appendTo(subSection);
				
				if (Array.isArray(acctAceListInfoList) == false)
				{
					$p('<div class="subLabel">' + acctAceListInfoList.name + '</div>').appendTo(secgroup);
					
					var contentTable = $p('<div class="contentTable"/>').appendTo(secgroup);
					var headerRow = $p('<div class="headerRow"/>').appendTo(contentTable);
					
					$p('<div class="radioColumn"/>').appendTo(headerRow);
					data = ['Name', 'List Owner'];
					displayData('acelistentryH', headerRow, data, 'columnHeader');
					
					aceListEntryInfo = acctAceListInfoList.aceListEntries;
					if (aceListEntryInfo != null && aceListEntryInfo != undefined)
					{
						if (Array.isArray(aceListEntryInfo))
						{
							for (var j = 0; j < aceListEntryInfo.length; j++)
							{
								var tableRow = $p('<div class="tableRow"/>').appendTo(contentTable);
								sel = '';
								if (j == 0)
								{
									sel = ' checked="checked"';
								}
								data = ['<a title="click to launch Ace List Entry page" href="' + serverURL + aceListEntryInfo[j].Id + '" target="_blank">' +  aceListEntryInfo[j].Name + '</a>', aceListEntryInfo[j].T1C_Base__Ace_List__r.Owner.Name];
								$p('<div class="radioColumn"><label class="control radio"><input name="listEntry0" type="radio" value=' + aceListEntryInfo[j].Id + sel + ' data-type="dateType"><span class="control-indicator"></span></label></div>').appendTo(tableRow);
								displayData('acelistentry0' + j, tableRow, data, 'column');
							}
						}
					}
				}
				else
				{
					for (var i = 0; i < acctAceListInfoList.length; i++)
					{
						$p('<div class="subLabel">' + acctAceListInfoList[i].name  + '</div>').appendTo(secgroup);
						
						var contentTable = $p('<div class="contentTable"/>').appendTo(secgroup);
						var headerRow = $p('<div class="headerRow"/>').appendTo(contentTable);
						
						$p('<div class="radioColumn"/>').appendTo(headerRow);
						data = ['Name', 'List Owner'];
						displayData('acelistentryH', headerRow, data, 'columnHeader');
						
						aceListEntryInfo = acctAceListInfoList[i].aceListEntries;
						if (aceListEntryInfo != null && aceListEntryInfo != undefined)
						{
							if (Array.isArray(aceListEntryInfo))
							{
								for (var j = 0; j < aceListEntryInfo.length; j++)
								{
									var tableRow = $p('<div class="tableRow"/>').appendTo(contentTable);
									sel = '';
									if (j == 0)
									{
										sel = ' checked="checked"';
									}
									data = ['<a title="click to launch Ace List Entry page" href="' + serverURL + aceListEntryInfo[j].Id + '" target="_blank">' +  aceListEntryInfo[j].Name + '</a>', aceListEntryInfo[j].T1C_Base__Ace_List__r.Owner.Name];
									$p('<div class="radioColumn"><label class="control radio"><input name="listEntry' + i + '" type="radio" value=' + aceListEntryInfo[j].Id + sel + ' data-type="dateType"><span class="control-indicator"></span></label></div>').appendTo(tableRow);
									displayData('acelistentry' + i + j, tableRow, data, 'column');
								}
							}
						}
					}
				}
			}
			
			
			//******print the interest subject infomation**********
			if (acctInterestSubjectInfoList != null && acctInterestSubjectInfoList != undefined && acctInterestSubjectInfoList.length > 0)
			{
				$p('<div class="sectionLabel">Interest Subject</div>').appendTo(subSection);
				var secgroup = $p('<div class="group"/>').appendTo(subSection);
				
				if (Array.isArray(acctInterestSubjectInfoList) == false)
				{
					$p('<div class="subLabel">' + acctInterestSubjectInfoList.name + '</div>').appendTo(secgroup);
					
					var contentTable = $p('<div class="contentTable"/>').appendTo(secgroup);
					var headerRow = $p('<div class="headerRow"/>').appendTo(contentTable);
					
					$p('<div class="radioColumn"/>').appendTo(headerRow);
					data = ['Description', 'Parent Interest Subject', 'Inactive'];
					displayData('interestsubjectH', headerRow, data, 'columnHeader');
					
					interestsubjectInfo = acctInterestSubjectInfoList.interestSubject;
					if (interestsubjectInfo != null && interestsubjectInfo != undefined)
					{
						if (Array.isArray(interestsubjectInfo))
						{
							for (var j = 0; j < interestsubjectInfo.length; j++)
							{
								var tableRow = $p('<div class="tableRow"/>').appendTo(contentTable);
								sel = '';
								if (j == 0)
								{
									sel = ' checked="checked"';
								}
								var parentIntSubName = '&nbsp;';
								if (interestsubjectInfo[j].T1C_Base__Parent_Interest_Subject__c != null)
								{
									parentIntSubName = interestsubjectInfo[j].T1C_Base__Parent_Interest_Subject__r.Name;
								}
								data = ['<a title="click to launch Interest Subject page" href="' + serverURL + interestsubjectInfo[j].Id + '" target="_blank">' +  interestsubjectInfo[j].T1C_Base__Description__c + '</a>', parentIntSubName, interestsubjectInfo[j].T1C_Base__Inactive__c];
								$p('<div class="radioColumn"><label class="control radio"><input name="interestSubject0" type="radio" value=' + interestsubjectInfo[j].Id + sel + ' data-type="dateType"><span class="control-indicator"></span></label></div>').appendTo(tableRow);
								displayData('interestSubject0' + j, tableRow, data, 'column');
							}
						}
					}
				}
				else
				{
					for (var i = 0; i < acctInterestSubjectInfoList.length; i++)
					{
						$p('<div class="subLabel">' + acctInterestSubjectInfoList[i].name  + '</div>').appendTo(secgroup);
						
						var contentTable = $p('<div class="contentTable"/>').appendTo(secgroup);
						var headerRow = $p('<div class="headerRow"/>').appendTo(contentTable);
						
						$p('<div class="radioColumn"/>').appendTo(headerRow);
						data = ['Description', 'Parent Interest Subject', 'Inactive'];
						displayData('interestsubjectH', headerRow, data, 'columnHeader');
						
						interestsubjectInfo = acctInterestSubjectInfoList[i].interestSubject;
						if (interestsubjectInfo != null && interestsubjectInfo != undefined)
						{
							if (Array.isArray(interestsubjectInfo))
							{
								for (var j = 0; j < interestsubjectInfo.length; j++)
								{
									var tableRow = $p('<div class="tableRow"/>').appendTo(contentTable);
									sel = '';
									if (j == 0)
									{
										sel = ' checked="checked"';
									}
									var parentIntSubName = '&nbsp;';
									if (interestsubjectInfo[j].T1C_Base__Parent_Interest_Subject__c != null)
									{
										parentIntSubName = interestsubjectInfo[j].T1C_Base__Parent_Interest_Subject__r.Name;
									}
									data = ['<a title="click to launch Interest Subject page" href="' + serverURL + interestsubjectInfo[j].Id + '" target="_blank">' +  interestsubjectInfo[j].T1C_Base__Description__c + '</a>', parentIntSubName, interestsubjectInfo[j].T1C_Base__Inactive__c];
									$p('<div class="radioColumn"><label class="control radio"><input name="interestSubject' + i + '" type="radio" value=' + interestsubjectInfo[j].Id + sel + ' data-type="dateType"><span class="control-indicator"></span></label></div>').appendTo(tableRow);
									displayData('interestSubject' + i + j, tableRow, data, 'column');
								}
							}
						}
					}
				}
			}
			
			//******Preserve Button************************
			preserveButton(subSection);
		}
		
		if (interestPage)
		{
			//******print the interest infomation**********
			if (acctInterestInfoList != null && acctInterestInfoList != undefined && acctInterestInfoList.length > 0)
			{
				var preserveOnlySection = $p('<div class="section preserveOnlySection"/>').appendTo(applicationContainer);
				var subSection = $p('<div class="subSection"/>').appendTo(preserveOnlySection);
				$p('<div class="sectionLabel">Interests</div>').appendTo(subSection);
				var secgroup = $p('<div class="group"/>').appendTo(subSection);
				
				////////Interest
				if (Array.isArray(acctInterestInfoList) == false)
				{
					var btnName;
					
					$p('<div class="subLabel">' + acctInterestInfoList.name + '</div>').appendTo(secgroup);
					
					var contentTable = $p('<div class="contentTable"/>').appendTo(secgroup);
					var headerRow = $p('<div class="headerRow"/>').appendTo(contentTable);
					
					$p('<div class="radioColumn"/>').appendTo(headerRow);
					data = ['Name', 'Last Discussed', 'Reason', 'Source', 'Notes'];
					displayData('interestH', headerRow, data, 'columnHeader');
					
					interestReasonInterestInfo = acctInterestInfoList.interestWithReasonInterest;
					if (interestReasonInterestInfo != null && interestReasonInterestInfo != undefined)
					{
						if (Array.isArray(interestReasonInterestInfo))
						{
							for (var j = 0; j < interestReasonInterestInfo.length; j++)
							{
								var tableRow = $p('<div class="tableRow"/>').appendTo(contentTable);
								sel = '';
								if (j == 0)
								{
									sel = ' checked="checked"';
								}
								data = ['<a title="click to launch Interest page" href="' + serverURL + interestReasonInterestInfo[j].Id + '" target="_blank">' +  accountName + '</a>', interestReasonInterestInfo[j].T1C_Base__Last_Discussed__c, interestReasonInterestInfo[j].T1C_Base__Reason__c, interestReasonInterestInfo[j].T1C_Base__Source__c, interestReasonInterestInfo[j].T1C_Base__Note__c];
								//$p('<div class="radioColumn"><label class="control radio"><input name="reasonI0" type="radio" value=' + interestReasonInterestInfo[j].Id + sel + ' data-type="dateType"><span class="control-indicator"></span></label></div>').appendTo(tableRow);
								var id = 'i0' + j;
								$p('<label class="control checkbox" for="' + id + '"><input id="0:' + j + '" name="reasonI0" type="checkbox" value=' + interestReasonInterestInfo[j].Id + ' data-type="dateType"><span class="control-indicator"></span></label>').appendTo(tableRow);
								displayData('interest0' + j, tableRow, data, 'column');
							}
							btnName = 'reasonI0';
						}
						//******Preserve/Merge Button************************
						interestButtons(secgroup, btnName);
					}
				}
				else
				{
					for (var i = 0; i < acctInterestInfoList.length; i++)
					{
						var btnName;
						
						$p('<div class="subLabel">' + acctInterestInfoList[i].name + '</div>').appendTo(secgroup);
						
						var contentTable = $p('<div class="contentTable"/>').appendTo(secgroup);
						var headerRow = $p('<div class="headerRow"/>').appendTo(contentTable);
						
						$p('<div class="radioColumn"/>').appendTo(headerRow);
						data = ['Name', 'Last Discussed', 'Reason', 'Source', 'Notes'];
						displayData('interestH', headerRow, data, 'columnHeader');
						
						interestReasonInterestInfo = acctInterestInfoList[i].interestWithReasonInterest;
						if (interestReasonInterestInfo != null && interestReasonInterestInfo != undefined)
						{
							if (Array.isArray(interestReasonInterestInfo))
							{
								for (var j = 0; j < interestReasonInterestInfo.length; j++)
								{
									var tableRow = $p('<div class="tableRow"/>').appendTo(contentTable);
									sel = '';
									if (j == 0)
									{
										sel = ' checked="checked"';
									}
									data = ['<a title="click to launch Interest page" href="' + serverURL + interestReasonInterestInfo[j].Id + '" target="_blank">' +  accountName + '</a>', interestReasonInterestInfo[j].T1C_Base__Last_Discussed__c, interestReasonInterestInfo[j].T1C_Base__Reason__c, interestReasonInterestInfo[j].T1C_Base__Source__c, interestReasonInterestInfo[j].T1C_Base__Note__c];
									//$p('<div class="radioColumn"><label class="control radio"><input name="reasonI' + i + '" type="radio" value=' + interestReasonInterestInfo[j].Id + sel + ' data-type="dateType"><span class="control-indicator"></span></label></div>').appendTo(tableRow);
									var id = 'i'+ i + '' + j;
									$p('<label class="control checkbox" for="' + id + '"><input id="' + id + '" name="reasonI' + i + '" type="checkbox" value=' + interestReasonInterestInfo[j].Id + ' data-type="dateType"><span class="control-indicator"></span></label>').appendTo(tableRow);
									displayData('interest' + i + j, tableRow, data, 'column');
								}
								btnName = 'reasonI';
							}
							//******Preserve/Merge Button************************
							interestButtons(secgroup, btnName + i);
						}
					}
				}
			}
		}
		
		//******print the account coverage infomation**********
		if (acctCoverageInfoList != null && acctCoverageInfoList != undefined && acctCoverageInfoList.length > 0)
		{
			var preserveOnlySection = $p('<div class="section preserveOnlySection"/>').appendTo(applicationContainer);
			var subSection = $p('<div class="subSection"/>').appendTo(preserveOnlySection);
			$p('<div class="sectionLabel">Account Coverage</div>').appendTo(subSection);
			var secgroup = $p('<div class="group"  id="acctCoverage"/>').appendTo(subSection);
			
			if (Array.isArray(acctCoverageInfoList) == false)
			{
				$p('<div class="subLabel">' + acctCoverageInfoList.name + '</div>').appendTo(secgroup);
				
				var contentTable = $p('<div class="contentTable" id="acctCoverageSec"/>').appendTo(secgroup);
				var headerRow = $p('<div class="headerRow"/>').appendTo(contentTable);
				
				$p('<div class="radioColumn"/>').appendTo(headerRow);
				data = ['Account','Role', 'Start Date', 'End Date', 'Is Backup', 'Product'];
				displayData('accountcoverageH', headerRow, data, 'columnHeader');
				
				accountCoverageInfo = acctCoverageInfoList.accountCoverages;
				if (accountCoverageInfo != null && accountCoverageInfo != undefined)
				{
					if (Array.isArray(accountCoverageInfo))
					{
						for (var j = 0; j < accountCoverageInfo.length; j++)
						{
							var tableRow = $p('<div id="accountcoverage0' + j + '" class="tableRow"/>').appendTo(contentTable);
							var productLookupName = '&nbsp;';
							if (accountCoverageInfo[j].T1C_Base__Product_Lookup__c != null)
							{
								productLookupName = accountCoverageInfo[j].T1C_Base__Product_Lookup__r.Name;
							}
							data = ['<a title="click to launch Account Coverage page" href="' + serverURL + accountCoverageInfo[j].Id + '" target="_blank">' +  accountCoverageInfo[j].T1C_Base__Account__r.Name + '</a>', accountCoverageInfo[j].T1C_Base__Role__c, accountCoverageInfo[j].T1C_Base__Start_Date__c, accountCoverageInfo[j].T1C_Base__End_Date__c, accountCoverageInfo[j].T1C_Base__Is_Backup__c, productLookupName];
							var id = '0' + j;
							var checkboxElem = $p('<label class="control checkbox" for="' + id + '"><input id="0:' + j + '" name="accountCoverages0" type="checkbox" value=' + accountCoverageInfo[j].Id + ' data-type="dateType"><span class="control-indicator"></span></label>').appendTo(tableRow);
							displayData('accountcoverage0' + j, tableRow, data, 'column');
							checkboxElem.on('click', function(){ acCheckboxHandlers() });
						}
					}
				}
				//******Preserve/Merge Button************************
				acButtons(secgroup, 'accountCoverages0', contentTable);
			}
			else
			{
				for (var i = 0; i < acctCoverageInfoList.length; i++)
				{
					$p('<div class="subLabel">' + acctCoverageInfoList[i].name  + '</div>').appendTo(secgroup);
					
					var contentTable = $p('<div class="contentTable" id="contCoverageSec"/>').appendTo(secgroup);
					var headerRow = $p('<div class="headerRow"/>').appendTo(contentTable);
					
					$p('<div class="radioColumn"/>').appendTo(headerRow);
					data = ['Account','Role', 'Start Date', 'End Date', 'Is Backup', 'Product'];
					displayData('accountcoverageH', headerRow, data, 'columnHeader');
					
					accountCoverageInfo = acctCoverageInfoList[i].accountCoverages;
					if (accountCoverageInfo != null && accountCoverageInfo != undefined)
					{
						if (Array.isArray(accountCoverageInfo))
						{
							for (var j = 0; j < accountCoverageInfo.length; j++)
							{
								var tableRow = $p('<div id="accountcoverage' + i + j + '" class="tableRow"/>').appendTo(contentTable);
								var productLookupName = '&nbsp;';
								if (accountCoverageInfo[j].T1C_Base__Product_Lookup__c != null)
								{
									productLookupName = accountCoverageInfo[j].T1C_Base__Product_Lookup__r.Name;
								}
								data = ['<a title="click to launch Account Coverage page" href="' + serverURL + accountCoverageInfo[j].Id + '" target="_blank">' +  accountCoverageInfo[j].T1C_Base__Account__r.Name + '</a>', accountCoverageInfo[j].T1C_Base__Role__c, accountCoverageInfo[j].T1C_Base__Start_Date__c, accountCoverageInfo[j].T1C_Base__End_Date__c, accountCoverageInfo[j].T1C_Base__Is_Backup__c, productLookupName];
								var id = i + '' + j;
								var checkboxElem = $p('<label class="control checkbox" for="' + id + '"><input id="' + id + '" name="accountCoverages' + i + '" type="checkbox" value=' + accountCoverageInfo[j].Id + ' data-type="dateType"><span class="control-indicator"></span></label>').appendTo(tableRow);
								displayData('accountcoverage' + i + j, tableRow, data, 'column');
								checkboxElem.on('click', function(){ acCheckboxHandlers() });
							}
						}
					}
					//******Preserve/Merge Button************************
					acButtons(secgroup, 'accountCoverages' + i, contentTable);
				}
				//openFieldToEdit
			}
		}
		
		//var footerPanel = $p('<div class="footer"/>').appendTo(mergeUtilityBody);
		//enableTriggers(footerPanel);
		
		$p('<script type="text/javascript"> acCheckboxHandlers() </script>').appendTo("html");
	}
	
	
	//  ==================================================================
	//  Method: displayData
	//  Description: Simplified way to display data in columns
	//  ==================================================================
	function displayData(identifier, columns, data, className)
	{
		if (data[0].indexOf('Account Coverage') != -1)
		{
			$p('<div id=' + identifier + ' class="' + className + '">' + data[0] + '</div>').appendTo(columns);
			
			var temData = data[1] == undefined ? '&nbsp;' : data[1];
			$p('<div id="' + identifier + x + '" class="' + className + '">' + temData + '</div>').appendTo(columns);
			
			temData = data[2] == undefined ? '&nbsp;' : data[2];
			$p('<div id="' + identifier + x + '" class="' + className + '">' + temData + '</div>').appendTo(columns);
			
			temData = data[3] == undefined ? '&nbsp;' : data[3];
			$p('<div id="' + identifier + x + '" class="' + className + '">' + temData + '</div>').appendTo(columns);
			
			if (data[4] == undefined || data[4] == false )
			{
				$p('<div id="' + identifier + x + '" class="' + className + '"><label class="control checkbox"><input type="checkbox" data-type="dateType" disabled="true"><span class="control-indicator"></span></label></div>').appendTo(columns);
			}
			else
			{
				$p('<div id="' + identifier + x + '" class="' + className + '"><label class="control checkbox"><input type="checkbox" checked="true" data-type="dateType" disabled="true"><span class="control-indicator"></span></label></div>').appendTo(columns);
			}
			
			temData = data[5] == undefined ? '&nbsp;' : data[5];
			$p('<div id="' + identifier + x + '" class="' + className + '">' + temData + '</div>').appendTo(columns);
		}
		else if (data[0].indexOf('Interest page') != -1)
		{
			for (var x = 0; x < data.length-1; x++)
			{
				var temData = data[x] == undefined ? '&nbsp;' : data[x];
				$p('<div id="' + identifier + x + '" class="' + className + '">' + temData + '</div>').appendTo(columns);
			}
			//print the notes with different column class (wider column)
			var temData = data[data.length-1] == undefined ? '&nbsp;' : data[data.length-1];
			className = className + ' notes';
			$p('<div id="' + identifier + x + '" class="' + className + '">' + temData + '</div>').appendTo(columns);
		}
		else
		{
			for (var x = 0; x < data.length; x++)
			{
				var temData = data[x] == undefined ? '&nbsp;' : data[x];
				if (temData == 'Notes')
				{
					//this is for the header column to be wider in interest section
					className = className + ' notes';
				}
				$p('<div id="' + identifier + x + '" class="' + className + '">' + temData + '</div>').appendTo(columns);
			}
		}
	}
	
	
	//  ==================================================================
	//  Method: blindUpdateRecords
	//  Description: Simplified way to update records
	//  ==================================================================
	function blindUpdateRecords(recordIdsToUpdate, objName)
	{
		if (recordIdsToUpdate != null && recordIdsToUpdate != undefined && recordIdsToUpdate.length > 0)
		{
			var sObjects = [];
			
			for (var x = 0; x < recordIdsToUpdate.length; x++) 
			{ 
				var sObj = new sforce.SObject(objName);
				sObj.Id = recordIdsToUpdate[x];
				sObjects[x] = sObj;
			}
			
			var uptResult = sforce.connection.update(sObjects);
			for (var y = 0; y < uptResult.length; y++) 
			{ 
				if (uptResult[y].getBoolean("success")) 
				{
					console.log(objName + " with the following id is updated:" + uptResult[y]);
				}
				else 
				{
					console.log(objName + " Failed to update the following " + uptResult[y]);
				}
	
			}
		}
	}
	
	//  ==================================================================
	//  Method: updateRecords
	//  Description: Simplified way to update records
	//  ==================================================================
	function updateRecords(recordIdsToUpdate, objName, fieldName, val, isMultiVal, isMultiField)
	{
		if (recordIdsToUpdate != null && recordIdsToUpdate != undefined && recordIdsToUpdate.length > 0)
		{
			var sObjects = [];
			
			if (Array.isArray(recordIdsToUpdate) == false)
			{
				var sObj = new sforce.SObject(objName);
				
				sObj.Id = recordIdsToUpdate;

				if (isMultiField === true)
				{
					if (isMultiVal == true && Array.isArray(val) == true)
					{
						sObj[fieldName[0]] = val[0];
						sObj[fieldName[1]] = val[1];
					}
					else
					{
						sObj[fieldName[0]] = val[0];
						sObj[fieldName[1]] = val[1];
					}
				}
				else
				{
					sObj[fieldName] = val;
				}
				sObjects[0] = sObj;
			}
			else
			{
				for (var x = 0; x < recordIdsToUpdate.length; x++) 
				{ 
					var sObj = new sforce.SObject(objName);
					
					sObj.Id = recordIdsToUpdate[x];
					
					if (isMultiField === true)
					{
						if (isMultiVal == true && Array.isArray(val) == true)
						{
							sObj[fieldName[0]] = val[x][0];
							sObj[fieldName[1]] = val[x][1];
						}
						else
						{
							sObj[fieldName[0]] = val[0];
							sObj[fieldName[1]] = val[1];
						}
					}
					else
					{
						if (isMultiVal == true && Array.isArray(val) == true)
						{
							sObj[fieldName] = val[x];
						}
						else
						{
							sObj[fieldName] = val;
						}
					}
					sObjects[x] = sObj;
				}
			}
			
			var uptResult = sforce.connection.update(sObjects);
			for (var y = 0; y < uptResult.length; y++) 
			{ 
				if (uptResult[y].getBoolean("success")) 
				{
					console.log(objName + " with the following id is updated:" + uptResult[y]);
				}
				else 
				{
					console.log(objName + " Failed to update the following " + uptResult[y]);
				}
	
			}
		}
	}
	
	//  ==================================================================
	//  Method: delRecords
	//  Description: Simplified way to delete duplicate values
	//  ==================================================================
	function delRecords(arrayValsDups, objName)
	{
		var delResult = sforce.connection.deleteIds(arrayValsDups);	
		for (var x = 0; x < delResult.length; x++) 
		{ 
			if (delResult[x].getBoolean("success")) 
			{
				console.log(objName + " with the following id is deleted:" + delResult[x]);
			}
			else 
			{
				console.log(objName + " Failed to delete the following " + delResult[x]);
			}

		}
	}
	
	//  ==================================================================
	//  Method: getNotes
	//  Description: Simplified way to get Notes from Interest records
	//  ==================================================================
	function getNotes(arrayVals, selectedId)
	{
		var notes = '';
		
		//fullNote is to check if we are adding duplicate notes. If we already have the note from
		//one interest, we should not add the same note again from another interest.
		var fullNote = '';
		
		//get notes only from selected interests. so before we get note from interest
		//see if the id is selected (in selectedIdString)
		var selectedIdString = selectedId.toString();
		
		if (arrayVals != null && arrayVals != undefined)
		{
			if (Array.isArray(arrayVals) == false)
			{
				if (arrayVals.T1C_Base__Note__c != undefined && selectedIdString.indexOf(arrayVals.Id) != -1)
				{
					notes += arrayVals.T1C_Base__Note__c;
				}
			}
			else
			{
				for (var a = 0; a < arrayVals.length; a++)
				{
					if (fullNote.indexOf(arrayVals[a].T1C_Base__Note__c) == -1)
					{
						if (arrayVals[a].T1C_Base__Note__c != undefined && selectedIdString.indexOf(arrayVals[a].Id) != -1)
						{
							var temp = notes + arrayVals[a].T1C_Base__Note__c + '\r\n\r\n';
							if (temp.length > 32000)
							{
								if (confirm("Warning – Merging these records will truncate historical notes from the Notes field. Proceed?") == true)
								{
									return (temp.substring(0, 32000));
								}
								else
								{
									return '-1';
								}
							}
							notes += arrayVals[a].T1C_Base__Note__c + '\r\n\r\n';
						    fullNote += arrayVals[a].T1C_Base__Note__c; 
						}
					}
				}
			}
		}
		return notes;
	}
	
	
	//  ==================================================================
	//  Method: getOldestAsOfDate
	//  Description: Simplified way to get oldest AsOfDate from Interest records
	//  ==================================================================
	function getOldestAsOfDate(arrayVals, selectedId)
	{
		var asOfDate = null;
		
		//get notes only from selected interests. so before we get note from interest
		//see if the id is selected (in selectedIdString)
		var selectedIdString = selectedId.toString();
		
		if (arrayVals != null && arrayVals != undefined)
		{
			if (Array.isArray(arrayVals) == false)
			{
				if (arrayVals.T1C_Base__As_Of_Date__c != undefined && selectedIdString.indexOf(arrayVals.Id) != -1)
				{
					asOfDate = new Date(arrayVals.T1C_Base__As_Of_Date__c);
				}
			}
			else
			{
				for (var a = 0; a < arrayVals.length; a++)
				{
					if (arrayVals[a].T1C_Base__As_Of_Date__c != undefined && selectedIdString.indexOf(arrayVals[a].Id) != -1)
					{
						var aDate;
						var s = arrayVals[a].T1C_Base__As_Of_Date__c;
						if (s == undefined || s == null)
						{
							aDate = null;
						}
						else
						{
							var sthisYear = s.split('-')[0];
					    	var sthisMonth = s.split('-')[1];
					    	var sthisDay = s.split('-')[2];
					    	
					    	aDate = new Date(sthisYear, sthisMonth -1, sthisDay,0,0);
						}
				    	
					    if (asOfDate >= aDate || asOfDate == null)
					    {	
					    	asOfDate = aDate;
					    }
					}				
				}
			}
		}
		return asOfDate;
	}


	//  ==================================================================
	//  Method: getDates
	//  Description: Simplified way to get dates from account coverage records
	//  ==================================================================
	function getDates(arrayVals, selectedId)
	{
		var startDate = null;
		var endDate = new Date(1971,01,01);
		if (arrayVals != null && arrayVals != undefined)
		{
			if (Array.isArray(arrayVals) == false)
			{
				startDate = new Date(arrayVals.T1C_Base__Start_Date__c);
				endDate = new Date(arrayVals.T1C_Base__End_Date__c);
			}
			else
			{
				for (var a = 0; a < arrayVals.length; a++)
				{
					var aDate;
					var s = arrayVals[a].T1C_Base__Start_Date__c;
					if (s == undefined || s == null)
					{
						aDate = null;
					}
					else
					{
						var sthisYear = s.split('-')[0];
				    	var sthisMonth = s.split('-')[1];
				    	var sthisDay = s.split('-')[2];
				    	
				    	aDate = new Date(sthisYear, sthisMonth -1, sthisDay,0,0);
					}
			    	
				    if (startDate >= aDate || startDate == null)
				    {	
				    	startDate = aDate;
				    }
				    ////////////////////
				    var eaDate
				    var e = arrayVals[a].T1C_Base__End_Date__c;
				    if (e == undefined || e == null)
					{
				    	eaDate = null;
					}
					else
					{
						var ethisYear = e.split('-')[0];
				    	var ethisMonth = e.split('-')[1];
				    	var ethisDay = e.split('-')[2];
				    	
				    	eaDate = new Date(ethisYear, ethisMonth -1, ethisDay,0,0);
					}
				    
				    if (endDate <= eaDate || eaDate == null)
				    {
				    	endDate = eaDate;
				    }
				}
			}
		}
		
		return [startDate, endDate];
	}
	
	
	//  ==================================================================
	//  Method: getDuplicates
	//  Description: Simplified way to get duplicate values
	//  ==================================================================
	function getDuplicates(arrayVals, selectedId)
	{
		var dupValues = [];
		if (arrayVals != null && arrayVals != undefined)
		{
			if (Array.isArray(arrayVals) == false)
			{
				if (arrayVals.Id != selectedId)
				{
			    	dupValues[0] = arrayVals.Id;
			    }
			}
			else
			{
				var b = 0;
				for (var a = 0; a < arrayVals.length; a++)
				{
				    if (arrayVals[a].Id != selectedId)
				    {
				    	dupValues[b] = arrayVals[a].Id;
				    	b++;
				    }
				}
			}
		}
		return dupValues;
	}
	
	//  ==================================================================
	//  Method: getRadioButtonValue
	//  Description: Simplified way to get selected radio button values
	//  ==================================================================
	function getRadioButtonValue(btnName)
	{
		var radiobtn_value;
		var values = document.getElementsByName(btnName);
		for (var y = 0; y < values.length; y++)
		{
		    if (values[y].checked === true)
		    {
		    	radiobtn_value = values[y].value;
		    }
		}
		return radiobtn_value;
	}
	
	//  ==================================================================
	//  Method: preserveButton
	//  Description: only to do preserve function
	//  ==================================================================
	function preserveButton(buttonsDiv)
	{
		var preserveButton = $p('<div class="sectionButtonBar"><div class="buttonBar"><div class="rightAlignedButtonWrapper"><button class="preserveBTN">Preserve</button></div></div></div>').appendTo(buttonsDiv);
		
		$(preserveButton).on('click', function() 
		{
			/*//check if the trigger are disabled or not
			if (tiggersEnabled == true)
			{
				alert('Please disable the merge triggers first.');
				return;
			}*/
			
			//when a user takes an action, display a warning prompt
			var proceed = false;
		    if (confirm("Any unselected records will be deleted. Proceed?") == true) 
		    {
		    	proceed = true;
		    } 
			
		    if (proceed == true)
		    {
				var radioButtonName;
				
				//******process the key account infomation**********
				var keyAcct_value = [];
				var keyAcctInfoDups = [];
				var tempkeyAcctInfoDups = [];
				var acctIds = '';
				var dupAcctIds = '';
				
				if (keyAcctInfoList != null && keyAcctInfoList != undefined && keyAcctInfoList.length > 0)
				{
					if (Array.isArray(keyAcctInfoList) == false)
					{
						keyAcct_value[0] = getRadioButtonValue('keyAccounts0');
						InfoDups = keyAcctInfoList.keyAccounts;
						tempkeyAcctInfoDups[0] = getDuplicates(InfoDups, keyAcct_value[0]);
						keyAcctInfoDups[keyAcctInfoDups.length] = tempkeyAcctInfoDups[0];
						acctIds = keyAcct_value[0];
						dupAcctIds += tempkeyAcctInfoDups[0].toString();
					}
					else
					{
						for (var x = 0; x < keyAcctInfoList.length; x++)
						{
							radioButtonName = 'keyAccounts' + x;
							keyAcct_value[x] = getRadioButtonValue(radioButtonName);
							InfoDups = keyAcctInfoList[x].keyAccounts;
							tempkeyAcctInfoDups[x] = getDuplicates(InfoDups, keyAcct_value[x]);
							keyAcctInfoDups[keyAcctInfoDups.length] = tempkeyAcctInfoDups[x];
							acctIds += keyAcct_value[x] + ':';
							dupAcctIds += tempkeyAcctInfoDups[x].toString() + ':';
						}
					}
				}
				
				
				//******process the ace list entry infomation**********
				var acctAceList_value = [];
				var acctAceListInfoDups = [];
				if (acctAceListInfoList != null && acctAceListInfoList != undefined && acctAceListInfoList.length > 0)
				{
					if (Array.isArray(acctAceListInfoList) == false)
					{
						acctAceList_value[0] = getRadioButtonValue('listEntry0');
						InfoDups = acctAceListInfoList.aceListEntries;
						acctAceListInfoDups[acctAceListInfoDups.length] = getDuplicates(InfoDups, acctAceList_value[0]);
					}
					else
					{
						for (var x = 0; x < acctAceListInfoList.length; x++)
						{
							radioButtonName = 'listEntry' + x;
							acctAceList_value[x] = getRadioButtonValue(radioButtonName);
							InfoDups = acctAceListInfoList[x].aceListEntries;
							acctAceListInfoDups[acctAceListInfoDups.length] = getDuplicates(InfoDups, acctAceList_value[x]);
						}
					}
				}
				
				
				//******process the interest subject infomation**********
				var interestSub_value = [];
				var interestSubInfoDups = [];
				if (acctInterestSubjectInfoList != null && acctInterestSubjectInfoList != undefined && acctInterestSubjectInfoList.length > 0)
				{
					if (Array.isArray(acctInterestSubjectInfoList) == false)
					{
						interestSub_value[0] = getRadioButtonValue('interestSubject0');
						InfoDups = acctInterestSubjectInfoList.interestSubject;
						interestSubInfoDups[acctAceListInfoDups.length] = getDuplicates(InfoDups, interestSub_value[0]);
					}
					else
					{
						for (var x = 0; x < acctInterestSubjectInfoList.length; x++)
						{
							radioButtonName = 'interestSubject' + x;
							interestSub_value[x] = getRadioButtonValue(radioButtonName);
							InfoDups = acctInterestSubjectInfoList[x].interestSubject;
							interestSubInfoDups[interestSubInfoDups.length] = getDuplicates(InfoDups, interestSub_value[x]);
						}
					}
				}
				
				
				var result = sforce.apex.execute("AccountPostMergeUtilityController", "ReparentKeyContact", {keyAccountIds : acctIds, dupkeyAccountIds : dupAcctIds}, 
				{ 
					onSuccess: function(result)
					{		
						delRecords(keyAcctInfoDups, "Key Accounts");
						blindUpdateRecords(keyAcct_value, "T1C_AEM__Event_Account__c");
					}, 
					onFailure: function(error)
					{		
						showError(error);
					} 
				});
				
				delRecords(acctAceListInfoDups, "Ace List Entry");
				updateRecords(acctAceList_value, 'T1C_Base__Ace_List_Entry__c', 'T1C_Base__Entry_List_Id__c', accountId, false, false);
				blindUpdateRecords(acctAceList_value, "T1C_Base__Ace_List_Entry__c");
				
				delRecords(interestSubInfoDups, "Interest Subject");
				blindUpdateRecords(interestSub_value, "T1C_Base__Interest_Subject__c");
				
				location.reload(true);
		    }
		});
	}
	
	//  ==================================================================
	//  Method: interestButtons
	//  Description: Simplified way to display top Buttons and function
	//  ==================================================================
	function interestButtons(buttonsDiv, buttonName)
	{
		var sectionButtonBar = $p('<div class="sectionButtonBar"/>').appendTo(buttonsDiv);
		var buttonBar = $p('<div class="buttonBar"/>').appendTo(sectionButtonBar);
		var rightAlignedButtonWrapper = $p('<div class="rightAlignedButtonWrapper"/>').appendTo(buttonBar);
		/*var preserveButton = $p('<button name="' + buttonName + 'Pres " class="preserveBTN">Preserve</button>').appendTo(rightAlignedButtonWrapper);
		
		$(preserveButton).on('click', function() 
		{
			//check if the trigger are disabled or not
			if (tiggersEnabled == true)
			{
				alert('Please disable the merge triggers first.');
				return;
			}
			
			//when a user takes an action, display a warning prompt
			var proceed = false;
		    if (confirm("Any unselected records will be deleted. Proceed?") == true) 
		    {
		    	proceed = true;
		    } 
			
		    if (proceed == true)
		    {
				processInterestInfomation(false, buttonName);
				location.reload(true);
		    }
		});*/
		
		var mergeButton = $p('<button name="' + buttonName + 'Merge " class="mergeBTN">Merge</button>').appendTo(rightAlignedButtonWrapper);
	
		$(mergeButton).on('click', function() 
		{
			/*//check if the trigger are disabled or not
			if (tiggersEnabled == true)
			{
				alert('Please disable the merge triggers first.');
				return;
			}*/
			
			//when a user takes an action, display a warning prompt
			var proceed = false;
		    if (confirm("Any unselected records will be deleted. Proceed?") == true) 
		    {
		    	proceed = true;
		    } 
			
		    if (proceed == true)
		    {
				processInterestInfomation(true, buttonName);
				location.reload(true);
		    }
		});
		
	}
	
	
	//  ==================================================================
	//  Method: processInterestInfomation
	//  Description: Simplified way to process Interest Infomation
	//  ==================================================================
	function processInterestInfomation(isMerge, buttonName)
	{
		//******process the interest infomation**********
		var acctInterest_value = [];
		var acctInterestInfoDups = [];
		var notesInterestInterest;
		var oldestAsOfDate;

		var fields = ['T1C_Base__Note__c', 'T1C_Base__As_Of_Date__c'];
		
		var x = Number(buttonName.slice(-1));
		
		acctInterest_value[0] = getCheckboxValue(buttonName);

		var InfoDups1;
		if (Array.isArray(acctInterestInfoList) == false)
		{
			InfoDups1 = acctInterestInfoList.interestWithReasonInterest;
		}
		else
		{
			InfoDups1 = acctInterestInfoList[x].interestWithReasonInterest;
		}
		
		//get notes from the selected interests
		notesInterestInterest = getNotes(InfoDups1, acctInterest_value[0]);
		//get oldest AsOfDate from the selected interests
		oldestAsOfDate = getOldestAsOfDate(InfoDups1, acctInterest_value[0]);
		//get all the interest as dupes except the selected interest with latest 'as of date'
		acctInterestInfoDups[acctInterestInfoDups.length] = getDuplicates(InfoDups1, acctInterest_value[0][0]);
		
		if(notesInterestInterest != '-1')
		{
			var values = [notesInterestInterest, oldestAsOfDate];

			delRecords(acctInterestInfoDups, "Interest");
			if (isMerge == true)
			{
				updateRecords(acctInterest_value[0][0], 'T1C_Base__Interest__c', fields, values, true, true);
			}
			blindUpdateRecords(acctInterest_value[0][0], "T1C_Base__Interest__c");
		}
	}
	
	
	//  ==================================================================
	//  Method: acButtons
	//  Description: Simplified way to display top Buttons and function
	//  ==================================================================
	function acButtons(buttonsDiv, buttonName, contentbtnDiv)
	{
		var sectionButtonBar = $p('<div class="sectionButtonBar"/>').appendTo(buttonsDiv);
		var buttonBar = $p('<div class="buttonBar"/>').appendTo(sectionButtonBar);
		var rightAlignedButtonWrapper = $p('<div class="rightAlignedButtonWrapper"/>').appendTo(buttonBar);
		var preserveButton = $p('<button name="' + buttonName + 'Pres " class="preserveBTN">Preserve</button>').appendTo(rightAlignedButtonWrapper);
		
		$(preserveButton).on('click', function() 
		{
			/*//check if the trigger are disabled or not
			if (tiggersEnabled == true)
			{
				alert('Please disable the merge triggers first.');
				return;
			}*/
			
			//when a user takes an action, display a warning prompt
		    if (confirm("Any unselected records will be deleted. Proceed?") == true) 
		    {
		    	processAccountCoverageInfomation(buttonName, contentbtnDiv);
				location.reload(true);
		    } 
		});
	}
	
	
//  ==================================================================
	//  Method: processAccountCoverageInfomation
	//  Description: Simplified way to process Account Coverage Infomation
	//  ==================================================================
	function processAccountCoverageInfomation(buttonName, topDiv)
	{
		//******process the account coverage infomation**********
		var accountCoverage_value = [];
		var accountCoverageInfoDups = [];
				
		var x = Number(buttonName.slice(-1));
		
		accountCoverage_value[0] = getCheckboxValue(buttonName);
		
		var InfoDups;
		if (Array.isArray(acctCoverageInfoList) == false)
		{
			InfoDups = acctCoverageInfoList.accountCoverages;
		}
		else
		{
			InfoDups = acctCoverageInfoList[x].accountCoverages;
		}
		if(accountCoverage_value[0].length < InfoDups.length)
		{
			accountCoverageInfoDups[accountCoverageInfoDups.length] = getDuplicatesWithMultiSelection(InfoDups, accountCoverage_value[0]);
			delRecords(accountCoverageInfoDups, "Account Coverages");
		}
		updateCCRecords("T1C_Base__Account_Coverage__c", topDiv);
	}
	
	//  ==================================================================
	//  Method: getCheckboxValue
	//  Description: Simplified way to get selected radio button values
	//  ==================================================================
	function getCheckboxValue(btnName)
	{
		var checkbox_value = [];
		var count = 0;
		var values = document.getElementsByName(btnName);
		for (var y = 0; y < values.length; y++)
		{
		    if (values[y].checked === true)
		    {
		    	checkbox_value[count] = values[y].value;
		    	count++;
		    }
		}
		return checkbox_value;
	}
	
	//  ==================================================================
	//  Method: getDuplicates
	//  Description: Simplified way to get duplicate values
	//  ==================================================================
	function getDuplicatesWithMultiSelection(arrayVals, selectedIds)
	{
		var dupValues = [];
		if (arrayVals != null && arrayVals != undefined)
		{
			if (Array.isArray(arrayVals) == false)
			{
				for (var b = 0; b < selectedIds.length; b++)
				{
					if (arrayVals.Id != selectedId[b])
					{
				    	dupValues[0] = arrayVals.Id;
				    }
				}
			}
			else
			{
				for (var a = 0; a < arrayVals.length; a++)
				{
					var count = false;
					for (var b = 0; b < selectedIds.length; b++)
					{
					    if (arrayVals[a].Id == selectedIds[b])
					    {
					    	count = true;
					    }
					}
					if (count === false)
					{
						dupValues[a] = arrayVals[a].Id;
					}
				}
			}
		}
		return dupValues;
	}
	
//  ==================================================================
	//  Method: updateCCRecords
	//  Description: Simplified way to update CC records
	//  ==================================================================
	function updateCCRecords(objName, topDiv)
	{
		var sObjects = [];
		var counter = 0;
		
		// get reference to element containing acctCoverageSec checkboxes
		var ccovList = topDiv["0"].childNodes;
	    
	    // 0 is the header row, 1-rest are the table data, last row is the button bar
	    for (var i=1; i<ccovList.length; i++) 
	    {
	    	var ccov = ccovList[i].childNodes["0"].childNodes["0"];
	        if (ccov != undefined && ccov.type === 'checkbox' && ccov.checked === true) 
	        {
	        	var sObj = new sforce.SObject(objName);
				
	        	var selectedCCFields = ccovList[i].childNodes;
	        	
	        	//get the new values
	        	for (var j=0; j<selectedCCFields.length; j++) 
			    {
			    	var cc = selectedCCFields[j];
			    	
			    	var defaultVal;
			    	if (j === 0)
			    	{
			    		sObj.Id = cc.childNodes["0"].value;
			    	}
			    	else if (j === 2)
			    	{
			    		sObj["T1C_Base__Role__c"] = cc.childNodes["0"].childNodes["0"].childNodes["0"].textContent;
			    	}
			    	else if (j === 3 || j === 4)
			    	{
			    		var aDate;
			    		var val =  cc.childNodes["0"].childNodes["0"].childNodes["0"].value;
			    		if (val != '')
			    		{
			    			aDate = new Date(val);
			    		}
			    		else
			    		{
			    			aDate = null;
			    		}
			    		if (j === 3)
			    		{
			    			sObj["T1C_Base__Start_Date__c"] = aDate;
			    		}
			    		else
			    		{
			    			sObj["T1C_Base__End_Date__c"] = aDate;
			    		}
			    	}
			    	else if (j == 5)
			    	{
			    		sObj["T1C_Base__Is_Backup__c"] = cc.childNodes["0"].childNodes["0"].checked;
			    	}
			    	else if (j == 6)
			    	{
			    		//var product = new sforce.SObject('T1C_Base__Product_Lookup__c');
			    		//product.Id = cc.childNodes["0"].Id;
			    		sObj["T1C_Base__Product_Lookup__c"] = cc.childNodes["0"].Id;
			    	}
			    }
	        	sObjects[counter] = sObj;
	        	counter++;
	        }
	    }
	    
	    var uptResult = sforce.connection.update(sObjects);
	    if (Array.isArray(uptResult) == false)
	    {
	    	if (uptResult.getBoolean("success")) 
			{
				console.log(objName + " with the following id is updated:" + uptResult);
			}
			else 
			{
				console.log(objName + " Failed to update the following " + uptResult);
			}
	    }
	    else
	    {
			for (var y = 0; y < uptResult.length; y++) 
			{ 
				if (uptResult[y].getBoolean("success")) 
				{
					console.log(objName + " with the following id is updated:" + uptResult[y]);
				}
				else 
				{
					console.log(objName + " Failed to update the following " + uptResult[y]);
				}
			}
		}
	}
	
	//==================================================================
	//  Method: acCheckboxHandlers
	//  Description: handle clicks on the account coverage checkboxes
	//  ==================================================================
	function acCheckboxHandlers() 
	{
		// get reference to element containing acctCoverage checkboxes
		var el = document.getElementById('acctCoverage');

		// get reference to input elements in acctCoverage container element
	    var ccov = el.getElementsByTagName('input');
	    
	    // assign openFieldToEdit function to onclick property of each checkbox
	    for (var j=0; j<ccov.length; j++) 
	    {
	        if ( ccov[j].type === 'checkbox' ) 
	        {
	        	ccov[j].onclick = openFieldToEdit;
	        }
	    }
	}
	
	//==================================================================
	//  Method: openFieldToEdit
	//  Description: when a CC is chosen, the fields will get open to edit
	//  ==================================================================
	function openFieldToEdit()
	{
		// get reference to element containing acctCoverage checkboxes
		var el = document.getElementById('accountcoverage' + this.id);
		
		if (el !== undefined && el !== null)
		{
			// get reference to input elements in acctCoverage container element
		    var selectedCCFields = el.getElementsByTagName('DIV');
					    
					    
			// 'this' is reference to checkbox clicked on
			if (this.checked  === true)
			{
			    // display selected field as editable field
			    for (var j=0; j<selectedCCFields.length; j++) 
			    {
			    	var cc = selectedCCFields[j];
			    	
			    	var defaultVal = cc.textContent;
			    	
			    	if (j === 1)
			    	{
			    		cc.innerHTML = '';
			    		rolepickList(defaultVal, cc, false);
			    	}
			    	else if (j === 4 || j === 6 )
			    	{
			    		cc.innerHTML = '';
			    		dateCalendar(defaultVal, cc, false);
			    	}
			    	else if (j == 8)
			    	{
			    		defaultVal = cc.childNodes["0"].childNodes["0"].checked;
			    		cc.innerHTML = '';
			    		if (defaultVal === true)
			    		{
			    			$p('<label class="control checkbox"><input type="checkbox" checked="true" data-type="dateType"><span class="control-indicator"></span></label>').appendTo(cc);
			    		}
			    		else
			    		{
			    			$p('<label class="control checkbox"><input type="checkbox" data-type="dateType"><span class="control-indicator"></span></label>').appendTo(cc);
			    		}
			    	}
			    	else if (j == 9)
			    	{
			    		cc.innerHTML = '';
			    		productlookup(defaultVal, cc, false);
			    	}
			    }
			}
			else
			{
				// display selected field as editable field
			    for (var j=0; j<selectedCCFields.length; j++) 
			    {
			    	var cc = selectedCCFields[j];
			    	
			    	var defaultVal;
			    	
			    	if (j === 1)
			    	{
			    		defaultVal = cc.childNodes["0"].defaultValue;
			    		cc.innerHTML = '';
			    		cc.innerHTML = defaultVal;
			    	}
			    	else if (j === 3 || j === 5 )
			    	{
			    		defaultVal = cc.defaultValue;
			    		cc.innerHTML = '';
			    		cc.innerHTML = defaultVal;
			    	}
			    	else if (j == 6)
			    	{
			    		defaultVal = cc.childNodes["0"].childNodes["0"].defaultChecked;
			    		cc.innerHTML = '';
			    		if (defaultVal === false)
			    		{
			    			cc.innerHTML = '<label class="control checkbox"><input type="checkbox" data-type="dateType" disabled="true"><span class="control-indicator"></span></label>';
			    		}
			    		else
			    		{
			    			cc.innerHTML = '<label class="control checkbox"><input type="checkbox" checked="true" data-type="dateType" disabled="true"><span class="control-indicator"></span></label>';
			    		}
			    	}
			    	else if (j == 7)
			    	{
				    	defaultVal = cc.childNodes["0"].defaultValue;
			    		cc.innerHTML = '';
			    		cc.innerHTML = defaultVal;
			    	}
			    }
			}
		}
	}
	
	//==================================================================
	//  Method: productlookup
	//  Description: Add a productlookup
	//  ==================================================================
	function productlookup(defaultVal, lookupDiv, isDisabled)
	{
		var formItem = $p('<input id="teamMemberEmployeeAutoComplete" type="text" autocomplete="off">').appendTo(lookupDiv);
		formItem[0].value = defaultVal;
		formItem[0].defaultValue = defaultVal;
		
		setTimeout(function()
		{
		    autoComplete = new SearchAutoComplete($(formItem),
		    {
		        mode: "CUSTOM:" + lookUPConfig.id,
		        maintainSelectedItem: true,
		        config: lookUPConfig,
		        select: function(item)
		        {                                                                              
		            if(item != null)
		            {
		            	formItem[0].value = item;
		            	formItem[0].Id = item.Id;
		            }                                                                                              
		        }                              
		    });                                                                                           
		}, 100);
	}
	
	//==================================================================
	//  Method: rolepickList
	//  Description: Add a rolepickList
	//  ==================================================================
	function rolepickList(defaultVal, picklistDiv, isDisabled)
	{
		var formItem = $p('<div id="picklistId"/>').appendTo(picklistDiv);
		formItem[0].id = rolepicklistConfig.id     

        if (defaultVal === null)
        {
        	defaultVal = '';
        }

        var picklistValues = rolepicklistConfig.dataProvider;
		var pickListItems = new ACEMenuButton($(formItem),{ dataProvider : picklistValues});
		formItem[0].pickListItems = pickListItems;
		formItem[0].defaultValue = defaultVal;
		
		pickListItems.setSelectedItem(defaultVal);
	}
	
	//==================================================================
	//  Method: dateCalendar
	//  Description: Add a date Calendar
	//  ==================================================================
	function dateCalendar(defaultVal, picklistDiv, isDisabled)
	{
		//var formItem = $p('<div id="dateFieldId"><input type="date" value="' + defaultVal + '" data-type="dateType"><div/>').appendTo(picklistDiv);
		
		var formItem = $p('<div id="dateFieldId"/>').appendTo(picklistDiv);
		var datePickerSpan = $p('<span/>').appendTo(formItem);
		var datePicker = $('<input/>').datepicker().appendTo(datePickerSpan);
		
		if (defaultVal != undefined && defaultVal != '' && defaultVal.split('-')[1] != undefined)
		{
			var sthisYear = defaultVal.split('-')[0];
	    	var sthisMonth = defaultVal.split('-')[1];
	    	var sthisDay = defaultVal.split('-')[2];
	    	
	    	var defaultDate = new Date(sthisYear, sthisMonth -1, sthisDay,0,0);
	    	
			datePicker.datepicker("setDate", defaultDate);
		}
		else 
		{
			datePicker.datepicker("setDate", 0);
		}
		formItem[0].datePicker = datePicker;
		formItem[0].defaultValue = defaultVal;

		datePicker.on('change', function() 
		{			
			var dateValue = new Date($(datePicker).val());
		});
	}
	
	//  ==================================================================
	//  Method: enableTriggers
	//  Description: only to do enable/disable triggers
	//  ==================================================================
	function enableTriggers()
	{
		/*var triggersButtonFooter = $p('<div class="footerButtonBar"><div class="rightAlignedButtonWrapper"></div></div>').appendTo(buttonsDiv);
		var enableTriggersButton;
		if (tiggersEnabled == true)
		{ 
			enableTriggersButton = $p('<button id="btnId" class="preserveBTN" ripple="true">Disable Triggers</button>').appendTo(triggersButtonFooter);
		}
		else
		{
			enableTriggersButton = $p('<button id="btnId" class="preserveBTN" ripple="true">Enable Triggers</button>').appendTo(triggersButtonFooter);
		}*/
		//if (tiggersEnabled == true)
		//{
			//$(enableTriggersButton).on('click', function() 
			//{
				var result = sforce.apex.execute("AccountPostMergeUtilityController", "enableDisableTriggers", {val : 'fasle'}, 
				{ 
					onSuccess: function(result)
					{		
						//error = result[0];
						//showError(error);
					}, 
					onFailure: function(error)
					{		
						showError(error);
					} 
				});
				tiggersEnabled = !tiggersEnabled;
				
				/*var elem = document.getElementById("btnId");
				if (elem.textContent == "Enable Triggers") 
				{
					elem.innerHTML = "Disable Triggers";
				}
			    else 
			    {
			    	elem.innerHTML = "Enable Triggers";
			    }*/
				//document.getElementById('btnId').style.visibility = 'hidden';
			//});
		//}
	}
	//  ==================================================================
	//  Method: closeWindow
	//  Description: Simplified way to close Window
	//  ==================================================================
	function closeWindow() 
	{       
		if( !window.close() )
		{
			var accountId = ACEUtil.getURLParameter('AccountId');
			if(accountId)
			{
				sforce.one.navigateToSObject(accountId);
			}			

		}
		/*//check if the trigger are disabled or not
		if (tiggersEnabled == false)
		{
		    if (confirm("Please enable Merge triggers before closing this form. Do you wish to continue?") == true) 
		    {
		       // x = "You pressed OK!";
		    } 
		    else 
		    {
		    	window.close(); 
		    }
		}
		else
		{
			window.close(); 
		}*/
		                          
	}
	
	//  ==================================================================
	//  Method: showError
	//  Description: Simplified way to display error message
	//  ==================================================================
	function showError(error)
	{
		$(modal).show();
		var errorDiv = document.getElementById("errorMessage");
		errorDiv.innerHTML = error;
	}
	
	
	$(modal).on('click', function() 
	{
		$(modal).hide();
	});
	
	//# sourceURL=AccountPostMerge.js
})(jQuery);