//=============================================================================
/**
 * Name: AddressManagerForm.js
 * Description: A form used to manage account addresses and bulk assign addresses to contacts that belong to that account
 *
 * Confidential & Proprietary, 2018 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1CRM.
 */
//=============================================================================
(function($)
{
    $('head').append('<title>Address Manager</title>');
    var _super = ACE.ClassUtil.inherit(AddressManagerForm, ACE.AbstractDialog);
    var _this;
    var savingToast;
    var config;
    var tabNavBar;
    var tabSection;
    //A dummy object that will be used as a template in order to create new Address objects
    var dummyObject;
    var mapIframe;
    var defaultFound;
    var tabGridListProviders = [];
    var cardCounter;
    var curGridFilter;
    var globalGridList = [];//Need this to perform grid invalidates on save
    var gridBackups = [];//Need this to update the undo state after save
    var cardsMap = {};//A map of the cards by ID to ensure that we have a reference to them
    
    /**
     * AddressManagerForm - Instance of the Address Manager's front end controller class
     *
     * @constructor
     */
    function AddressManagerForm()
    {
        this.parent = $('<div class="address-manager-wrapper"/>').appendTo('body');
        _this = this;
        _super.constructor.call(this, 
        {
            target      : this.parent,
            width       : "100%",
            height      : window.innerHeight,
            left        : "0px",
            dialogClass: "address-manager-dialog",
            buttons: 
            [
                {
                    text   : "Save",
                    id     : "FooterSaveBtn",
                    "class": "save-button confirmButton",
                    click  : function()
                    {
                        window.setTimeout(function()
                        {
                            Slick.GlobalEditorLock.commitCurrentEdit();
                        },  0);//Save Grid edits if any
                        _this._save(false);
                    }
                },
                {
                    text   : "Save and Close",
                    id     : "FooterCloseBtn",
                    "class": "save-close-button confirmButton",
                    click  : function()
                    {
                        window.setTimeout(function()
                        {
                            Slick.GlobalEditorLock.commitCurrentEdit();
                        },  0);//Save Grid edits if any
                        _this._save(true);
                    }
                },
                {
                    text   : "Cancel",
                    "class": "address-manager-cancel-button cancelButton",
                    click  : function()
                    {
                        //$("#customQuickAddSaveBtn")[0].disabled = false;
                        _this.close();
                    }
                }
            ]
        });
        this.show();
        
    }
    
    /**
     * @public
     * 
     * Closes the form when called. Typically only called by the save and close functionality or the close button
     */
    AddressManagerForm.prototype.close = function()
	{
	   //TODO: pop close dialog only when changes occur
		window.close();
	}
    
    /**
     * @public
     * 
     * Renders the form by calling the super class. Required signature for Core Wrappers
     */
    AddressManagerForm.prototype.show = function()
    {
        _super.show.call(this);    
    };
        
    /**
     * @public
     * 
     * Initializes the form by getting configs.
     */    
    AddressManagerForm.prototype.init = function()
    {
        ACE.AML.Parser.APEXImporter.import(ACE.Salesforce.baseUrl + '/apex/AddressManagerControllerPage').then(function(e,args)
        {
            //Init sforce connection
            //establish connection using session ID
            sforce.connection.sessionId = ACE.Salesforce.sessionId;

            _this.msgParser = new DOMParser();
            //this.dummyObject = {Id:null};
            
            var recordID = ACEUtil.getURLParameter('recordId');
            
            sforce.apex.execute('AddressManagerController', 'getConfig', { "recordId": recordID },
            {
                onSuccess: function(result)
                {
                    _this.config = JSON.parse(result);
                    _this._internalShow();   
                    _this._renderTabs();
                },//onSuccess
                onFailure: function(error)
                {
                    var invalidIdPattern = /.*AddressManagerController.AddressManagerException:(.*)/ ; 
                    
                    var invalidIdMsg = error.faultstring.match( invalidIdPattern ).length > 1 ? error.faultstring.match( invalidIdPattern )[1] : '';
                    
                    msg = invalidIdMsg.length > 0 ? invalidIdMsg : "An Unexpected Error Occured, please send the details to your Administrator";
                    
                    ACE.FaultHandlerDialog.show
                    ({
                        title:   "Error",
                        message: msg, 
                        error:   error.faultstring
                    });
                    
                                //If we have editors open, and changes made to items we should warn the user before saving 
                ACEConfirm.show
                (
                    msg,
                    "Error",
                    [ "OK::confirmButton"],
                    function(result)
                    {
                        _this.close();
                    },
                    {
                        "dialogClass" : "confirmationDialog aceDialog"
                    }
                );
                    
                }//onFailure
            });//sforce.apex.execute 
        });
    }
    
    /**
     * @protected
     * 
     * Renders the child components onto the form. Required signature for Core Wrappers
     */
    AddressManagerForm.prototype._createChildren = function()
    {
        var main = $('<main class="address-main-content"></main>').appendTo(this.parent);
        
        if( _this.config.tabsList.length > 1 ){
            //Render the nav bar which we'll attach tab buttons to. But not if there is only 1 tab
            _this.tabNavBar = $('<div class="tab-nav-bar"/>').appendTo(main);
        }
        //Render the Tabs wrapper. This will contain the content of all tabs
        _this.tabSection = $('<div class="tab-section"/>').appendTo(main);
        
        _this.topHeaderSection = $('<header class="tab-content-header"></header>').appendTo( _this.tabSection );
        
        var headerDiv = $('<div class="title-wrapper"></div>').appendTo(_this.topHeaderSection);
        
        var innerHeader = $('<h2 class="tab-content-title"></h2>').appendTo(headerDiv);
        innerHeader.text(this.config.title);
        
        this.title("Address Manager");
        
        $('.ui-button.ui-widget.ui-state-default.ui-corner-all.ui-button-icon-only.ui-dialog-titlebar-close').hide();
        
        _this.cardCounter = 0;
    }
    
    /**
     * @protected
     * 
     * Renders the tabs and the nav bar. Uses the config that was fetched in the init
     */
    AddressManagerForm.prototype._renderTabs = function()
    {
        _this.cardsMap = {};
        _this.tabGridListProviders = [];
        _this.globalGridList = [];
        _this.globalAddressMap = {};
        _this.addressGrids = [];
        _this.cardContainers = [];
        _this.globalEditFlag = {};
        _this.gridBackups = [];
        //A dummy Address object. We deep copy this when creating new addresses
        _this.dummyObject = 
        {
            Id:null,
            T1C_Base__Account__c : _this.config.accountId
        };
        
        //normalize the id fields to be consistent along all SlickGrids. SFDC backend is smart enough to see this and will assign corretly when we save
        $.each(_this.config.sharedAddressList, function(index, item)
        {
            //acedatagrid will not show the record due to the uppercasing in "Id". Making a copy of the Id to a lower case "id"
            if(item.id == null || item.id == undefined )
            {
              item.id = item.Id;
              delete item.Id;
            }
            
            //Mark off the Billing and Shipping addresses
            if(item.T1C_Base__BillingAccounts__r != undefined && item.T1C_Base__BillingAccounts__r != null)
            {
                _this.billingAddressId = item.id;
            }
            if(item.T1C_Base__ShippingAccounts__r != undefined && item.T1C_Base__ShippingAccounts__r != null)
            {
                _this.shippingAddressId = item.id;
            }
            
            _this.globalAddressMap[item.id] = item;
            
        });
        
        
        $.each(_this.config.tabsList, function(index,item){
            
            var currentTab = $('<div class="tab-content-wrapper"/>').appendTo(_this.tabSection);
            
            var curGrid = _this._renderTab(currentTab, item);
            
            if( _this.config.tabsList.length > 1 ){
                //Render each tab. If there is only one Tab, we do not need the buttons
                var tabButton = $('<button class="tablinks"></button>').appendTo(_this.tabNavBar);
                
                tabButton.text(item.tabName);

                tabButton.on('click', 
                function(e,args){
                    //if the button is clicked, hide all other tabs except this one
                    if(Object.keys(_this.globalEditFlag).length > 0)
                    {
                        ACEConfirm.show
                        (
                            "You have unsaved Address Changes, please save your changes before changing tabs",
                            "Confirm",
                            [ "OK::confirmButton","Cancel::cancelButton"],
                            function(result)
                            {
                                console.log("Intercepted Tab change while Address is being edited");
                            },
                            {
                                "dialogClass" : "confirmationDialog aceDialog"
                            }
                        );	
                        return;
                    }
                    
                    _this.currentTab = item.tabName;
                    
                    $(".tablinks").removeClass("selected");
                    
                    $(e.currentTarget).addClass("selected");//add selected class to the current tab
                    
                    $.each( $(_this.tabSection[0]).children(".tab-content-wrapper") ,function(index, item)
                    {
                        item.style.display = "none";
                    });
                    currentTab[0].style.display = "";
                    //Look into the shared address list and refresh the right panel from it
                    curGrid.grid.resizeCanvas();
                });
                
                var curTab = decodeURI(ACEUtil.getURLParameter('currentTab'));
                
                //If we refreshed from a save or delete, find the current Tab
                if(curTab == "null")
                {
                    if(!item.defaultTab || _this.defaultFound)
                    {
                        currentTab[0].style.display = "none";
                    }else
                    {
                        _this.currentTab = item.tabName;
                        $(tabButton).addClass("selected");
                        _this.defaultFound = true;
                        curGrid.grid.resizeCanvas();
                    }
                } 
                else
                {
                    
                    if(item.tabName == curTab)
                    {
                        _this.currentTab = item.tabName;
                        $(tabButton).addClass("selected");
                        _this.defaultFound = true;
                        curGrid.grid.resizeCanvas();
                    }else
                    {
                        currentTab[0].style.display = "none";
                    }
                }
            }//if( _this.config.tabsList.length > 1 )

            
        });
    }//AddressManagerForm.prototype._renderTabs
    
    /**
     * @protected
     * 
     * Renderer function for each tab section.
     *
     * @param {jquery component} component - the target element to append the tab to
     * 
     * @param {object}           config    - All configurations for this tab. Contains grid fields and editor fields
     */
    AddressManagerForm.prototype._renderTab = function(component, config)
    {
        //Render left section returns the generated grid
        var currentLeftGrid = this._renderLeftSection(component, config);
                
        if(!config.isAccountAddressTab){
            _this.cardContainers.push( this._renderRightSection(component, config, currentLeftGrid) );
        }else
        {
            this._renderAddressRightSection(component, config, currentLeftGrid);
        }
        return currentLeftGrid;//return the grid reference. 
        //Needed for grid resizing to make sure all of the grids are the same size after everything is rendered
    }
    
    /**
     * @protected
     * 
     * Renderer function for the left panel of each tab.
     *
     * @param {jquery component} component - the target element to append the panel to
     * 
     * @param {object}           config    - All configurations for this panel. Contains grid fields
     *
     * @return returns the generated left grid
     */
    AddressManagerForm.prototype._renderLeftSection = function(component, config)
    {
        //Render the grid panel on the left.
        var leftPanel = $('<div class="tab-panel-left"/>').appendTo(component);
        
        //Header Components
        var tabLeftHeaderSection = $('<div class="tab-panel-left-header-section"></div>').appendTo(leftPanel);
		var tabLeftHeader = $('<header class="tab-panel-left-header"></header>').appendTo(tabLeftHeaderSection);
        var tabLeftHeadThree = $('<h3 class="header-title">'+ config.tabName +'</h3>').appendTo(tabLeftHeader);//Make AFR
        //Select contact below and update their address on the right
		var tabLeftHeaderP = $('<p class="instruction-text"></p>').appendTo(tabLeftHeaderSection);//Make AFR
        
        tabLeftHeaderP.text( config.instructionLabel );

        var inputsSection = $('<div class="tab-panel-left-input-section"/>').appendTo(tabLeftHeaderSection);
        //Render filter input text field for the grid
        var filterInput = $('<la-input class="tab-panel-left-filter"/>').appendTo(inputsSection);
        $(filterInput).attr("placeholder", config.gridFilterPlaceHolder);
        //Find the inner input element and set the autocomplete attribute to off
        var input = filterInput.find('input');
        input.attr("autocomplete","off");

        if(!_this.config.hideBilling)
        {
            //Render checkboxes for billing and shipping
            var billingBoxWrapper = $('<div class="checkbox-wrapper primary" data-aml-properties="" title="" data-has-title="true"/>').appendTo(inputsSection);
            var billingBoxLabel = $('<label class="control checkbox"/>').appendTo(billingBoxWrapper);
            var billingBox = $('<input type="checkbox" class="address-check">').appendTo(billingBoxLabel);
            var billingSpan = $('<span class="control-indicator"></span>').appendTo(billingBoxLabel);
            var billingBoxInnerLabel = $('<span class="controlLabel primaryAddress"></span>').appendTo(billingBoxLabel);

            billingBoxInnerLabel.text( _this.config.billingBoxLabel );

        }
        
        if(!_this.config.hideShipping)
        {
            var shippingBoxWrapper = $('<div class="checkbox-wrapper delivery" data-aml-properties="" title="" data-has-title="true"/>').appendTo(inputsSection);
            var shippingBoxLabel = $('<label class="control checkbox"/>').appendTo(shippingBoxWrapper);
            var shippingBox = $('<input type="checkbox" name="shipping-box">').appendTo(shippingBoxLabel);
            var shippingSpan = $('<span class="control-indicator"></span>').appendTo(shippingBoxLabel);
            var shippingBoxInnerLabel = $('<span class="shipping-checkbox-label shippingAddress"></span>').appendTo(shippingBoxLabel);

            shippingBoxInnerLabel.text( _this.config.shippingBoxLabel );
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //Render the grid using the columns provided in the AFRs for this tab
        var gridSection = $('<div class="tab-panel-left-grid-section"/>').appendTo(leftPanel);

        var gridCols = ACE.AddressManagerFormItemFactory.getGridColumns(config);
        
        var accountLocFilter = function(item, value)
        {
            if(!item.T1C_Base__Mailing_Address_Id__c)
            {
                return false;
            }

            var locName = _this.globalAddressMap[item.T1C_Base__Mailing_Address_Id__c]['T1C_Base__Address_Name__c'];

            return locName && locName.match(value) && locName.match(value).length > 0;
        };

        //In order to make the BIlling and shipping checkboxes tab-specific we need to use vars
        var tabFilterBilling = false;
        var tabFilterShipping = false;

        var tabGrid = new ACEDataGrid(gridSection, 
        {  
            //autoHeight              : true, 
            forceFitColumns         : true, 
            explicitInitialization  : true,
            enableColumnReorder     : false,
            textFilter              : filterInput,
            //fullWidthRows           : true,
            editable                : false,
            headerRowHeight         : 40,
            autoEdit                : false,
            additionalFilterFunction: function(item)
            {
                var filterDisabled = (tabFilterBilling != true && tabFilterShipping != true);
                
                //Make the match fields AFR configurable by tab
                var matchesBilling = (item["id"] && item["id"] == _this.billingAddressId) || 
                                     (item["T1C_Base__Mailing_Address_Id__c"] && item["T1C_Base__Mailing_Address_Id__c"] == _this.billingAddressId);
                
                var matchesShipping = (item["id"] && item["id"] == _this.shippingAddressId) || 
                                      (item["T1C_Base__Mailing_Address_Id__c"] && item["T1C_Base__Mailing_Address_Id__c"] == _this.shippingAddressId);
                
                
                return filterDisabled || (tabFilterBilling == true && matchesBilling) || (tabFilterShipping == true && matchesShipping);
            },
            itemRendererFilterFunctions : 
            {
                "AccountLocRenderer" : accountLocFilter
            }
            //maxNumberOfRows         : 10
        });
        
        //Event listeners for the checkboxes. When checked, filter by billing/shipping address
        $(billingBox).on("click", 
        function(e, args)
        {
            tabFilterBilling = e.currentTarget.checked;
            tabGrid.grid.invalidate();
            tabGrid.dataProvider.refresh();
        });
        
        $(shippingBox).on("click", 
        function(e, args)
        {
            tabFilterShipping = e.currentTarget.checked;
            tabGrid.grid.invalidate();
            tabGrid.dataProvider.refresh();
        });
        
        tabGrid.setColumns( gridCols,
        {
            "UndoItemRenderer"   : new ItemRenderer(function(row, cell, value, columnDef, dataContext)
            {
                var buttonDiv;
                
                if (value)
                {                     
                    buttonDiv =  '<div data-type="undoButton" class="undoButton"></div>';
                }else{
                    buttonDiv =  '<div></div>';//Empty div
                }
                
                return buttonDiv;
                
            }),
            "AccountLocRenderer" : new ItemRenderer(function(row, cell, value, columnDef, dataContext)
            {
                if (value.length > 0)
                {          
                    //This Column only looks at the Mailiing Address ID of the item
                    buttonDiv = _this.globalAddressMap[value]['T1C_Base__Address_Name__c'];
                    buttonDiv = buttonDiv.replace(/[<>=]/g, '');
                }
                else
                {
                    //The plus sign is intentional
                    buttonDiv =  '<div data-type="addLocation" class="add-location">+</div>';
                }
                
                return buttonDiv;
                
            })
        });
        
        var initEditor = function(e)
        {
            var target = $(e.event.target);
            var dataType = target.attr("data-type");
            //If the undo button was clicked, refer to the old data for the item
            if(dataType == 'undoButton')
            {
                //Undo the changes by referring to the gridDataBackup map. We use the ID as the key
                var origItem = $.extend({},config.gridDataBackup[e.data.id]);
                tabGrid.dataProvider.updateItem(e.data.id, origItem);
            }
            else if (dataType == "addLocation" && _this.config.addressCreate)
            {
                //Take this Contact's address info and stick it into an address editor in the current panel
                var contact = JSON.parse( JSON.stringify(config.gridDataBackup[e.data.id]) );
                var newItem = JSON.parse(JSON.stringify(_this.dummyObject));
                newItem.id = 'newItem';//We do not need unique IDs here for new objects since they'll be cut out in the apex backend. We just need to mark them as new
                
                //Map the Contact Address fields into the Address fields
                $.each(config.applyToSelectedFieldMap, 
                function(addressField, sObjectField)
                {
                    newItem[addressField] = contact[sObjectField] ? contact[sObjectField] : null;
                });
                
                var cardsContainer = $(e.event.target).parents('.tab-content-wrapper').find('.tab-card-container');
                var rightPanel = $(e.event.target).parents('.tab-content-wrapper').find('.tab-panel-right');
                
                var mapDiv = $(rightPanel).find('.map-container').hide()
                var card = $('<div id="card' + _this.cardCounter++ +  '" class="card-container"/>').appendTo(cardsContainer);
                
                var cardClickHandler = function(e)
                {   
                    _this._selectCard( $(e.target), e.data, mapDiv );
                };
                
                card.on("click", newItem, cardClickHandler);

                _this._renderCard(newItem, card, config, tabGrid);
                //now we pop open the modal to create a new Address object
                _this._renderEditModal( newItem, card, config.editControls, tabGrid )
            }
            tabGrid.grid.invalidate()
            tabGrid.dataProvider.refresh();
        };
        
        if(!config.isAccountAddressTab){
            tabGrid.on("itemClick", function(e)
            {
                //If we have an edit currently going on, warn the user
                if( Object.keys(_this.globalEditFlag).length > 0 )
                {
                    //If we have editors open, and changes made to items we should warn the user before saving 
                    ACEConfirm.show
                    (
                        'Warning: You have unsaved Address Changes. Discard them?',
                        "Confirm",
                        [ "OK::confirmButton","Cancel::cancelButton"],
                        function(result)
                        {
                            if(result == 'OK')
                            {  
                                initEditor(e);
                            }//else do nothing
                        },
                        {
                            "dialogClass" : "confirmationDialog aceDialog"
                        }
                    );
                }else
                {
                    initEditor(e);
                } 
            });
        }

        tabGrid.grid.onBeforeEditCell.subscribe( 
        function(e,args)
        {
            return !JSON.parse(args.column.disabled);
        });
        
        tabGrid.grid.init();
        
        $(window).resize(function() {
            if( $(component)[0].style.display != "none" )//calling resize on hidden grids causes a bug
            {
                tabGrid.grid.resizeCanvas();
            }
        });
        
        $.each(config.gridData, function(index, item)
        {
            //acedatagrid will not show the record due to the uppercasing in "Id". Making a copy of the Id to a lower case "id"
            if(item.id == null || item.id == undefined )
            {
              item.id = item.Id;
              delete item.Id;
            }
        });
        
        
        $.each(config.gridDataBackup, function(index, item)
        {
            //acedatagrid will not show the record due to the uppercasing in "Id". Making a copy of the Id to a lower case "id"
            if(item.id == null || item.id == undefined )
            {
              item.id = item.Id;
              delete item.Id;
            }
        });
        
        if(!config.isAccountAddressTab){
            tabGrid.addItems( config.gridData );
            _this.globalGridList.push(tabGrid);
            _this.gridBackups.push(config.gridDataBackup);
            //_this.tabGridListProviders = _this.tabGridListProviders.concat( config.gridData );//we exclude the Address data since it's managed in its own shared list
        }else
        {
            tabGrid.addItems( _this.config.sharedAddressList );//if it's the address tab, we use the global address list
            _this.addressGrids.push(tabGrid);
            
        }
        
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        return tabGrid;
    }//AddressManagerForm.prototype._renderLeftSection
    
    /**
     * @protected
     * 
     * Renderer function for the right panel of the Addresses tab.
     *
     * @param {jquery component} component - the target element to append the panel to
     * 
     * @param {object}           config    - All configurations for this panel.
     *
     * @param {object}           leftGrid  - The left grid for this panel. Necessary for adding new addresses to the grid
     */
    AddressManagerForm.prototype._renderAddressRightSection = function(component, config, leftGrid)
    {
        var rightPanel = $('<div class="tab-panel-right"/>').appendTo(component);
        var tabPanelHeader = $('<div class="tab-panel-right-header-section"></div>').appendTo(rightPanel);
        var tabPanelInnerHeader = $('<header class="tab-panel-right-header"></header>').appendTo(tabPanelHeader);
        var tabPanelHeadThree = $('<h3 class="header-title"></h3>').appendTo(tabPanelInnerHeader);//Make AFR configurable?

        tabPanelHeadThree.text( _this.config.addressRightPanelHeader );

        var tabControls = $('<div class="tab-panel-right-controls"/>').appendTo(tabPanelHeader);
        
        var cardsContainer = $('<div class="tab-card-container"/>').appendTo(rightPanel);
        
        var mapButton = $('<button class="map-collapse" title="Click to collapse map"></button>').appendTo(rightPanel);
        var mapDiv = $('<div class="map-container"></div>').appendTo(rightPanel);
        
        if(_this.config.collapseMapDiv==true)
        {
            $(mapDiv).slideToggle(300);
        }
        
        if(_this.config.hideMap==true)
        {
            $(mapButton).hide();
            $(mapDiv).hide();
        }
        
        mapButton.on("click",
        function(e)
        {
            if( $(mapButton).hasClass("map-collapse") )
            {
                $(mapButton).removeClass("map-collapse").addClass("map-expand");
                $(mapButton).prop("title","Click to show map")
            }else
            {
                $(mapButton).removeClass("map-expand").addClass("map-collapse");
                $(mapButton).prop("title","Click to collapse map")
            }
            $(mapDiv).slideToggle(300);
        });
        
        
        if(_this.config.addressCreate == true)
        {
            var addNewButton = $('<button class="card-create-button"></button>').appendTo(tabControls);

            addNewButton.text(_this.config.createNewButtonLabel);
            
            addNewButton.on("click",
            function(e)
            {
                //Create a deep copy of the template: the JSON parse command creates a new reference, while copying absolutely all of the properties of the stringified object.
                //This is because the stringify command converts the dummy object into a string, so the reference itself is lost on the way into the parse command.
                var newItem = JSON.parse(JSON.stringify(_this.dummyObject));
                newItem.id = 'newItem';//We do not need unique IDs here for new objects since they'll be cut out in the apex backend. We just need to mark them as new
                
                cardsContainer.empty();
                
                //now we pop open the modal to create a new Address object
                _this._renderAddressEditModal(cardsContainer, config.editControls, newItem, leftGrid);
            });
        }
        
        //If user does not have edit rights, the modal gets disabled on its own
		leftGrid.grid.onClick.subscribe(
		function(e,args)
		{
			//Remove all other edit modals
			cardsContainer.empty();
			//show edit modal
			var item = this.getDataItem(args.row);
			_this._renderAddressEditModal(cardsContainer, config.editControls, item, leftGrid);
            _this._getMapIFrame(item, mapDiv);
		});
		
		//Set selected item to the first item, then render the edit modal for it
		leftGrid.grid.setSelectedRows([0]);
		var firstItem = leftGrid.grid.getDataItem(0);
        if(firstItem){
            _this._renderAddressEditModal(cardsContainer, config.editControls, firstItem, leftGrid);
        }
        
        _this._getMapIFrame(firstItem, mapDiv);
    }
    
    /**
     * @protected
     * 
     * Renderer function for the right panel of the Addresses tab.
     *
     * @param {jquery component} component - the target element to append the editor to
     * 
     * @param {object}           config    - All configurations for this editor.
     *
     * @param {object}           item      - Address item being edited
     *
     * @param {object}           tabGrid   - The left grid for this panel. Necessary for adding new or updating addresses
     *                                       and reflecting on the grid
     */
    AddressManagerForm.prototype._renderAddressEditModal = function(component, config, item, tabGrid)
    {
        //Edit the associated Address item. Afterwards look for its ID in all of the tab grid data providers and update the SObjects (Associated to the edited address)
        //ID fields are T1C_Base__Mailing_Address_Id__c or id 
        
        //create a deep clone of the original. do not want to pass references here
        var originalItem = JSON.parse( JSON.stringify(item) );
        
        //tabGrid refers to the left panel address grid. It needs to be invalidated after we change the shared address list entry for this address
        var addressEditor = $('<div class="address-editor"/>').appendTo(component);

        if(_this.config.addressDelete == true)
        {
            
            //Billing and Shipping addresses are not deleteable under any circumstance; but they can be overridden
            var allowDelete = (!item.T1C_Base__BillingAccounts__r && !item.T1C_Base__ShippingAccounts__r);
            
            if(allowDelete)
            {
                var delOptions = 
                {
                    callback :  function(e, val, options)
                                {
                                    if(item.id != 'newItem')
                                    {
                                        ACEConfirm.show
                                        (
                                            _this.config.cardDeleteMessage,
                                            "Confirm",
                                            [ "OK::confirmButton","Cancel::cancelButton"],
                                            function(result)
                                            {
                                                if(result == 'OK')
                                                {_this._deleteButtonhandler(val, null, true);}//else do nothing
                                            },
                                            {
                                                "dialogClass" : "confirmationDialog aceDialog"
                                            }
                                        );	
                                    }
                                }
                    ,label   :  'Search'
                                
                };
                
                ACE.AddressManagerFormItemFactory
                    .inputMap['DELETE']
                    .call(_this, addressEditor, item, delOptions, disableModal);
                
            }
            
        }
        
        var addressSearchInput = $('<input class="google-search-input" placeholder="' + _this.config.searchPlaceHolder + '"></input>').appendTo(addressEditor);
        
        //Attach the google autocomplete above the other fields
        var autocomplete = new google.maps.places.Autocomplete(addressSearchInput[0]);

        var disableModal = false;
        //disable if we dont have edit rights or if the address is billing/shipping and we're not allowed to edit
        if(_this.config.addressEdit!=true || 
            (item.T1C_Base__BillingAccounts__r && _this.config.allowBillShip!=true) || 
            (item.T1C_Base__ShippingAccounts__r && _this.config.allowBillShip!=true))
        {
            disableModal = true;
            $(addressSearchInput).prop('disabled', true);
        }
        
        $(addressSearchInput).focus();
       
        $(addressSearchInput).on('keyup', function(e, args) 
        {
            if(item.id == 'newItem' && e.keyCode == 8)//8 is backspace
            {
                var curVal = $(this).val();
                //if emptied, we have to wipe the fields in the edit modal
                var newAddress = 
                {
                    address_components : []
                };
                if(curVal=="")
                {
                    _this._applyGoogleSearchToEditor(newAddress, addressEditor, item);
                }
            }
        });
        //Group them by row <div class="address-card-row">
        //We create an array of row divs and use the row num to assign. This MUST be configured properly as a combination of order and row number
        var addressRowArray = [];
        var addressRow  = null;
        $.each(config.editModalColumns, function(index, column)
        {
            //If we detect a new row, create one
            if(addressRow == null || column.rowNum > addressRowArray.length)
            {
                addressRow = $('<div class="address-card-row">').appendTo(addressEditor);
                addressRowArray.push(addressRow);
            }
            
            var fieldType = column.fieldType.toUpperCase();
            if( ACE.AddressManagerFormItemFactory.inputMap[fieldType] != null || ACE.AddressManagerFormItemFactory.inputMap[fieldType] != undefined )
        	{
        		ACE.AddressManagerFormItemFactory
        			.inputMap[fieldType]
        			.call(_this, addressRow, item, column, disableModal);
        	}
        });
        
        var editorFooter = $('<div class="address-card-row address-card-footer"></div>').appendTo(addressEditor);
        
        //add the Billing/shipping checkboxes
        if(_this.config.allowBillShip)
        {
            item.setBilling = (item.T1C_Base__BillingAccounts__r != undefined && item.T1C_Base__BillingAccounts__r != null);
            item.setShipping = (item.T1C_Base__ShippingAccounts__r != undefined && item.T1C_Base__ShippingAccounts__r != null);
            
            if(!_this.config.hideBilling)
            {
                var billingColumn = 
                {
                    dataField : 'setBilling',
                    label     : _this.config.billingBoxLabel,
                    innerCSS  : 'primary-loc'
                };
                
                ACE.AddressManagerFormItemFactory
                        .inputMap["CHECKBOX"]
                        .call(_this, editorFooter, item, billingColumn, disableModal);
            }       

            if(!_this.config.hideShipping)
            {
                var shippingColumn = 
                {
                    dataField : 'setShipping',
                    label     : _this.config.shippingBoxLabel,
                    innerCSS  : 'shipping-loc'
                };
                
                ACE.AddressManagerFormItemFactory
                        .inputMap["CHECKBOX"]
                        .call(_this, editorFooter, item, shippingColumn, disableModal);
            }       
        } 
        else 
        {
            $('<div class="card-editor-checkbox-placeholder"></div>').appendTo(editorFooter);
        }
        
        var renderButtons = _this.config.allowBillShip || (item.T1C_Base__BillingAccounts__r == undefined && item.T1C_Base__ShippingAccounts__r == undefined);
        //If billing is allowed or item is not billing while the allow Billing and Shipping is set to false
        if( renderButtons )
        {
            //Add the save and cancel buttons
            
            var saveMessage = item.id=='newItem' ? _this.config.cardNewSaveMessage : _this.config.cardSaveMessage;
            
            //Add the save and cancel buttons
            var saveButton = $('<button class="card-editor-save"></button>').appendTo(editorFooter);
            saveButton.text(_this.config.saveCardEditLabel);

            $(saveButton).on("click",
            function(e,args)
            {
                //on save do aceConfirm and save on Ok
                ACEConfirm.show
                (
                    saveMessage,
                    "Confirm",
                    [ "OK::confirmButton","Cancel::cancelButton"],
                    function(result)
                    {
                        if(result == 'OK')
                        {
                            delete _this.globalEditFlag[item.id];
                            _this._saveButtonhandler(addressEditor, item, null, tabGrid, null);
                        }//else do nothing
                    },
                    {
                        "dialogClass" : "confirmationDialog aceDialog"
                    }
                );	
                
            });
            
            
            var cancelButton = $('<button class="card-editor-cancel"></button>').appendTo(editorFooter);
            cancelButton.text(_this.config.cancelCardEditLabel);

            $(cancelButton).on("click",
            function(e,args)
            {
                //Revert the item to the pre-edit state. using a loop to keep references the same
                //in this context orig item will be all nulls if the items is new; thats what we want
                //TODO ACE CONFIRM?
                for(var prop in originalItem) item[prop]=originalItem[prop];
                
                if(item.id == 'newItem')//if it was a new one, get the selected item in the tab's grid
                {
                    item = tabGrid.getSelectedItems()[0];//This special grid does not have multi select enabled
                }
                
                delete _this.globalEditFlag[item.id];
                addressEditor.remove();
                //Afterwards, refresh the edit modal
                _this._renderAddressEditModal(component, config, item, tabGrid);
            });
            
            autocomplete.addListener('place_changed', function() 
            {
                var address = autocomplete.getPlace();
                _this._applyGoogleSearchToEditor( address, addressEditor, item );
                _this.globalEditFlag[item.id] = saveButton;
            });
            
            //Listen for edits, then trip the flag
            $(addressEditor).on("keyup", 
            function(e, args)
            {
                if(! $(e.originalEvent.target).hasClass("google-search-input") )
                {
                    _this.globalEditFlag[item.id] = saveButton;
                }
            });
            
        }
        
        
        
    }//_renderAddressEditModal
    
    /**
     * @protected
     * 
     * Function for adding CSS classes to the currently selected card
     *
     * @param {jquery component} card    - the target element to append the editor to
     * 
     * @param {object}           address - Address item being selected. Needs to be passed into the google map div
     *
     * @param {iframe}           mapDiv  - The iframe parent that houses the google map
     */
    AddressManagerForm.prototype._selectCard = function(card, address, mapDiv)
    {
        //Unfocus all other cards and select the target card
        card.closest('.card-container').siblings('.card-container').removeClass('card-focused');
        card.closest('.card-container').addClass('card-focused');
        _this._getMapIFrame(address, mapDiv);
    }

    /**
     * @protected
     * 
     * Renderer function for the right panel of each non-address tab.
     *
     * @param {jquery component} component - the target element to append the panel to
     * 
     * @param {object}           config    - All configurations for this panel.
     *
     * @param {object}           tabGrid   - The left grid for this panel. Necessary for adding new addresses to the grid
     */
    AddressManagerForm.prototype._renderRightSection = function(component,config, tabGrid)
    {
        //Render the card panel on the right.
        var rightPanel = $('<div class="tab-panel-right"/>').appendTo(component);
        var tabPanelHeader = $('<div class="tab-panel-right-header-section"></div>').appendTo(rightPanel);
        var tabPanelInnerHeader = $('<header class="tab-panel-right-header"></header>').appendTo(tabPanelHeader);
        var tabPanelHeadThree = $('<h3 class="header-title"></h3>').appendTo(tabPanelInnerHeader);
        tabPanelHeadThree.text( _this.config.rightPanelHeader );

        var tabPanelInstructions = $('<p class="instruction-text">Select address below to apply to contacts selected</p>').appendTo(tabPanelHeader);//make AFR configurable
        var tabControls = $('<div class="tab-panel-right-controls"/>').appendTo(tabPanelHeader);
        //Render the filter and the add new button controls
        
        var tabTextFilter = $('<la-input class="right-panel-filter"></input>').appendTo(tabControls);
        $(tabTextFilter).attr("placeholder", _this.config.rightPanelFilterPlaceHolder);
        
        var input = tabTextFilter.find('input');
        input.attr("autocomplete","off");
        
        
        if(_this.config.addressCreate == true)
        {
            var addNewButton = $('<button class="card-create-button"></button>').appendTo(tabControls);

            addNewButton.text(_this.config.createNewButtonLabel);

            var cardClickHandler = function(e)
            {   
                _this._selectCard( $(e.target), e.data, mapDiv );
            };

            addNewButton.on("click",
            function(e)
            {
                //Create a deep copy of the template: the JSON parse command creates a new reference, while copying absolutely all of the properties of the stringified object.
                //This is because the stringify command converts the dummy object into a string, so the reference itself is lost on the way into the parse command.
                var newItem = JSON.parse(JSON.stringify(_this.dummyObject));
                newItem.id = 'newItem';//We do not need unique IDs here for new objects since they'll be cut out in the apex backend. We just need to mark them as new
                
                $(rightPanel).find('.map-container').hide()
                var card = $('<div id="card' + _this.cardCounter++ +  '" class="card-container"/>').appendTo(cardsContainer);
                
                card.on("click", newItem, cardClickHandler);

                _this._renderCard(newItem, card, config, tabGrid);
                //now we pop open the modal to create a new Address object
                _this._renderEditModal( newItem, card, config.editControls, tabGrid )
            });
        }
        //Render cards for every existing shared address
        var cardsContainer = $('<div class="tab-card-container"/>').appendTo(rightPanel);
        
        $.each(_this.config.sharedAddressList, function(index,item)
        {
            var card = $('<div id="card' + _this.cardCounter++ +  '" class="card-container"/>').appendTo(cardsContainer);
            
            //Associate this card with this address
            var cardsList = _this.cardsMap[item.id] ? _this.cardsMap[item.id] : [];
            cardsList.push( card );
            
            _this.cardsMap[item.id] = cardsList;
            card.on("click", item, cardClickHandler) ;
            
            _this._renderCard(item, card, config, tabGrid);
        });
        
        //Highlight the first card
        var firstCard = $(cardsContainer).children()[0];

        $(firstCard).addClass('card-focused')
        
        var mapButton = $('<button class="map-collapse" title="Click to collapse map"></button>').appendTo(rightPanel);
        var mapDiv = $('<div class="map-container"></div>').appendTo(rightPanel);
        
        if(_this.config.hideMap==true)
        {
            $(mapButton).hide();
            $(mapDiv).hide();
        }
        
        if(_this.config.collapseMapDiv==true)
        {
            $(mapDiv).slideToggle(300);
        }
        
        mapButton.on("click",
        function(e)
        {
            if( $(mapButton).hasClass("map-collapse") )
            {
                $(mapButton).removeClass("map-collapse").addClass("map-expand");
                $(mapButton).prop("title","Click to show map")
            }else
            {
                $(mapButton).removeClass("map-expand").addClass("map-collapse");
                $(mapButton).prop("title","Click to collapse map")
            }
            $(mapDiv).slideToggle(300);
        });
        
        
        //Set the map to the first card
        _this._getMapIFrame(_this.config.sharedAddressList[0], mapDiv);
        
        //Now that we've rendered the cards, we can add the listener for the filter text box
        tabTextFilter.on("keyup change", 
        function(e,args)
        {
            //on keyup, filter both lists if not undefined
            var cards = cardsContainer.children();
            var filter = e.target.value.toUpperCase();
            // Loop through all list items, and hide those who don't match the search query
            
            for (i = 0; i < cards.length; i++) {
                a = cards[i];
                var childElements = $(a).find('.card-address-text');
                var hideDiv = true;//Used to detect if we should Hide this div                
                var txtValue = childElements[0].innerText;
                
                var titleElement = $(a).find('.card-title');
                var txtTitleValue = titleElement[0].innerText;
                if(txtTitleValue.toUpperCase().indexOf(filter) > -1 || txtValue.toUpperCase().indexOf(filter) > -1)
                {
                    hideDiv = false;
                }

                if(hideDiv)
                {
                    a.style.display = "none";
                }else
                {
                    a.style.display = "";
                }
            }
            
            
        });
        cardsContainer.prop("config", config);
        cardsContainer.prop("leftGrid", tabGrid);
        return cardsContainer;
    }//AddressManagerForm.prototype._renderRightSection
    
    /**
     * @protected
     * 
     * Renderer function for the right panel of each non-address tab.
     *
     * @param {jquery component} component - the target element to append the panel to
     * 
     * @param {object}           config    - All configurations for this panel.
     *
     * @param {object}           tabGrid   - The left grid for this panel. Necessary for adding new addresses to the grid
     */
    AddressManagerForm.prototype._getMapIFrame = function(item, component)
    {
        var curMap = $(component).find('.tab-panel-right-mapframe');
        $(curMap).remove();
        /*document.getElementById("mapView").src=newLocation;*/
        if(_this.config.hideMap == true)
        {
            return;
        }
        
        var mapDiv = $('<div class="tab-panel-right-mapframe"/>').appendTo(component);
        //var minimizeButton = 
        //Render the google map iframe
        var mapsKey = _this.config.mapKey ? _this.config.mapKey : 'AIzaSyAV0QZNL1-69Raqo9XxZTs9EBkSG1Nue24';

        var address = 'current location';
        
        if(item){
            address = item.T1C_Base__Street__c          ?        item.T1C_Base__Street__c : '';
            address += item.T1C_Base__City__c            ? ', ' + item.T1C_Base__City__c : '';
            address += item.T1C_Base__State_Province__c  ? ', ' + item.T1C_Base__State_Province__c : '';
            address += item.T1C_Base__Country__c         ? ', ' + item.T1C_Base__Country__c : '';
            address += item.T1C_Base__Zip_Postal_Code__c ? ', ' + item.T1C_Base__Zip_Postal_Code__c : '';
        }
        
        var encodedAddress = '&q=' + encodeURI(address);
        encodedAddress = encodedAddress.replace('#','%23'); // encodeURI does not encode hashtags

        var addressValid = address.replace(/,\s/g,'');
        
        if(addressValid.length < 1)
        {
            encodedAddress = '&q=currentlocation&zoom=15';
        }
        
        var mapIframe = $('<iframe class="googleMap" frameborder="0" height="250" src="https://www.google.com/maps/embed/v1/place?key=' 
                        + mapsKey
                        + encodedAddress
                        +'" allowfullscreen tabindex="-1" title="Map" width="100%"></iframe>').appendTo(mapDiv);
        
    }
    
    /**
     * @protected
     * 
     * Renderer function for each individual Address Card on the right panel
     *
     * @param {object}           item      - The Address object containing field information for this card view
     *
     * @param {jquery component} component - the target element to append the card to
     * 
     * @param {object}           config    - All configurations for this card including visible fields.
     *
     * @param {object}           tabGrid   - The left grid for the corresponding parent panel. Necessary for updating addresses on the grid
     */
    AddressManagerForm.prototype._renderCard = function(item, component, config, tabGrid)
    {

        //Call back in order to open the modal popup
        var cloneConfig = JSON.parse( JSON.stringify(config) );
        
        cloneConfig.editControls.callback = function(e, val, options)
        {
            //Stop the event from bubbling up to the card
            e.stopImmediatePropagation();
            $(component).parent().siblings('.map-container').hide()
            _this._renderEditModal( val, component, options, tabGrid );
        };
        
        //We use this to see which field we will retrieve the address object from
        cloneConfig.editControls.dataField = 'addressItem';
        
        component.prop('addressItem',item);
        
        var titleField = config.titleField.trim();
        
        var header = $('<header class="card-header"></header>').appendTo(component);
        //Add the title field in a div before the buttons
        var title = $('<div class="card-title"></div>').appendTo(header);

        title.text(item[titleField]);
        
        //We need to pass updated Items to 
        var buttonWrapper = $('<div class="card-header-btn-wrapper"></div>').appendTo(header);

        //If the account is not the billing account nor is it the shipping account
        if( (item.T1C_Base__BillingAccounts__r == undefined && item.T1C_Base__ShippingAccounts__r == undefined) || _this.config.allowBillShip )
        {
            //Create the edit and delete buttons. pass in the whole Config so we can configure the dialog form
            //Only render the edit button if we have edit access
            
            if(_this.config.addressEdit == true)
            {
                ACE.AddressManagerFormItemFactory
                    .inputMap['EDIT']
                    .call(_this, buttonWrapper, component, cloneConfig.editControls);
            }
            
            if(_this.config.addressDelete == true && !item.T1C_Base__BillingAccounts__r && !item.T1C_Base__ShippingAccounts__r)
            {
                var delOptions = 
                {
                    callback :  function(e, val, options)
                                {
                                    ACEConfirm.show
                                    (
                                        _this.config.cardDeleteMessage,//Make AFR configurable? maybe for locales?
                                        "Confirm",
                                        [ "OK::confirmButton","Cancel::cancelButton"],
                                        function(result)
                                        {
                                            if(result == 'OK')
                                            {_this._deleteButtonhandler(val, component, true);}//else do nothing
                                        },
                                        {
                                            "dialogClass" : "confirmationDialog aceDialog"
                                        }
                                    );	
                                    
                                }
                }
                
                ACE.AddressManagerFormItemFactory
                    .inputMap['DELETE']
                    .call(_this, buttonWrapper, item, delOptions);
            }
            
        }
       
        var addressString = '';
        var fields = [];//A field array for refreshing the cards. We need this so that we know which address fields to take and display from the object
                        //When we refresh the card
        //The addresses are not editable from their card view. 
        $.each(config.cardColumns, function(index, column)
        {
            //fill in our Dummy object with a null value for this field so we can clone it for new items.
            //Basically this fills the template with fields which we can clone to produce new cards when needed
            _this.dummyObject[column.dataField] = null;
            
            //Just in case they put the Title field into the card columns AFR
            if (column.dataField !== titleField){
                //Concatenate all of the address fields in the order that theyre given in a paragraph
                if(item[column.dataField] != undefined && item[column.dataField] != null)
                {
                    addressString = addressString + item[column.dataField] + ' ';
                }
                fields.push(column.dataField);
            }
        });
        
        var addressParagraph = $('<p class="card-address-text"></p>').appendTo(component);

        addressParagraph.text(addressString);
        
        addressParagraph.prop("titleField", titleField);
        addressParagraph.prop("addressFields", fields);
        
        if(config.objEditable)
        {
            //Add the apply to all button
            var buttonOptions = 
            {
                callback :  function(e, val, options)
                            {
                                _this._applyAddressToSelected(component, config.applyToSelectedFieldMap, tabGrid);
                            },
                label    : _this.config.applyToSelectedLabel,
                innerCSS : 'card-apply',
                dataField : 'addressItem',
                hoverText : _this.config.applyToSelectedHover
            }
            ACE.AddressManagerFormItemFactory
                .inputMap['EDIT']
                .call(_this, component, component, buttonOptions);
        }
    }
    
    /**
     * @protected
     * 
     * Renderer function for Address editors on the right panel
     *
     * @param {object}           item      - The Address object containing field information for this editor
     *
     * @param {jquery component} component - the target element to append the card to
     * 
     * @param {object}           config    - All configurations for this card including visible and read-only fields.
     *
     * @param {object}           tabGrid   - The left grid for the corresponding parent panel. Necessary for updating or adding new addresses onto the grid
     */
    AddressManagerForm.prototype._renderEditModal = function(item, component, config, tabGrid)
    {
        //Remove any open editors
        component.parent().find('.address-editor').remove()
        
        //Hide the cards - which are the children of the card-container
        component.parent().children().hide();
        //Also hide the new button
        component.parent().siblings('.tab-panel-right-header-section').children('.tab-panel-right-controls').addClass('control-hidden');
        
        //create a deep clone of the original. do not want to pass references here
        var originalItem = JSON.parse( JSON.stringify(item) );
        
        //Create new class of div under the cards container
        var addressEditor = $('<div class="address-editor"/>').appendTo(component.parent());

        var addressSearchInput = $('<input class="google-search-input"></input>').appendTo(addressEditor);
        $(addressSearchInput).attr("placeholder", _this.config.searchPlaceHolder);
        //Attach the google autocomplete above the other fields
        var autocomplete = new google.maps.places.Autocomplete(addressSearchInput[0]);
        
        $(addressSearchInput).focus();
      
        $(addressSearchInput).on('keyup', function(e, args) 
        {
            if(item.id == 'newItem' && e.keyCode == 8)//8 is backspace
            {
                var curVal = $(this).val();
                //if emptied, we have to wipe the fields in the edit modal
                var newAddress = 
                {
                    address_components : []
                };
                if(curVal=="")
                {
                    _this._applyGoogleSearchToEditor(newAddress, addressEditor, item);
                }
            }
        });
        
        //Group them by row <div class="address-card-row">
        //We create an array of row divs and use the row num to assign. This MUST be configured properly as a combination of order and row number
        var addressRowArray = [];
        var addressRow  = null;
        $.each(config.editModalColumns, function(index, column)
        {
            //If we detect a new row, create one
            if(addressRow == null || column.rowNum > addressRowArray.length)
            {
                addressRow = $('<div class="address-card-row">').appendTo(addressEditor);
                addressRowArray.push(addressRow);
            }
            
            var fieldType = column.fieldType.toUpperCase();
            if( ACE.AddressManagerFormItemFactory.inputMap[fieldType] != null || ACE.AddressManagerFormItemFactory.inputMap[fieldType] != undefined )
        	{
        		ACE.AddressManagerFormItemFactory
        			.inputMap[fieldType]
        			.call(_this, addressRow, item, column, false);
        	}
        });
        
        var editorFooter = $('<div class="address-card-row address-card-footer"></div>').appendTo(addressEditor);
        
        //add the Billing/shipping checkboxes
        if(_this.config.allowBillShip)
        {
            item.setBilling = (item.T1C_Base__BillingAccounts__r != undefined && item.T1C_Base__BillingAccounts__r != null);
            item.setShipping = (item.T1C_Base__ShippingAccounts__r != undefined && item.T1C_Base__ShippingAccounts__r != null);
            
            if(!_this.config.hideBilling)
            {
                var billingColumn = 
                {
                    dataField : 'setBilling',
                    label     : 'Primary',
                    innerCSS  : 'primary-loc'
                };
                
                ACE.AddressManagerFormItemFactory
                        .inputMap["CHECKBOX"]
                        .call(_this, editorFooter, item, billingColumn, false);
            }    

            if(!_this.config.hideShipping)
            {
                var shippingColumn = 
                {
                    dataField : 'setShipping',
                    label     : 'Shipping',
                    innerCSS  : 'shipping-loc'
                };
                
                ACE.AddressManagerFormItemFactory
                        .inputMap["CHECKBOX"]
                        .call(_this, editorFooter, item, shippingColumn, false);
            }       
        } else 
        {
            $('<div class="card-editor-checkbox-placeholder"></div>').appendTo(editorFooter);
            
        }
        
        var saveMessage = item.id=='newItem' ? _this.config.cardNewSaveMessage : _this.config.cardSaveMessage;
        
        //Add the save and cancel buttons
        var saveButton = $('<button class="card-editor-save"></button>').appendTo(editorFooter);
        saveButton.text(_this.config.saveCardEditLabel);

        $(saveButton).on("click",
        function(e,args)
        {
            //on save do aceConfirm and save on Ok
            ACEConfirm.show
            (
                saveMessage,
                "Confirm",
                [ "OK::confirmButton","Cancel::cancelButton"],
                function(result)
                {
                    if(result == 'OK')
                    {
                        delete _this.globalEditFlag[item.id];
                        $(component).parent().siblings('.map-container').show();
                        component.parent().siblings('.tab-panel-right-header-section').children('.tab-panel-right-controls').removeClass('control-hidden');
                        _this._saveButtonhandler(addressEditor, item, component, tabGrid, null);
                    }//else do nothing
                },
                {
                    "dialogClass" : "confirmationDialog aceDialog"
                }
            );	
            
        });
        
        
        var cancelButton = $('<button class="card-editor-cancel"></button>').appendTo(editorFooter);
        cancelButton.text(_this.config.cancelCardEditLabel);
        $(cancelButton).on("click",
        function(e,args)
        {
            //on cancel, remove the editor and show the children again
            addressEditor.remove();
        });
        
        //If the editor is forcibly removed for any reason, revert the item or delete the card
        $(addressEditor).on('remove', function(e)
        {
            delete _this.globalEditFlag[item.id];
            
            component.parent().children().show();
            //also show the new button
            component.parent().siblings('.tab-panel-right-header-section').children('.tab-panel-right-controls').removeClass('control-hidden');
            //if(!_this.config.hideMap){$(component).parent().siblings('.map-container').show();}
            //If it's a new item, remove the card since they dont want to save it. If they hit save then it would have a proper ID
            //See the controller method saveAddress for more info
            if(item.id == 'newItem')
            {
                $('#' + component[0].id).remove();
            }
            else
            {
                //Revert the item to the pre-edit state. using a loop to keep references the same
                for(var prop in originalItem) item[prop]=originalItem[prop];
            }
        })
        
        
        //Listen for edits, then trip the flag
        $(addressEditor).on("keyup", 
        function(e, args)
        {
            if(! $(e.originalEvent.target).hasClass("google-search-input") )
            {
                _this.globalEditFlag[item.id] = saveButton;
            }
        });
        
        autocomplete.addListener('place_changed', function()
        {
            var address = autocomplete.getPlace();
            _this._applyGoogleSearchToEditor( address, addressEditor, item );
            _this.globalEditFlag[item.id] = saveButton;
        });
    }//AddressManagerForm.prototype._renderEditModal 

    /**
     * @protected
     * 
     * Parser function for processing the Google search result on the autocomplete at the top of the editor
     * and pass the address result components into the the address object and the Address object.
     * Normally acts as part of the event listener on the autocomplete
     *
     * @param {object}           address - The Google Address object containing field information for the editor
     *
     * @param {jquery component} editor  - the target Editor to fill the fields into
     * 
     * @param {object}           item    - The Address Object to apply the google search to
     */
    AddressManagerForm.prototype._applyGoogleSearchToEditor = function(address, editor, item)
    {
        //Parse out the address fields and map them out to the editor fields.
        //Make AFR configurable?? have a map of Google datafield -> SFDC Datafield
        var googleToSFDCFields = 
        {
            'subpremise'                    : 'T1C_Base__Street__c',
            'street_number'                 : 'T1C_Base__Street__c', //street is a compound field from google. make AFR config?
            
            'street_address'                : 'T1C_Base__Street__c',
            'route'                         : 'T1C_Base__Street__c',
                        
            'neighborhood'                  : 'T1C_Base__City__c',             
            'administrative_area_level_2'   : 'T1C_Base__City__c',
            'locality'                      : 'T1C_Base__City__c',            
            'postal_town'                   : 'T1C_Base__City__c',
            'administrative_area_level_3'   : 'T1C_Base__City__c',
            'colloquial_area'               : 'T1C_Base__City__c',
            'sublocality_level_1'           : 'T1C_Base__City__c',
            'sublocality'                   : 'T1C_Base__City__c',

            'country'                       : 'T1C_Base__Country__c',
            
            'administrative_area_level_1'   : 'T1C_Base__State_Province__c',
            'political'                     : 'T1C_Base__State_Province__c',
            
            'postal_code'                   : 'T1C_Base__Zip_Postal_Code__c'
        }



        //primary type fields in order (address.address_components[i].type[0])
        /*
        -----------------------
        "street_number"
        "route"
        -----------------------
        "sublocality_level_1" - Sub city eg. North York or Etobicoke 
        "locality" - City eg. Toronto - using this one for now
        -----------------------
        "administrative_area_level_1" - Province
        "country" 
        "postal_code"
        */
        
        var streetAddress = '';
        
        //Blank out the item fields. We have a copy of the original so this is fine to do
        $.each(googleToSFDCFields, 
        function(index, itemField)
        {
            item[itemField] = '';
        });

        var cityValues = {};
        var addressObject = {};
        var addressObjectShortNames = {};
        
        var stateProvinceShortName = '';
        var countryShortName = '';
        
        //Go through the editor fields and use the mapping to put in the data into the item
        $.each(address.address_components, 
        function(index, addressComponent)
        {
            var googleField = addressComponent.types[0];

            //var inSearch = address.formatted_address.includes(addressComponent.long_name) || address.formatted_address.includes(addressComponent.short_name)
            
            var itemField = googleToSFDCFields[googleField];
            if( (itemField != undefined && addressComponent != undefined )) // Need to get all values for the address components then go through an order of importance.
                //&& inSearch && !item[itemField]) /*|| !item[itemField] || item[itemField].length < 1 */)//Prioritize previous values for each contact address value
            {
                if(itemField != 'T1C_Base__Street__c')
                {
                    addressObjectShortNames[itemField] = addressComponent.short_name;

                    if(itemField == 'T1C_Base__City__c')
                    {
                        cityValues[googleField] = addressComponent.long_name;
                    }
                    else
                    {
                        addressObject[itemField] = addressComponent.long_name;
                        //if it's the country or the state/province field, keep the short name in case we have configuration to use the short name instead
                        //There should only be one of these
                        stateProvinceShortName = itemField == 'T1C_Base__State_Province__c' ? addressComponent.short_name : stateProvinceShortName;
                        countryShortName = itemField == 'T1C_Base__Country__c' ? addressComponent.short_name : countryShortName;
                    }
                }
                else//assemble the street address using the components
                {
                    streetAddress = streetAddress + ' ' + addressComponent.long_name;
                }
            }
        });

        var cityFields = [
            'locality',
            'postal_town',   
            'neighborhood',            
            'sublocality_level_1', 
            'administrative_area_level_2',       
            'administrative_area_level_3',
            'colloquial_area',                           
            'sublocality'
        ];

        // Order of importance with duplicate values, like City States where Singapore will be the city, state and country
        var addrFields = 
        [
            'T1C_Base__Zip_Postal_Code__c',
            'T1C_Base__Country__c',
            'T1C_Base__State_Province__c',
            'T1C_Base__City__c'
        ];

        var usedCityField;

        for(var x=0; x<cityFields.length;x++)
        {
            var cityField = cityFields[x];
            var cityFieldValue = cityValues[cityField];
            if(cityFieldValue != null)
            {
                addressObject['T1C_Base__City__c'] = cityFieldValue;
                usedCityField = cityField;
                break;
            }
        }

        var addrValueArray = [];

        for(var x=0; x<addrFields.length;x++)
        {
            var addrFld = addrFields[x];
            var addrVal = addressObject[addrFld];
            var addrShortName = addressObjectShortNames[addrFld];

            if($.inArray(addrShortName,addrValueArray) === -1)
            {
                addrValueArray.push(addrShortName);
                item[addrFld] = addrVal;
            }
            else if(addrFld == 'T1C_Base__City__c')
            {
                // Populate the next best City value, if available
                for(var i=0; i<cityFields.length;i++)
                {
                    var cityField = cityFields[i];
                    var cityFieldValue = cityValues[cityField];

                    if(cityFieldValue != null && usedCityField != cityField && $.inArray(cityFieldValue,addrValueArray) === -1)
                    {
                        item[addrFld] = cityValues[cityField];
                        break;
                    }
                }
            }
        }

        var isAuto = ($($(editor).children(".address-card-row").children(".card-item-input")[0]).prop('AutoPopulatedName') == true);
        
        var autoPopulatedName = isAuto;
        var addressName = item['T1C_Base__Address_Name__c'];
        if(autoPopulatedName || ((addressName == null || addressName.length == 0) && address.name != null))
        {
            item['T1C_Base__Address_Name__c'] = address.name;
            autoPopulatedName = true;
        }
        
        item['T1C_Base__Street__c'] = streetAddress.trim();
        
         var shortenRegion = _this.config.shortNameRegionCountries.length > 0 && _this.config.shortNameRegionCountries.includes( item['T1C_Base__Country__c'] );
         var shortenCountry = _this.config.shortNameCountries.length > 0 && _this.config.shortNameCountries.includes( item['T1C_Base__Country__c'] );
        
        if(shortenRegion){ item['T1C_Base__State_Province__c'] = stateProvinceShortName;}
        if(shortenCountry){ item['T1C_Base__Country__c'] = countryShortName;}
        
        //afterwards, fill in the editor with all of the values. the item already has the approprite values at this point
        //click for picklists and checkboxes
        //"select" for lookups
        //change for texts
        
        $.each( $(editor).children(".address-card-row"), function(index, row)
        {
            $.each( $(row).children(".card-item-input"), function(index, column)
            {
                var itemValue = item[column.datafield] ? item[column.datafield] : null;

                if(itemValue != null)
                {
                    itemValue = itemValue.replace(/"/g, '&quot;');
                }

                if(column.datafield == 'T1C_Base__Address_Name__c')
                {
                    $(column).prop('AutoPopulatedName',autoPopulatedName);
                }
               
                $(column).children("input").val(itemValue);
            });
        });
    }
    
    /**
     * @protected
     * 
     * Renderer function for Address editors on the right panel
     *
     * @param {jquery component} component - the target card. This contains the Address as a property
     * 
     * @param {object}           config    - All configurations for this card including visible and read-only fields.
     *
     * @param {object}           tabGrid   - The left grid for the corresponding parent panel. Necessary for updating or adding new addresses onto the grid
     */
    AddressManagerForm.prototype._applyAddressToSelected = function(component,  applyToSelectedFieldMap, tabGrid)
    {
        var item = component.prop('addressItem');
        var selectedItems = tabGrid.getSelectedItems();
        
        $.each(selectedItems, 
        function(index, sObject)
        {
            //Assign the address lookup ID to the sObjects
            sObject['T1C_Base__Mailing_Address_Id__c'] = item.id;//Make AFR configurable
            sObject['hasChanged'] = true;//Demarcate to show undo button
            //Loop through the field map and synchronize the sObject to the selected Address item
            $.each(applyToSelectedFieldMap, 
            function(addressField, sObjectField)
            {
                sObject[sObjectField] = item[addressField] ? item[addressField] : null;
            });
            
        });
        
        tabGrid.grid.invalidate()
        tabGrid.dataProvider.refresh();
    }
    
    /**
     * @protected
     * 
     * Renders and adds a card to the right panel of every tab based on the input Address 
     *
     * @param {object} item - The Address object containing field information for the rendered cards
     */
    AddressManagerForm.prototype._addCardToAllTabs = function(item)
    {
        //Go through all the tabs except for the address tab, and render a card with the new address item in it 
        //and attach it to its right section

        $.each(_this.cardContainers, 
        function(index, container)
        {
            var mapDiv = $(container).find('.map-container');
            
            var cardClickHandler = function(e)
            {   
                _this._selectCard( $(e.target), e.data, mapDiv );
            };
            
            var card = $('<div id="card' + _this.cardCounter++ +  '" class="card-container"/>').appendTo(container);
            
            card.on("click", item, cardClickHandler);
            
            _this._renderCard(item, card, container[0].config, container.prop('leftGrid') );
        });
    }
    
    /**
     * @protected
     * 
     * Renderer function for Address editors on the right panel
     *
     * @param {jquery component} closingComponent - The target component to close once the save finishes, Usually an Address Edit modal
     * 
     * @param {object}           item             - The Address object to save
     *
     * @param {object}           card             - An input card to refresh once saved, can be null
     *
     * @param {object}           tabGrid          - The corresponding left grid that the editor is synched with
     *
     * @param {function}         callBack         - Callback function to call once the save is finished. Used for applying the address edits to the contacts (see _reapplyAddressToContacts)
     */
    //The save button handler for the cards when they are in edit mode, only one address at a time is saved since it's an inline save in the modal
    AddressManagerForm.prototype._saveButtonhandler = function(closingComponent, item, card, tabGrid, callBack)
    {
        //Strip out the Billing/shipping markers and send the markers to SFDC as separate flags
        var itemJSON = JSON.stringify(item);
        var createBilling = item.setBilling == true ? true : false;
        var createShipping = item.setShipping == true ? true : false;
        
        if(item.setBilling != undefined) delete item.setBilling;
        if(item.setShipping != undefined) delete item.setShipping;
        
        ACE.LoadingIndicator.hide();
        ACE.LoadingIndicator.show('Saving');
        //The apex method returns the updated address with recalculated formulas in a JSON serialized format
        sforce.apex.execute('AddressManagerController', 'saveAddress', { "addressRecord": itemJSON, "createBill":JSON.stringify(createBilling), "createShip":JSON.stringify(createShipping) },
        {
            onSuccess: function(result)
            {
                //after saving, refresh the card with the new values, remove the editor and show the children again
                var successMessage = _this.config.saveMessageLabel ? _this.config.saveMessageLabel : "Success";
                
                ACE.LoadingIndicator.hide();
                ACE.LoadingIndicator.show(successMessage);
                window.setTimeout(function()
                {
                    ACE.LoadingIndicator.hide();
                }, 1700);
                
                //if it's a new item, append it to the shared address list and update with the new ID
                var newAddress = JSON.parse( result );
                newAddress.id = newAddress.Id;
                delete newAddress.Id;

                _this.globalAddressMap[newAddress.id] = newAddress;
                if(!card)//new address was made through the addresses tab
                {
                    _this._addCardToAllTabs(newAddress);
                }
                //if it's new we need to add it to all of the address grids
                if(item.id == 'newItem')
                {
                    $.each(_this.addressGrids, 
                    function(index, addressGrid)
                    {
                        addressGrid.addItems(newAddress);
                        var lastIndex = addressGrid.dataProvider.getLength() - 1;
                        //The new item always ends up at the very last row
                        addressGrid.grid.setSelectedRows([lastIndex])
                    });

                    _this.config.sharedAddressList.push(newAddress);
                }//if not, then we update the item in the grids
                else
                {
                    $.each(_this.addressGrids, 
                    function(index, addressGrid)
                    {
                        addressGrid.dataProvider.updateItem(newAddress.id, newAddress);
                        //addressGrid.grid.setSelectedRows(  );
                        addressGrid.dataProvider.refresh();
                    });

                    //Update in the shared address list as well
                    //_this.config.sharedAddressList.push(newAddress);
                }

                _this._reapplyAddressToContacts(newAddress);
                __applyToCards(_this, this, card, closingComponent, newAddress, callBack);

                //Refresh; Replacing the Billing and Shipping addresses inline is too complex for now
                if(createShipping || createBilling)
                {
                    
                    var curTab = encodeURI(_this.currentTab);
                    
                    location.search += "&currentTab=" + curTab;
                }
                
                
            },//onSuccess
            onFailure: function(error)
            {
                //the greedy [\s\S]* is because there is a line break
                var pattern = /.*first error:.*?,(.*):[\s\S]*Class.AddressManagerController/;
                var errorString = error.faultstring;
                if(errorString == null)
                {
                    errorString = error;
                }

                var errorMessageMatch =errorString.match(pattern);

                if(errorMessageMatch != null && errorMessageMatch.length > 1)
                {
                    var errMessage = errorString.match(pattern)[1];
                    ACE.LoadingIndicator.hide();
                    
                    var msg = _this.msgParser.parseFromString(errMessage, 'text/html');
                    
                    ACE.FaultHandlerDialog.show
                    ({
                        title:   "Error",
                        message: "The address could not be saved:\r\n" + msg.body.textContent, 
                        error:   errorString
                    });
                } 
                
            }//onFailure
        });//sforce.apex.execute 
        
    }//AddressManagerForm.prototype._saveButtonhandler
    
    /**
     * @private
     * 
     * Utility function for applying the Specified Address to its corresponding cards
     *
     * @param {object}           _this            - The instance of the Address Manager so we can call our functions
     *
     * @param {object}           context          - The execution context. Wanted it to be the same context as the Ace remoting callback. Needed for the callback "call" function
     * 
     * @param {object}           card             - An input card to refresh once applied.
     *
     * @param {jquery component} closingComponent - The target component to close once the save finishes, Usually an Address Edit modal
     *
     * @param {object}           item             - The Address object to apply
     *
     * @param {function}         callBack         - Callback function to call once the application is finished. Used for applying the address edits to the contacts (see _reapplyAddressToContacts)
     */
    function __applyToCards(_this, context, card, closingComponent, item, callBack)
    {
        if(card && closingComponent)
        {
            //remove the cancel button listener
            closingComponent.off('remove');
            closingComponent.remove();
            card.parent().children().show();
            card.parent().siblings('.tab-panel-right-controls').show();
            ACE.AddressManagerFormItemFactory.refreshCard(card, item);//This does essentially nothing if the card is a new item. I just didnt want to branch
            
            var mapDiv = card.parent().siblings('.map-container');

            _this._selectCard( $(card), item, $(mapDiv) );
        }
        
        //Sort all of the cards. This is a quack fix, better design is to throw events when saving and listen for them in the card holder divs
        _this._sortAddresses();
        
        if(callBack != null)
        {
            callBack.call(context, null, item);
        }
    }

    /**
     * @protected
     * 
     * Utility function for applying the Specified Address to its corresponding cards
     *
     * @param {object} item - The Address object to apply to all Contacts that are linked to it
     */
    AddressManagerForm.prototype._reapplyAddressToContacts = function(item)
    {
        //Update all SObjects that have this address as their corresponding T1C_Base__Mailing_Address_Id__c or other lookup fields
        //Loop through and update each contact Mailingaddress fields
        for(var x = 0 ; x < _this.globalGridList.length; x++ )
        {
            console.log('Changed Quack');
            var grid = _this.globalGridList[x];
            var addressToSelectedMap = _this.config.tabsList[x].applyToSelectedFieldMap;
            $.each(grid.dataProvider.getItems(), function(index, gridItem){
                if(gridItem["T1C_Base__Mailing_Address_Id__c"] == item.id)//make AFR configurable?
                {
                    $.each(addressToSelectedMap, 
                    function(addressField, sObjectField)
                    {
                        gridItem[sObjectField] = item[addressField] ? item[addressField] : null;
                    });
                    
                }
            });
        };

        $.each(_this.addressGrids, 
        function(index, addressGrid)
        {
            addressGrid.dataProvider.updateItem(item.id, item);
        });
        
        _this._invalidateGrids();
        //_this._save(false);//save the edited grid items
        
        //refresh all cards that belong to this address
        $.each(_this.config.sharedAddressList,
        function(index, item)
        {
            if(_this.cardsMap[item.id] != undefined){
                $.each( _this.cardsMap[item.id], 
                function(cardIndex, card)
                {
                    ACE.AddressManagerFormItemFactory.refreshCard(card, item);
                });
            }
        });
        
    }
    
    /**
     * @protected
     * 
     * Function for handling deletes on the address editor
     *
     * @param {object}  item        - The Address object to delete
     *
     * @param {object}  card        - An input card to delete once applied.
     *
     * @param {Boolean} refreshForm - The target component to close once the save finishes, Usually an Address Edit modal
     */
    AddressManagerForm.prototype._deleteButtonhandler = function(item, card, refreshForm)
    {
        //Delete the card using card.remove() after a successful delete if the object exists in the backend
        //Otherwise, just delete the card. We can tell this by the IDs
        //_this.config.sharedAddressList.push(newItem);
        var itemJSON = JSON.stringify(item);
        
        if(item.id != "newItem"){
            sforce.apex.execute('AddressManagerController', 'deleteAddress', { "addressRecord": itemJSON },
            {
                onSuccess: function(result)
                {
                    //_this.config.sharedAddressList.push(newItem);
                    if(card)
                    {
                        $('#' + card[0].id).remove();
                    }
                    if(refreshForm == true)
                    {
                        var curTab = encodeURI(_this.currentTab);
                        
                        var newSearch = location.search.replace(/&currentTab=.*/, '');
                        
                        newSearch += "&currentTab=" + curTab;
                        
                        location.search = newSearch;
                    }
                },
                onFailure: function(error)
                {
                    //the greedy [\s\S]* is because there is a line break
                    var pattern = /.*first error:.*?,(.*):[\s\S]*Class.AddressManagerController/;
                    var errMessage = error.faultstring.match(pattern)[1];
                    
                    var msg = _this.msgParser.parseFromString(errMessage, 'text/html');
                    
                    ACE.LoadingIndicator.hide();
                    ACE.FaultHandlerDialog.show
                    ({
                        title:   "Error",
                        message: "The address could not be deleted:\r\n" + msg.body.textContent, 
                        error:   error.faultstring
                    });
                }//onFailure
            });
        }
        else
        {
            if(card)
            {
                //_this.config.sharedAddressList.push(newItem);
                $(card).remove();
            }
        }
        
    }//AddressManagerForm.prototype._deleteButtonhandler
    
    /**
     * @protected
     * 
     * Function for forcing a re-render of the grids for all tabs
     */
    AddressManagerForm.prototype._invalidateGrids = function()
    {
        $.each(_this.globalGridList, 
        function(index,tabGrid)
        {
            //tabGrid.clearSelectedRows();
            tabGrid.updateCheckBoxSelection(false, false, null);	
            tabGrid.grid.invalidate();
            tabGrid.dataProvider.refresh();
        });
    }
    
    /**
     * @protected
     * 
     * Function for handling calls to save everything in the grids. This is for the save button at the footer. 
     * Not to be confused with the save button handler for the cards
     *
     * @param {Boolean} closeAfter - Flag to tell the for to close after saving
     */
    AddressManagerForm.prototype._save = function(closeAfter)
    {
        //Before saving, we need to make sure that we are not editing any addresses
        if (Object.keys(_this.globalEditFlag).length > 0)
        {
            //If we have editors open, and changes made to items we should warn the user before saving 
            ACEConfirm.show
            (
                'Warning: You have unsaved Address Changes. Save them now?',
                "Confirm",
                [ "OK::confirmButton","Cancel::cancelButton"],
                function(result)
                {
                    if(result == 'OK')
                    {  
                        _this._sortAddresses(_this.globalEditFlag, closeAfter);
                    }//else do nothing
                },
                {
                    "dialogClass" : "confirmationDialog aceDialog"
                }
            );
            
            //This is to halt execution, we do not want to proceed once we pop open the async modal
            return;
        }

        var items = [];//_this.tabGridListProviders;
        
        $.each(_this.globalGridList, function(index, tabGrid)
        {
            items = items.concat( tabGrid.dataProvider.getItems() );
        });
        
        var saveItems = [];
        $.each(items, 
        function( index, item )
        {
            if(item.hasChanged)
            {
                saveItems.push(item);
                delete item.hasChanged;
            }
        });
        
        _this._invalidateGrids();
        
        ACE.LoadingIndicator.hide();
        ACE.LoadingIndicator.show('Saving');
        
        sforce.apex.execute('AddressManagerController', 'updateSObjects', { "inputJSON": JSON.stringify(saveItems) },
        {
            onSuccess: function(result)
            {
                var successMessage = _this.config.successMessageLabel ? _this.config.successMessageLabel : "Success";
                
                //Commit the grid items to their new states in the backup grid
                //
                for(var x = 0 ; x < _this.globalGridList.length; x++ )
                {   
                    var grid = _this.globalGridList[x];
                    var backup = _this.gridBackups[x];
                    $.each(grid.dataProvider.getItems(),function(index, item)
                    {
                        $.extend(backup[item.id], item);
                    });
                };

                ACE.LoadingIndicator.hide();
                ACE.LoadingIndicator.show(successMessage);
                window.setTimeout(function()
                {
                    ACE.LoadingIndicator.hide();
                    window.setTimeout(function()
                    {
                        if(closeAfter)
                        {
                        	_this.close();
                        }
                    }, 300);
                }, 1700);
                
            },//onSuccess
            onFailure: function(error)
            {
                //Fill out Error messages. For now permission is the only one detected
                //Permission denied: error.faultcode = sf:INSUFFICIENT_ACCESS
                var errormsg = "";
                ACE.LoadingIndicator.hide();
                if ( error.faultstring.indexOf("INSUFFICIENT_ACCESS") > -1)
                {
                    errormsg = 
                        "Insufficient Access for this User";
                }else if ( error.faultstring.indexOf("FIELD_CUSTOM") > -1)
                {
                    var pattern = /.*FIELD_CUSTOM_VALIDATION_EXCEPTION,\s(.*):\s\[[\s\S]*/
                    errormsg = error.faultstring.replace(pattern, /$1/).replace(/\//g,"");
                }                
                else if ( error.faultstring.indexOf("invalid") > -1)
                {
                    //var pattern = /.*INVALID.*?,.*:\s(.*):\s\[[\s\S]*/
                    //errormsg = "Invalid input: " + error.faultstring.replace(pattern, /$1/).replace(/\//g,"");
                    var typePattern = /\sinvalid\s(.*?):/
                    var type = error.faultstring.match(typePattern);
                    errormsg = "Please enter a valid " + type[1];
                    
                }else
                {
                    errormsg = "An Unexpected Error Occured. Please send the details to your administrator";
                }

                var msg = _this.msgParser.parseFromString(errormsg, 'text/html');
                
                ACE.FaultHandlerDialog.show
                ({
                    title:   "Warning",
                    message: msg.body.textContent, 
                    error:   error.faultstring
                });

            }//onFailure
		});//sforce.apex.execute upsertToSFDC
    };//AddressManagerForm.prototype._save
    
    /**
     * @protected
     * 
     * Function for sorting the cards in each right panel. (overloaded) version
     *
     * @param {object}  itemDict   - The dictionary used for sensing edits. We pass it into this function so we can clear it
     *
     * @param {Boolean} closeAfter - NO LONGER USED - 
     */
    AddressManagerForm.prototype._sortAddresses = function(itemDict, closeAfter)
    {
        ACE.LoadingIndicator.show('Saving');
        $.each(itemDict, 
        function(index, button)
        {
            button.click();
        });
        
        //We do not need the edited objects anymore.
        Object.keys(itemDict).forEach(function(key) { delete itemDict[key]; });
        _this._sortAddresses();
        //_this._save(closeAfter);
    }
    
    /**
     * @protected
     * 
     * Function for sorting the cards in each right panel
     */
    AddressManagerForm.prototype._sortAddresses = function()
    {
        //go through all of the containers and sort by their title field
        $.each(_this.cardContainers, 
        function(index, container)
        {
            //in each child (card) look for $('<div class="card-title"> and sort while taking the Billing and Shipping addresses into account
            var cards = container.children();//this will get the card wrapper
            cards.sort(function( compA, compB )
            {
                var textA = $(compA).find('.card-title').text();
                var textB = $(compB).find('.card-title').text();
                
                var itemA = $(compA).prop('addressItem');
                var itemB = $(compB).prop('addressItem');
                
                //It is already sorted by location in the query, so we only need to resolve billing/shipping addresses
                if(!itemA.T1C_Base__BillingAccounts__r && !itemB.T1C_Base__BillingAccounts__r &&
                    !itemA.T1C_Base__ShippingAccounts__r && !itemB.T1C_Base__ShippingAccounts__r  )
                    {
                        //if neither item has the shipping/billing address return the difference between their title texts
                        var order = textA >= textB ? 1 : -1;
                        
                        return order;
                    }
                                
                else if(itemA.T1C_Base__BillingAccounts__r && !itemB.T1C_Base__BillingAccounts__r){return -1;}
         
                else if(itemA.T1C_Base__ShippingAccounts__r && !itemB.T1C_Base__ShippingAccounts__r){return -1;}

                else if(!itemA.T1C_Base__BillingAccounts__r && itemB.T1C_Base__BillingAccounts__r){return 1;}

                else if(!itemA.T1C_Base__ShippingAccounts__r && itemB.T1C_Base__ShippingAccounts__r){return 1;}
                
                
                
            });
            
            cards.appendTo(container);
            
        });
    }
    
    
    //AddressManagerForm
    
    $.extend(true, window,
    {
        "ACE":
        {
            "AddressManagerForm": new AddressManagerForm()
        }
    });
//# sourceURL=AddressManagerForm.js
})(jQuery);