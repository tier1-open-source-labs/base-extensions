//=============================================================================
/**
 * Name: AddressManagerFormItemFactory.js
 * Description: Factory class for inputs and labels for the address manager form
 *
 * Confidential & Proprietary, 2018 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1CRM.
 */
//=============================================================================
(function($)
{
    
	var inputMap;
	//SVGs
	//Define SVGs
    var undoIcon = '<svg class="undo" style="width:24px;height:24px" viewBox="0 0 24 24">' +
    '<path fill="#797979" d="M17.65,6.35C16.2,4.9 14.21,4 12,4A8,8 0 0,0 4,12A8,8 0 0,0 12,20C15.73,20 18.84,17.45 19.73,14H17.65C16.83,16.33 14.61,18 12,18A6,6 0 0,1 6,12A6,6 0 0,1 12,6C13.66,6 15.14,6.69 16.22,7.78L13,11H20V4L17.65,6.35Z" />'+
    '</svg>';
    
    var trashIcon = '<svg class="trash" style="width:20px;height:20px" viewBox="0 0 24 24">'+
    '<path fill="#797979" d="M19,4H15.5L14.5,3H9.5L8.5,4H5V6H19M6,19A2,2 0 0,0 8,21H16A2,2 0 0,0 18,19V7H6V19Z" />'+
    '</svg>';  
	
    //The following are all expected to have the signature: function (component, val, options, manualDisable) since they will all go into the map    
    /*
        Lookup Input
    */
	var createLookupInput = function (component, val, options, manualDisable) 
    {

		var dataField = options.dataField;
		
        var lookupItem = $('<div class="card-item-input"/>').appendTo(component);
        if(options.label && options.label.length > 0)
        {
            var label = $('<label class="affilLabel"></label>').appendTo(lookupItem);
            label.text(options.label);
        }
        var lookup = $p('<input></input>').appendTo(lookupItem);
        lookup.attr('title', options.hoverText);

        $(lookupItem).prop('datafield', options.dataField);//keep a reference in case we want to refresh the value
        if(options.required == true){$(lookup).addClass("required");}
        //DISABLE LOOKUP if(manualDisable == true){ thisTextBox.prop("disabled",true); }

        setTimeout(function()
        {
            var autoComplete = 
                new SearchAutoComplete($(lookup),
        		{
	                mode: 					"CUSTOM:" + options.additionalConfig.id,
	            	config: 				options.additionalConfig,
	                coverageOnly: 			false,
	                maintainSelectedItem:	true
        		});	
            
            autoComplete.set("select", function(item)
			{
            	var relationString = dataField.replace("__r$", "__c");
            	val[relationString] = item == null ? null : item.Id;
                
                //Put the relation item in the address
                var relationField = dataField.replace("__c", "__r");
                val[relationField] = item;

            	//$(lookup)[0].setAttribute("value", item[ dataField[i] ]);       
            });
            //autoComplete
            autoComplete.setSelectedItem( val[ dataField ], autoComplete.select, null );
        }
        ,100);
    }//Create Lookup Input

    /*
        Picklist Input
    */
    var createPickListInput = function (component, val, options, manualDisable) 
    {
    
    	var datafield = options.dataField
        var lookupItem = $('<div class="card-item-input"/>').appendTo(component);
        if(options.label && options.label.length > 0)
        {
            var label = $('<label class="affilLabel"></label>').appendTo(lookupItem);
            label.text(options.label);
        }
    
        $(lookupItem).prop('datafield', options.dataField);//keep a reference in case we want to refresh the value
    
        
        
        if(options.additionalConfig)
        {
        	var picklistValues = options.additionalConfig.dataProvider;
        }
        
        var menuButton = new ACEMenuButton($(lookupItem),
            {
        		dataProvider : picklistValues
        	}
        );
        
        if(options.required == true){$(menuButton).addClass("required");}
        //DISABLE MENU BUTTON if(manualDisable == true){ thisTextBox.prop("disabled",true); };
        
        if(val[datafield])
        {
        	menuButton.setSelectedItem( val[datafield], false );
        } 
        else
        {
        	menuButton.clearSelectedItem();
        }
        
        menuButton.on("click", function(event)
		{
        	val[datafield] = event.item;
		});
        
    }
    
    /*
        Text Input
    */
    var createTextInput = function (component, val, options, manualDisable) 
    {
        var datafield = options.dataField
        var lookupItem = $('<div class="card-item-input"/>').appendTo(component);
        if(options.label && options.label.length > 0)
        {
            var label = $('<label class="affilLabel"></label>').appendTo(lookupItem);
            label.text(options.label);
        }
        
        $(lookupItem).prop('datafield', options.dataField);//keep a reference in case we want to refresh the value
        
        var curVal = val[datafield] ? val[datafield] : "";
        curVal = curVal.replace(/"/g, '&quot;');
        var thisTextBox = $('<input title="' + options.hoverText + '" value="' + curVal + '"type="text"/>').appendTo(lookupItem);
        
        thisTextBox.attr("autocomplete","new-password");
        
        if(options.required == true){$(thisTextBox).addClass("required");}
        
        if(manualDisable == true || options.disabled == true){ thisTextBox.prop("disabled",true); };
        
        $(thisTextBox).change( [val, datafield] ,function(event)
		{
        	var object = event.data[0];
        	var field = event.data[1];
        	object[field] =  this.value;
		});
        
        $(thisTextBox).keyup(function()
        {
            var autoPop = $(this.parentElement).prop('AutoPopulatedName');
            var thisDataField = $(this.parentElement).prop('datafield');
            
            if(autoPop == true && thisDataField == 'T1C_Base__Address_Name__c')
            {
                $(this.parentElement).prop('AutoPopulatedName',false);
            }
        })
    }
    
    /*
        Checkbox Input
    */
    var createCheckBoxInput = function (component, val, options, manualDisable) 
    {
    	var datafield = options.dataField
    	
        var lookupItem = $('<div class="card-item-input"/>').appendTo(component);
        var outerLabel = $('<label class="control checkbox"></label>').appendTo(lookupItem);//Necessary so that design team can style the default checkbox
        
        $(lookupItem)['datafield'] = options.dataField;//keep a reference in case we want to refresh the value
        
        var thisCheckBox = $('<input type="checkbox"></input>').appendTo(outerLabel);
        thisCheckBox.attr('title', options.hoverText);

        var innerSpan = $('<span class="control-indicator"></span>').appendTo(outerLabel);
        
        if(options.required == true){$(thisCheckBox).addClass("required");}
        
        var labelString = '';
        if(options.label && options.label.length > 0)
        {
            labelString = options.label;
        }
        
        var innerLabel = $('<span></span>').appendTo(outerLabel);
        innerLabel.text(labelString);
        innerLabel.addClass(options.innerCSS)
        
        $(thisCheckBox)[0].checked = val[datafield] === true;
        
        if(manualDisable == true){ thisCheckBox.prop("disabled",true); };
        
        $(thisCheckBox).click(function() 
		{
        	val[datafield] = $(thisCheckBox)[0].checked;
		});
        
    }
    
    /*
        Date Input
    */   
    var createDateInput = function (component, val, options, manualDisable) 
    {
    	var datafield = options.dataField
    	
        var lookupItem = $('<div class="card-item-input"/>').appendTo(component);
        if(options.label && options.label.length > 0)
        {
            var label = $('<label class="affilLabel"></label>').appendTo(lookupItem);
            label.text(options.label);
        }
        
        $(lookupItem).prop('datafield', options.dataField);//keep a reference in case we want to refresh the value
        
        var datePicker = $('<input class="card-datepicker"/>').datepicker();
        datePicker.attr('title', options.hoverText);
        
        if(manualDisable == true){ datePicker.prop("disabled",true); };
		
        if(options.required == true){$(datePicker).addClass("required");}
        
		$(datePicker).appendTo(lookupItem);
		$(datePicker).val( val[datafield] );
		
		$(datePicker).on('change', function() 
		{
			val[datafield] = $(datePicker).val();
		});
		
    }
    
    /*
    	Delete/Undo button. Default behaviour is ignored if there is a callback
	*/   
	var createDeleteToggle = function (component, val, options, manualDisable) 
	{
		var datafield = options.dataField;
		
	    var lookupItem = $('<div class="card-btn-wrapper"/>').appendTo(component);
	    
        var label = (options.label) ? options.label : null;
        
        if (label != null){
            var buttonLabel = $('<label class="affilLabel"></label>').appendTo(lookupItem);
            buttonLabel.text(label);
        }
    	//Trash and undo icons are defined at the beginning
	    var deleteButton = $('<button class="card-delete"></button>').appendTo(lookupItem);
        deleteButton.attr('title', options.hoverText);
        
        if(manualDisable == true){ deleteButton.prop("disabled",true); };
        
	    //Look for a callback. If it doesnt have one, then just revert to the undo behaviour
        if(options.callback != null)
        {
            $(deleteButton).on("click",
                function(e)
                {
                    options.callback.call(this, e, val, options);
                }
            );
        }else
        {
            $(deleteButton).on("click",
                function(e)
                {
                    if ( $(e.target).closest('.componentContainer').find('.trash')[0] != null )//Look inside the particular cell for the svg class
                    {
                        //Set object value to the undo button                        
                        if(val.Id.indexOf('new') >= 0)//If it's an new item, remove the row else mark it for delete
                        {
                            //Delete the card if it's a newly created one, but not yet inserted
                            $(component).remove();
                        }else
                        {
                            $(e.target).closest('.componentContainer').find('.trash').replaceWith(undoIcon);
                            
                            val[datafield] = false; //set active to false
                        }
                        
                    }else if ($(e.target).closest('.componentContainer').find('.undo')[0] != null)
                    {
                        $(e.target).closest('.componentContainer').find('.undo').replaceWith(trashIcon);
                        
                        val[datafield] = true; //set active to true
                    }
                }
            );
	    }
		
	}
    
    /*
    Create a button input with a callback. Does nothing if the callback is null
    */
    var createGenericButton = function (component, valContainer, options, manualDisable) 
	{
        //Val container is the storage medium for the item. This is used for when the address
        //is edited and retrieved from database ST-268
        
        //look for a callback and use that. callbacks are expected to be defined in the calling class
        var lookupItem = $('<div class="card-btn-wrapper"/>').appendTo(component);
	    
        var label = (options.label) ? options.label : '';
        
        var innerClass = options.innerCSS ? options.innerCSS : "card-edit";
        
    	//Trash and undo icons are defined at the beginning
        var editButton = $('<button></button>').appendTo(lookupItem);
        editButton.attr('title', options.hoverText);
        editButton.addClass(innerClass);
        editButton.text(label);
        
        if(options.callback != null)
        {
            $(editButton).on("click", 
            function(e)
            {
                var currentItem = valContainer[0][options.dataField];
                options.callback.call(this, e, currentItem, options);
            });
        }
    }
    
    //"Constructor"
    function AddressManagerFormItemFactory()
    {
    	this.inputMap = 
        {
            'LOOKUP'    : createLookupInput,
            'PICKLIST'  : createPickListInput,
            'STRING'    : createTextInput,
            'TEXT'    	: createTextInput,
            'CHECKBOX'  : createCheckBoxInput,
            'DELETE'    : createDeleteToggle,
            'EDIT'      : createGenericButton,   
            'DATE'      : createDateInput
	    };
    }
    
    AddressManagerFormItemFactory.prototype.refreshCard = function(card, item)
    {
        var inputs = card.find('.card-address-text');
        var titleField = $(inputs).prop('titleField');
        var addressFields = $(inputs).prop('addressFields');
        var titleHolder = card.find('.card-title');
        
        card.prop('addressItem', item);
        
        var addressString = '';
        $.each(addressFields, function(index, val)
        {
            //Loop through the item and Append it's properties together (Except for the title)
            if(val != titleField && item[val] != undefined && item[val] != null)
            {
                addressString = addressString + item[val] + ' ';                
            }
        });
        inputs[0].innerText = addressString;
        titleHolder[0].innerText = item[titleField];
    }
    
    AddressManagerFormItemFactory.prototype.getGridColumns = function(config)
    {
        var columns = [];  
        if(config.gridColumns.length > 0)
        {

            var datePickerFormat = function(row,cell,value,columnDef,dataContext)
            {  

                if(value === '')
                {
                  return "<span class='fakeDatePicker'></span>";
                }
                var dateValue = new Date(value);
                var dateString = '';
                var correctedMilliTime = dateValue.getTime()  + (dateValue.getTimezoneOffset() * 60000);
                if(correctedMilliTime > 0)
                {
                    dateValue = new Date(correctedMilliTime);
                    dateString = dateValue.toLocaleDateString();
                }

                return "<span class='fakeDatePicker'>" + dateString + "</span>";
            }

            var phoneNumberFormatter= function(row,cell,value,columnDef,dataContext)
            {
              var phoneMatch = value.match(/\d{10}/);
              if(phoneMatch != null)
              {
                  console.log("Changing number " + value);
                  value = value.replace(/(\d{3})(\d{3})(\d{4})/, "$1-$2-$3");
                  console.log("Number Changed to " + value);
                  dataContext[columnDef.field.toString()] = value;
              }
              return value;
            }
          
            var lookupFormat = function(row, cell, value, columnDef, dataContext)
            {
                //Use the columns in the config to display the value.
                //Convert the data field to the relation field. For now this only supports 1 layer of parenting
                var relationField = columnDef.field.replace('__c','__r');
                var relationObj = dataContext[relationField];
                var finalVal = '';

                if(relationObj)
                {
                    $.each(columnDef.additionalConfig.columnList, function(index, column)
                    {
                        finalVal = finalVal + ' ' + relationObj[column.field];
                    });
                }
                return finalVal.trim();
            }

            //Formatter that returns a checked checkbox if the string length is greater than 0
            var StringToCheckbox = function (row, cell, value, columnDef, dataContext) 
            {
                //we always assume string
                var checkbox;
                
                if(value && value.length > 0)
                {
                  checkbox = '<div title="" class="linked ' + columnDef.innerCSS + '"></div>';
                }
                else
                {
                  checkbox = '<div title="" class="not-linked ' + columnDef.innerCSS + '"></div>';
                }
                
                return checkbox;
            }
            
            var pickListFormat = function (row, cell, value, columnDef, dataContext) 
            {
                var val = value.length > 0 ? value : columnDef.defaultValue;

                return '<span class="fakeACEMenuButton" title="' + val + '">' + val + '</div>';
            }
            
            var yesNoFormat = function (row, cell, value, columnDef, dataContext) 
            {
                if(val==undefined || val==null)
                {
                    return "Y";
                }
                return "N";
            }
            
            if(!config.isAccountAddressTab)
            {
                var checkBoxColumn = 
                {
                    sortable        : false,
                    fieldType       : "CHECKBOX",
                    id              : "checkcolumn",
                    field           : "selected",
                    maxWidth        : 30,
                    formatter       : "CheckBoxSelectorItemRenderer",//ACEDataGrid.DEFAULT_ITEM_RENDERERS.CheckBoxSelectorItemRenderer,
                    editor          : Slick.Editors.Checkbox
                };
            
                columns.push(checkBoxColumn);
            }
            
            $.each(config.gridColumns, function(index, item)
            {
                var o = {};
                if(item.fieldType == "LOOKUP")
                {
                    o = {
                    id              : item.id,
                    field           : item.dataField,
                    width           : item.width,
                    name            : item.label,
                    visible         : item.visible,
                    sortable        : true,
                    fieldType       : "STRING",
                    placeHolder     : item.placeHolder,    
                    editable        : !JSON.parse(item.disabled.toString()),
                    additionalConfig: item.additionalConfig,
                    disabled        : item.disabled.toString().toLowerCase(),
                    editor          : Slick.Editors.AutoComplete,
                    formatter       : lookupFormat
                  };                  
                }
                else if(item.fieldType == "PICKLIST")
                {
                    o = {
                    id                  : item.id,
                    field               : item.dataField,
                    width               : item.width,
                    name                : item.label,
                    visible             : item.visible,
                    sortable            : true,
                    fieldType           : "STRING",
                    editorDataProvider  : item.additionalConfig.dataProvider,
                    editable            : !JSON.parse(item.disabled.toString()),
                    disabled            : item.disabled.toString().toLowerCase(),
                    formatter           : pickListFormat,
                    defaultValue        : item.defaultValue,
                    cssClass            : "menuButtonColumn",
                    editor              : Slick.Editors.ACEMenuButton  
                    };
                } 
                else if(item.fieldType == "CHECKBOX")
                {
                    o = {
                    id              : item.id,
                    field           : item.dataField,
                    maxWidth        : item.width,
                    name            : item.label,
                    innerCSS        : item.innerCSS ? item.innerCSS : '',
                    visible         : item.visible,
                    sortable        : true,
                    fieldType       : "CHECKBOX",
                    editable        : !JSON.parse(item.disabled.toString()),
                    disabled        : item.disabled.toString().toLowerCase(),
                    formatter       : StringToCheckbox
                    };
                }
                else if(item.fieldType == "DATE")
                {
                    o = {
                    id                : item.id,
                    field             : item.dataField,
                    width             : item.width,
                    name              :item.label,
                    visible           : item.visible,
                    selectable        : true,
                    sortable          : true,
                    focusable         : true,
                    fieldType         : "DATE",
                    editable          : !JSON.parse(item.disabled.toString()),
                    formatter         : datePickerFormat,
                    disabled          : item.disabled.toString().toLowerCase(),
                    editor            : Slick.Editors.DateEditor
                    };
                }
                else if(item.fieldType == "ACCOUNTLOCATION")//Data field should always be mailing address ID
                {
                    o = {
                    id                : item.id,
                    field             : "T1C_Base__Mailing_Address_Id__c",
                    width             : item.width,
                    name              : item.label,
                    visible           : item.visible,
                    selectable        : true,
                    sortable          : true,
                    focusable         : true,
                    fieldType         : "STRING",
                    editable          : !JSON.parse(item.disabled.toString()),
                    formatter         : "AccountLocRenderer",
                    disabled          : false
                    };
                }
                else
                {
                    o = {
                    id              : item.id,
                    field           : item.dataField,
                    width           : item.width,
                    name            : item.label,
                    visible         : item.visible,
                    sortable        : true,
                    fieldType       : "STRING",
                    placeHolder     : item.placeHolder,
                    editable        : !JSON.parse(item.disabled.toString()),
                    /*formatter       : plainTextFormatter,*/
                    disabled        : item.disabled.toString().toLowerCase(),
                    editor          : Slick.Editors.Text  
                    };
                }
                o.color="#FF9968";
                columns.push(o);

            });//$.each
          
            if(!config.isAccountAddressTab)
            {
                var undoColumn = 
                {
                    id              : "undoColumn",
                    field           : "hasChanged",
                    sortable        : true,
                    maxWidth        : 30,
                    formatter       : "UndoItemRenderer"//ACEDataGrid.DEFAULT_ITEM_RENDERERS.CheckBoxSelectorItemRenderer,
                };
                columns.splice(config.undoColumnPosition, 0, undoColumn);
            }
      }//if(this.config.fieldFormFields.length > 0)

      return columns;
    }//AddressManagerFormItemFactory.prototype.getGridColumns
    
    $.extend(true, window,
		{
			"ACE":
			{
				"AddressManagerFormItemFactory": new AddressManagerFormItemFactory()
			}
		}
    );	

//# sourceURL=AddressManagerFormItemFactory.js
})(jQuery);