//=============================================================================
/**
 * Name: SlickGridExtension.js
 * Description: Adding editors to the slick grid component 
 *
 * Confidential & Proprietary, 2015 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1CRM.
 */
//=============================================================================
(function($)
{

    $.extend(true, window, {
      "Slick": {
        "Editors": {
          "AutoComplete": AutoCompleteEditor,
          "Picklist": PickListEditor, 
          "DateEditor": DateEditor,    
          "Checkbox" : CheckboxEditor
        }
      }
  });

    function CheckboxEditor(args) {
    	var myInput;
        this.args = args;
        var _this;
    	
    }

    function AutoCompleteEditor(args) {
     var myInput;
     this.args = args;
     var _this;

     this.init = function () {
        myInput = $p('<la-input />');
        myInput.appendTo(args.container);
      setTimeout(function()
    {
          var autoComplete = new SearchAutoComplete($(myInput[0]),
      {       
        mode: "CUSTOM:" + args.column.additionalConfig.id,
        config: args.column.additionalConfig,
        coverageOnly: false,
        select: function(value)
        {
          _this.applyValue(value); 
        }   
      });
        },5);
        
     };

     this.loadValue = function (item) {
        if(item[args.column.field] != null)
        {
            $(myInput[0]).val(item[args.column.field]);
        }
        else if(item[args.column.field.substring(0,args.column.field.indexOf("."))] != null)
        {
            $(myInput[0]).val(item[args.column.field.substring(0,args.column.field.indexOf("."))].Name);
        }
        else
        {
           $(myInput[0]).val = "";
        }
     
      this.item = item;
      _this = this;

     }

     this.serializeValue = function () {
      //return $(myInput[0]).val();
     }

     this.isValueChanged = function () {
       //return !($(myInput[0]).val() == ""); 
     }

    this.destroy = function () {
        $(myInput[0]).remove();
     };

     this.applyValue = function (value) {

        //updates the datagrid view with the selected name
      _this.item[_this.args.column.field] = value.Name;

        //update the fields in "item" so that we can save the ids to salesforce 
        var field = _this.args.column.field;
        _this.item[field.substring(0,field.indexOf(".")).replace(/.$/,"c")] = value.Id;

        // _this.args.item[field.substring(0,field.indexOf("."))] = value;
        // field = field.substring(0,field.indexOf("_")).concat("__c");
        // _this.args.item[field] = value.Id;
      _this.args.commitChanges();
     };

    this.validate = function() {

  };

     this.init();
   }


    function PickListEditor(args) {
        var $select;
        var defaultValue = args.item[args.column.field];
        var _this = this;

        this.init = function() {

          opt_values = args.column.picklistOptions;
            option_str = ""
            for(var i = 0; i < opt_values.length; i++){
              v = opt_values[i];
              option_str += "<OPTION value='"+v+"'>"+v+"</OPTION>";
              //option_str += "<li class='aceMenuItem' data-value='"+v+"'>"+v+"</li>";
            }
            $select = $("<SELECT tabIndex='0' style='width:100%;' class='editor-select'>"+ option_str +"</SELECT>");
            //$select = $("<ul data-type='aceMenu' class='buttonMenu aceMenu'>"+ option_str +"</ul>");
            $select.appendTo(args.container);
            //$select.val(defaultValue);
            $select.focus();
        };

        this.destroy = function() {
            $select.remove();
        };

        this.focus = function() {
            $select.focus();
        };

        this.loadValue = function(item) {
            defaultValue = item[args.column.field];
            $select.val(defaultValue);
        };

        this.serializeValue = function() {
            if(args.column.picklistOptions){
              return $select.val();
            }
        };


        this.applyValue = function(item,state) {
            item[args.column.field] = state;
        };

        this.isValueChanged = function() {
            return ($select.val() != defaultValue);
        };

        this.validate = function() {
            return {
                valid: true,
                msg: null
            };
        };

        this.init();
    }

    function DateEditor(args) {
      var $input;
      var defaultValue;
      var scope = this;
      var calendarOpen = false;
    
      this.init = function () {
        var datePickerDiv = $p('<div/>');
        var datePickerSpan = $p('<span/>').appendTo(datePickerDiv); 
    
       $input = $("<INPUT type=text class text='editor-text' style='position: relative; z-index: 10000' readonly/>")
       .datepicker()
       .bind("change",function(e){
                  debugger;
                  var d = new Date(e.currentTarget.value);
                  var dc = new Date(this.defaultValue);
                  if(d.setHours(0,0,0,0) != dc.setHours(0,0,0,0))
                  {
                    $(args.column.topDiv).show(333);
                  }
              })
       .appendTo(datePickerSpan);
       datePickerDiv.appendTo(args.container);
       //args.column.topDiv.show();
    
        $input.datepicker({
          beforeShow: function () {
            calendarOpen = true
          },
          onClose: function () {
            calendarOpen = false
          }
        });
      };
    
      this.destroy = function () {
        $.datepicker.dpDiv.stop(true, true);
        $input.datepicker("hide");
        $input.removeClass("hasDatepicker");
        $input.datepicker("destroy");
        $input.remove();
      };
    
      this.show = function () {
        if (calendarOpen) {
          $.datepicker.dpDiv.stop(true, true).show();
        }
      };
    
      this.hide = function () {
        if (calendarOpen) {
          $.datepicker.dpDiv.stop(true, true).hide();
        }
      };
    
      this.position = function (position) {
        if (!calendarOpen) {
          return;
        }
    
      };
    
      this.focus = function () {
       $input.focus();
      };
    
    
      this.loadValue = function (item) {
        if(item[args.column.field] != "" && item[args.column.field] != null)
        {
          defaultValue = new Date(item[args.column.field]);;
          var dateValue = new Date(defaultValue.getTime() + (defaultValue.getTimezoneOffset() *  60000));
          $input.val(dateValue.toLocaleDateString());
          $input[0].defaultValue = dateValue.getTime();
        }
    
        $input.select();
      };
    
      this.serializeValue = function () {
        return $input.val();
      };
    
      this.applyValue = function (item, state) {
        var date = new Date(state);
        var miliTime = date.getTime()  + (date.getTimezoneOffset() * 60000)
        item[args.column.field] = miliTime;
      };
    
      this.isValueChanged = function () {
        return (!($input.val() == "" && defaultValue == null)) && ($input.val() != defaultValue);
      };
    
      this.validate = function () {
        return {
          valid: true,
          msg: null
        };
      };
    
      this.init();
      }
        
    
  //# sourceURL=SlickGridExtensions.js
})(jQuery);