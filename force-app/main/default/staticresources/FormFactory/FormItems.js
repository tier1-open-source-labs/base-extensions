//=============================================================================
/**
 * Name: FormItems.js
 * Description: List of form items that can be used.
 *
 * Confidential & Proprietary, 2017 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1CRM.
 */
//=============================================================================


(function($)
{	

	function FormItems()
	{

	}

	//  =========================================================
    //  Method: Button
    //  Desc:   Method that will attach a button to the target div. Any 
    //			click event will be handled through the "callBack" funciton. 
    //  Args:   config, targetDiv, callBack
    //  Return: formItem
    //  =========================================================   
	FormItems.prototype.Button = function(config, targetDiv, form, callBack)
	{	
		var formItem = $('<button type="button" id="'+ config.id + 
								'" class= "applyToSelected la-ripple-container"' +
								' style = "' + config.style + 
								'">' + config.label + '</button>').appendTo(targetDiv);

		//Check if its disabled
		if(config.isEditable !== undefined)
		{
			if(config.isEditable.toLowerCase() === 'false')
			{			
				formItem[0].disabled = true;
			}	
		}

		//check if its visible
		if(config.visible !== undefined)
		{
			if(config.visible.toLowerCase() === 'false')
			{			
				formItem.hide();
			}	
		}


		formItem.on('click', function(){
			callBack(config.id, form);
		});

		return formItem;
	};

	//  =========================================================
    //  Method: Input
    //  Desc:   Method that will attach a input to the target div. The type of input 
    //			will be provided by the config.type. Any change event will be 
    //			handled through the "callBack" funciton. 
    //  Args:   config, targetDiv, callBack
    //  Return: formItem
    //  ========================================================= 
	FormItems.prototype.Input = function(config, targetDiv, form, callBack)
	{	
		var parentContainer = $p('<div class="componentContainer subSectionItem"' +
									'style="' + config.parentContainerStyle +  
									'"></div>').appendTo(targetDiv);
		var parentLabel;

		if(config.required !== undefined)
		{
			if(config.required.toLowerCase() === 'true')
			{
				parentLabel = $p('<div class="req label" style="color:#db4437">' + config.parentLabel + '</div>').appendTo(parentContainer);	
			}
		}
		else
		{
			parentLabel = $p('<div style="' + config.parentLabelStyle + 
								   '" class="label">' + config.parentLabel +
								  '</div>').appendTo(parentContainer);	
		}

		 
		if(config.placeHolder === null || config.placeHolder === undefined)
		{
			config.placeHolder = '';
		}

		if(config.defaultValue === null || config.defaultValue === undefined)
		{
			config.defaultValue = '';
		}

		if(config.defaultValue !== undefined)
			config.label = config.defaultValue;

		var editContainer = $p('<div class="cubeComponentContainer"></div>').appendTo(parentContainer);
		var formItem = $p('<la-input id="' + config.id + 
									'" style="' + config.style + 
									'" type="' + config.type +  
									'" placeholder="' + config.placeHolder + 
									'"></la-input>').appendTo(editContainer);
		

		//set default value										
		formItem[0].value = config.defaultValue;

		//Check if its editable
		if(config.isEditable !== undefined)
		{
			if(config.isEditable.toLowerCase() === 'false')
			{			
				formItem[0].disabled = true;
				var clear = formItem.find('#la-input-clear');
				clear.hide();
			}	
		}

		formItem.on('change', function(){
			callBack(config.id, form);
		});

		return formItem;
	};

		//  =========================================================
    //  Method: BasicInput (no class styles)
    //  Desc:   Method that will attach a input to the target div. The type of input 
    //			will be provided by the config.type. Any change event will be 
    //			handled through the "callBack" funciton. 
    //  Args:   config, targetDiv, callBack
    //  Return: formItem
    //  ========================================================= 
	FormItems.prototype.BasicInput = function(config, targetDiv, form, callBack)
	{	
		var parentContainer = $p('<div class="' + config.parentClass + 
									'" style="' + config.parentContainerStyle + 
								 '"></div>').appendTo(targetDiv);
		var parentLabel;

		if(config.required !== undefined)
		{
			if(config.required.toLowerCase() === 'true')
			{
				parentLabel = $p('<div class="req label" style="color:#db4437;font-size: 0.7em;font-weight: 600;height: 21px;">' + config.parentLabel + '</div>').appendTo(parentContainer);	
			}
		}
		else
		{
			parentLabel = $p('<div style="' + config.parentLabelStyle + 
								   '" class="label">' + config.parentLabel +
								  '</div>').appendTo(parentContainer);	
		}

		 
		if(config.placeHolder === null || config.placeHolder === undefined)
		{
			config.placeHolder = '';
		}

		if(config.defaultValue === null || config.defaultValue === undefined)
		{
			config.defaultValue = '';
		}

		if(config.defaultValue !== undefined)
		{
			config.label = config.defaultValue;
			if(form._dialogOptions.globalSaveRecord !== undefined && config.dataField !== undefined)
			{
				form._dialogOptions.globalSaveRecord[config.dataField] = config.defaultValue;
			}
		}

		var editContainer = $p('<div class="' + config.className + '"></div>').appendTo(parentContainer);
		var formItem = $p('<la-input id="' + config.id + 
									'" style="' + config.style + 
									'" type="' + config.inputType +  
									'" placeholder="' + config.placeHolder + 
									'"></la-input>').appendTo(editContainer);
		

		//set default value										
		formItem[0].value = config.defaultValue;

		//Check if its editable
		if(config.isEditable !== undefined)
		{
			if(config.isEditable.toLowerCase() === 'false')
			{			
				formItem[0].disabled = true;
				var clear = formItem.find('#la-input-clear');
				clear.hide();
			}	
		}

		formItem.on('change', function(){
			callBack(config.id, form, formItem);
		});

		return formItem;
	};

	//  =========================================================
    //  Method: Checkbox
    //  Desc:   Method that will attach a Checkbox element to the target div. Any change 
    //			event will be will be handled through the "callBack" funciton. 
    //  Args:   config, targetDiv, form, callBack
    //  Return: formItem
    //  ========================================================= 
	FormItems.prototype.Checkbox = function(config, targetDiv, form, callBack)
	{
	
		var parentContainer = $p('<div class="' + config.parentClass + 
									'" style="' + config.parentContainerStyle + 
								'"></div>').appendTo(targetDiv);
	 
		var parentLabel;

		if(config.required !== undefined)
		{
			if(config.required.toLowerCase() === 'true')
			{
				parentLabel = $p('<div class="req label" style="color:#db4437;font-size: 0.7em;font-weight: 600;height: 21px;">' + config.parentLabel + '</div>').appendTo(parentContainer);	
			}
		}
		else
		{
			parentLabel = $p('<div style="' + config.parentLabelStyle + 
								   '" class="label">' + config.parentLabel +
								  '</div>').appendTo(parentContainer);	
		}

		var editContainer = $p('<div class="' + config.className + '"></div>').appendTo(parentContainer);
		var formItem= $('<input type="checkbox" id="' +  config.id + '"></input>').appendTo(editContainer).checkbox();


		if(config.defaultValue !== undefined)
		{
			//setting the default value
			if(config.defaultValue.toLowerCase() === 'true')
			{
				formItem[0].checked = true;
			}
			if(form._dialogOptions.globalSaveRecord !== undefined && config.dataField !== undefined)
			{
				form._dialogOptions.globalSaveRecord[config.dataField] = formItem[0].checked;
			}
		}

		//Check if its editable
		if(config.isEditable !== undefined)
		{
			if(config.isEditable.toLowerCase() === 'false')
			{			
				formItem[0].disabled = true;				
			}	
		}

		formItem.on('click', function(){
			callBack(config.id, form, formItem);
		});

		return formItem;
	};

	//  =========================================================
    //  Method: TextArea
    //  Desc:   Method that will attach a TextArea element to the target div. Any change 
    //			event will be will be handled through the "callBack" funciton. 
    //  Args:   config, targetDiv, form, callBack
    //  Return: formItem
    //  ========================================================= 
	FormItems.prototype.TextArea = function(config, targetDiv, form, callBack)
	{
		var parentContainer = $p('<div style="' + config.parentContainerStyle + '"></div>').appendTo(targetDiv);
		var parentLabel;

		if(config.required !== undefined)
		{
			if(config.required.toLowerCase() === 'true')
			{
				parentLabel = $p('<div class="req label" style="color:#db4437;font-size: 0.7em;font-weight: 600;height: 21px;">' + config.parentLabel + '</div>').appendTo(parentContainer);	
			}
		}
		else
		{
			parentLabel = $p('<div style="' + config.parentLabelStyle + 
								   '" class="label">' + config.parentLabel +
								  '</div>').appendTo(parentContainer);	
		}

		 
		if(config.placeHolder === null || config.placeHolder === undefined)
		{
			config.placeHolder = '';
		}

		if(config.defaultValue === null || config.defaultValue === undefined)
		{
			config.defaultValue = '';
		}

		var editContainer = $p('<div></div>').appendTo(parentContainer);
		var formItem = $p('<la-textarea id="' + config.id + 
										'" style="' + config.style + 
										'" placeholder="' + config.placeHolder + 
										'"></la-textarea>').appendTo(editContainer);
		

		//set default value										
		formItem[0].value = config.defaultValue;

		//Check if its editable
		if(config.isEditable !== undefined)
		{
			if(config.isEditable.toLowerCase() === 'false')
			{			
				formItem[0].disabled = true;
				var clear = formItem.find('#la-input-clear');
				clear.hide();
			}	
		}

		formItem.on('change', function(){
			callBack(config.id, form, formItem);
		});

		return formItem;
	}


	//  =========================================================
    //  Method: FilterInput
    //  Desc:   Method that will attach a FilterInput element to the target div. Any change 
    //			event will be will be handled through the "callBack" funciton. 
    //  Args:   config, targetDiv, form, callBack
    //  Return: formItem
    //  ========================================================= 
	FormItems.prototype.FilterInput = function(config, targetDiv, form, callBack)
	{	
		if(config.placeHolder === null || config.placeHolder === undefined)
		{
			config.placeHolder = '';
		}

		if(config.defaultValue === null || config.defaultValue === undefined)
		{
			config.defaultValue = '';
		}

		if(config.label === null || config.label === undefined)
		{
			config.label = '';
		}

		var parentContainer = $p('<div style="width:100%"></div>').appendTo(targetDiv);
		var formItem = $p('<la-input id="' + config.id + 
									'" style="' + config.style  + 
									'" label="' + config.label + 
									'" type="' + config.type +  
									'" placeholder="' + config.placeHolder + 
									'"></la-input>').appendTo(parentContainer);
		
		var clear = formItem.find('#la-input-clear');				
		clear.hide();

		formItem.keyup(function()
		{
			callBack(config.id, form);
		});	
			
		return formItem;	
	};

	//  =========================================================
    //  Method: Text
    //  Desc:   Method that will attach a Text element to the target div. Any change 
    //			event will be will be handled through the "callBack" funciton. 
    //  Args:   config, targetDiv, form, callBack
    //  Return: formItem
    //  ========================================================= 
	FormItems.prototype.Text = function(config, targetDiv, form, callBack)
	{
		if(config.parentLabel === null || config.parentLabel === undefined)
			config.parentLabel = '';

		if(config.label === null || config.label === undefined)
			config.label = '';

		if(config.defaultValue !== undefined)
			config.label = config.defaultValue;

		var parentContainer = 	$p('<div>'+ config.parentLabel + '</div>').appendTo(targetDiv);

		var formItem = 	$p('<div id="' + config.id + 
								 '" style=" ' + config.style + '">'
								       + config.label 
								       + '</div>').appendTo(targetDiv);

		return formItem;
	};

	//  =========================================================
    //  Method: CheckboxListDropDown
    //  Desc:   Method that will attach a CheckboxListDropDown element to the target div. Any change 
    //			event will be will be handled through the "callBack" funciton. 
    //  Args:   config, targetDiv, form, callBack
    //  Return: formItem
    //  ========================================================= 
	FormItems.prototype.CheckboxListDropDown = function(config, targetDiv, form, callBack)
	{	
		var parentContainer = $p('<div class="' + config.parentClass + 
									'" style="' + config.parentContainerStyle + 
								'"></div>').appendTo(targetDiv);
								

		if(config.required !== undefined)
		{
			if(config.required.toLowerCase() === 'true')
			{
				$p('<div class="req label" style="color:#db4437;font-size: 0.7em;font-weight: 600;height: 21px;">' + config.parentLabel + '</div>').appendTo(parentContainer);	
			}
			else
			{
				$p('<div class="label" style="' + config.parentLabelStyle + '" >' + config.parentLabel + '</div>').appendTo(parentContainer);	
			}
		}
		else
		{
			$p('<div style="' + config.parentLabelStyle + '" >' + config.parentLabel + '</div>').appendTo(parentContainer);
		}

		var checkBoxDropDownContainer = $p('<div class="checkBoxDropDownContainerClass" style="width:50%" />').appendTo(parentContainer);
		
		var defaultList = config.defaultValue !== undefined ? config.defaultValue.split(','): [];
		if(config.additionalConfig.dataProvider !== undefined)
		{
			defaultList =  config.additionalConfig.dataProvider;
		}
		var formItem = new ACE.CheckboxListDropDown({
														dataProvider: defaultList,
														placeholder : config.placeHolder == null ? '' : config.placeHolder
										  			});

		formItem.on('change', function(){
			callBack(config.id, form, formItem);
		});

		formItem.appendTo(checkBoxDropDownContainer);
		return formItem;

	};

	//  =========================================================
    //  Method: MultiSelect
    //  Desc:   Method that will attach a multiSelect element to the target div.	Any change 
    //			event will be will be handled through the "callBack" funciton. 
    //  Args:   config, targetDiv, form, callBack
    //  Return: formItem
    //  ========================================================= 
	FormItems.prototype.MultiSelect = function(config, targetDiv, form, callBack)
	{	

		var defaultList = config.defaultValue !== ''? config.defaultValue.split(','): [];
		var formItem = new ACEMultiSelect({
											 dataProvider: defaultList,
											 selectedItems: [],
											 labelField: "",
											 availableItemsHeaderText: config.availableHeaderText,
											 selectedItemsHeaderText: config.selectedHeaderText,
											 containerStyleName: "multiSelect",
											 addMissingValues: false,
											 title: ""
										   });
		formItem.appendTo(targetDiv);
		return formItem;

	};

	//  =========================================================
    //  Method: PhoneNumber
    //  Desc:   Method that will attach a phone element to the target div.	Any change 
    //			event will be will be handled through the "callBack" funciton. 
    //  Args:   config, targetDiv, form, callBack
    //  Return: formItem
    //  ========================================================= 
	FormItems.prototype.PhoneNumber = function(config, targetDiv, form, callBack)
	{		
		var parentContainer = $p('<div class="' + config.parentClass + 
									'" style="' + config.parentContainerStyle + 
								'"></div>').appendTo(targetDiv);
		
		if(config.required !== undefined)
		{
			if(config.required.toLowerCase() === 'true')
			{
				parentLabel = $p('<div class="req label" style="color:#db4437;font-size: 0.7em;font-weight: 600;height: 21px;">' + config.parentLabel + '</div>').appendTo(parentContainer);	
			}
		}
		else
		{
			parentLabel = $p('<div style="' + config.parentLabelStyle + 
									'" class="label">' + config.parentLabel +
									'</div>').appendTo(parentContainer);	
		}

		var elementContainer     = $p('<div class="' + config.className + '" style="display:flex"></div>').appendTo(parentContainer);
								   $p('<div class="plusSignClass" style="padding: 0px 2px 0px 2px;"> + </div>').appendTo(elementContainer);								  							  
		var countryCodeContainer = $p('<div class="countryCodeClass" style="padding: 0px 5px 0px 5px;"></div>').appendTo(elementContainer);								   										 
		var phoneNumberContainer = $p('<div class="phoneNumberClass"/>').appendTo(elementContainer);
		var extensionContainer   = $p('<div class="extensionClass" style="padding: 0px 0px 0px 5px;"/>').appendTo(elementContainer);
		var countryCodeFormItem  = $p('<la-input style="width:40px" type="numbers" placeholder="1" title="Country Code"></la-input>').appendTo(countryCodeContainer);
		var phoneNumberFormItem  = $p('<la-input style="width:125px" placeholder="(123) 456-7890" title="' + config.phoneNumberTitle  + '"></la-input> ').appendTo(phoneNumberContainer);
		var extensionFormItem    = $p('<la-input style="width:50px"  type="numbers" placeholder="Ext"></la-input> ').appendTo(extensionContainer);
		$(countryCodeFormItem.find('#la-input')).prop('maxlength','3');
		$(extensionFormItem.find('#la-input')).prop('maxlength','6');

		phoneNumberFormItem.on('change', function(){
			callBack(config.id, form, phoneNumberFormItem, {countryCode:countryCodeFormItem, 															
															phoneNumber:phoneNumberFormItem,
															extensionNumber:extensionFormItem});
		});

		countryCodeFormItem.on('change', function(){
			callBack(config.id, form, countryCodeFormItem, {countryCode:countryCodeFormItem, 															
															phoneNumber:phoneNumberFormItem,
															extensionNumber:extensionFormItem});
		});

		extensionFormItem.on('change', function(){
			callBack(config.id, form, extensionFormItem, {countryCode:countryCodeFormItem, 															
															phoneNumber:phoneNumberFormItem,
															extensionNumber:extensionFormItem});
		});

		return parentContainer;
	};

	//  =========================================================
    //  Method: CountryCodeToAreaCodePhone
    //  Desc:   Method that will attach a phone element(Country to areaCode relationship) to the target div. Any change 
    //			event will be will be handled through the "callBack" funciton. 
    //  Args:   config, targetDiv, form, callBack
    //  Return: formItem
    //  ========================================================= 
	FormItems.prototype.CountryCodeToAreaCodePhone = function(config, targetDiv, form, callBack)
	{

		var parentContainer = $p('<div style="' + config.parentContainerStyle +  '" />').appendTo(targetDiv);
		
		if(config.required !== undefined)
		{
			if(config.required.toLowerCase() === 'true')
			{
				parentLabel = $p('<div class="req label" style="color:#db4437;font-size: 0.7em;font-weight: 600;height: 21px;">' + config.parentLabel + '</div>').appendTo(parentContainer);	
			}
		}
		else
		{
			parentLabel = $p('<div style="' + config.parentLabelStyle + 
									'" class="label">' + config.parentLabel +
									'</div>').appendTo(parentContainer);	
		}
		
		var elementContainer     = $p('<div style="display:flex" />').appendTo(parentContainer);	
		var countryCodeContainer = $p('<div></div>').appendTo(elementContainer);	
								   $p('<div style="padding: 0px 10px 0px 10px;"> (</div>').appendTo(elementContainer);	
		var areaCodeContainer    = $p('<div></div>').appendTo(elementContainer);
								   $p('<div style="padding: 0px 10px 0px 10px;">) </div>').appendTo(elementContainer);
		var phoneNumberContainer = $p('<div/>').appendTo(elementContainer);
		
		var countryCodeFormItem  = $p('<la-input style="width:40px"  title="Country Code"></la-input>').appendTo(countryCodeContainer);
		var areaCodeFormItem     = $p('<la-input style="width:40px"  title="Area Code"></la-input>').appendTo(areaCodeContainer);
		
		$(countryCodeFormItem.find('#la-input')).prop('maxlength','4');
		$(areaCodeFormItem.find('#la-input')).prop('maxlength','4');

		var phoneNumberFormItem  = $p('<la-input type="numbers"></la-input> ').appendTo(phoneNumberContainer);

		setTimeout(function()
		{
			var autoComplete = new SearchAutoComplete($(countryCodeFormItem),
			{
				mode: "CUSTOM:" + config.additionalCountryConfig.id,
				maintainSelectedItem: true,
				minLength: 1,
				selectedItemFormat: "{Country_Code__c}",
				config: config.additionalCountryConfig,
				select: function(item)
				{							
					if(item != null)
					{	
						countryCodeFormItem[0].value = item.Country_Code__c;									
						areaCodeFormItem.SearchAutoComplete.set('additionalWhereClause', ' Country__r.Country_Code__c = \'' +  item.Country_Code__c + '\'' + ' AND Country__r.Name = \'' + item.Name + '\''); 
						callBack(config.id, form, item, {countryCode:countryCodeFormItem, 
														 areaCode: areaCodeFormItem, 
														 phoneNumber:phoneNumberFormItem});		
					}
					else
					{
						areaCodeFormItem[0].value = '';
					}						
				}		
			});	

		}, 100);


		setTimeout(function()
		{
			var autoComplete = new SearchAutoComplete($(areaCodeFormItem),
			{
				mode: "CUSTOM:" + config.additionalAreaCodeConfig.id,
				maintainSelectedItem: true,
				minLength: 1,
				selectedItemFormat: "{Area_Code__c}",
				config: config.additionalAreaCodeConfig,				
				select: function(item)
				{					
					if(item != null)
					{
						areaCodeFormItem[0].value = item.Area_Code__c; 		
						callBack(config.id, form, item, {countryCode:countryCodeFormItem, 
														 areaCode: areaCodeFormItem, 
														 phoneNumber:phoneNumberFormItem});			
					}						
				}		
			});	
			
			areaCodeFormItem.SearchAutoComplete = autoComplete;
		}, 100);

		phoneNumberFormItem.on('change', function(){
			callBack(config.id, form, phoneNumberFormItem, {countryCode:countryCodeFormItem, 
															areaCode: areaCodeFormItem, 
															phoneNumber:phoneNumberFormItem});
		});

		return parentContainer;
	};



	//  =========================================================
    //  Method: EmailDomain
    //  Desc:   Method that will attach a email element to the target div.	Any change 
    //			event will be will be handled through the "callBack" funciton. 
    //  Args:   config, targetDiv, form, callBack
    //  Return: formItem
    //  ========================================================= 
	FormItems.prototype.EmailDomain = function(config, targetDiv, form, callBack)
	{
		var parentContainer = $p('<div class="' + config.parentClass + '" style="' + config.parentContainerStyle +  '" />').appendTo(targetDiv);
		var elementContainer = $p('<div class="' + config.elementClass + '" />').appendTo(parentContainer);

		if(config.required !== undefined)
		{
			if(config.required.toLowerCase() === 'true')
			{
				parentLabel = $p('<div class="req label" style="color:#db4437;font-size: 0.7em;font-weight: 600;height: 21px;">' + config.parentLabel + '</div>').appendTo(elementContainer);	
			}
		}
		else
		{
			parentLabel = $p('<div style="' + config.parentLabelStyle + 
								   '" class="label">' + config.parentLabel +
								  '</div>').appendTo(elementContainer);	
		}
		
		var inputContainer = $p('<la-input id="' + config.id + 
										'" style="' + config.style + 
										'" type="Input'  +  
										'" placeholder="' + config.placeHolder + 
										'"></la-input>').appendTo(elementContainer);

		var domainContainer = $p('<div class="' + config.domainClass + '" style="display:flex" />').appendTo(parentContainer);
		$p('<div class="label" style="font-size: 0.7em;font-weight: 600;height: 21px;padding-top: 10px;">Select email domain: </div>').appendTo(domainContainer);	
		var pickListItems = new ACEMenuButton($(domainContainer),{ dataProvider : config.picklistValues});
		
		inputContainer.on('change', function(){
			callBack(config.id, form, inputContainer, {picklist:pickListItems, input:inputContainer});
		});		

		pickListItems.on('click', function(pickListItems) 
		{
			callBack(config.id, form, inputContainer, {picklist:pickListItems, input:inputContainer});
		});


		return parentContainer;
	};

	//  =========================================================
    //  Method: PickList
    //  Desc:   Method that will attach a picklist element to the target div. Any change 
    //			event will be will be handled through the "callBack" funciton. 
    //  Args:   config, targetDiv, form, callBack
    //  Return: formItem
    //  ========================================================= 
	FormItems.prototype.PickList = function(config, targetDiv, form, callBack)
	{									
		var parentContainer = $p('<div class="' + config.parentClass + 
									'" style="' + config.parentContainerStyle + 
								  '"></div>').appendTo(targetDiv);
			 
		var title = config.title == null ? "" : config.title;

		if(config.required !== undefined)
		{
			if(config.required.toLowerCase() === 'true')
			{
				parentLabel = $p('<div class="req label" style="color:#db4437;font-size: 0.7em;font-weight: 600;height: 21px;" title="' + title + '">' + config.parentLabel + '</div>').appendTo(parentContainer);	
			}
		}
		else
		{
			parentLabel = $p('<div style="' + config.parentLabelStyle + 
								   '" class="label" title="'+ title + '">' + config.parentLabel +
								  '</div>').appendTo(parentContainer);	
		}


		var editContainer = $p('<div class="cubeComponentContainer"></div>').appendTo(parentContainer);

		var fieldValue = config.defaultValue;	

		if(fieldValue === null)
		{
			fieldValue = '';
		}

		var picklistValues = config.picklistValues !== undefined ? config.picklistValues:  
																   config.additionalConfig.dataProvider;
		var pickListItems = new ACEMenuButton($(editContainer),{ dataProvider : picklistValues});
		
		editContainer[0].pickListItems = pickListItems;
		editContainer[0].id =  config.id;

		pickListItems.on('click', function() 
		{
			callBack(config.id, form, pickListItems);
		});

		if(config.defaultValue !== "")
		{
			if(form._dialogOptions.globalSaveRecord !== undefined && config.dataField !== undefined)
			{
				if(config.defaultRecordId != null) {
					form._dialogOptions.globalSaveRecord[config.dataField] = config.defaultRecordId;
				}
				else {
					form._dialogOptions.globalSaveRecord[config.dataField] = config.defaultValue;	
				}				
			}
			pickListItems.setSelectedItem(config.defaultValue);
		}
		
		//disable formitem if isEditable equals false
		if(config.isEditable == 'False' || config.isEditable == 'false')
		{
			formItem[0].disabled = true;
		}

		 return pickListItems;

	};	

	//  =========================================================
    //  Method: Date
    //  Desc:   Method that will attach a date to the target div. Any change 
    //			event will be will be handled through the "callBack" funciton. 
    //  Args:   config, targetDiv, callBack
    //  Return: datePicker
    //  ========================================================= 
	FormItems.prototype.Date = function(config, targetDiv, form, callBack)
	{
		var parentContainer = $p('<div class="componentContainer subSectionItem"' +
							'style="' + config.parentContainerStyle +  
							'"></div>').appendTo(targetDiv);

		if(config.required !== undefined)
		{
			if(config.required.toLowerCase() === 'true')
			{
				$p('<div class="req label" style="color:#db4437">' + config.parentLabel + '</div>').appendTo(parentContainer);
			}
		}
		else
		{
			    $p('<div class="label">' + config.parentLabel + '</div>').appendTo(parentContainer);
		}
	
	    var editContainer = $p('<div class="cubeComponentContainer"></div>').appendTo(parentContainer);

		var formItem = $p('<div style="'+ config.style +'"/>').appendTo(editContainer);
		var datePickerSpan = $p('<span/>').appendTo(formItem);
		var datePicker = $('<input Id="' + config.id + '"/>')
			.datepicker()
			.appendTo(datePickerSpan);

		if(config.disabled !== undefined)
		{
			if(config.disabled.toLowerCase() === 'true')
			{
				datePicker[0].disabled = true;
			}
		}	

		datePicker.on('change', function() 
		{			
			var dateValue = new Date($(datePicker).val());
			callBack(config.id, form, dateValue.getTime());
		});

		// datePicker.datePicker = datePicker;

		//set default date
		if(config.defaultValue != "")
		{
			var defaultDate =  new Date();
			var miliTime = parseInt(config.defaultValue);
			defaultDate.setTime(miliTime);
			datePicker.datepicker("setDate", defaultDate);
		}
	    else {
	      datePicker.datepicker("setDate", new Date());
	    }	

	    return datePicker;
	};

	//  =========================================================
    //  Method: Lookup
    //  Desc:   Method that will attach a Lookup to the target div.	Any change 
    //			event will be will be handled through the "callBack" funciton. 
    //  Args:   config, targetDiv, callBack
    //  Return: formItem
    //  ========================================================= 
	FormItems.prototype.Lookup = function(config, targetDiv, form, callBack)
	{
		var parentContainer = $p('<div class="' + config.parentClass + '" ' +
							'style="' + config.parentContainerStyle +  
							'"></div>').appendTo(targetDiv);

		if(config.defaultValue === null || config.defaultValue === undefined)
		{
			config.defaultValue = '';
		}

		if(config.placeHolder === null || config.placeHolder === undefined)
		{
			config.placeHolder = '';
		}

		var parentLabel;
		if(config.required !== undefined)
		{
			if(config.required.toLowerCase() === 'true')
			{
				//parentLabel = $p('<div class="label" style="' + config.parentLabelStyle +'">' + config.parentLabel + '</div>').appendTo(parentContainer);	
				parentLabel = $p('<div class="req label"  style="color:#db4437;font-size: 0.7em;font-weight: 600;height: 21px;">' + config.parentLabel + '</div>').appendTo(parentContainer);
			}
			else
			{
				parentLabel = $p('<div class="label" style="font-size: 0.7em;font-weight: 600;height: 21px;">' + config.parentLabel + '</div>').appendTo(parentContainer);	
			}
		}
		else
		{
			parentLabel = $p('<div class="label">' + config.parentLabel + '</div>').appendTo(parentContainer);	
		}

		var editContainer = $p('<div class="cubeComponentContainer"></div>').appendTo(parentContainer);
		var formItem = $p('<la-input id="' + config.id + 
									'" style="' + config.style + 
									'" type="' + config.laType +  
									'" placeholder="' + config.placeHolder +
									 '"></la-input>').appendTo(editContainer);	
		//set default value	
		if( config.defaultEmpName != null){
			formItem[0].value = config.defaultEmpName;
		}	
		else{	
				formItem[0].value = config.defaultValue;
		}
		if(form._dialogOptions.globalSaveRecord !== undefined && config.dataField !== undefined)
		{
			form._dialogOptions.globalSaveRecord[config.dataField] = config.defaultValue;
		}

		//Check if its editable
		if(config.isEditable !== undefined)
		{
			if(config.isEditable.toLowerCase() === 'false')
			{			
				formItem[0].disabled = true;
			}	
		}

		setTimeout(function()
		{
			var autoComplete = new SearchAutoComplete($(formItem),
			{
				mode: "CUSTOM:" + config.additionalConfig.id,
				maintainSelectedItem: true,
				selectedItemFormat: config.selectedItemFormat != null ? config.selectedItemFormat: "{Name}",
				config: config.additionalConfig,
				select: function(item)
				{					
					if(item != null)
					{
						callBack(config.id, form, item);
					}															
				}		
			});	

			if(config.customAdditionalWhereClause != null)
			{
				autoComplete.set('additionalWhereClause', config.customAdditionalWhereClause); 
			}

			formItem.SearchAutoComplete = autoComplete;
			if( config.defaultValue !== '')
			{
				var autoCompleteResultItem = config.selectedItemFormat != null ? config.selectedItemFormat: "Name";
				var autoCompleteResultObject = {};
				autoCompleteResultObject[autoCompleteResultItem] = config.defaultValue;
				formItem.autoCompleteResult = autoCompleteResultObject;
				autoComplete.cancel();
			}				
		}, 100);

		return formItem;
	};

	//  =========================================================
    //  Method: AutoCompleteResultsToGrid
    //  Desc:   Method that will attach a lookup with datagrid to the target div. Any change 
    //			event will be will be handled through the list of "callBack" function. 
    //  Args:   config, gridRecords, targetDiv, callBack
    //  Return: autolookup + grid
    //  ========================================================= 
	FormItems.prototype.AutoCompleteResultsToGrid = function(config, targetDiv, form, callBack, gridRecords)
	{
		var parentContainer = $p('<div class="' + config.parentClass + '" ' +
									'style="' + config.parentContainerStyle +  
									'"></div>').appendTo(targetDiv);

		var parentLabel;
		if(config.required !== undefined)
		{
			if(config.required.toLowerCase() === 'true')
			{
				parentLabel = $p('<div class="req label"  style="color:#db4437;font-size: 0.7em;font-weight: 600;height: 21px;">' + config.parentLabel + '</div>').appendTo(parentContainer);
			}
			else
			{
				parentLabel = $p('<div class="label" style="font-size: 0.7em;font-weight: 600;height: 21px;">' + config.parentLabel + '</div>').appendTo(parentContainer);	
			}
		}
		else
		{
			parentLabel = $p('<div class="label">' + config.parentLabel + '</div>').appendTo(parentContainer);	
		}
		var editContainer = $p('<div class="cubeComponentContainer"></div>').appendTo(parentContainer);
		var formItem = $p('<la-input id="' + config.id + 
									'" style="' + config.style + 
									'" type="' + config.laType +  
									'" placeholder="' + config.placeHolder +
									 '"></la-input>').appendTo(editContainer);	
									 
		setTimeout(function()
		{
			var autoComplete = new SearchAutoComplete($(formItem),
			{
				mode: "CUSTOM:" + config.additionalConfig.id,
				maintainSelectedItem: false,
				selectedItemFormat: config.selectedItemFormat != null ? config.selectedItemFormat: "{Name}",
				config: config.additionalConfig,
				customFilterFunction: function(e)
				{
					var length = dataGrid.dataProvider.getLength();
					if (length === 0)
					{
						return true;
					}
					else
					{
						for(i = 0; i < length; i++)
						{
							if (dataGrid.dataProvider.getItemById(e.Id) == undefined) 
							{
								return true;
							}
						}
					}
				},
				select: function(item)
				{	
					
					var gridItem = {
										id  : item.Id,
										name: item.Name
								   };
					
					if(dataGrid.dataProvider.getItemById(item.Id) === undefined)
					{
						dataGrid.addItems(gridItem); 
					}
					
					dataGrid.grid.resizeCanvas();
					callBack(config.id, form, dataGrid.dataProvider.getItems());						
				}		
			});	

			if(config.customAdditionalWhereClause != null)
			{
				autoComplete.set('additionalWhereClause', config.customAdditionalWhereClause); 
			}			

		}, 1000);								 
		

		function getColumns()
		{
			var columns = []; 

			if(config.gridColumnList.length > 0)
			{
			  columns.push({
							id: "delete",
							field: "dummy",
							name: "",
							fieldType: "BOOLEAN",
							width: 25,
							maxWidth: 25,
							minWidth: 25,
							sortable: false,
							formatter: "deleteFormatter"
						 });
									
			  $.each(config.gridColumnList, function(index, item)
			  {
				  columns.push({
								 id        : item.gridColumnConfig.Id,
								 field     : item.gridColumnConfig.DataField,
								 name      : item.gridColumnConfig.Label,
								 sortable  : true,
								 editable  : true,
								 fieldType : item.gridColumnConfig.FieldType,
								 minWidth  : 200
							  });
			  });
			}
			return columns;
		}

		var gridContainer = $('<div style="height:125px" />').appendTo(editContainer);
		var dataGrid 	  = new ACEDataGrid(gridContainer,
								{ 
									explicitInitialization: true, 
									sortNullsLast: true,
									forceFitColumns: true,
									autoHeight:true,
									headerRowHeight: 5,
									maxNumberOfRows:4,
								});
					  
					  
		dataGrid.setColumns(getColumns(),{
			"deleteFormatter": new ItemRenderer(function(row, cell, value, columnDef, dataContext)
			{  
				return '<div data-type="deleteButton" class="deleteButton" title="Remove">X</div>'			
			})
		});
		
		setTimeout(function(){
			dataGrid.grid.init();
		},100);
		
		dataGrid.on("itemClick", function(e)
		{
			deleteGridItem(e, 'contactGrid');
		});

		function deleteGridItem(e, grid)
		{	
			var target = $(e.event.target);
			var dataType = target.attr("data-type");

			if(dataType === "deleteButton")
			{
				dataGrid.dataProvider.deleteItem(e.data.id);
				callBack(config.id, form, dataGrid.dataProvider.getItems());			
			}
		}
		
		return dataGrid;
	};


	//  =========================================================
    //  Method: AceDataGrid
    //  Desc:   Method that will attach a datagrid to the target div. Any change 
    //			event will be will be handled through the list of "callBack" function. 
    //  Args:   config, gridRecords, targetDiv, callBack
    //  Return: datePicker
    //  ========================================================= 
	FormItems.prototype.AceDataGrid = function(config, targetDiv, form, callBack, gridRecords)
	{
		function getColumns()
      	{
      		var columns = []; 

      		if(config.gridColumnList.length > 0)
      		{	
				if(config.selector)
				{
					columns.push({ 
									id: "checkBox",
									name: "", 
									field: ACEDataGrid.CUSTOM_SELECTED_PROPERTY ,
									formatter: "CheckBoxSelectorItemRenderer", 
									maxWidth: 30
								});

				}

      			$.each(config.gridColumnList, function(index, item)
            	{
					if(item.gridColumnConfig.FieldType === "CHECKBOX")
					{
						columns.push({
										id        : item.gridColumnConfig.Id,
										field     : item.gridColumnConfig.DataField,
										name      : item.gridColumnConfig.Label,
										sortable  : true,
										editable  : true,
										fieldType : "STRING",
										formatter : "checBoxFormatter"
									});   
					}
					else
					{
						columns.push({
										id        : item.gridColumnConfig.Id,
										field     : item.gridColumnConfig.DataField,
										name      : item.gridColumnConfig.Label,
										sortable  : true,
										editable  : true,
										fieldType : "STRING",
										editor    : Slick.Editors.Text
						 });
					}

            	});
      		}
      		return columns;
      	}

		var dataGrid = new ACEDataGrid(targetDiv,{   
													maxNumberOfRows:15, 
													forceFitColumns: true, 
													explicitInitialization: true,
													editable: false,																					
													headerRowHeight: 30,
													autoHeight:true,
													autoEdit:true,
													multiSelect:false,
													enableCellNavigation: true,
													allowRowSelection: true
												});       
	
		dataGrid.setColumns(getColumns(),{
			"checBoxFormatter": new ItemRenderer(function(row, cell, value, columnDef, dataContext)
			{  				
				if(value)
				{
					return  '<div class="cubeComponentContainer"><label class="control checkbox"><input type="checkbox" checked><span class="control-indicator" style="position: relative;"></span></label></div>';
				}
				else
				{
					return '<div class="cubeComponentContainer"><label class="control checkbox"><input type="checkbox"><span class="control-indicator" style="position: relative;"></span></label></div>';
				}
			
			})
		});	
		dataGrid.grid.init();	
		
   			
		setTimeout(function(){	
			$.each(gridRecords, function(index, item)
			{
				delete item.attributes;
				item.id = item.Id;
			});
	
		   dataGrid.addItems(gridRecords);			
		   dataGrid.grid.resizeCanvas();	
		},100);

		if(config.selector)
		{
			dataGrid.onRowSelectionChange(function(item){
				callBack(config.id, form, item.data);
			});
		}
		else
		{
			dataGrid.grid.onClick.subscribe(function(e, args) {
				var cell = dataGrid.grid.getCellFromEvent(e);
				var item = dataGrid.grid.getDataItem(cell.row);
				callBack(config.id, form, item);
			});
		}

		return dataGrid;
	};

	$.extend(true, window,
	{
		"FormFactory":
		{
			"FormItems": new FormItems()
		}
	});
//# sourceURL=FormItems.js
})(jQuery);
