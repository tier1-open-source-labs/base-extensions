//=============================================================================
/**
 * Name: Form.js
 * Description: 
 *
 * Confidential & Proprietary, 2015 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1CRM.
 */
//=============================================================================


(function($)
{	
	var _this;
	var BASE_OPTIONS = 
	{	
		target: null,
		formItems: {},		
		formItemsConfig: [],
		requiredFields: {},
		grids: []
	}
	
	function Form()
	{	

	}

	//  =========================================================
    //  Method: init
    //  Desc:   Initialize the form and copy the optionns to dialogOptions
    //			 
    //  Args:   options
    //  Return: 
    //  ========================================================= 
	Form.prototype.init = function(options)
	{

		if(options !== undefined)
        {
        	this._dialogOptions = $.extend(true, {}, BASE_OPTIONS, options);
        }
        _this = this;
	};

	//  =========================================================
    //  Method: reset
    //  Desc:   Clear up any instances of acedatagrid or any other form element
    //			 
    //  Args:   
    //  Return: 
    //  ========================================================= 
	Form.prototype.reset = function()
	{	
		if(this._dialogOptions.formItems !== null || !$.isEmptyObject(this._dialogOptions.formItems))
		{
			for (var formObj in this._dialogOptions.formItems) {
				
				//destroy instance acedatagrid 
				if(this._dialogOptions.formItems[formObj]['formItem'].hasOwnProperty('grid'))
				{
					this._dialogOptions.formItems[formObj]['formItem'].destroy();
				}
			}
		
		}
	};

	//  =========================================================
    //  Method: createChildren
    //  Desc:  Create the form items
    //			 
    //  Args:   
    //  Return: 
    //  ========================================================= 
	Form.prototype.createChildren = function()
	{
		var generateHTMLQueue = [];

		this._dialogOptions.formItemsConfig.forEach(function(formItem){
			
			var config     = formItem.formItemsConfig;
			var formObject = { 
								config    : config, 
								formItem  : null
					  		 }

			//add default value
			if(_this._dialogOptions.defaultValues !== undefined)
			{
				if(_this._dialogOptions.defaultValues[config.id] !== undefined)
				{
					config.defaultValue = _this._dialogOptions.defaultValues[config.id];
				}
			}
			
			generateHTMLQueue.push
			({
				method    : FormFactory.FormItems[config.type],
				params    : [ config, 
						      $(config.target), 
						  	 _this, 
						  	 _this._dialogOptions.callBackList[config.callBack], 
							   _this._dialogOptions.records[config.gridRecords]
							],
				formObject: formObject		  
			});
														 	
		});

		processGenerateHTMLQueue();

		function processGenerateHTMLQueue() 
		{
			if (generateHTMLQueue.length > 0)
			{
				var methodToCall = generateHTMLQueue.shift();
				var params       = methodToCall.params;
				var formObject   = methodToCall.formObject;
				methodToCall     = methodToCall.method;			

				generateHTMLTimeout = setTimeout(function()
				{
					formObject.formItem = methodToCall.apply(this, params);
					if(formObject.config.required !== undefined)
					{
						if(formObject.config.required.toLowerCase() === 'true')
						{
							_this._dialogOptions.requiredFields[formObject.config.id] = {
																							config  : formObject.config, 
																							formItem: formObject.formItem
																						};
						}
					}

					if(formObject.config.type === 'AceDataGrid' || formObject.config.type === 'AutoCompleteResultsToGrid')
					{
						_this._dialogOptions.grids.push(formObject.formItem);
					}

					_this._dialogOptions.formItems[formObject.config.id] = formObject;		   

					processGenerateHTMLQueue();
				}, 15);	
			}
		}	
	};

	
	//  =========================================================
    //  Method: validateRequiredFields
    //  Desc:  Check if the required fields are properly filled in
    //			 
    //  Args:   
    //  Return: requiredErrorFields
    //  ========================================================= 
	Form.prototype.validateRequiredFields = function()
	{
		var requiredErrorFields = [];		
		if(!$.isEmptyObject(this._dialogOptions.requiredFields))
		{
			for (var formObj in this._dialogOptions.requiredFields) {

				var config   = this._dialogOptions.requiredFields[formObj]['config'];
				var formItem = this._dialogOptions.requiredFields[formObj]['formItem'];

				if(config.type === 'CheckboxListDropDown')
				{
					if(formItem._checkboxList.selectedItems.length === 0)
					{
						requiredErrorFields.push(config.parentLabel);
					}
				}
				else if(config.type === 'PickList')
				{
					if(formItem._selectedItem[0].innerText.toLowerCase().indexOf('select') !== -1)
					{
						requiredErrorFields.push(config.parentLabel);
					}
				}
				else if(config.type === 'EmailDomain')
				{
					if(formItem[0].children[0].children[1].value === '' || !formItem[0].children[0].children[1].value.replace(/\s/g, '').length)
					{
						requiredErrorFields.push(config.parentLabel);
					}
				}
				else if(config.type === 'PhoneNumber')
				{
					if(formItem[0].children[1].children[1].children[0].value === '' ||
					formItem[0].children[1].children[2].children[0].value === '')
					{
						requiredErrorFields.push(config.parentLabel);
					}
				}
				else if(config.type === 'AutoCompleteResultsToGrid')
				{
					if(formItem.dataProvider.getLength() === 0)
					{
						requiredErrorFields.push(config.parentLabel);
					}
				}
				else if(config.type === 'Lookup')
				{
					// If statement added by Vamshi in order to bypass the null value for defaultvalue config 
					if(config.defaultValue == null || config.defaultValue === '' || formItem[0].value === '')						
					{
 
						if(formItem.autoCompleteResult == null)
						{
							requiredErrorFields.push(config.parentLabel);	
						}
						else
						{
							var autoCompleteResultItem = config.selectedItemFormat != null ? config.selectedItemFormat: "Name";
							var formValue = '';
							if(config.selectedItemFormat != null )
							{
								//autoCompleteResultItem =  StringUtil.replaceAll(/{|}/, '' ,config.selectedItemFormat);
								autoCompleteResultItem = config.selectedItemFormat.replace('}','').replace('{','');
								if(autoCompleteResultItem.indexOf('.') != -1)
								{
									autoCompleteResultItem = autoCompleteResultItem.split('.');
									formValue =	formItem.autoCompleteResult[autoCompleteResultItem[0]][autoCompleteResultItem[1]];
								}						
							}
							else
							{
								formValue =	formItem.autoCompleteResult[autoCompleteResultItem];
							}
		
							if(formItem[0].value !== formValue)					
							{
								requiredErrorFields.push(config.parentLabel);	
							}					
						}
					}
				}
				else
				{
					if(formItem[0].value === '' || !formItem[0].value.replace(/\s/g, '').length)
					{					
						requiredErrorFields.push(config.parentLabel);
					}
				}
			}	
		}
		return requiredErrorFields;
	};	

	//  =========================================================
    //  Method: getCallBackRelatedItems
    //  Desc:  Retrieve form elements that are related to each other
    //			 
    //  Args: formItemName
    //  Return: formRelatedItems
    //  ========================================================= 
	Form.prototype.getCallBackRelatedItems = function(formItemName)
	{
		var formRelatedItems = {}; 
		_this = this;

		var formItem = this._dialogOptions.formItems[formItemName];
		if(formItem.config.callBackRelatedItems !== undefined)
		{
			formItem.config.callBackRelatedItems.forEach(function(formItem){

				formRelatedItems[formItem] = _this._dialogOptions.formItems[formItem];
			});
		} 

		return formRelatedItems;
	};


	$.extend(true, window,
	{
		"FormFactory":
		{
			"Form": Form
		}
	});

//# sourceURL=Form.js	
})(jQuery);
