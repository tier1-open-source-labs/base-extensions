//=============================================================================
/**
 * Name: PostMergeUtilityJS.js
 * Description: Post Merge Utility UI for the merge button on Contact detail page
 *
 * Confidential & Proprietary, 2016 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1CRM.
 */
//=============================================================================
(function($)
{
	var sessionId = ACE.Salesforce.sessionId;
	var serverURL = ACE.Salesforce.serverUrl;
	serverURL = serverURL.substr(0,serverURL.indexOf("/services")) + "/";
	
	sforce.connection.sessionId = ACE.Salesforce.sessionId;
	
	//link the css file
	var cssResources;
	var result = sforce.apex.execute("PostMergeUtilityController", "getStaticResource", {resourceName : 'PostMergeUtilityCSS'}, 
	{ 
		onSuccess: function(result)
		{		
			cssResources  = serverURL + result[0];
			var cssId = 'PostMergeUtilityCSS.css';
			if (!document.getElementById(cssId) && cssResources != null)
			{
			    var head  = document.getElementsByTagName('head')[0];
			    var link  = document.createElement('link');
			    link.id   = cssId;
			    link.rel  = 'stylesheet';
			    link.type = 'text/css';
			    link.href = cssResources;
			    link.media = 'all';
			    head.appendChild(link);
			}
		}, 
		onFailure: function(error)
		{		
			error = 'No CSS file in the static resource : PostMergeUtilityCSS';
			showError(error);
		} 
	});
	
	var mergeUtilityBody = $p('<div class="mergeUtilityWrapper"/>').appendTo("body");
	
	var modal = $p('<div id="errorModal" class="modal" style="display: none;position: fixed;z-index: 2147483647;padding-top: 100px;left: 0;top: 0;vertical-align:middle;text-align: center;width: 100%;height: 100%;overflow: auto;background-color: rgb(255,255,255);background-color: rgba(255,255,255,0.4)">');
	var errorMessageContent = $p('<div id="errorMessage" class="modal-content" style="position: relative;margin: auto;height: auto;width: 80%;padding: 20px;-opacity: 0.5;color: black;font-weight: bold;z-index: 2147483647;border: 1px solid grey;-moz-border-radius: 10px;-webkit-border-radius: 10px;-moz-box-shadow: 0 0 5px grey;-webkit-box-shadow: 0px 0px 5px grey;-text-shadow: 1px 1px 1px white;box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19);-webkit-animation-name: animatetop;-webkit-animation-duration: 0.4s;animation-name: animatetop;animation-duration: 0.4s;vertical-align:middle;text-align: center; background-color: rgb(255,255,255);">');
	$(modal).append(errorMessageContent);
	$(mergeUtilityBody).append(modal);
	
	var headerPanel = $p('<div class="headerPanelContainer"/>').appendTo(mergeUtilityBody);
	$p('<div class="appTitle">Post Merge Utility</div>').appendTo(headerPanel);
	$p('<div class="brandingMessage">Powered by Tier1CRM</div>').appendTo(headerPanel);
	
	var topcloseButtonDiv = $p('<button class="closeFAB" id="hideShowBtn" title="Click to Close"/>').appendTo(headerPanel);
	$p('<div class="closeFABX"></div>').appendTo(topcloseButtonDiv);
	$(topcloseButtonDiv).on('mousedown', function() 
	{
		closeWindow();
	});
	
	var scrollContainer = $p('<div class="scrollContainer"/>').appendTo(mergeUtilityBody);
	var applicationContainer = $p('<div class="applicationContainer"/>').appendTo(scrollContainer);
	
	var contactName;
	var contactId;
	var contMeetingInfoList;
	var keyContInfoList;
	var collectionKeyContInfoList;
	var contAceListInfoList;
	var contInterestInfoList;
	var contInterestAPMInfoList;
	var contCoverageInfoList;
	var callReportAttendeeInfoList;
	var dealContactInfoList;
	
	var rolepicklistConfig;
	var lookUPConfig;
	
	var valueExist = false;
	var interestPage = false;
	var coveragePage = false;
	
	var tiggersEnabled = false;
	
	var result = sforce.apex.execute("PostMergeUtilityController", "getContactRelatedList", {contactId : ACEUtil.getURLParameter('ContactId')}, 
	{ 
		onSuccess: function(result)
		{		
			contRelatedListInfo = JSON.parse(result);
			
			//contact info
			contact = contRelatedListInfo.cont;
			contactName = contact.Name;
			contactId = contact.Id;
			
			tiggersEnabled = contRelatedListInfo.tiggersEnabled;

			//enable the triggers
			enableTriggers();
			
			// contact coverage role picklist
			rolepicklistConfig = contRelatedListInfo.rolepicklistConfig;
			
			// contact coverage product lookup
			lookUPConfig = contRelatedListInfo.lookUPConfig;
			
			//meeting info
			contMeetingInfoList = contRelatedListInfo.meetingInfo;
			if (contMeetingInfoList.length > 0)
			{
				valueExist = true;
			}
			
			//key contact info
			keyContInfoList = contRelatedListInfo.keyContactInfo;
			if (keyContInfoList.length > 0)
			{
				valueExist = true;
			}
			
			//collection key contact info
			collectionKeyContInfoList = contRelatedListInfo.collectionKeyContInfo;
			if (collectionKeyContInfoList.length > 0)
			{
				valueExist = true;
			}
			
			//aceList info
			contAceListInfoList = contRelatedListInfo.aceListInfo;
			if (contAceListInfoList.length > 0)
			{
				valueExist = true;
			}
			
			//callReport attendee info
			callReportAttendeeInfoList = contRelatedListInfo.callReportInfo;
			if (callReportAttendeeInfoList.length > 0)
			{
				valueExist = true;
			}
			
			//deal contact info
			dealContactInfoList = contRelatedListInfo.dealInfo;
			if (dealContactInfoList.length > 0)
			{
				valueExist = true;
			}
			
			//interst info
			contInterestInfoList = contRelatedListInfo.interestInfo;
			if (contInterestInfoList.length > 0)
			{
				interestPage = true;
			}
			
			//Preferences info
			contInterestAPMInfoList = contRelatedListInfo.interestAPMInfo;
			if (contInterestAPMInfoList.length > 0)
			{
				valueExist = true;
			}
			
			//contact coverage
			contCoverageInfoList = contRelatedListInfo.contCoverageInfo;
			if (contCoverageInfoList.length > 0)
			{
				coveragePage = true;
			}
			
			
			generateHTML();
		}, 
		onFailure: function(error)
		{
			showError(error);
		} 
	});

	
	
	//  ==================================================================
	//  Method: generateHTML
	//  Description: generate the whole UI
	//  ==================================================================
	function generateHTML()
	{	
		//print the contact name
		var headerSection = $p('<div class="section headerSection"/>').appendTo(applicationContainer);
		if (!valueExist && !coveragePage && !interestPage)
		{
			$p('<div class="nameContainer"><div class="contactName">'+'<a title="click to launch contact page" href="' + serverURL + contactId + '" target="_blank">' +  contactName + '</a>'+'</div><div class="appMessage">No duplicates</div></div>').appendTo(headerSection);
		}
		else
		{
			$p('<div class="nameContainer"><div class="contactName">'+'<a title="click to launch contact page" href="' + serverURL + contactId + '" target="_blank">' +  contactName + '</a>'+'</div><div class="appMessage">The following duplicates were created during the merge process</div></div>').appendTo(headerSection);
		}
		
		if (valueExist)
		{
			var preserveOnlySection = $p('<div class="section preserveOnlySection"/>').appendTo(applicationContainer);
			var subSection = $p('<div class="subSection"/>').appendTo(preserveOnlySection);
			
			//******print the meeting attendee infomation**********
			if (contMeetingInfoList != null && contMeetingInfoList != undefined && contMeetingInfoList.length > 0)
			{
				$p('<div class="sectionLabel">Meeting Attendees</div>').appendTo(subSection);
				var secgroup = $p('<div class="group"/>').appendTo(subSection);
				
				if (Array.isArray(contMeetingInfoList) == false)
				{
					$p('<div class="subLabel">' + contMeetingInfoList.name + '</div>').appendTo(secgroup);
					
					var contentTable = $p('<div class="contentTable"/>').appendTo(secgroup);
					var headerRow = $p('<div class="headerRow"/>').appendTo(contentTable);
					
					$p('<div class="radioColumn"/>').appendTo(headerRow);
					data = ['Attendee Name', 'Status ', 'Role', 'Booking Owner'];
					displayData('meetingH', headerRow, data, 'columnHeader');
					
					meetingAttendeeInfo = contMeetingInfoList.meetingAttendees;
					if (meetingAttendeeInfo != null && meetingAttendeeInfo != undefined)
					{
						if (Array.isArray(meetingAttendeeInfo))
						{
							for (var j = 0; j < meetingAttendeeInfo.length; j++)
							{
								var tableRow = $p('<div class="tableRow"/>').appendTo(contentTable);
								sel = '';
								if (j == 0)
								{
									sel = ' checked="checked"';
								}
								data = ['<a title="click to launch Meeting Attendee page" href="' + serverURL + meetingAttendeeInfo[j].Id + '" target="_blank">' +  meetingAttendeeInfo[j].Name + '</a>', meetingAttendeeInfo[j].T1C_AEM__Status__c, meetingAttendeeInfo[j].T1C_AEM__Role__c, meetingAttendeeInfo[j].T1C_AEM__Booking_Owner__r.Name];
								$p('<div class="radioColumn"><label class="control radio"><input name="meetingAttendees0" type="radio" value=' + meetingAttendeeInfo[j].Id + sel + ' data-type="dateType"><span class="control-indicator"></span></label></div>').appendTo(tableRow);
								displayData('meetingattendee0' + j, tableRow, data, 'column');
							}
						}
					}
				}
				else
				{
					for (var i = 0; i < contMeetingInfoList.length; i++)
					{
						$p('<div class="subLabel">' + contMeetingInfoList[i].name  + '</div>').appendTo(secgroup);
						
						var contentTable = $p('<div class="contentTable"/>').appendTo(secgroup);
						var headerRow = $p('<div class="headerRow"/>').appendTo(contentTable);
						
						$p('<div class="radioColumn"/>').appendTo(headerRow);
						data = ['Attendee Name', 'Status ', 'Role', 'Booking Owner'];
						displayData('meetingH', headerRow, data, 'columnHeader');
						
						meetingAttendeeInfo = contMeetingInfoList[i].meetingAttendees;
						if (meetingAttendeeInfo != null && meetingAttendeeInfo != undefined)
						{
							if (Array.isArray(meetingAttendeeInfo))
							{
								for (var j = 0; j < meetingAttendeeInfo.length; j++)
								{
									var tableRow = $p('<div class="tableRow"/>').appendTo(contentTable);
									sel = '';
									if (j == 0)
									{
										sel = ' checked="checked"';
									}
									data = ['<a title="click to launch Meeting Attendee page" href="' + serverURL + meetingAttendeeInfo[j].Id + '" target="_blank">' +  meetingAttendeeInfo[j].Name + '</a>', meetingAttendeeInfo[j].T1C_AEM__Status__c, meetingAttendeeInfo[j].T1C_AEM__Role__c, meetingAttendeeInfo[j].T1C_AEM__Booking_Owner__r.Name];
									$p('<div class="radioColumn"><label class="control radio"><input name="meetingAttendees' + i + '" type="radio" value=' + meetingAttendeeInfo[j].Id + sel + ' data-type="dateType"><span class="control-indicator"></span></label></div>').appendTo(tableRow);
									displayData('meetingattendee' + i + j, tableRow, data, 'column');
								}
							}
						}
					}
				}
			}
			
			
			//******print the key contact infomation**********
			if (keyContInfoList != null && keyContInfoList != undefined && keyContInfoList.length > 0)
			{
				$p('<div class="sectionLabel">Key Contacts</div>').appendTo(subSection);
				var secgroup = $p('<div class="group"/>').appendTo(subSection);
				
				if (Array.isArray(keyContInfoList) == false)
				{
					$p('<div class="subLabel">' + keyContInfoList.name + '</div>').appendTo(secgroup);
					
					var contentTable = $p('<div class="contentTable"/>').appendTo(secgroup);
					var headerRow = $p('<div class="headerRow"/>').appendTo(contentTable);
					
					$p('<div class="radioColumn"/>').appendTo(headerRow);
					data = ['KP Name', 'KP Role', 'Title'];
					displayData('keycontactH', headerRow, data, 'columnHeader');
					
					keyContactsInfo = keyContInfoList.keyContacts;
					if (keyContactsInfo != null && keyContactsInfo != undefined)
					{
						if (Array.isArray(keyContactsInfo))
						{
							for (var j = 0; j < keyContactsInfo.length; j++)
							{
								var tableRow = $p('<div class="tableRow"/>').appendTo(contentTable);
								sel = '';
								if (j == 0)
								{
									sel = ' checked="checked"';
								}
								data = ['<a title="click to launch Key Contact page" href="' + serverURL + keyContactsInfo[j].Id + '" target="_blank">' +  keyContactsInfo[j].Name + '</a>', keyContactsInfo[j].T1C_AEM__Role__c, keyContactsInfo[j].T1C_AEM__Title__c];
								$p('<div class="radioColumn"><label class="control radio"><input name="keyContacts0" type="radio" value=' + keyContactsInfo[j].Id + sel + ' data-type="dateType"><span class="control-indicator"></span></label></div>').appendTo(tableRow);
								displayData('keycontact0' + j, tableRow, data, 'column');
							}
						}
					}
				}
				else
				{
					for (var i = 0; i < keyContInfoList.length; i++)
					{
						$p('<div class="subLabel">' + keyContInfoList[i].name  + '</div>').appendTo(secgroup);
						
						var contentTable = $p('<div class="contentTable"/>').appendTo(secgroup);
						var headerRow = $p('<div class="headerRow"/>').appendTo(contentTable);
						
						$p('<div class="radioColumn"/>').appendTo(headerRow);
						data = ['KP Name', 'KP Role', 'Title'];
						displayData('keycontactH', headerRow, data, 'columnHeader');
						
						keyContactsInfo = keyContInfoList[i].keyContacts;
						if (keyContactsInfo != null && keyContactsInfo != undefined)
						{
							if (Array.isArray(keyContactsInfo))
							{
								for (var j = 0; j < keyContactsInfo.length; j++)
								{
									var tableRow = $p('<div class="tableRow"/>').appendTo(contentTable);
									sel = '';
									if (j == 0)
									{
										sel = ' checked="checked"';
									}
									data = ['<a title="click to launch Key Contact page" href="' + serverURL + keyContactsInfo[j].Id + '" target="_blank">' +  keyContactsInfo[j].Name + '</a>', keyContactsInfo[j].T1C_AEM__Role__c, keyContactsInfo[j].T1C_AEM__Title__c];
									$p('<div class="radioColumn"><label class="control radio"><input name="keyContacts' + i + '" type="radio" value=' + keyContactsInfo[j].Id + sel + ' data-type="dateType"><span class="control-indicator"></span></label></div>').appendTo(tableRow);
									displayData('keycontact' + i + j, tableRow, data, 'column');
								}
							}
						}
					}
				}
			}
			
			
			//******print the collection key contact infomation**********
			if (collectionKeyContInfoList != null && collectionKeyContInfoList != undefined && collectionKeyContInfoList.length > 0)
			{
				$p('<div class="sectionLabel">Collection Key Contacts</div>').appendTo(subSection);
				var secgroup = $p('<div class="group"/>').appendTo(subSection);
				
				if (Array.isArray(collectionKeyContInfoList) == false)
				{
					$p('<div class="subLabel">' + collectionKeyContInfoList.name + '</div>').appendTo(secgroup);
					
					var contentTable = $p('<div class="contentTable"/>').appendTo(secgroup);
					var headerRow = $p('<div class="headerRow"/>').appendTo(contentTable);
					
					$p('<div class="radioColumn"/>').appendTo(headerRow);
					data = ['Collection KP Name', 'Collection KP Role', 'Title'];
					displayData('collectionkeycontactH', headerRow, data, 'columnHeader');
				
					colkeyContactsInfo = collectionKeyContInfoList.collectionKeyContacts;
					if (colkeyContactsInfo != null && colkeyContactsInfo != undefined)
					{
						if (Array.isArray(colkeyContactsInfo))
						{
							for (var j = 0; j < colkeyContactsInfo.length; j++)
							{
								var tableRow = $p('<div class="tableRow"/>').appendTo(contentTable);
								sel = '';
								if (j == 0)
								{
									sel = ' checked="checked"';
								}
								data = ['<a title="click to launch Collection Key Contact page" href="' + serverURL + colkeyContactsInfo[j].Id + '" target="_blank">' +  colkeyContactsInfo[j].Name + '</a>', colkeyContactsInfo[j].T1C_AEM__Role__c, colkeyContactsInfo[j].T1C_AEM__Title__c];
								$p('<div class="radioColumn"><label class="control radio"><input name="colkeyContacts0" type="radio" value=' + colkeyContactsInfo[j].Id + sel + ' data-type="dateType"><span class="control-indicator"></span></label></div>').appendTo(tableRow);
								displayData('collectionkeycontact0' + j, tableRow, data, 'column');
							}
						}
					}
				}
				else
				{
					for (var i = 0; i < collectionKeyContInfoList.length; i++)
					{
						$p('<div class="subLabel">' + collectionKeyContInfoList[i].name  + '</div>').appendTo(secgroup);
						
						var contentTable = $p('<div class="contentTable"/>').appendTo(secgroup);
						var headerRow = $p('<div class="headerRow"/>').appendTo(contentTable);
						
						$p('<div class="radioColumn"/>').appendTo(headerRow);
						data = ['Collection KP Name', 'Collection KP Role', 'Title'];
						displayData('collectionkeycontactH', headerRow, data, 'columnHeader');
						
						colkeyContactsInfo = collectionKeyContInfoList[i].collectionKeyContacts;
						if (colkeyContactsInfo != null && colkeyContactsInfo != undefined)
						{
							if (Array.isArray(colkeyContactsInfo))
							{
								for (var j = 0; j < colkeyContactsInfo.length; j++)
								{
									var tableRow = $p('<div class="tableRow"/>').appendTo(contentTable);
									sel = '';
									if (j == 0)
									{
										sel = ' checked="checked"';
									}
									data = ['<a title="click to launch Collection Key Contact page" href="' + serverURL + colkeyContactsInfo[j].Id + '" target="_blank">' +  colkeyContactsInfo[j].Name + '</a>', colkeyContactsInfo[j].T1C_AEM__Role__c, colkeyContactsInfo[j].T1C_AEM__Title__c];
									$p('<div class="radioColumn"><label class="control radio"><input name="colkeyContacts' + i + '" type="radio" value=' + colkeyContactsInfo[j].Id + sel + ' data-type="dateType"><span class="control-indicator"></span></label></div>').appendTo(tableRow);
									displayData('collectionkeycontact' + i + j, tableRow, data, 'column');
								}
							}
						}
					}
				}
			}
			
			
			//******print the ace list entry infomation**********
			if (contAceListInfoList != null && contAceListInfoList != undefined && contAceListInfoList.length > 0)
			{
				$p('<div class="sectionLabel">Ace List Entries</div>').appendTo(subSection);
				var secgroup = $p('<div class="group"/>').appendTo(subSection);
				
				if (Array.isArray(contAceListInfoList) == false)
				{
					$p('<div class="subLabel">' + contAceListInfoList.name + '</div>').appendTo(secgroup);
					
					var contentTable = $p('<div class="contentTable"/>').appendTo(secgroup);
					var headerRow = $p('<div class="headerRow"/>').appendTo(contentTable);
					
					$p('<div class="radioColumn"/>').appendTo(headerRow);
					data = ['Name', 'List Owner'];
					displayData('acelistentryH', headerRow, data, 'columnHeader');
					
					aceListEntryInfo = contAceListInfoList.aceListEntries;
					if (aceListEntryInfo != null && aceListEntryInfo != undefined)
					{
						if (Array.isArray(aceListEntryInfo))
						{
							for (var j = 0; j < aceListEntryInfo.length; j++)
							{
								var tableRow = $p('<div class="tableRow"/>').appendTo(contentTable);
								sel = '';
								if (j == 0)
								{
									sel = ' checked="checked"';
								}
								data = ['<a title="click to launch Ace List Entry page" href="' + serverURL + aceListEntryInfo[j].Id + '" target="_blank">' +  aceListEntryInfo[j].Name + '</a>', aceListEntryInfo[j].T1C_Base__Ace_List__r.Owner.Name];
								$p('<div class="radioColumn"><label class="control radio"><input name="listEntry0" type="radio" value=' + aceListEntryInfo[j].Id + sel + ' data-type="dateType"><span class="control-indicator"></span></label></div>').appendTo(tableRow);
								displayData('acelistentry0' + j, tableRow, data, 'column');
							}
						}
					}
				}
				else
				{
					for (var i = 0; i < contAceListInfoList.length; i++)
					{
						$p('<div class="subLabel">' + contAceListInfoList[i].name  + '</div>').appendTo(secgroup);
						
						var contentTable = $p('<div class="contentTable"/>').appendTo(secgroup);
						var headerRow = $p('<div class="headerRow"/>').appendTo(contentTable);
						
						$p('<div class="radioColumn"/>').appendTo(headerRow);
						data = ['Name', 'List Owner'];
						displayData('acelistentryH', headerRow, data, 'columnHeader');
						
						aceListEntryInfo = contAceListInfoList[i].aceListEntries;
						if (aceListEntryInfo != null && aceListEntryInfo != undefined)
						{
							if (Array.isArray(aceListEntryInfo))
							{
								for (var j = 0; j < aceListEntryInfo.length; j++)
								{
									var tableRow = $p('<div class="tableRow"/>').appendTo(contentTable);
									sel = '';
									if (j == 0)
									{
										sel = ' checked="checked"';
									}
									data = ['<a title="click to launch Ace List Entry page" href="' + serverURL + aceListEntryInfo[j].Id + '" target="_blank">' +  aceListEntryInfo[j].Name + '</a>', aceListEntryInfo[j].T1C_Base__Ace_List__r.Owner.Name];
									$p('<div class="radioColumn"><label class="control radio"><input name="listEntry' + i + '" type="radio" value=' + aceListEntryInfo[j].Id + sel + ' data-type="dateType"><span class="control-indicator"></span></label></div>').appendTo(tableRow);
									displayData('acelistentry' + i + j, tableRow, data, 'column');
								}
							}
						}
					}
				}
			}
			
			
			//******print the call report attendee infomation**********
			if (callReportAttendeeInfoList != null && callReportAttendeeInfoList != undefined && callReportAttendeeInfoList.length > 0)
			{
				$p('<div class="sectionLabel">CallReport Attendees</div>').appendTo(subSection);
				var secgroup = $p('<div class="group"/>').appendTo(subSection);
				
				if (Array.isArray(callReportAttendeeInfoList) == false)
				{
					$p('<div class="subLabel">' + callReportAttendeeInfoList.name + '</div>').appendTo(secgroup);
					
					var contentTable = $p('<div class="contentTable"/>').appendTo(secgroup);
					var headerRow = $p('<div class="headerRow"/>').appendTo(contentTable);
					
					$p('<div class="radioColumn"/>').appendTo(headerRow);
					data = ['Attendee Name', 'Subject'];
					displayData('callReportattendeeH', headerRow, data, 'columnHeader');
					
					callReportInfo = callReportAttendeeInfoList.crAttendees;
					if (callReportInfo != null && callReportInfo != undefined)
					{
						if (Array.isArray(callReportInfo))
						{
							for (var j = 0; j < callReportInfo.length; j++)
							{
								var tableRow = $p('<div class="tableRow"/>').appendTo(contentTable);
								sel = '';
								if (j == 0)
								{
									sel = ' checked="checked"';
								}
								data = ['<a title="click to CallReport Attendee page" href="' + serverURL + callReportInfo[j].Id + '" target="_blank">' +  callReportInfo[j].T1C_Base__Contact__r.Name + '</a>', callReportInfo[j].T1C_Base__Call_Report__r.T1C_Base__Subject_Meeting_Objectives__c];
								$p('<div class="radioColumn"><label class="control radio"><input name="callReport0" type="radio" value=' + callReportInfo[j].Id + sel + ' data-type="dateType"><span class="control-indicator"></span></label></div>').appendTo(tableRow);
								displayData('callReportattendee0' + j, tableRow, data, 'column');
							}
						}
					}
				}
				else
				{
					for (var i = 0; i < callReportAttendeeInfoList.length; i++)
					{
						$p('<div class="subLabel">' + callReportAttendeeInfoList[i].name  + '</div>').appendTo(secgroup);
						
						var contentTable = $p('<div class="contentTable"/>').appendTo(secgroup);
						var headerRow = $p('<div class="headerRow"/>').appendTo(contentTable);
						
						$p('<div class="radioColumn"/>').appendTo(headerRow);
						data = ['Attendee Name', 'Subject'];
						displayData('callReportattendeeH', headerRow, data, 'columnHeader');
						
						callReportInfo = callReportAttendeeInfoList[i].crAttendees;
						if (callReportInfo != null && callReportInfo != undefined)
						{
							if (Array.isArray(callReportInfo))
							{
								for (var j = 0; j < callReportInfo.length; j++)
								{
									var tableRow = $p('<div class="tableRow"/>').appendTo(contentTable);
									sel = '';
									if (j == 0)
									{
										sel = ' checked="checked"';
									}
									data = ['<a title="click to launch CallReport Attendee" href="' + serverURL + callReportInfo[j].Id + '" target="_blank">' +  callReportInfo[j].T1C_Base__Contact__r.Name + '</a>', callReportInfo[j].T1C_Base__Call_Report__r.T1C_Base__Subject_Meeting_Objectives__c];
									$p('<div class="radioColumn"><label class="control radio"><input name="callReport' + i + '" type="radio" value=' + callReportInfo[j].Id + sel + ' data-type="dateType"><span class="control-indicator"></span></label></div>').appendTo(tableRow);
									displayData('callReportattendee' + i + j, tableRow, data, 'column');
								}
							}
						}
					}
				}
			}
			
			
			//******print the deal contact infomation**********
			if (dealContactInfoList != null && dealContactInfoList != undefined && dealContactInfoList.length > 0)
			{
				$p('<div class="sectionLabel">Deal Contacts</div>').appendTo(subSection);
				var secgroup = $p('<div class="group"/>').appendTo(subSection);
				
				if (Array.isArray(dealContactInfoList) == false)
				{
					$p('<div class="subLabel">' + dealContactInfoList.name + '</div>').appendTo(secgroup);
					
					var contentTable = $p('<div class="contentTable"/>').appendTo(secgroup);
					var headerRow = $p('<div class="headerRow"/>').appendTo(contentTable);
					
					$p('<div class="radioColumn"/>').appendTo(headerRow);
					data = ['Contact Name', 'Role'];
					displayData('dealcontactsH', headerRow, data, 'columnHeader');
					
					dealContInfo = dealContactInfoList.dealContacts;
					if (dealContInfo != null && dealContInfo != undefined)
					{
						if (Array.isArray(dealContInfo))
						{
							for (var j = 0; j < dealContInfo.length; j++)
							{
								var tableRow = $p('<div class="tableRow"/>').appendTo(contentTable);
								sel = '';
								if (j == 0)
								{
									sel = ' checked="checked"';
								}
								data = ['<a title="click to Deal Contact page" href="' + serverURL + dealContInfo[j].Id + '" target="_blank">' +  dealContInfo[j].T1C_Base__Contact__r.Name + '</a>', dealContInfo[j].T1C_Base__Role__c];
								$p('<div class="radioColumn"><label class="control radio"><input name="deal0" type="radio" value=' + dealContInfo[j].Id + sel + ' data-type="dateType"><span class="control-indicator"></span></label></div>').appendTo(tableRow);
								displayData('dealcontacts0' + j, tableRow, data, 'column');
							}
						}
					}
				}
				else
				{
					for (var i = 0; i < dealContactInfoList.length; i++)
					{
						$p('<div class="subLabel">' + dealContactInfoList[i].name  + '</div>').appendTo(secgroup);
						
						var contentTable = $p('<div class="contentTable"/>').appendTo(secgroup);
						var headerRow = $p('<div class="headerRow"/>').appendTo(contentTable);
						
						$p('<div class="radioColumn"/>').appendTo(headerRow);
						data = ['Contact Name', 'Role'];
						displayData('dealcontactsH', headerRow, data, 'columnHeader');
						
						dealContInfo = dealContactInfoList[i].dealContacts;
						if (dealContInfo != null && dealContInfo != undefined)
						{
							if (Array.isArray(dealContInfo))
							{
								for (var j = 0; j < dealContInfo.length; j++)
								{
									var tableRow = $p('<div class="tableRow"/>').appendTo(contentTable);
									sel = '';
									if (j == 0)
									{
										sel = ' checked="checked"';
									}
									data = ['<a title="click to launch Deal Contacts" href="' + serverURL + dealContInfo[j].Id + '" target="_blank">' +  dealContInfo[j].T1C_Base__Contact__r.Name + '</a>', dealContInfo[j].T1C_Base__Role__c];
									$p('<div class="radioColumn"><label class="control radio"><input name="deal' + i + '" type="radio" value=' + dealContInfo[j].Id + sel + ' data-type="dateType"><span class="control-indicator"></span></label></div>').appendTo(tableRow);
									displayData('dealcontacts' + i + j, tableRow, data, 'column');
								}
							}
						}
					}
				}
			}
			
			//******print the interest APM infomation**********
			if (contInterestAPMInfoList != null && contInterestAPMInfoList != undefined && contInterestAPMInfoList.length > 0)
			{
				$p('<div class="sectionLabel">Preferences</div>').appendTo(subSection);
				secgroup = $p('<div class="group"/>').appendTo(subSection);
				
				////////APM
				if (Array.isArray(contInterestAPMInfoList) == false)
				{
					$p('<div class="subLabel">' + contInterestAPMInfoList.name + '</div>').appendTo(secgroup);
					
					var contentTable = $p('<div class="contentTable"/>').appendTo(secgroup);
					var headerRow = $p('<div class="headerRow"/>').appendTo(contentTable);
					
					$p('<div class="radioColumn"/>').appendTo(headerRow);
					data = ['Name', 'Last Discussed', 'Reason', 'Source', 'Notes'];
					displayData('preferencesH', headerRow, data, 'columnHeader');
					
					interestReasonResearchInfo = contInterestAPMInfoList.interestWithReasonResearch;
					if (interestReasonResearchInfo != null && interestReasonResearchInfo != undefined)
					{
						if (Array.isArray(interestReasonResearchInfo))
						{
							for (var j = 0; j < interestReasonResearchInfo.length; j++)
							{
								var tableRow = $p('<div class="tableRow"/>').appendTo(contentTable);
								sel = '';
								if (j == 0)
								{
									sel = ' checked="checked"';
								}
								data = ['<a title="click to launch Preference page" href="' + serverURL + interestReasonResearchInfo[j].Id + '" target="_blank">' +  contactName + '</a>', interestReasonResearchInfo[j].T1C_Base__Last_Discussed__c, interestReasonResearchInfo[j].T1C_Base__Reason__c, interestReasonResearchInfo[j].T1C_Base__Source__c, interestReasonResearchInfo[j].T1C_Base__Note__c];
								$p('<div class="radioColumn"><label class="control radio"><input name="reasonR0" type="radio" value=' + interestReasonResearchInfo[j].Id + sel + ' data-type="dateType"><span class="control-indicator"></span></label></div>').appendTo(tableRow);
								displayData('preferences0' + j, tableRow, data, 'column');
							}
						}
					}
				}
				else
				{
					for (var i = 0; i < contInterestAPMInfoList.length; i++)
					{
						$p('<div class="subLabel">' + contInterestAPMInfoList[i].name + '</div>').appendTo(secgroup);
						
						var contentTable = $p('<div class="contentTable"/>').appendTo(secgroup);
						var headerRow = $p('<div class="headerRow"/>').appendTo(contentTable);
						
						$p('<div class="radioColumn"/>').appendTo(headerRow);
						data = ['Name', 'Last Discussed', 'Reason', 'Source', 'Notes'];
						displayData('preferencesH', headerRow, data, 'columnHeader');
						
						interestReasonResearchInfo = contInterestAPMInfoList[i].interestWithReasonResearch;
						if (interestReasonResearchInfo != null && interestReasonResearchInfo != undefined)
						{
							if (Array.isArray(interestReasonResearchInfo))
							{
								for (var j = 0; j < interestReasonResearchInfo.length; j++)
								{
									var tableRow = $p('<div class="tableRow"/>').appendTo(contentTable);
									sel = '';
									if (j == 0)
									{
										sel = ' checked="checked"';
									}
									data = ['<a title="click to launch Preference page" href="' + serverURL + interestReasonResearchInfo[j].Id + '" target="_blank">' +  contactName + '</a>', interestReasonResearchInfo[j].T1C_Base__Last_Discussed__c, interestReasonResearchInfo[j].T1C_Base__Reason__c, interestReasonResearchInfo[j].T1C_Base__Source__c, interestReasonResearchInfo[j].T1C_Base__Note__c];
									$p('<div class="radioColumn"><label class="control radio"><input name="reasonR' + i + '" type="radio" value=' + interestReasonResearchInfo[j].Id + sel + ' data-type="dateType"><span class="control-indicator"></span></label></div>').appendTo(tableRow);
									displayData('preferences' + i + j, tableRow, data, 'column');
								}
							}
						}
					}
				}
			}
			
			//******Preserve Button************************
			preserveButton(subSection);
		}
		
		if (interestPage)
		{
			//******print the interest infomation**********
			if (contInterestInfoList != null && contInterestInfoList != undefined && contInterestInfoList.length > 0)
			{
				var preserveOnlySection = $p('<div class="section preserveOnlySection"/>').appendTo(applicationContainer);
				var subSection = $p('<div class="subSection"/>').appendTo(preserveOnlySection);
				$p('<div class="sectionLabel">Interests</div>').appendTo(subSection);
				var secgroup = $p('<div class="group"/>').appendTo(subSection);
				
				////////Interest
				if (Array.isArray(contInterestInfoList) == false)
				{
					var btnName;
					
					$p('<div class="subLabel">' + contInterestInfoList.name + '</div>').appendTo(secgroup);
					
					var contentTable = $p('<div class="contentTable"/>').appendTo(secgroup);
					var headerRow = $p('<div class="headerRow"/>').appendTo(contentTable);
					
					$p('<div class="radioColumn"/>').appendTo(headerRow);
					data = ['Name', 'Last Discussed', 'Reason', 'Source', 'Notes'];
					displayData('interestH', headerRow, data, 'columnHeader');
					
					interestReasonInterestInfo = contInterestInfoList.interestWithReasonInterest;
					if (interestReasonInterestInfo != null && interestReasonInterestInfo != undefined)
					{
						if (Array.isArray(interestReasonInterestInfo))
						{
							for (var j = 0; j < interestReasonInterestInfo.length; j++)
							{
								var tableRow = $p('<div class="tableRow"/>').appendTo(contentTable);
								sel = '';
								if (j == 0)
								{
									sel = ' checked="checked"';
								}
								data = ['<a title="click to launch Interest page" href="' + serverURL + interestReasonInterestInfo[j].Id + '" target="_blank">' +  contactName + '</a>', interestReasonInterestInfo[j].T1C_Base__Last_Discussed__c, interestReasonInterestInfo[j].T1C_Base__Reason__c, interestReasonInterestInfo[j].T1C_Base__Source__c, interestReasonInterestInfo[j].T1C_Base__Note__c];
								//$p('<div class="radioColumn"><label class="control radio"><input name="reasonI0" type="radio" value=' + interestReasonInterestInfo[j].Id + sel + ' data-type="dateType"><span class="control-indicator"></span></label></div>').appendTo(tableRow);
								var id = 'i0' + j;
								$p('<label class="control checkbox" for="' + id + '"><input id="0:' + j + '" name="reasonI0" type="checkbox" value=' + interestReasonInterestInfo[j].Id + ' data-type="dateType"><span class="control-indicator"></span></label>').appendTo(tableRow);
								displayData('interest0' + j, tableRow, data, 'column');
							}
							btnName = 'reasonI0';
						}
						//******Preserve/Merge Button************************
						interestButtons(secgroup, btnName);
					}
				}
				else
				{
					for (var i = 0; i < contInterestInfoList.length; i++)
					{
						var btnName;
						
						$p('<div class="subLabel">' + contInterestInfoList[i].name + '</div>').appendTo(secgroup);
						
						var contentTable = $p('<div class="contentTable"/>').appendTo(secgroup);
						var headerRow = $p('<div class="headerRow"/>').appendTo(contentTable);
						
						$p('<div class="radioColumn"/>').appendTo(headerRow);
						data = ['Name', 'Last Discussed', 'Reason', 'Source', 'Notes'];
						displayData('interestH', headerRow, data, 'columnHeader');
						
						interestReasonInterestInfo = contInterestInfoList[i].interestWithReasonInterest;
						if (interestReasonInterestInfo != null && interestReasonInterestInfo != undefined)
						{
							if (Array.isArray(interestReasonInterestInfo))
							{
								for (var j = 0; j < interestReasonInterestInfo.length; j++)
								{
									var tableRow = $p('<div class="tableRow"/>').appendTo(contentTable);
									sel = '';
									if (j == 0)
									{
										sel = ' checked="checked"';
									}
									data = ['<a title="click to launch Interest page" href="' + serverURL + interestReasonInterestInfo[j].Id + '" target="_blank">' +  contactName + '</a>', interestReasonInterestInfo[j].T1C_Base__Last_Discussed__c, interestReasonInterestInfo[j].T1C_Base__Reason__c, interestReasonInterestInfo[j].T1C_Base__Source__c, interestReasonInterestInfo[j].T1C_Base__Note__c];
									//$p('<div class="radioColumn"><label class="control radio"><input name="reasonI' + i + '" type="radio" value=' + interestReasonInterestInfo[j].Id + sel + ' data-type="dateType"><span class="control-indicator"></span></label></div>').appendTo(tableRow);
									var id = 'i'+ i + '' + j;
									$p('<label class="control checkbox" for="' + id + '"><input id="' + id + '" name="reasonI' + i + '" type="checkbox" value=' + interestReasonInterestInfo[j].Id + ' data-type="dateType"><span class="control-indicator"></span></label>').appendTo(tableRow);
									displayData('interest' + i + j, tableRow, data, 'column');
								}
								btnName = 'reasonI';
							}
							//******Preserve/Merge Button************************
							interestButtons(secgroup, btnName + i);
						}
					}
				}
			}
		}
		
		//******print the contact coverage infomation**********
		if (contCoverageInfoList != null && contCoverageInfoList != undefined && contCoverageInfoList.length > 0)
		{
			var preserveOnlySection = $p('<div class="section preserveOnlySection"/>').appendTo(applicationContainer);
			var subSection = $p('<div class="subSection"/>').appendTo(preserveOnlySection);
			$p('<div class="sectionLabel">Contact Coverage</div>').appendTo(subSection);
			var secgroup = $p('<div class="group"  id="contCoverage"/>').appendTo(subSection);
			
			if (Array.isArray(contCoverageInfoList) == false)
			{
				$p('<div class="subLabel">' + contCoverageInfoList.name + '</div>').appendTo(secgroup);
				
				var contentTable = $p('<div class="contentTable" id="contCoverageSec"/>').appendTo(secgroup);
				var headerRow = $p('<div class="headerRow"/>').appendTo(contentTable);
				
				$p('<div class="radioColumn"/>').appendTo(headerRow);
				data = ['Contact','Role', 'Start Date', 'End Date', 'Is Backup'];
				displayData('contactcoverageH', headerRow, data, 'columnHeader');
				
				contactCoverageInfo = contCoverageInfoList.contactCoverages;
				if (contactCoverageInfo != null && contactCoverageInfo != undefined)
				{
					if (Array.isArray(contactCoverageInfo))
					{
						for (var j = 0; j < contactCoverageInfo.length; j++)
						{
							var tableRow = $p('<div id="contactcoverage0' + j + '" class="tableRow"/>').appendTo(contentTable);
							data = ['<a title="click to launch Contact Coverage page" href="' + serverURL + contactCoverageInfo[j].Id + '" target="_blank">' +  contactCoverageInfo[j].T1C_Base__Contact__r.Name + '</a>', contactCoverageInfo[j].T1C_Base__Role__c, contactCoverageInfo[j].T1C_Base__Start_Date__c, contactCoverageInfo[j].T1C_Base__End_Date__c, contactCoverageInfo[j].T1C_Base__Is_Backup__c];
							var id = '0' + j;
							var checkboxElem = $p('<label class="control checkbox" for="' + id + '"><input id="0:' + j + '" name="contactCoverages0" type="checkbox" value=' + contactCoverageInfo[j].Id + ' data-type="dateType"><span class="control-indicator"></span></label>').appendTo(tableRow);
							displayData('contactcoverage0' + j, tableRow, data, 'column');
							checkboxElem.on('click', function(){ ccCheckboxHandlers() });
						}
					}
				}
				//******Preserve/Merge Button************************
				ccButtons(secgroup, 'contactCoverages0', contentTable);
			}
			else
			{
				for (var i = 0; i < contCoverageInfoList.length; i++)
				{
					$p('<div class="subLabel">' + contCoverageInfoList[i].name  + '</div>').appendTo(secgroup);
					
					var contentTable = $p('<div class="contentTable" id="contCoverageSec"/>').appendTo(secgroup);
					var headerRow = $p('<div class="headerRow"/>').appendTo(contentTable);
					
					$p('<div class="radioColumn"/>').appendTo(headerRow);
					data = ['Contact','Role', 'Start Date', 'End Date', 'Is Backup'];
					displayData('contactcoverageH', headerRow, data, 'columnHeader');
					
					contactCoverageInfo = contCoverageInfoList[i].contactCoverages;
					if (contactCoverageInfo != null && contactCoverageInfo != undefined)
					{
						if (Array.isArray(contactCoverageInfo))
						{
							for (var j = 0; j < contactCoverageInfo.length; j++)
							{
								var tableRow = $p('<div id="contactcoverage' + i + j + '" class="tableRow"/>').appendTo(contentTable);
								data = ['<a title="click to launch Contact Coverage page" href="' + serverURL + contactCoverageInfo[j].Id + '" target="_blank">' +  contactCoverageInfo[j].T1C_Base__Contact__r.Name + '</a>', contactCoverageInfo[j].T1C_Base__Role__c, contactCoverageInfo[j].T1C_Base__Start_Date__c, contactCoverageInfo[j].T1C_Base__End_Date__c, contactCoverageInfo[j].T1C_Base__Is_Backup__c];
								var id = i + '' + j;
								var checkboxElem = $p('<label class="control checkbox" for="' + id + '"><input id="' + id + '" name="contactCoverages' + i + '" type="checkbox" value=' + contactCoverageInfo[j].Id + ' data-type="dateType"><span class="control-indicator"></span></label>').appendTo(tableRow);
								displayData('contactcoverage' + i + j, tableRow, data, 'column');
								checkboxElem.on('click', function(){ ccCheckboxHandlers() });
							}
						}
					}
					//******Preserve/Merge Button************************
					ccButtons(secgroup, 'contactCoverages' + i, contentTable);
				}
				//openFieldToEdit
			}
		}
		
		//var footerPanel = $p('<div class="footer"/>').appendTo(mergeUtilityBody);
		//enableTriggers(footerPanel);
		
		$p('<script type="text/javascript"> ccCheckboxHandlers() </script>').appendTo("html");
	}
	
	
	//  ==================================================================
	//  Method: displayData
	//  Description: Simplified way to display data in columns
	//  ==================================================================
	function displayData(identifier, columns, data, className)
	{
		if (data[0].indexOf('Contact Coverage') != -1)
		{
			$p('<div id=' + identifier + ' class="' + className + '">' + data[0] + '</div>').appendTo(columns);
			
			var temData = data[1] == undefined ? '&nbsp;' : data[1];
			$p('<div id="' + identifier + x + '" class="' + className + '">' + temData + '</div>').appendTo(columns);
			
			temData = data[2] == undefined ? '&nbsp;' : data[2];
			$p('<div id="' + identifier + x + '" class="' + className + '">' + temData + '</div>').appendTo(columns);
			
			temData = data[3] == undefined ? '&nbsp;' : data[3];
			$p('<div id="' + identifier + x + '" class="' + className + '">' + temData + '</div>').appendTo(columns);
			
			if (data[4] == undefined || data[4] == false )
			{
				$p('<div id="' + identifier + x + '" class="' + className + '"><label class="control checkbox"><input type="checkbox" data-type="dateType" disabled="true"><span class="control-indicator"></span></label></div>').appendTo(columns);
			}
			else
			{
				$p('<div id="' + identifier + x + '" class="' + className + '"><label class="control checkbox"><input type="checkbox" checked="true" data-type="dateType" disabled="true"><span class="control-indicator"></span></label></div>').appendTo(columns);
			}
			
			/*temData = data[5] == undefined ? '&nbsp;' : data[5];
			$p('<div id="' + identifier + x + '" class="' + className + '">' + temData + '</div>').appendTo(columns);*/
		}
		else if (data[0].indexOf('Preference') != -1 || data[0].indexOf('Interest') != -1)
		{
			for (var x = 0; x < data.length-1; x++)
			{
				var temData = data[x] == undefined ? '&nbsp;' : data[x];
				if (temData == 'Notes')
				{
					//this is for the header column to be wider
					className = className + ' notes';
				}
				$p('<div id="' + identifier + x + '" class="' + className + '">' + temData + '</div>').appendTo(columns);
			}
			//print the notes with different column class (wider column)
			var temData = data[data.length-1] == undefined ? '&nbsp;' : data[data.length-1];
			className = className + ' notes';
			$p('<div id="' + identifier + x + '" class="' + className + '">' + temData + '</div>').appendTo(columns);
		}
		else
		{
			for (var x = 0; x < data.length; x++)
			{
				var temData = data[x] == undefined ? '&nbsp;' : data[x];
				$p('<div id="' + identifier + x + '" class="' + className + '">' + temData + '</div>').appendTo(columns);
			}
		}
	}
	
	
	//  ==================================================================
	//  Method: blindUpdateRecords
	//  Description: Simplified way to update records
	//  ==================================================================
	function blindUpdateRecords(recordIdsToUpdate, objName)
	{
		if (recordIdsToUpdate != null && recordIdsToUpdate != undefined && recordIdsToUpdate.length > 0)
		{
			var sObjects = [];
			
			for (var x = 0; x < recordIdsToUpdate.length; x++) 
			{ 
				var sObj = new sforce.SObject(objName);
				sObj.Id = recordIdsToUpdate[x];
				sObjects[x] = sObj;
			}
			
			var uptResult = sforce.connection.update(sObjects);
			for (var y = 0; y < uptResult.length; y++) 
			{ 
				if (uptResult[y].getBoolean("success")) 
				{
					console.log(objName + " with the following id is updated:" + uptResult[y]);
				}
				else 
				{
					console.log(objName + " Failed to update the following " + uptResult[y]);
				}
	
			}
		}
	}


	//  ==================================================================
	//  Method: updateRecords
	//  Description: Simplified way to update records
	//  ==================================================================
	function updateRecords(recordIdsToUpdate, objName, fieldName, val, isMultiVal, isMultiField)
	{
		if (recordIdsToUpdate != null && recordIdsToUpdate != undefined && recordIdsToUpdate.length > 0)
		{
			var sObjects = [];
			
			if (Array.isArray(recordIdsToUpdate) == false)
			{
				var sObj = new sforce.SObject(objName);
				
				sObj.Id = recordIdsToUpdate;

				if (isMultiField === true)
				{
					if (isMultiVal == true && Array.isArray(val) == true)
					{
						sObj[fieldName[0]] = val[0];
						sObj[fieldName[1]] = val[1];
					}
					else
					{
						sObj[fieldName[0]] = val[0];
						sObj[fieldName[1]] = val[1];
					}
				}
				else
				{
					sObj[fieldName] = val;
				}
				sObjects[0] = sObj;
			}
			else
			{
				for (var x = 0; x < recordIdsToUpdate.length; x++) 
				{ 
					var sObj = new sforce.SObject(objName);

					sObj.Id = recordIdsToUpdate[x];
					
					if (isMultiField === true)
					{
						if (isMultiVal == true && Array.isArray(val) == true)
						{
							sObj[fieldName[0]] = val[x][0];
							sObj[fieldName[1]] = val[x][1];
						}
						else
						{
							sObj[fieldName[0]] = val[0];
							sObj[fieldName[1]] = val[1];
						}
					}
					else
					{
						if (isMultiVal == true && Array.isArray(val) == true)
						{
							sObj[fieldName] = val[x];
						}
						else
						{
							sObj[fieldName] = val;
						}
					}
					sObjects[x] = sObj;
				}
			}
			
			var uptResult = sforce.connection.update(sObjects);
			for (var y = 0; y < uptResult.length; y++) 
			{ 
				if (uptResult[y].getBoolean("success")) 
				{
					console.log(objName + " with the following id is updated:" + uptResult[y]);
				}
				else 
				{
					console.log(objName + " Failed to update the following " + uptResult[y]);
				}
	
			}
		}
	}
	
	//  ==================================================================
	//  Method: delRecords
	//  Description: Simplified way to delete duplicate values
	//  ==================================================================
	function delRecords(arrayValsDups, objName)
	{
		var delResult = sforce.connection.deleteIds(arrayValsDups);	
		for (var x = 0; x < delResult.length; x++) 
		{ 
			if (delResult[x].getBoolean("success")) 
			{
				console.log(objName + " with the following id is deleted:" + delResult[x]);
			}
			else 
			{
				console.log(objName + " Failed to delete the following " + delResult[x]);
			}

		}
	}
	
	//  ==================================================================
	//  Method: getNotes
	//  Description: Simplified way to get Notes from Interest records
	//  ==================================================================
	function getNotes(arrayVals, selectedId)
	{
		var notes = '';
		
		//fullNote is to check if we are adding duplicate notes. If we already have the note from
		//one interest, we should not add the same note again from another interest.
		var fullNote = '';
		
		//get notes only from selected interests. so before we get note from interest
		//see if the id is selected (in selectedIdString)
		var selectedIdString = selectedId.toString();
		
		if (arrayVals != null && arrayVals != undefined)
		{
			if (Array.isArray(arrayVals) == false)
			{
				if (arrayVals.T1C_Base__Note__c != undefined && selectedIdString.indexOf(arrayVals.Id) != -1)
				{
					notes += arrayVals.T1C_Base__Note__c;
				}
			}
			else
			{
				for (var a = 0; a < arrayVals.length; a++)
				{
					if (fullNote.indexOf(arrayVals[a].T1C_Base__Note__c) == -1)
					{
						if (arrayVals[a].T1C_Base__Note__c != undefined && selectedIdString.indexOf(arrayVals[a].Id) != -1)
						{
							var temp = notes + arrayVals[a].T1C_Base__Note__c + '\r\n\r\n';
							if (temp.length > 32000)
							{
								if (confirm("Warning – Merging these records will truncate historical notes from the Notes field. Proceed?") == true)
								{
									return (temp.substring(0, 32000));
								}
								else
								{
									return '-1';
								}
							}
							notes += arrayVals[a].T1C_Base__Note__c + '\r\n\r\n';
						    fullNote += arrayVals[a].T1C_Base__Note__c; 
						}
					}
				}
			}
		}
		return notes;
	}
	

	//  ==================================================================
	//  Method: getOldestAsOfDate
	//  Description: Simplified way to get oldest AsOfDate from Interest records
	//  ==================================================================
	function getOldestAsOfDate(arrayVals, selectedId)
	{
		var asOfDate = null;
		
		//get notes only from selected interests. so before we get note from interest
		//see if the id is selected (in selectedIdString)
		var selectedIdString = selectedId.toString();
		
		if (arrayVals != null && arrayVals != undefined)
		{
			if (Array.isArray(arrayVals) == false)
			{
				if (arrayVals.T1C_Base__As_Of_Date__c != undefined && selectedIdString.indexOf(arrayVals.Id) != -1)
				{
					asOfDate = new Date(arrayVals.T1C_Base__As_Of_Date__c);
				}
			}
			else
			{
				for (var a = 0; a < arrayVals.length; a++)
				{
					if (arrayVals[a].T1C_Base__As_Of_Date__c != undefined && selectedIdString.indexOf(arrayVals[a].Id) != -1)
					{
						var aDate;
						var s = arrayVals[a].T1C_Base__As_Of_Date__c;
						if (s == undefined || s == null)
						{
							aDate = null;
						}
						else
						{
							var sthisYear = s.split('-')[0];
					    	var sthisMonth = s.split('-')[1];
					    	var sthisDay = s.split('-')[2];
					    	
					    	aDate = new Date(sthisYear, sthisMonth -1, sthisDay,0,0);
						}
				    	
					    if (asOfDate >= aDate || asOfDate == null)
					    {	
					    	asOfDate = aDate;
					    }
					}				
				}
			}
		}
		return asOfDate;
	}
	

	//  ==================================================================
	//  Method: getDates
	//  Description: Simplified way to get dates from contact coverage records
	//  ==================================================================
	function getDates(arrayVals, selectedId)
	{
		var startDate = null;
		var endDate = new Date(1971,01,01);
		if (arrayVals != null && arrayVals != undefined)
		{
			if (Array.isArray(arrayVals) == false)
			{
				startDate = new Date(arrayVals.T1C_Base__Start_Date__c);
				endDate = new Date(arrayVals.T1C_Base__End_Date__c);
			}
			else
			{
				for (var a = 0; a < arrayVals.length; a++)
				{
					var aDate;
					var s = arrayVals[a].T1C_Base__Start_Date__c;
					if (s == undefined || s == null)
					{
						aDate = null;
					}
					else
					{
						var sthisYear = s.split('-')[0];
				    	var sthisMonth = s.split('-')[1];
				    	var sthisDay = s.split('-')[2];
				    	
				    	aDate = new Date(sthisYear, sthisMonth -1, sthisDay,0,0);
					}
			    	
				    if (startDate >= aDate || startDate == null)
				    {	
				    	startDate = aDate;
				    }
				    ////////////////////
				    var eaDate;
				    var e = arrayVals[a].T1C_Base__End_Date__c;
				    if (e == undefined || e == null)
					{
				    	eaDate = null;
					}
					else
					{
						var ethisYear = e.split('-')[0];
				    	var ethisMonth = e.split('-')[1];
				    	var ethisDay = e.split('-')[2];
				    	
				    	eaDate = new Date(ethisYear, ethisMonth -1, ethisDay,0,0);
					}
				    
				    if (endDate <= eaDate || eaDate == null)
				    {
				    	endDate = eaDate;
				    }
				}
			}
		}
		
		return [startDate, endDate];
	}
	
	
	//  ==================================================================
	//  Method: getDuplicates
	//  Description: Simplified way to get duplicate values
	//  ==================================================================
	function getDuplicates(arrayVals, selectedId)
	{
		var dupValues = [];
		if (arrayVals != null && arrayVals != undefined)
		{
			if (Array.isArray(arrayVals) == false)
			{
				if (arrayVals.Id != selectedId)
				{
			    	dupValues[0] = arrayVals.Id;
			    }
			}
			else
			{
				var b = 0;
				for (var a = 0; a < arrayVals.length; a++)
				{
				    if (arrayVals[a].Id != selectedId)
				    {
				    	dupValues[b] = arrayVals[a].Id;
				    	b++;
				    }
				}
			}
		}
		return dupValues;
	}
	
	//  ==================================================================
	//  Method: getRadioButtonValue
	//  Description: Simplified way to get selected radio button values
	//  ==================================================================
	function getRadioButtonValue(btnName)
	{
		var radiobtn_value;
		var values = document.getElementsByName(btnName);
		for (var y = 0; y < values.length; y++)
		{
		    if (values[y].checked === true)
		    {
		    	radiobtn_value = values[y].value;
		    }
		}
		return radiobtn_value;
	}
	
	//  ==================================================================
	//  Method: preserveButton
	//  Description: only to do preserve function
	//  ==================================================================
	function preserveButton(buttonsDiv)
	{
		var preserveButton = $p('<div class="sectionButtonBar"><div class="buttonBar"><div class="rightAlignedButtonWrapper"><button class="preserveBTN">Preserve</button></div></div></div>').appendTo(buttonsDiv);
		
		$(preserveButton).on('click', function() 
		{
			/*//check if the trigger are disabled or not
			if (tiggersEnabled == true)
			{
				alert('Please disable the merge triggers first.');
				return;
			}*/
			
			//when a user takes an action, display a warning prompt
			var proceed = false;
		    if (confirm("Any unselected records will be deleted. Proceed?") == true) 
		    {
		    	proceed = true;
		    } 
			
		    if (proceed == true)
		    {
				var radioButtonName;
				
				//******process the meeting attendee infomation**********
				var meetingAttendee_value = [];
				var meetingAttendeeInfoDups = [];
				if (contMeetingInfoList != null && contMeetingInfoList != undefined && contMeetingInfoList.length > 0)
				{
					if (Array.isArray(contMeetingInfoList) == false)
					{
						meetingAttendee_value[0] = getRadioButtonValue('meetingAttendees0');
						InfoDups = contMeetingInfoList.meetingAttendees;
						meetingAttendeeInfoDups[meetingAttendeeInfoDups.length] = getDuplicates(InfoDups, meetingAttendee_value[0]);
					}
					else
					{
						for (var x = 0; x < contMeetingInfoList.length; x++)
						{
							radioButtonName = 'meetingAttendees' + x;
							meetingAttendee_value[x] = getRadioButtonValue(radioButtonName);
							InfoDups = contMeetingInfoList[x].meetingAttendees;
							meetingAttendeeInfoDups[meetingAttendeeInfoDups.length] = getDuplicates(InfoDups, meetingAttendee_value[x]);
						}
					}
				}
				
				
				//******process the key contact infomation**********
				var keyCont_value = [];
				var keyContInfoDups = [];
				if (keyContInfoList != null && keyContInfoList != undefined && keyContInfoList.length > 0)
				{
					if (Array.isArray(keyContInfoList) == false)
					{
						keyCont_value[0] = getRadioButtonValue('keyContacts0');
						InfoDups = keyContInfoList.keyContacts;
						keyContInfoDups[keyContInfoDups.length] = getDuplicates(InfoDups, keyCont_value[0]);
					}
					else
					{
						for (var x = 0; x < keyContInfoList.length; x++)
						{
							radioButtonName = 'keyContacts' + x;
							keyCont_value[x] = getRadioButtonValue(radioButtonName);
							InfoDups = keyContInfoList[x].keyContacts;
							keyContInfoDups[keyContInfoDups.length] = getDuplicates(InfoDups, keyCont_value[x]);
						}
					}
				}
				
				
				//******process the collection key contact infomation**********
				var collectionKeyCont_value = [];
				var collectionKeyContInfoDups = [];
				if (collectionKeyContInfoList != null && collectionKeyContInfoList != undefined && collectionKeyContInfoList.length > 0)
				{
					if (Array.isArray(collectionKeyContInfoList) == false)
					{
						collectionKeyCont_value[0] = getRadioButtonValue('colkeyContacts0');
						InfoDups = collectionKeyContInfoList.collectionKeyContacts;
						collectionKeyContInfoDups[collectionKeyContInfoDups.length] = getDuplicates(InfoDups, collectionKeyCont_value[0]);
					}
					else
					{
						for (var x = 0; x < collectionKeyContInfoList.length; x++)
						{
							radioButtonName = 'colkeyContacts' + x;
							collectionKeyCont_value[x] = getRadioButtonValue(radioButtonName);
							InfoDups = collectionKeyContInfoList[x].collectionKeyContacts;
							collectionKeyContInfoDups[collectionKeyContInfoDups.length] = getDuplicates(InfoDups, collectionKeyCont_value[x]);
						}
					}
				}
				
				
				//******process the ace list entry infomation**********
				var contAceList_value = [];
				var contAceListInfoDups = [];
				if (contAceListInfoList != null && contAceListInfoList != undefined && contAceListInfoList.length > 0)
				{
					if (Array.isArray(contAceListInfoList) == false)
					{
						contAceList_value[0] = getRadioButtonValue('listEntry0');
						InfoDups = contAceListInfoList.aceListEntries;
						contAceListInfoDups[contAceListInfoDups.length] = getDuplicates(InfoDups, contAceList_value[0]);
					}
					else
					{
						for (var x = 0; x < contAceListInfoList.length; x++)
						{
							radioButtonName = 'listEntry' + x;
							contAceList_value[x] = getRadioButtonValue(radioButtonName);
							InfoDups = contAceListInfoList[x].aceListEntries;
							contAceListInfoDups[contAceListInfoDups.length] = getDuplicates(InfoDups, contAceList_value[x]);
						}
					}
				}
				
				
				//******process the call report attendee infomation**********
				var callReportAttendee_value = [];
				var callReportAttendeeInfoDups = [];
				if (callReportAttendeeInfoList != null && callReportAttendeeInfoList != undefined && callReportAttendeeInfoList.length > 0)
				{
					if (Array.isArray(callReportAttendeeInfoList) == false)
					{
						callReportAttendee_value[0] = getRadioButtonValue('callReport0');
						InfoDups = callReportAttendeeInfoList.crAttendees;
						callReportAttendeeInfoDups[callReportAttendeeInfoDups.length] = getDuplicates(InfoDups, callReportAttendee_value[0]);
					}
					else
					{
						for (var x = 0; x < callReportAttendeeInfoList.length; x++)
						{
							radioButtonName = 'callReport' + x;
							callReportAttendee_value[x] = getRadioButtonValue(radioButtonName);
							InfoDups = callReportAttendeeInfoList[x].crAttendees;
							callReportAttendeeInfoDups[callReportAttendeeInfoDups.length] = getDuplicates(InfoDups, callReportAttendee_value[x]);
						}
					}
				}
				
				
				//******process the deal contact infomation**********
				var dealContact_value = [];
				var dealContactInfoDups = [];
				if (dealContactInfoList != null && dealContactInfoList != undefined && dealContactInfoList.length > 0)
				{
					if (Array.isArray(dealContactInfoList) == false)
					{
						dealContact_value[0] = getRadioButtonValue('deal0');
						InfoDups = dealContactInfoList.dealContacts;
						dealContactInfoDups[dealContactInfoDups.length] = getDuplicates(InfoDups, dealContact_value[0]);
					}
					else
					{
						for (var x = 0; x < dealContactInfoList.length; x++)
						{
							radioButtonName = 'deal' + x;
							dealContact_value[x] = getRadioButtonValue(radioButtonName);
							InfoDups = dealContactInfoList[x].dealContacts;
							dealContactInfoDups[dealContactInfoDups.length] = getDuplicates(InfoDups, dealContact_value[x]);
						}
					}
				}
				
				//******process the preferences infomation**********
				var preferences_value = [];
				var preferencesInfoDups = [];
				if (contInterestAPMInfoList != null && contInterestAPMInfoList != undefined && contInterestAPMInfoList.length > 0)
				{
					if (Array.isArray(contInterestAPMInfoList) == false)
					{
						preferences_value[0] = getRadioButtonValue('reasonR0');
						InfoDups = contInterestAPMInfoList.interestWithReasonResearch;
						preferencesInfoDups[preferencesInfoDups.length] = getDuplicates(InfoDups, preferences_value[0]);
					}
					else
					{
						for (var x = 0; x < contInterestAPMInfoList.length; x++)
						{
							radioButtonName = 'reasonR' + x;
							preferences_value[x] = getRadioButtonValue(radioButtonName);
							InfoDups = contInterestAPMInfoList[x].interestWithReasonResearch;
							preferencesInfoDups[preferencesInfoDups.length] = getDuplicates(InfoDups, preferences_value[x]);
						}
					}
				}
				
				delRecords(meetingAttendeeInfoDups, "Meeting Attendee");
				blindUpdateRecords(meetingAttendee_value, "T1C_AEM__Invited_Contact__c");
				
				delRecords(keyContInfoDups, "Key Contacts");
				blindUpdateRecords(keyCont_value, "T1C_AEM__Event_Contact__c");
				
				delRecords(collectionKeyContInfoDups, "Collection Key Contacts");
				blindUpdateRecords(collectionKeyCont_value, "T1C_AEM__Event_Collection_Contact__c");
				
				delRecords(contAceListInfoDups, "Ace List Entry");
				updateRecords(contAceList_value, 'T1C_Base__Ace_List_Entry__c', 'T1C_Base__Entry_List_Id__c', contactId, false, false);
				blindUpdateRecords(contAceList_value, "T1C_Base__Ace_List_Entry__c");
				
				delRecords(callReportAttendeeInfoDups, "CallReport Attendee");
				blindUpdateRecords(callReportAttendee_value, "T1C_Base__Call_Report_Attendee_Client__c");
				
				delRecords(dealContactInfoDups, "Deal Contacts");
				blindUpdateRecords(dealContact_value, "T1C_Base__Deal_Contact__c");
				
				delRecords(preferencesInfoDups, "Preferences");
				blindUpdateRecords(preferences_value, "T1C_Base__Interest__c");
				
				location.reload(true);
		    }
		});
	}
	
	//  ==================================================================
	//  Method: interestButtons
	//  Description: Simplified way to display top Buttons and function
	//  ==================================================================
	function interestButtons(buttonsDiv, buttonName)
	{
		var sectionButtonBar = $p('<div class="sectionButtonBar"/>').appendTo(buttonsDiv);
		var buttonBar = $p('<div class="buttonBar"/>').appendTo(sectionButtonBar);
		var rightAlignedButtonWrapper = $p('<div class="rightAlignedButtonWrapper"/>').appendTo(buttonBar);
		/*var preserveButton = $p('<button name="' + buttonName + 'Pres " class="preserveBTN">Preserve</button>').appendTo(rightAlignedButtonWrapper);
		
		$(preserveButton).on('click', function() 
		{
			//check if the trigger are disabled or not
			if (tiggersEnabled == true)
			{
				alert('Please disable the merge triggers first.');
				return;
			}
			
			//when a user takes an action, display a warning prompt
			var proceed = false;
		    if (confirm("Any unselected records will be deleted. Proceed?") == true) 
		    {
		    	proceed = true;
		    } 
			
		    if (proceed == true)
		    {
				processInterestInfomation(false, buttonName);
				location.reload(true);
		    }
		});*/
		
		var mergeButton = $p('<button name="' + buttonName + 'Merge " class="mergeBTN">Merge</button>').appendTo(rightAlignedButtonWrapper);
	
		$(mergeButton).on('click', function() 
		{
			/*//check if the trigger are disabled or not
			if (tiggersEnabled == true)
			{
				alert('Please disable the merge triggers first.');
				return;
			}*/
			
			//when a user takes an action, display a warning prompt
			var proceed = false;
		    if (confirm("Any unselected records will be deleted. Proceed?") == true) 
		    {
		    	proceed = true;
		    } 
			
		    if (proceed == true)
		    {
				processInterestInfomation(true, buttonName);
				location.reload(true);
		    }
		});
		
	}
	
	
	//  ==================================================================
	//  Method: processInterestInfomation
	//  Description: Simplified way to process Interest Infomation
	//  ==================================================================
	function processInterestInfomation(isMerge, buttonName)
	{
		//******process the interest infomation**********
		var contInterest_value = [];
		var contInterestInfoDups = [];
		var notesInterestInterest;
		var oldestAsOfDate;

		var fields = ['T1C_Base__Note__c', 'T1C_Base__As_Of_Date__c'];
		
		var x = Number(buttonName.slice(-1));
		
		contInterest_value[0] = getCheckboxValue(buttonName);

		var InfoDups1;
		if (Array.isArray(contInterestInfoList) == false)
		{
			InfoDups1 = contInterestInfoList.interestWithReasonInterest;
		}
		else
		{
			InfoDups1 = contInterestInfoList[x].interestWithReasonInterest;
		}
		
		//get notes from the selected interests
		notesInterestInterest = getNotes(InfoDups1, contInterest_value[0]);
		//get oldest AsOfDate from the selected interests
		oldestAsOfDate = getOldestAsOfDate(InfoDups1, contInterest_value[0]);
		//get all the interest as dupes except the selected interest with latest 'as of date'
		contInterestInfoDups[contInterestInfoDups.length] = getDuplicates(InfoDups1, contInterest_value[0][0]);

		if(notesInterestInterest != '-1')
		{
			var values = [notesInterestInterest, oldestAsOfDate];

			delRecords(contInterestInfoDups, "Interest");
			if (isMerge == true)
			{
				updateRecords(contInterest_value[0][0], 'T1C_Base__Interest__c', fields, values, true, true);
			}
			blindUpdateRecords(contInterest_value[0][0], "T1C_Base__Interest__c");
		}
	}
	
	
	//  ==================================================================
	//  Method: ccButtons
	//  Description: Simplified way to display top Buttons and function
	//  ==================================================================
	function ccButtons(buttonsDiv, buttonName, contentbtnDiv)
	{
		var sectionButtonBar = $p('<div class="sectionButtonBar"/>').appendTo(buttonsDiv);
		var buttonBar = $p('<div class="buttonBar"/>').appendTo(sectionButtonBar);
		var rightAlignedButtonWrapper = $p('<div class="rightAlignedButtonWrapper"/>').appendTo(buttonBar);
		var preserveButton = $p('<button name="' + buttonName + 'Pres " class="preserveBTN">Preserve</button>').appendTo(rightAlignedButtonWrapper);
		
		$(preserveButton).on('click', function() 
		{
			/*//check if the trigger are disabled or not
			if (tiggersEnabled == true)
			{
				alert('Please disable the merge triggers first.');
				return;
			}*/
			
			//when a user takes an action, display a warning prompt
		    if (confirm("Any unselected records will be deleted. Proceed?") == true) 
		    {
		    	processContactCoverageInfomation(buttonName, contentbtnDiv);
				location.reload(true);
		    } 
		});
	}
	
	
//  ==================================================================
	//  Method: processContactCoverageInfomation
	//  Description: Simplified way to process Contact Coverage Infomation
	//  ==================================================================
	function processContactCoverageInfomation(buttonName, topDiv)
	{
		//******process the contact coverage infomation**********
		var contactCoverage_value = [];
		var contactCoverageInfoDups = [];
				
		var x = Number(buttonName.slice(-1));
		
		contactCoverage_value[0] = getCheckboxValue(buttonName);
		
		var InfoDups;
		if (Array.isArray(contCoverageInfoList) == false)
		{
			InfoDups = contCoverageInfoList.contactCoverages;
		}
		else
		{
			InfoDups = contCoverageInfoList[x].contactCoverages;
		}
		if(contactCoverage_value[0].length < InfoDups.length)
		{
			contactCoverageInfoDups[contactCoverageInfoDups.length] = getDuplicatesWithMultiSelection(InfoDups, contactCoverage_value[0]);
			delRecords(contactCoverageInfoDups, "Contact Coverages");
		}
		updateCCRecords("T1C_Base__Contact_Coverage__c", topDiv);
	}
	
	//  ==================================================================
	//  Method: getCheckboxValue
	//  Description: Simplified way to get selected radio button values
	//  ==================================================================
	function getCheckboxValue(btnName)
	{
		var checkbox_value = [];
		var count = 0;
		var values = document.getElementsByName(btnName);
		for (var y = 0; y < values.length; y++)
		{
		    if (values[y].checked === true)
		    {
		    	checkbox_value[count] = values[y].value;
		    	count++;
		    }
		}
		return checkbox_value;
	}
	
	//  ==================================================================
	//  Method: getDuplicates
	//  Description: Simplified way to get duplicate values
	//  ==================================================================
	function getDuplicatesWithMultiSelection(arrayVals, selectedIds)
	{
		var dupValues = [];
		if (arrayVals != null && arrayVals != undefined)
		{
			if (Array.isArray(arrayVals) == false)
			{
				for (var b = 0; b < selectedIds.length; b++)
				{
					if (arrayVals.Id != selectedId[b])
					{
				    	dupValues[0] = arrayVals.Id;
				    }
				}
			}
			else
			{
				for (var a = 0; a < arrayVals.length; a++)
				{
					var count = false;
					for (var b = 0; b < selectedIds.length; b++)
					{
					    if (arrayVals[a].Id == selectedIds[b])
					    {
					    	count = true;
					    }
					}
					if (count === false)
					{
						dupValues[a] = arrayVals[a].Id;
					}
				}
			}
		}
		return dupValues;
	}
	
//  ==================================================================
	//  Method: updateCCRecords
	//  Description: Simplified way to update CC records
	//  ==================================================================
	function updateCCRecords(objName, topDiv)
	{
		var sObjects = [];
		var counter = 0;
		
		// get reference to element containing contCoverageSec checkboxes
		var ccovList = topDiv["0"].childNodes;
	    
	    // 0 is the header row, 1-rest are the table data, last row is the button bar
	    for (var i=1; i<ccovList.length; i++) 
	    {
	    	var ccov = ccovList[i].childNodes["0"].childNodes["0"];
	        if (ccov != undefined && ccov.type === 'checkbox' && ccov.checked === true) 
	        {
	        	var sObj = new sforce.SObject(objName);
				
	        	var selectedCCFields = ccovList[i].childNodes;
	        	
	        	//get the new values
	        	for (var j=0; j<selectedCCFields.length; j++) 
			    {
			    	var cc = selectedCCFields[j];
			    	
			    	var defaultVal;
			    	if (j === 0)
			    	{
			    		sObj.Id = cc.childNodes["0"].value;
			    	}
			    	else if (j === 2)
			    	{
			    		sObj["T1C_Base__Role__c"] = cc.childNodes["0"].childNodes["0"].childNodes["0"].textContent;
			    	}
			    	else if (j === 3 || j === 4)
			    	{
			    		var aDate;
			    		var val =  cc.childNodes["0"].childNodes["0"].childNodes["0"].value;
			    		if (val != '')
			    		{
			    			aDate = new Date(val);
			    		}
			    		else
			    		{
			    			aDate = null;
			    		}
			    		if (j === 3)
			    		{
			    			sObj["T1C_Base__Start_Date__c"] = aDate;
			    		}
			    		else
			    		{
			    			sObj["T1C_Base__End_Date__c"] = aDate;
			    		}
			    	}
			    	else if (j == 5)
			    	{
			    		sObj["T1C_Base__Is_Backup__c"] = cc.childNodes["0"].childNodes["0"].checked;
			    	}
			    	/*else if (j == 6)
			    	{
			    		sObj["T1C_Base__Product__c"] = cc.childNodes["0"].innerText;
			    	}*/
			    }
	        	sObjects[counter] = sObj;
	        	counter++;
	        }
	    }
	    
	    var uptResult = sforce.connection.update(sObjects);
	    /*if (Array.isArray(uptResult) == false)
	    {
	    	if (uptResult.getBoolean("success")) 
			{
				console.log(objName + " with the following id is updated:" + uptResult);
			}
			else 
			{
				console.log(objName + " Failed to update the following " + uptResult);
			}
	    }
	    else
	    {
			for (var y = 0; y < uptResult.length; y++) 
			{ 
				if (uptResult[y].getBoolean("success")) 
				{
					console.log(objName + " with the following id is updated:" + uptResult[y]);
				}
				else 
				{
					console.log(objName + " Failed to update the following " + uptResult[y]);
				}
			}
		}*/
	}
	
	//==================================================================
	//  Method: ccCheckboxHandlers
	//  Description: handle clicks on the contact coverage checkboxes
	//  ==================================================================
	function ccCheckboxHandlers() 
	{
		// get reference to element containing contCoverage checkboxes
		var el = document.getElementById('contCoverage');

		// get reference to input elements in contCoverage container element
	    var ccov = el.getElementsByTagName('input');
	    
	    // assign openFieldToEdit function to onclick property of each checkbox
	    for (var j=0; j<ccov.length; j++) 
	    {
	        if ( ccov[j].type === 'checkbox' ) 
	        {
	        	ccov[j].onclick = openFieldToEdit;
	        }
	    }
	}
	
	//==================================================================
	//  Method: openFieldToEdit
	//  Description: when a CC is chosen, the fields will get open to edit
	//  ==================================================================
	function openFieldToEdit()
	{
		// get reference to element containing contCoverage checkboxes
		var el = document.getElementById('contactcoverage' + this.id);
		
		if (el !== undefined && el !== null)
		{
			// get reference to input elements in contCoverage container element
		    var selectedCCFields = el.getElementsByTagName('DIV');
					    
					    
			// 'this' is reference to checkbox clicked on
			if (this.checked  === true)
			{
			    // display selected field as editable field
			    for (var j=0; j<selectedCCFields.length; j++) 
			    {
			    	var cc = selectedCCFields[j];
			    	
			    	var defaultVal = cc.textContent;
			    	
			    	if (j === 1)
			    	{
			    		cc.innerHTML = '';
			    		rolepickList(defaultVal, cc, false);
			    	}
			    	else if (j === 4 || j === 6 )
			    	{
			    		cc.innerHTML = '';
			    		dateCalendar(defaultVal, cc, false);
			    	}
			    	else if (j == 8)
			    	{
			    		defaultVal = cc.childNodes["0"].childNodes["0"].checked;
			    		cc.innerHTML = '';
			    		if (defaultVal === true)
			    		{
			    			$p('<label class="control checkbox"><input type="checkbox" checked="true" data-type="dateType"><span class="control-indicator"></span></label>').appendTo(cc);
			    		}
			    		else
			    		{
			    			$p('<label class="control checkbox"><input type="checkbox" data-type="dateType"><span class="control-indicator"></span></label>').appendTo(cc);
			    		}
			    	}
			    	/*else if (j == 11)
			    	{
			    		cc.innerHTML = '';
			    		productlookup(defaultVal, cc, false);
			    	}*/
			    }
			}
			else
			{
				// display selected field as editable field
			    for (var j=0; j<selectedCCFields.length; j++) 
			    {
			    	var cc = selectedCCFields[j];
			    	
			    	var defaultVal;
			    	
			    	if (j === 1)
			    	{
			    		defaultVal = cc.childNodes["0"].defaultValue;
			    		cc.innerHTML = '';
			    		cc.innerHTML = defaultVal;
			    	}
			    	else if (j === 3 || j === 5 )
			    	{
			    		defaultVal = cc.defaultValue;
			    		cc.innerHTML = '';
			    		cc.innerHTML = defaultVal;
			    	}
			    	else if (j == 6)
			    	{
			    		defaultVal = cc.childNodes["0"].childNodes["0"].defaultChecked;
			    		cc.innerHTML = '';
			    		if (defaultVal === false)
			    		{
			    			cc.innerHTML = '<label class="control checkbox"><input type="checkbox" data-type="dateType" disabled="true"><span class="control-indicator"></span></label>';
			    		}
			    		else
			    		{
			    			cc.innerHTML = '<label class="control checkbox"><input type="checkbox" checked="true" data-type="dateType" disabled="true"><span class="control-indicator"></span></label>';
			    		}
			    	}
			    	/*else if (j == 5)
			    	{
				    	defaultVal = cc.childNodes["0"].defaultValue;
			    		cc.innerHTML = '';
			    		cc.innerHTML = defaultVal;
			    	}*/
			    }
			}
		}
	}
	
	//==================================================================
	//  Method: productlookup
	//  Description: Add a productlookup
	//  ==================================================================
	function productlookup(defaultVal, lookupDiv, isDisabled)
	{
		var formItem = $p('<input id="teamMemberEmployeeAutoComplete" type="text" autocomplete="off">').appendTo(lookupDiv);
		formItem[0].value = defaultVal;
		formItem[0].defaultvalue = defaultVal;
		
		setTimeout(function()
		{
		    autoComplete = new SearchAutoComplete($(formItem),
		    {
		        mode: "CUSTOM:" + lookUPConfig.id,
		        maintainSelectedItem: true,
		        config: lookUPConfig,
		        select: function(item)
		        {                                                                              
		            if(item != null)
		            {
		            	formItem[0].value = item;
		            	
		            }                                                                                              
		        }                              
		    });                                                                                           
		}, 100);
	}
	
	//==================================================================
	//  Method: rolepickList
	//  Description: Add a rolepickList
	//  ==================================================================
	function rolepickList(defaultVal, picklistDiv, isDisabled)
	{
		var formItem = $p('<div id="picklistId"/>').appendTo(picklistDiv);
		formItem[0].id = rolepicklistConfig.id     

        if (defaultVal === null)
        {
        	defaultVal = '';
        }

        var picklistValues = rolepicklistConfig.dataProvider;
		var pickListItems = new ACEMenuButton($(formItem),{ dataProvider : picklistValues});
		formItem[0].pickListItems = pickListItems;
		formItem[0].defaultValue = defaultVal;
		
		pickListItems.setSelectedItem(defaultVal);
	}
	
	//==================================================================
	//  Method: dateCalendar
	//  Description: Add a date Calendar
	//  ==================================================================
	function dateCalendar(defaultVal, picklistDiv, isDisabled)
	{
		//var formItem = $p('<div id="dateFieldId"><input type="date" value="' + defaultVal + '" data-type="dateType"><div/>').appendTo(picklistDiv);
		
		var formItem = $p('<div id="dateFieldId"/>').appendTo(picklistDiv);
		var datePickerSpan = $p('<span/>').appendTo(formItem);
		var datePicker = $('<input/>').datepicker().appendTo(datePickerSpan);
		
		if (defaultVal != undefined && defaultVal != '' && defaultVal.split('-')[1] != undefined)
		{
			var sthisYear = defaultVal.split('-')[0];
	    	var sthisMonth = defaultVal.split('-')[1];
	    	var sthisDay = defaultVal.split('-')[2];
	    	
	    	var defaultDate = new Date(sthisYear, sthisMonth -1, sthisDay,0,0);
	    	
			datePicker.datepicker("setDate", defaultDate);
		}
		else 
		{
			datePicker.datepicker("setDate", 0);
		}
		formItem[0].datePicker = datePicker;
		formItem[0].defaultValue = defaultVal;

		datePicker.on('change', function() 
		{			
			var dateValue = new Date($(datePicker).val());
		});
	}
	
	//  ==================================================================
	//  Method: enableTriggers
	//  Description: only to do enable/disable triggers
	//  ==================================================================
	function enableTriggers()
	{
		/*var triggersButtonFooter = $p('<div class="footerButtonBar"><div class="rightAlignedButtonWrapper"></div></div>').appendTo(buttonsDiv);
		var enableTriggersButton;
		if (tiggersEnabled == true)
		{ 
			enableTriggersButton = $p('<button id="btnId" class="preserveBTN" ripple="true">Disable Triggers</button>').appendTo(triggersButtonFooter);
		}
		else
		{
			enableTriggersButton = $p('<button id="btnId" class="preserveBTN" ripple="true">Enable Triggers</button>').appendTo(triggersButtonFooter);
		}*/
		//if (tiggersEnabled == true)
		//{
			//$(enableTriggersButton).on('click', function() 
			//{
				var result = sforce.apex.execute("PostMergeUtilityController", "enableDisableTriggers", {val : 'false'}, 
				{ 
					onSuccess: function(result)
					{		
						//error = result[0];
						//showError(error);
					}, 
					onFailure: function(error)
					{		
						showError(error);
					} 
				});
				tiggersEnabled = !tiggersEnabled;
				
				/*var elem = document.getElementById("btnId");
				if (elem.textContent == "Enable Triggers") 
				{
					elem.innerHTML = "Disable Triggers";
				}
			    else 
			    {
			    	elem.innerHTML = "Enable Triggers";
			    }*/
				//document.getElementById('btnId').style.visibility = 'hidden';
			//});
		//}
	}
	//  ==================================================================
	//  Method: closeWindow
	//  Description: Simplified way to close Window
	//  ==================================================================
	function closeWindow() 
	{   		
		if( !window.close() )
		{
			var contactId = ACEUtil.getURLParameter('ContactId');
			if(contactId)
			{
				sforce.one.navigateToSObject(contactId);
			}			

		}
		/*//check if the trigger are disabled or not
		if (tiggersEnabled == false)
		{
		    if (confirm("Please enable Merge triggers before closing this form.") == true) 
		    {
		       // x = "You pressed OK!";
		    } 
		    else 
		    {
		    	window.close(); 
		    }
		}
		else
		{
			window.close(); 
		}*/
		                          
	}
	
	//  ==================================================================
	//  Method: showError
	//  Description: Simplified way to display error message
	//  ==================================================================
	function showError(error)
	{
		$(modal).show();
		var errorDiv = document.getElementById("errorMessage");
		errorDiv.innerHTML = error;
	}
	
	
	$(modal).on('click', function() 
	{
		$(modal).hide();
	});
	
	//# sourceURL=PostMerge.js
})(jQuery);